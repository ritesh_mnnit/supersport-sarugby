﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class playerprofile : System.Web.UI.Page
{
    public String reqCategory = "";

    public int leagueID = 0;

    private String thisURL = "";

    private int id = -1; //18205

    private String testRecordSearchname = "";

    private int totCaps = 0;

    playersquadItem_list playersquadList = new playersquadItem_list();
    List<playersquadItem> playersquadItemList = new List<playersquadItem>();

    private playerwriteup playerWriteup;
    private personaldetails personalDetails;
    private rugbycareer rugbyCareer;
    private springbokcareer springbokCareer;

    private testrecordItem_list list = new testrecordItem_list();
    private List<testrecordItem> testrecordItemList = new List<testrecordItem>();

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["category"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        if (func.isNumericType(Request.QueryString["id"], "int"))
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
        }

        playersquadItemList = playersquadList.getPlayerSquadItemList(id);

        if (playersquadItemList != null)
        {
            buildSquads();
        }

        playerWriteup = new playerwriteup(id);

        if (playerWriteup.count > 0)
        {
            buildPlayerWriteup();
        }

        personalDetails = new personaldetails(id);

        if (personalDetails.count > 0)
        {
            buildPersonalDetails();
        }

        rugbyCareer = new rugbycareer(id);

        if (rugbyCareer != null)
        {
            pan_rugbycareer.Visible = true;

            buildRugbyCareerDetails();
        }

        testrecordItemList = list.getTestrecordItemList(testRecordSearchname);

        if (testrecordItemList != null)
        {
            buildTestRecord();
        }

        springbokCareer = new springbokcareer(id);

        //if (springbokCareer != null)
        //if (springbokCareer.caps != 0)
        if(totCaps != 0)
        {
            pan_springbokcareer.Visible = true;
            buildSpringbokCareerDetails();
        }

        
    }

    protected void buildPersonalDetails()
    {
        StringBuilder outStr = new StringBuilder();

        if (personalDetails.image == "")
        {
            lit_image.Text = "<span class=\"txt_black_10\">No image available</span>";
        }
        else
        {
            lit_image.Text = "<img src=\"http://images.supersport.co.za/" + personalDetails.image + "\" alt=\"\" />";
        }

        outStr.Append(design.buildHeading("PERSONAL DETAILS", 12, "", "", false));

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        //if ((personalDetails.fullname != null) && (personalDetails.fullname != ""))
        if (!string.IsNullOrEmpty(personalDetails.displayname))
        {
            testRecordSearchname = personalDetails.displayname + " " + personalDetails.surname;

            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">FULL NAME</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.fullname + "</td>");
            outStr.Append("    </tr>");
        }
        else
        {
            testRecordSearchname = personalDetails.nickname + " " + personalDetails.surname;
        }

        if ((personalDetails.surname != null) && (personalDetails.surname != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">SURNAME</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.surname + "</td>");
            outStr.Append("    </tr>");
        }

        if ((personalDetails.surname != null) && (personalDetails.height != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">HEIGHT</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.height + " cm</td>");
            outStr.Append("    </tr>");
        }

        if ((personalDetails.surname != null) && (personalDetails.weight != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">WEIGHT</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.weight + " Kg</td>");
            outStr.Append("    </tr>");
        }

        if (personalDetails.birthdate != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">BIRTH DATE</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.birthdate.ToString("dd MMMM yyyy") + "</td>");
            outStr.Append("    </tr>");
        }

        if ((personalDetails.birthplace != null) && (personalDetails.birthplace != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">BIRTH PLACE</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.birthplace + "</td>");
            outStr.Append("    </tr>");
        }

        if ((personalDetails.maritialstatus != null) && (personalDetails.maritialstatus != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">MARITIAL STATUS</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.maritialstatus + "</td>");
            outStr.Append("    </tr>");
        }

        if ((personalDetails.favouritefilm != null) && (personalDetails.favouritefilm != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">FAVOURITE FILM</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.favouritefilm + "</td>");
            outStr.Append("    </tr>");
        }

        if ((personalDetails.favouritemusic != null) && (personalDetails.favouritemusic != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">FAVOURITE MUSIC</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.favouritemusic + "</td>");
            outStr.Append("    </tr>");
        }

        if ((personalDetails.favouritefood != null) && (personalDetails.favouritefood != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">FAVOURITE FOOD</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.favouritefood + "</td>");
            outStr.Append("    </tr>");
        }

        if ((personalDetails.primaryschool != null) && (personalDetails.primaryschool != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">PRIMARY SCHOOL</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.primaryschool + "</td>");
            outStr.Append("    </tr>");
        }

        if ((personalDetails.secondaryschool != null) && (personalDetails.secondaryschool != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">SECONDARY SCHOOL</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.secondaryschool + "</td>");
            outStr.Append("    </tr>");
        }

        if ((personalDetails.tertiaryeducation != null) && (personalDetails.tertiaryeducation != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">TERTIARY EDUCATION</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + personalDetails.tertiaryeducation + "</td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_personaldetails.Text = outStr.ToString();
    }

    protected void buildSquads()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append(design.buildHeading("SQUAD LIST", 12, "", "", false));

        outStr.Append("<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\" bgcolor=\"#CCCCCC\">");

        foreach (playersquadItem item in playersquadItemList)
        {
            outStr.Append("	<tr class=\"txt_black_12\">");
            outStr.Append("		<td width=\"200\" bgcolor=\"#F2F2F2\"><a href=\"teams.aspx?id=" + item.teamid + "&category=" + reqCategory + "&leagueid=" + leagueID + "\">" + item.team + "</a></td>");
            outStr.Append("		<td bgcolor=\"#FFFFFF\">" + item.leaguename + " - " + item.datestart.ToString("yyyy") + "</td>");
            outStr.Append("	</tr>");
        }

        outStr.Append("</table>");

        lit_squads.Text = outStr.ToString();
    }

    protected void buildPlayerWriteup()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append(design.buildHeading("PLAYER WRITE-UP", 12, "", "", false));

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        if ((playerWriteup.writeup != null) && (playerWriteup.writeup != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + playerWriteup.writeup + "</td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_playerwriteup.Text = outStr.ToString();
    }

    protected void buildRugbyCareerDetails()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append(design.buildHeading("RUGBY CAREER", 12, "", "", false));

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        if ((rugbyCareer.position != null) && (rugbyCareer.position != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">POSITION</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + rugbyCareer.position + "</td>");
            outStr.Append("    </tr>");
        }

        if ((rugbyCareer.province != null) && (rugbyCareer.province != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">PROVINCE</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + rugbyCareer.province + "</td>");
            outStr.Append("    </tr>");
        }

        if (rugbyCareer.tries != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">TRIES</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + rugbyCareer.tries + "</td>");
            outStr.Append("    </tr>");
        }

        if (rugbyCareer.conversions != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">CONVERSIONS</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + rugbyCareer.conversions + "</td>");
            outStr.Append("    </tr>");
        }

        if (rugbyCareer.penalties != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">PENALTIES</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + rugbyCareer.penalties + "</td>");
            outStr.Append("    </tr>");
        }

        if (rugbyCareer.dropgoals != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">DROP GOALS</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + rugbyCareer.dropgoals + "</td>");
            outStr.Append("    </tr>");
        }

        if (rugbyCareer.totalpoints != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">TOTAL POINTS</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + rugbyCareer.totalpoints + "</td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_rugbycareer.Text = outStr.ToString();
    }

    protected void buildSpringbokCareerDetails()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append(design.buildHeading("SPRINGBOK CAREER", 12, "", "", false));

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        if ((springbokCareer.debut != null) && (springbokCareer.debut != ""))
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">DEBUT</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + springbokCareer.debut + "</td>");
            outStr.Append("    </tr>");
        }

        if (totCaps != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">CAPS</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + totCaps.ToString() + "</td>");
            outStr.Append("    </tr>");
        }

        if (springbokCareer.tries != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">TRIES</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + springbokCareer.tries + "</td>");
            outStr.Append("    </tr>");
        }

        if (springbokCareer.conversions != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">CONVERSIONS</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + springbokCareer.conversions + "</td>");
            outStr.Append("    </tr>");
        }

        if (springbokCareer.penalties != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">PENALTIES</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + springbokCareer.penalties + "</td>");
            outStr.Append("    </tr>");
        }

        if (springbokCareer.dropgoals != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">DROP GOALS</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + springbokCareer.dropgoals + "</td>");
            outStr.Append("    </tr>");
        }

        if (springbokCareer.testpoints != null)
        {
            outStr.Append("    <tr>");
            outStr.Append("        <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">TOTAL POINTS</td>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + springbokCareer.testpoints + "</td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_springbokcareer.Text = outStr.ToString();
    }

    protected void buildTestRecord()
    { 
        StringBuilder outStr = new StringBuilder();

        outStr.Append(design.buildHeading("TEST RECORD", 12, "", "", false));

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");
        outStr.Append("    <tr>");
        outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">OPPOSITION</td>");
        outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">P</td>");
        outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">W</td>");
        outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">D</td>");
        outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">L</td>");
        outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">T</td>");
        outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">C</td>");
        outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">PE</td>");
        outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">DG</td>");
        outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">PTS</td>");
        outStr.Append("    </tr>");

        foreach (testrecordItem item in testrecordItemList)
        {
            totCaps = totCaps + item.played;

            outStr.Append("    <tr>");
            outStr.Append("        <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">"+ item.team +"</td>");
            outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + item.played.ToString() + "</td>");
            outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + item.won.ToString() +"</td>");
            outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + item.drew.ToString() + "</td>");
            outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + item.lost.ToString() + "</td>");
            outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + item.tries.ToString() + "</td>");
            outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + item.conversions.ToString() + "</td>");
            outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + item.penalties.ToString() + "</td>");
            outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + item.dropgoals.ToString() + "</td>");
            outStr.Append("        <td width=\"40\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_black_12\">" + item.points.ToString() + "</td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_testrecord.Text = outStr.ToString();
    }
}