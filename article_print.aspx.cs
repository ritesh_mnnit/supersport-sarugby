﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class article_print : System.Web.UI.Page
{
    public int id;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (func.isNumericType(Request.QueryString["id"], "int"))
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
        }

        if (!IsPostBack)
        {
            newsArticle currArticle = new newsArticle();
            newsItem item = currArticle.getArticle(id);

            if (item != null)
            {
                lit_headline.Text = item.headline;
                lit_author.Text = item.author;
                lit_credit.Text = item.credit;
                lit_date.Text = item.date.ToString("MMMM dd, yyyy");
                lit_body.Text = item.body;
                img_image.ImageUrl = "http://images.supersport.co.za/" + item.imageLarge;
            }
        }
    }
}