﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class uc_profilespreview : System.Web.UI.UserControl
{
    public String reqCategory = "";

    public int leagueID = 0;

    private profileshort profile = new profileshort(-1);

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["category"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        if (!IsPostBack)
        {
            lit_image.Text = "<a href=\"playerprofile.aspx?id="+ profile.id +"&category=&leagueid=0\"><img src=\"http://images.supersport.co.za/"+ profile.image +"\" width=\"135\" height=\"180\" border=\"0\" /></a>";

            lab_fullname.Text = "<a href=\"playerprofile.aspx?id=" + profile.id + "&category=&leagueid=0\">" + profile.fullname + "</a>";
            lab_dob.Text = profile.dob.ToString("dd MMM yyyy");
            lab_position.Text = profile.position;
            lab_province.Text = profile.province;
            lab_height.Text = profile.height;
            lab_weight.Text = profile.weight;
            lab_tests.Text = profile.tests.ToString();
            lab_tries.Text = profile.testtries.ToString();
        }
    }

    protected void searchClick(object sender, EventArgs e)
    {
        Response.Redirect("profilesearch.aspx?search=" + f_search.Text + "&category="+ reqCategory +"&leagueid=" + leagueID);
    }
}