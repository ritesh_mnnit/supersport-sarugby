﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="profilesearch.aspx.cs" Inherits="profilesearch" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

    <%=design.buildHeading("SEARCH PLAYER PROFILES", 20, "", "", true) %>

    <%=design.makeSpace(10,"",true) %>

    <asp:Panel ID="pan_search" runat="server" DefaultButton="btn_search">
        <div class="profile_searchleft"></div>
        <asp:TextBox ID="f_search" runat="server" CssClass="profile_seachbox"></asp:TextBox>
        <asp:ImageButton ID="btn_search" runat="server" ImageUrl="~/External/Graphics/search_btn.jpg" Height="22px" Width="27px" OnClick="searchClick" />
    </asp:Panel>

    <%=design.makeSpace(20,"",true) %>

    <%=design.buildHeading("SEARCH RESULTS", 12, "", "", true) %>

    <asp:Panel ID="pan_results" runat="server">

        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <div class="txt_black_12">
            
                <asp:Literal ID="lit_results" runat="server">Please type a name in the box above.</asp:Literal>
                
            </div>
        </table>
        
    </asp:Panel>

</asp:Content>