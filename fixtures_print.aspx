﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fixtures_print.aspx.cs" Inherits="fixtures_print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">

        <link href="/External/Styles/text.css?v=<%=ConfigurationManager.AppSettings["FileVersion"] %>" rel="stylesheet" type="text/css" />

        <link href="/External/Styles/general.css?v=<%=ConfigurationManager.AppSettings["FileVersion"] %>" rel="stylesheet" type="text/css" />

        <title>S.A. RUGBY - Print Fixtures</title>

    </head>
    <body>
        <form id="formm_print" runat="server">
            <div style="width:900px; margin:10px 10px 10px 10px;">

                <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">

                    <asp:Repeater ID="rep_fixtures" runat="server" OnItemDataBound="rep_fixtures_OnItemDataBound"></asp:Repeater>

                </table>

            </div>
        </form>
    </body>
</html>
