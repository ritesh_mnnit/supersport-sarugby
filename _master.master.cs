﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class _master : System.Web.UI.MasterPage
{
    public bool fullview;

    protected void Page_Load(object sender, EventArgs e)
    {
        try { fullview = Convert.ToBoolean(Request.QueryString["fullview"]); }
        catch { fullview = false; }

        SitecontentItem sc1 = new SitecontentItem(16, "SAR2_RIGHT_1");

        if (sc1 != null)
        {
            sc_right_1.Text = sc1.body;
        }

        SitecontentItem sc2 = new SitecontentItem(16, "SAR2_RIGHT_2");

        if (sc2 != null)
        {
            sc_right_2.Text = sc2.body;
        }

        SitecontentItem sc3 = new SitecontentItem(16, "SAR2_RIGHT_3");

        if (sc3 != null)
        {
            sc_right_3.Text = sc3.body;
        }

        SitecontentItem scBot = new SitecontentItem(16, "SAR2_BOTTOM");

        if (scBot != null)
        {
            sc_bottom.Text = scBot.body;
        }

        SitecontentItem scStats = new SitecontentItem(16, "STATS TAG");

        if (scStats != null)
        {
            lit_stats.Text = scStats.body;
        }

        StringBuilder menStr = new StringBuilder();

        menuItem_list list1 = new menuItem_list();
        List<menuItem> menuItemList1 = new List<menuItem>();

        menuItemList1 = list1.getMenuItemList(1);

        if (menuItemList1 != null)
        {
            menStr.Append("<td class=\"MENU_SPACER\"></td>");

            foreach (menuItem item1 in menuItemList1)
            {
                menStr.Append("<td>");
                menStr.Append("<ul>");

                if (item1.newwin == true)
                {
                    menStr.Append("<li><a href=\"" + item1.link + "\" target=\"_blank\">" + item1.name.ToUpper() + "</a>");
                }
                else
                {
                    menStr.Append("<li><a href=\"" + item1.link + "\">" + item1.name.ToUpper() + "</a>");
                }

                menuItem_list list2 = new menuItem_list();
                List<menuItem> menuItemList2 = new List<menuItem>();

                menuItemList2 = list2.getMenuItemList(item1.ID);

                if (menuItemList2 != null)
                {
                    menStr.Append("<ul>");

                    foreach (menuItem item2 in menuItemList2)
                    {
                        if (item2.newwin == true)
                        {
                            menStr.Append("<li><a href=\"" + item2.link + "\" target=\"_blank\">" + item2.name.ToUpper() + "</a>");
                        }
                        else
                        {
                            menStr.Append("<li><a href=\"" + item2.link + "\">" + item2.name.ToUpper() + "</a>");
                        }

                        //menStr.Append("<li><a href=\"" + item2.link + "\">" + item2.name.ToUpper() + "</a>");

                        menuItem_list list3 = new menuItem_list();
                        List<menuItem> menuItemList3 = new List<menuItem>();

                        menuItemList3 = list3.getMenuItemList(item2.ID);

                        if (menuItemList3 != null)
                        {
                            menStr.Append("<ul>");

                            foreach (menuItem item3 in menuItemList3)
                            {
                                if (item3.newwin == true)
                                {
                                    menStr.Append("<li><a href=\"" + item3.link + "\" target=\"_blank\">" + item3.name.ToUpper() + "</a>");
                                }
                                else
                                {
                                    menStr.Append("<li><a href=\"" + item3.link + "\">" + item3.name.ToUpper() + "</a>");
                                }

                                //menStr.Append("<li><a href=\"" + item3.link + "\">" + item3.name.ToUpper() + "</a>");

                                menuItem_list list4 = new menuItem_list();
                                List<menuItem> menuItemList4 = new List<menuItem>();

                                menuItemList4 = list4.getMenuItemList(item3.ID);

                                if (menuItemList4 != null)
                                {
                                    menStr.Append("<ul>");

                                    foreach (menuItem item4 in menuItemList4)
                                    {
                                        if (item4.newwin == true)
                                        {
                                            menStr.Append("<li><a href=\"" + item4.link + "\" target=\"_blank\">" + item4.name.ToUpper() + "</a>");
                                        }
                                        else
                                        {
                                            menStr.Append("<li><a href=\"" + item4.link + "\">" + item4.name.ToUpper() + "</a>");
                                        }

                                        //menStr.Append("<li><a href=\"" + item4.link + "\">" + item4.name.ToUpper() + "</a>");

                                        menuItem_list list5 = new menuItem_list();
                                        List<menuItem> menuItemList5 = new List<menuItem>();

                                        menuItemList5 = list5.getMenuItemList(item4.ID);

                                        if (menuItemList5 != null)
                                        {
                                            menStr.Append("<ul>");

                                            foreach (menuItem item5 in menuItemList5)
                                            {
                                                if (item5.newwin == true)
                                                {
                                                    menStr.Append("<li><a href=\"" + item5.link + "\" target=\"_blank\">" + item5.name.ToUpper() + "</a>");
                                                }
                                                else
                                                {
                                                    menStr.Append("<li><a href=\"" + item5.link + "\">" + item5.name.ToUpper() + "</a>");
                                                }

                                                //menStr.Append("<li><a href=\"" + item5.link + "\">" + item5.name.ToUpper() + "</a>");

                                                menStr.Append("</li>");
                                            }

                                            menStr.Append("</ul>");
                                        }

                                        menStr.Append("</li>");
                                    }

                                    menStr.Append("</ul>");
                                }

                                menStr.Append("</li>");
                            }

                            menStr.Append("</ul>");
                        }

                        menStr.Append("</li>");
                    }

                    menStr.Append("</ul>");
                }

                menStr.Append("</li>");
                menStr.Append("</ul>");
                menStr.Append("</td>");
                menStr.Append("<td class=\"MENU_SPACER\"></td>");
            }

            lit_mainmenu.Text = menStr.ToString();
        }
    }
}