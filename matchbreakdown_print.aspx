﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="matchbreakdown_print.aspx.cs" Inherits="matchbreakdown_print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">

        <link href="/External/Styles/text.css?v=<%=ConfigurationManager.AppSettings["FileVersion"] %>" rel="stylesheet" type="text/css" />

        <link href="/External/Styles/general.css?v=<%=ConfigurationManager.AppSettings["FileVersion"] %>" rel="stylesheet" type="text/css" />

        <title>S.A. RUGBY - Print Match Breakdown</title>

    </head>
    <body>
        <form id="formm_print" runat="server">
            <div style="width:900px; margin:10px 10px 10px 10px;">

                <asp:Literal ID="lit_summary" runat="server"></asp:Literal>

                <%=design.makeSpace(20,"",true) %>

                <asp:Literal ID="lit_officials" runat="server"></asp:Literal>
    
                <%=design.makeSpace(20,"",true) %>

                <%=design.buildHeading("SCORING SUMMARY", 12, "", "", false)%>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
		                <td align="left" valign="top">
		    
		                    <asp:Literal ID="lit_scoringteamA" runat="server"></asp:Literal>

		                </td>
		                <td width="20" align="left" valign="top">&nbsp;</td>
		                <td align="left" valign="top">
		
		                    <asp:Literal ID="lit_scoringteamB" runat="server"></asp:Literal>

		                </td>
	                </tr>
                </table>

                <%=design.makeSpace(20,"",true) %>

                <%=design.buildHeading("LINE-UP", 12, "", "", false)%>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
		                <td align="left" valign="top">
		
		                    <asp:Literal ID="lit_lineupA" runat="server"></asp:Literal>
		    
		                </td>
		                <td width="20" align="left" valign="top">&nbsp;</td>
		                <td align="left" valign="top">
		
		                    <asp:Literal ID="lit_lineupB" runat="server"></asp:Literal>
		    
		                </td>
	                </tr>
                </table>

                <%=design.makeSpace(20,"",true) %>

                <%=design.buildHeading("RESERVES", 12, "", "", false)%>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
		                <td align="left" valign="top">
		
		                    <asp:Literal ID="lit_reservesA" runat="server"></asp:Literal>
		    
		                </td>
		                <td width="20" align="left" valign="top">&nbsp;</td>
		                <td align="left" valign="top">
		
		                    <asp:Literal ID="lit_reservesB" runat="server"></asp:Literal>
		    
		                </td>
	                </tr>
                </table>

                <%=design.makeSpace(20,"",true) %>

                <%=design.buildHeading("REPLACEMENTS", 12, "", "", false)%>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
		                <td align="left" valign="top">

                            <asp:Literal ID="lit_replacementsA" runat="server"></asp:Literal>

		                </td>
		                <td width="20" align="left" valign="top">&nbsp;</td>
		                <td align="left" valign="top">

		                    <asp:Literal ID="lit_replacementsB" runat="server"></asp:Literal>

		                </td>
	                </tr>
                </table>

                <%=design.makeSpace(20,"",true) %>

                <%=design.buildHeading("RUN OF PLAY", 12, "", "", false)%>

                <asp:Literal ID="lit_runofplay" runat="server"></asp:Literal>

            </div>
        </form>
    </body>
</html>
