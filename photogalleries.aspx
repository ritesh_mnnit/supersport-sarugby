﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="photogalleries.aspx.cs" Inherits="photogalleries" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<%=design.buildHeading("PHOTO GALLERIES", 20, "", "", true)%>

<%=design.makeSpace(10,"",true) %>

    <div class="wrap_interface_top">

        <asp:Panel ID="pan_page" runat="server" DefaultButton="btn_ok">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="40" class="txt_black_10">&nbsp;PAGE</td>
                    <td width="40"><asp:TextBox CssClass="edit_page" ID="f_page" MaxLength="4" runat="server" /></td>
                    <td width="40" class="txt_black_10"> of <asp:Literal ID="lit_totalpages" runat="server"></asp:Literal></td>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle" style="padding: 0 2px 0 0;"><asp:Button ID="btn_ok" Text="OK" runat="server" CssClass="btn_grey" OnClick="btn_ok_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 2px 0 2px;"><asp:Button ID="btn_prev" Text="PREVIOUS" runat="server" CssClass="btn_grey" OnClick="btn_prev_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 0 0 2px;"><asp:Button ID="btn_next" Text="NEXT" runat="server" CssClass="btn_grey" OnClick="btn_next_Click" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>

    <%=design.makeSpace(20,"",true) %>

    <asp:Repeater ID="rep_photogalleries" runat="server">
        <ItemTemplate>
        
            <div class="wrap_media_inside">
                <div class="media_date txt_green_10"><strong><%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "date")).ToString("MMMM dd, yyyy") %></strong></div>
                <div style="text-align:center"><a href="photos.aspx?galid=<%# DataBinder.Eval(Container.DataItem, "id")%>"><img src="http://images.supersport.co.za/<%# DataBinder.Eval(Container.DataItem, "imageSmall")%>" border="0" style="max-height:103px; max-width:136px;" /></a></div>
                <div class="media_info txt_black_10"><%# DataBinder.Eval(Container.DataItem, "name")%></div>
            </div>

        </ItemTemplate>
    </asp:Repeater>

</asp:Content>