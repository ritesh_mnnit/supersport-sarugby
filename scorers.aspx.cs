﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class scorers : System.Web.UI.Page
{
    public String reqCategory = "";

    public int leagueID = 0; // 767

    private String thisURL = "";

    public int pcurr;

    private pointItem_list pointList = new pointItem_list();
    private List<pointItem> pointItemList = new List<pointItem>();

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["category"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        if (func.isNumericType(Request.QueryString["pcurr"], "int"))
        {
            pcurr = Convert.ToInt32(Request.QueryString["pcurr"]);
        }
        else
        {
            pcurr = 1;
        }

        thisURL = func.GetCurrentPageName(Request.Url.Query, true);

        if (!IsPostBack)
        {
            btn_prev.Enabled = true;
            btn_next.Enabled = true;

            pointItemList = pointList.getPointItemList(500, 0, reqCategory, leagueID);

            if (pointItemList != null)
            {
                PagedDataSource tmpSource = new PagedDataSource();

                tmpSource.DataSource = pointItemList;

                tmpSource.AllowPaging = true;

                tmpSource.PageSize = 40;

                if (pcurr < 1) { pcurr = 1; }

                if (pcurr > tmpSource.PageCount) { pcurr = tmpSource.PageCount; }

                f_page.Text = pcurr.ToString();

                tmpSource.CurrentPageIndex = pcurr - 1;

                if (tmpSource.IsFirstPage) { btn_prev.Enabled = false; }

                if (tmpSource.IsLastPage) { btn_next.Enabled = false; }

                lit_totalpages.Text = tmpSource.PageCount.ToString();

                rep_scorers.DataSource = tmpSource;

                rep_scorers.DataBind();
            }
            else
            {
                pan_page.Enabled = false;
            }
        }
    }

    protected void btn_ok_Click(object sender, EventArgs e)
    {
        Response.Redirect("scorers.aspx?pcurr=" + f_page.Text + "&category="+ reqCategory +"&leagueid="+ leagueID +"&fullview=true");
    }

    protected void btn_prev_Click(object sender, EventArgs e)
    {
        Response.Redirect("scorers.aspx?pcurr=" + (pcurr - 1).ToString() + "&category=" + reqCategory + "&leagueid=" + leagueID + "&fullview=true");
    }

    protected void btn_next_Click(object sender, EventArgs e)
    {
        Response.Redirect("scorers.aspx?pcurr=" + (pcurr + 1).ToString() + "&category=" + reqCategory + "&leagueid=" + leagueID + "&fullview=true");
    }
}