﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class uc_scorerspreview : System.Web.UI.UserControl
{
    public String reqCategory = "";

    public int leagueID = 0; // 767

    public int tabCnt = 3;

    private tryItem_list tryList = new tryItem_list();
    private List<tryItem> tryItemList = new List<tryItem>();

    private capItem_list capList = new capItem_list();
    private List<capItem> capItemList = new List<capItem>();

    private pointItem_list pointList = new pointItem_list();
    private List<pointItem> pointItemList = new List<pointItem>();

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["category"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        if (!IsPostBack)
        {
            if ((reqCategory != "") && (reqCategory != "sarugby") && (reqCategory != null))
            {
                tabCnt = 2;
                pan_nonhometab.Visible = false;
                pan_nonhomedata.Visible = false;
            }

            tryItemList = tryList.getTryItemList(10, reqCategory, leagueID);

            if (tryItemList != null)
            {
                rep_tries.DataSource = tryItemList;
                rep_tries.DataBind();
            }

            capItemList = capList.getCapItemList(10, reqCategory, leagueID);

            if (capItemList != null)
            {
                rep_caps.DataSource = capItemList;
                rep_caps.DataBind();
            }

            pointItemList = pointList.getPointItemList(10, 99, reqCategory, leagueID);

            if (pointItemList != null)
            {
                rep_points.DataSource = pointItemList;
                rep_points.DataBind();
            }
        }
    }

    protected void rep_tries_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            tryItem item = (tryItem)e.Item.DataItem;

            HtmlGenericControl wrapTry = new HtmlGenericControl("div");

            StringBuilder outStr = new StringBuilder();

            outStr.Append("<tr class=\"txt_grey_12\">");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.player + "</td>");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.tries + "</td>");
            outStr.Append("</tr>");

            wrapTry.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapTry);
        }
    }

    protected void rep_caps_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            capItem item = (capItem)e.Item.DataItem;

            HtmlGenericControl wrapCap = new HtmlGenericControl("div");

            StringBuilder outStr = new StringBuilder();

            outStr.Append("<tr class=\"txt_grey_12\">");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.player + "</td>");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.caps + "</td>");
            outStr.Append("</tr>");

            wrapCap.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapCap);
        }
    }

    protected void rep_points_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            pointItem item = (pointItem)e.Item.DataItem;

            HtmlGenericControl wrapPoi = new HtmlGenericControl("div");

            StringBuilder outStr = new StringBuilder();

            outStr.Append("<tr class=\"txt_grey_12\">");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.player + "</td>");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">"+ item.tries +"</td>");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">"+ item.conversions +"</td>");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">"+ item.penalties +"</td>");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">"+ item.drops +"</td>");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">"+ item.points +"</td>");
            outStr.Append("</tr>");

            wrapPoi.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapPoi);
        }
    }
}


