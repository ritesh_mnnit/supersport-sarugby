﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_profilespreview.ascx.cs" Inherits="uc_profilespreview" %>

<div id="container_media">

<%=design.buildHeading("PLAYER PROFILES", 20, "", "", true)%>

    <div class="profile_container txt_grey_12">

        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2" align="left" valign="top">
                    <div class="profile_name txt_grey_14"><strong><asp:Label ID="lab_fullname" runat="server"></asp:Label></strong></div>
                </td>
            </tr>
	        <tr>
		        <td width="135" align="left" valign="top">
                    <div class="profile_imageholder"><asp:Literal ID="lit_image" runat="server"></asp:Literal></div>
                </td>
		        <td align="left" valign="top">
                    <div style="float:left;">
                        <div class="profile_info"><strong>DOB:&nbsp;</strong><asp:Label ID="lab_dob" runat="server"></asp:Label></div>
                        <div class="profile_info"><strong>Position:&nbsp;</strong><asp:Label ID="lab_position" runat="server"></asp:Label></div>
                        <div class="profile_info"><strong>Province:&nbsp;</strong><asp:Label ID="lab_province" runat="server"></asp:Label></div>
                        <div class="profile_info"><strong>Height:&nbsp;</strong><asp:Label ID="lab_height" runat="server"></asp:Label></div>
                        <div class="profile_info"><strong>Weight:&nbsp;</strong><asp:Label ID="lab_weight" runat="server"></asp:Label></div>
                        <div class="profile_info"><strong>Tests:&nbsp;</strong><asp:Label ID="lab_tests" runat="server"></asp:Label></div>
                        <div class="profile_info"><strong>Test tries:&nbsp;</strong><asp:Label ID="lab_tries" runat="server"></asp:Label></div>
                    </div>
                </td>
	        </tr>
        </table>

        <div style="width:100%; margin:10px 0 0 0">
            <div style="margin:0 0 5px 0;" class="txt_black_10">Search</div>
            <asp:Panel ID="pan_search" runat="server" DefaultButton="btn_search">
                <div class="profile_searchleft"></div>
                <asp:TextBox ID="f_search" runat="server" CssClass="profile_seachbox"></asp:TextBox>
                <asp:ImageButton ID="btn_search" runat="server" ImageUrl="~/External/Graphics/search_btn.jpg" Height="22px" Width="27px" OnClick="searchClick" />
            </asp:Panel>
            
        </div>
    	
    </div>

</div>
