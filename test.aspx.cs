﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Routing;

public partial class test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RouteValueDictionary parameters = new RouteValueDictionary 
                           { 
                                  {"category", "testCat" }, 
                                  { "id", "123" }              
                           };

        string url = Page.GetRouteUrl("FrontpageRoute", parameters);

        Response.Write("<a href=\"" + url + "\">" + url + "</a>");
    }
}