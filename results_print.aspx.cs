﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class results_print : System.Web.UI.Page
{
    public String reqCategory = "";

    public int leagueID = 0;

    private String thisURL = "";

    public int pcurr;

    private resultItem_list resList = new resultItem_list();
    private List<resultItem> resultItemList = new List<resultItem>();

    private DateTime curDate = Convert.ToDateTime("1900-01-01");

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["category"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        if (func.isNumericType(Request.QueryString["pcurr"], "int"))
        {
            pcurr = Convert.ToInt32(Request.QueryString["pcurr"]);
        }
        else
        {
            pcurr = 1;
        }

        thisURL = func.GetCurrentPageName(Request.Url.Query, true);

        if (!IsPostBack)
        {
            resultItemList = resList.getResultItemList(1000, reqCategory, leagueID);

            if (resultItemList != null)
            {
                rep_results.DataSource = resultItemList;
                rep_results.DataBind();
            }
        }
    }

    protected void rep_results_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            resultItem item = (resultItem)e.Item.DataItem;

            StringBuilder outStr = new StringBuilder();

            HtmlGenericControl wrapRes = new HtmlGenericControl("div");

            if (curDate.ToString("MMMM") != item.date.ToString("MMMM"))
            {
                curDate = item.date;

                outStr.Append("<tr class=\"txt_black_12\">");
                outStr.Append("     <td colspan=\"6\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">" + item.date.ToString("MMMM yyyy") + "</td>");
                outStr.Append("</tr>");
            }

            outStr.Append("<tr class=\"txt_black_12\">");
            outStr.Append("	<td width=\"30\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.date.ToString("dd") + "</td>");
            outStr.Append("	<td align=\"right\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"teams.aspx?id=" + item.homeid + "&category=" + reqCategory + "&leagueid=" + leagueID + "\">" + item.home + "</a></td>");
            outStr.Append("	<td width=\"50\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"matchbreakdown.aspx?id=" + item.id + "&category=" + reqCategory + "&leagueid=" + leagueID + "&homeid=" + item.homeid + "&awayid=" + item.awayid + "&fullview=true\">" + item.homescore + " - " + item.awayscore + "</a></td>");
            outStr.Append("	<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"teams.aspx?id=" + item.awayid + "&category=" + reqCategory + "&leagueid=" + leagueID + "\">" + item.away + "</a></td>");
            outStr.Append("	<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.venue + "</td>");
            outStr.Append("	<td class=\"txt_grey_12\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.tournament + "</td>");
            outStr.Append("</tr>");

            wrapRes.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapRes);
        }
    }
}