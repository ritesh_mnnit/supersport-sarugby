﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="logs.aspx.cs" Inherits="logs" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<%=design.buildHeading("FULL LOG : " + leagueName.ToUpper(), 20, "", "", true)%>



    <div class="wrap_interface_top">

        <asp:Panel ID="pan_page" runat="server" DefaultButton="btn_ok" Visible="false">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="40" class="txt_black_10">&nbsp;PAGE</td>
                    <td width="40"><asp:TextBox CssClass="edit_page" ID="f_page" MaxLength="4" runat="server" /></td>
                    <td width="40" class="txt_black_10"> of <asp:Literal ID="lit_totalpages" runat="server"></asp:Literal></td>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle" style="padding: 0 2px 0 0;"><asp:Button ID="btn_ok" Text="OK" runat="server" CssClass="btn_grey" OnClick="btn_ok_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 2px 0 2px;"><asp:Button ID="btn_prev" Text="PRIOR" runat="server" CssClass="btn_grey" OnClick="btn_prev_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 0 0 2px;"><asp:Button ID="btn_next" Text="NEXT" runat="server" CssClass="btn_grey" OnClick="btn_next_Click" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>

<%=design.makeSpace(10,"",true) %>

    <asp:Panel id="pan_std" runat="server">

    <asp:Panel ID="EP_DISCLAIMER" runat="server" Visible="false">

        <div style="margin:0 0 20px 0;" class="txt_green_12">
            * EP Kings had log points deducted for fielding ineligible player
        </div>

    </asp:Panel>

        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">

	        <tr class="txt_grey_12">
		        <td align="left" bgcolor="#F2F2F2"><strong>TEAM</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>PLAYED</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>WON</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>LOST</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>DREW</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>POINTS FOR</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>POINTS AGAINST</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>TRIES FOR</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>TRIES AGAINST</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>LOSS BONUS</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>TRIES BONUS</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>POINTS</strong></td>
	        </tr>
	        
	        <asp:Repeater ID="rep_logs" runat="server" OnItemDataBound="rep_logs_OnItemDataBound"></asp:Repeater>
	        
        </table>

    </asp:Panel>

    
    <%--<asp:Panel id="pan_extra" runat="server" Visible="false">

    <%=design.makeSpace(20,"",true) %>

    <%=design.buildHeading("COMBINED LOG : " + leagueName.ToUpper(), 20, "", "", true)%>

    <%=design.makeSpace(10,"",true) %>

        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">

	        <tr class="txt_grey_12">
		        <td align="left" bgcolor="#F2F2F2"><strong>TEAM</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>PLAYED</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>WON</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>LOST</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>DREW</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>POINTS FOR</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>POINTS AGAINST</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>TRIES FOR</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>TRIES AGAINST</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>LOSS BONUS</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>TRIES BONUS</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>POINTS</strong></td>
	        </tr>
	        
	        <asp:Repeater ID="rep_extra" runat="server" OnItemDataBound="rep_extra_OnItemDataBound"></asp:Repeater>
	        
        </table>

    </asp:Panel>--%>

</asp:Content>