﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class feeds_rss_news_springboks : System.Web.UI.Page
{
    private SqlConnection DatabaseConn = new SqlConnection(ConfigurationManager.ConnectionStrings["connRead"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.ContentType = "text/xml";
        Response.ContentEncoding = Encoding.UTF8;

        int categoryId = 354;

        if (Request.QueryString["cat"] != null)
        {
            bool result = Int32.TryParse(Request.QueryString["cat"].ToString(), out categoryId);
        }

        XmlWriterSettings ws = new XmlWriterSettings();
        ws.CheckCharacters = true;
        ws.CloseOutput = false;
        ws.Indent = true;
        Encoding utf8;
        utf8 = Encoding.UTF8;
        ws.Encoding = utf8;

        DatabaseConn.Open();

        using (XmlWriter tmpWriter = XmlWriter.Create(Response.Output, ws))
        {
            SqlCommand SqlQuery = new SqlCommand("Select Displayname From SuperSportZone.dbo.ZoneCategories Where (Id = " + categoryId + ")", DatabaseConn);
            string Heading = Convert.ToString(SqlQuery.ExecuteScalar());
            SqlQuery = new SqlCommand("Select Top 1 a.Created From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category INNER JOIN SuperSportZone.dbo.ZoneAuthors d ON d.Id = a.Author INNER JOIN SuperSportZone.dbo.ZoneSites e ON e.Id = c.Site Where (b.Category = " + categoryId + ") And (a.Active = 1) And (a.Utilised = 1) Order By a.Id desc", DatabaseConn);
            DateTime Modified = Convert.ToDateTime(SqlQuery.ExecuteScalar());

            tmpWriter.WriteStartDocument();
            tmpWriter.WriteStartElement("rss");
            tmpWriter.WriteAttributeString("version", "2.0");
            tmpWriter.WriteStartElement("channel");

            tmpWriter.WriteElementString("title", "SA Rugby News | " + Heading);
            tmpWriter.WriteElementString("link", "http://www.sarugby.co.za");
            tmpWriter.WriteElementString("description", "Get all the latest Rugby news.");
            tmpWriter.WriteElementString("language", "en-gb");
            tmpWriter.WriteElementString("lastBuildDate", Modified.AddHours(-2).ToString("ddd',' dd MMM yyyy HH':'mm':'ss 'GMT'"));
            tmpWriter.WriteElementString("copyright", "Copyright: (C) SA Rugby");
            tmpWriter.WriteElementString("ttl", "15");

            tmpWriter.WriteStartElement("image");
            tmpWriter.WriteElementString("title", "SA Rugby News");
            tmpWriter.WriteElementString("url", "http://images.supersport.co.za/rss_sarugby_logo.jpg");
            tmpWriter.WriteElementString("link", "http://www.sarugby.co.za");
            tmpWriter.WriteEndElement();

            SqlQuery = new SqlCommand("Select Top 32 a.Id, a.Headline, a.Blurb, a.Modified, a.SmallImage, a.SmallImageAlt, b.ArticleLevel, b.ArticleDate, c.DisplayName As Category, d.Name As Author, e.ss_Folder As Folder, 1 As Pos From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category INNER JOIN SuperSportZone.dbo.ZoneAuthors d ON d.Id = a.Author INNER JOIN SuperSportZone.dbo.ZoneSites e ON e.Id = c.Site Where (b.Category = " + categoryId + ") And (a.Active = 1) And (a.Utilised = 1) Order By b.ArticleLevel Desc, b.ArticleDate Desc, a.Id Desc", DatabaseConn);
            SqlDataReader RsRec = SqlQuery.ExecuteReader();
            while (RsRec.Read())
            {
                DateTime tmpDate = Convert.ToDateTime(RsRec["Modified"]);
                tmpWriter.WriteStartElement("item");
                tmpWriter.WriteElementString("title", RsRec["Headline"].ToString());
                tmpWriter.WriteElementString("description", RsRec["Blurb"].ToString());
                if (Convert.ToString(RsRec["Folder"]) != "")
                {
                    tmpWriter.WriteElementString("link", "http://www.sarugby.co.za/article.aspx?Id=" + RsRec["Id"]);
                }
                else
                {
                    tmpWriter.WriteElementString("link", "http://www.sarugby.co.za/article.aspx?Id=" + RsRec["Id"]);
                }
                tmpWriter.WriteElementString("author", RsRec["Author"].ToString());
                tmpWriter.WriteElementString("pubDate", tmpDate.AddHours(-2).ToString("ddd',' dd MMM yyyy HH':'mm':'ss 'GMT'"));
                tmpWriter.WriteElementString("category", RsRec["Category"].ToString());
                if (Convert.ToString(RsRec["SmallImage"]) != "")
                {
                    tmpWriter.WriteStartElement("enclosure");
                    tmpWriter.WriteAttributeString("url", "http://images.supersport.co.za/" + RsRec["SmallImage"]);
                    tmpWriter.WriteAttributeString("length", "1");
                    tmpWriter.WriteAttributeString("type", "image/jpeg");
                    tmpWriter.WriteEndElement();
                }

                tmpWriter.WriteEndElement(); 
            }
            RsRec.Close();

            tmpWriter.WriteEndElement();
            tmpWriter.WriteEndElement();
            tmpWriter.WriteEndDocument();
        }

        DatabaseConn.Close();
    }
}