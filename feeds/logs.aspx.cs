using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Xml;

using System.Text;

public partial class logs : System.Web.UI.Page
{
    private int TouramentID;
    private int LeagueID;
    private SqlConnection DatabaseConn = new SqlConnection(ConfigurationManager.ConnectionStrings["connRead"].ConnectionString);
    private SqlCommand SqlQuery;
    private String query;

    protected void Page_Load(object sender, EventArgs e)
    {
        TouramentID = Convert.ToInt32(Request.QueryString["tid"]);

        query = "";
        query += "select top 1 tLEA.ID as LeagueID from rugby.dbo.Leagues tLEA where TournamentID=" + TouramentID + " order by tLEA.ID desc";
        DatabaseConn.Open();
        SqlQuery = new SqlCommand(query, DatabaseConn);
        LeagueID = System.Convert.ToInt32(SqlQuery.ExecuteScalar());
        DatabaseConn.Close();

        MemoryStream xmlStream = new MemoryStream();
        XmlWriterSettings ws = new XmlWriterSettings();
        Encoding utf8;
        utf8 = Encoding.UTF8;
        ws.CheckCharacters = true;
        ws.CloseOutput = false;
        ws.Encoding = utf8;

        using (XmlWriter tmpWriter = XmlWriter.Create(Response.Output, ws))
        {
            tmpWriter.WriteStartDocument();

            query = "";
            query += "SELECT ";
            query += "b.Name As GroupName, ";
            query += "c.Name As League, ";
            query += "c.StartDate As StartDate, ";
            query += "d.Id As TeamId, ";
            query += "d.Name As Team, ";
            query += "d.ShortName as ShortName, ";
            query += "d.MobileName As MobileName, ";
            query += "a.Played, ";
            query += "a.Won, ";
            query += "a.Drew, ";
            query += "a.Lost, ";
            query += "a.PointsFor, ";
            query += "a.PointsAgainst, ";
            query += "a.TriesFor, ";
            query += "a.TriesAgainst, ";
            query += "a.LossBonus, ";
            query += "a.TriesBonus, ";
            query += "a.LogPoints ";
            query += "FROM Rugby.dbo.Logs a ";
            query += "INNER JOIN Rugby.dbo.Groups b ON b.Id = a.GroupId ";
            query += "INNER JOIN Rugby.dbo.Leagues c ON c.Id = a.LeagueId ";
            query += "INNER JOIN Rugby.dbo.TeamsByLeague d ON d.Id = a.TeamId ";
            query += "WHERE (c.Tournamentid = (SELECT tournamentid FROM Rugby.dbo.Leagues WHERE id=" + LeagueID + ")) ";
            query += "ORDER BY c.StartDate desc, b.Name, a.LogPoints DESC, a.PointsFor - a.PointsAgainst DESC, a.TriesFor DESC, d.Name ";

            //query = "Execute Rugby.dbo.LogsByLeague " + LeagueID;

            DatabaseConn.Open();

            SqlQuery = new SqlCommand(query, DatabaseConn);

            SqlDataReader RsRec = SqlQuery.ExecuteReader();

            tmpWriter.WriteStartElement("logs");

            if (RsRec.HasRows)
            {
                tmpWriter.WriteStartElement("logtable");

                while (RsRec.Read())
                {
                    tmpWriter.WriteStartElement("logentry");

                    tmpWriter.WriteAttributeString("league", System.Convert.ToString(RsRec["League"]));
                    tmpWriter.WriteAttributeString("year", System.Convert.ToString(Convert.ToDateTime(RsRec["StartDate"]).ToString("yyyy")));

                    tmpWriter.WriteElementString("group", System.Convert.ToString(RsRec["GroupName"]));
                    tmpWriter.WriteElementString("team", System.Convert.ToString(RsRec["team"]));
                    tmpWriter.WriteElementString("shortName", System.Convert.ToString(RsRec["shortName"]));
                    tmpWriter.WriteElementString("played", System.Convert.ToString(RsRec["Played"]));
                    tmpWriter.WriteElementString("won", System.Convert.ToString(RsRec["Won"]));
                    tmpWriter.WriteElementString("drew", System.Convert.ToString(RsRec["Drew"]));
                    tmpWriter.WriteElementString("lost", System.Convert.ToString(RsRec["Lost"]));
                    tmpWriter.WriteElementString("pointsFor", System.Convert.ToString(RsRec["PointsFor"]));
                    tmpWriter.WriteElementString("pointsAgainst", System.Convert.ToString(RsRec["PointsAgainst"]));
                    tmpWriter.WriteElementString("triesFor", System.Convert.ToString(RsRec["TriesFor"]));
                    tmpWriter.WriteElementString("triesAgainst", System.Convert.ToString(RsRec["TriesAgainst"]));
                    tmpWriter.WriteElementString("lossBonus", System.Convert.ToString(RsRec["LossBonus"]));
                    tmpWriter.WriteElementString("triesBonus", System.Convert.ToString(RsRec["TriesBonus"]));
                    tmpWriter.WriteElementString("pointsDifference", System.Convert.ToString(Convert.ToInt32(RsRec["PointsFor"]) - Convert.ToInt32(RsRec["PointsAgainst"])));
                    tmpWriter.WriteElementString("bonusPoints", System.Convert.ToString(Convert.ToInt32(RsRec["LossBonus"]) - Convert.ToInt32(RsRec["TriesBonus"])));
                    tmpWriter.WriteElementString("logPoints", System.Convert.ToString(RsRec["LogPoints"]));
                    tmpWriter.WriteEndElement();
                }

                tmpWriter.WriteEndElement();
            }

            tmpWriter.WriteEndElement();
        }
    }
}
