﻿<%@ Page Language="C#" ContentType="text/xml" %>
<%@ OutputCache Duration="30" VaryByParam="comp;des;id;type;lang;search;squad;list" %>

<script runat="server">
	//private string connString = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;
	System.Data.SqlClient.SqlConnection dbcon = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connRead"].ConnectionString);
    private System.Data.SqlClient.SqlCommand sqlQuery;

	protected void Page_Load(object sender, EventArgs e)
	{
		dbcon.Open();
		
		//Request vars
		string Id = "";
		string Search = "";
		
		if (!string.IsNullOrEmpty(Request.QueryString["id"]))
		{
			Id = Request.QueryString["id"];
		}
		
		if (!string.IsNullOrEmpty(Request.QueryString["search"]))
		{
			Search = Request.QueryString["search"];
		}				
		
		//setup xml
        System.IO.MemoryStream xmlStream = new System.IO.MemoryStream();
        System.Xml.XmlWriterSettings ws = new System.Xml.XmlWriterSettings();
        Encoding utf8;
		utf8 = Encoding.UTF8;
        ws.CheckCharacters = true;
        ws.CloseOutput = false;
        ws.Encoding = utf8;	
		
		using (System.Xml.XmlWriter tmpWriter = System.Xml.XmlWriter.Create(xmlStream, ws))
		{
            tmpWriter.WriteStartDocument();
			
			if(!string.IsNullOrEmpty(Search))
			{
				tmpWriter.WriteStartElement("profiles");
				searchProfile(tmpWriter, Search);
				tmpWriter.WriteEndElement();
			}
			else if(!string.IsNullOrEmpty(Id))
			{
				tmpWriter.WriteStartElement("profile");
				getProfile(tmpWriter, Id);
				tmpWriter.WriteEndElement();
			}
			else
			{
				tmpWriter.WriteStartElement("profiles");
				getSpringbokSquad(tmpWriter);
				tmpWriter.WriteEndElement();
			}			
        }
		dbcon.Close();
		xmlStream.Seek(0, System.IO.SeekOrigin.Begin);

		System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
		System.Xml.XmlTextReader xmlTextReader = new System.Xml.XmlTextReader(xmlStream);
		xmlTextReader.Read();
		xmlDoc.Load(xmlTextReader);
		xmlTextReader.Close();

		System.Xml.XmlTextWriter tmpTextWriter = new System.Xml.XmlTextWriter(Response.Output);
		xmlDoc.WriteTo(tmpTextWriter);
		tmpTextWriter.Flush();
		tmpTextWriter.Close();
        
		xmlStream.Close();
        			
	}

	private void searchProfile(System.Xml.XmlWriter tmpWriter, string searchVariable)
	{
		System.Data.SqlClient.SqlDataReader rdr = null;

		sqlQuery = new System.Data.SqlClient.SqlCommand("SELECT TOP 100 * FROM rugby.dbo.persons WHERE FullName LIKE @searchvar or Surname LIKE @searchvar or DisplayName LIKE @searchvar or NickName LIKE @searchvar ",
		dbcon);

		System.Data.SqlClient.SqlParameter parm = new System.Data.SqlClient.SqlParameter("searchvar", System.Data.SqlDbType.VarChar);
		parm.Value = "%"+searchVariable+"%";

		sqlQuery.Parameters.Clear();
		sqlQuery.Parameters.Add(parm);		

		rdr = sqlQuery.ExecuteReader();

		if (rdr.HasRows)
		{
			tmpWriter.WriteStartElement("profilesList");

			while (rdr.Read())
			{
				tmpWriter.WriteStartElement("profile");
				tmpWriter.WriteAttributeString("ID", rdr["ID"].ToString());
				if (Convert.ToBoolean(rdr["springbokPlayer"]))
				{
					tmpWriter.WriteAttributeString("IsSpringbok", "true");
				}
				else
				{
					tmpWriter.WriteAttributeString("IsSpringbok", "false");
				}
				tmpWriter.WriteElementString("DisplayName", rdr["DisplayName"].ToString());
				tmpWriter.WriteElementString("Surname", rdr["Surname"].ToString());
				tmpWriter.WriteElementString("NickName", rdr["NickName"].ToString());
				tmpWriter.WriteElementString("FullName", rdr["FullName"].ToString());
				if (!string.IsNullOrEmpty(rdr["SmallImage"].ToString()))
				{
					tmpWriter.WriteElementString("image", "http://images.supersport.co.za/" + rdr["SmallImage"].ToString());
				}
				else
				{
					tmpWriter.WriteElementString("image", "");
				}
				tmpWriter.WriteEndElement();
			}
			rdr.Close();
			tmpWriter.WriteEndElement();
			
		}
		else
		{
			
		}		
	}

	private void getProfile(System.Xml.XmlWriter tmpWriter, string Id)
	{
		System.Data.SqlClient.SqlDataReader rdr = null;

		sqlQuery = new System.Data.SqlClient.SqlCommand("SELECT TOP 1 * FROM rugby.dbo.persons a INNER JOIN SSZGeneral.dbo.rugbystats_players b ON a.birthdate = b.birthdate WHERE a.id = @id",
		dbcon);

		System.Data.SqlClient.SqlParameter parm = new System.Data.SqlClient.SqlParameter("id", System.Data.SqlDbType.Int);
		parm.Value = Id;

		sqlQuery.Parameters.Clear();
		sqlQuery.Parameters.Add(parm);

		rdr = sqlQuery.ExecuteReader();

		if (rdr.HasRows)
		{
			//tmpWriter.WriteStartElement("profile");

			while (rdr.Read())
			{
				tmpWriter.WriteStartElement("player");
				tmpWriter.WriteAttributeString("ID", rdr["ID"].ToString());
				if (Convert.ToBoolean(rdr["springbokPlayer"]))
				{
					tmpWriter.WriteAttributeString("IsSpringbok", "true");
				}
				else
				{
					tmpWriter.WriteAttributeString("IsSpringbok", "false");
				}
				tmpWriter.WriteElementString("DisplayName", rdr["DisplayName"].ToString());
				tmpWriter.WriteElementString("Surname", rdr["Surname"].ToString());
				tmpWriter.WriteElementString("NickName", rdr["NickName"].ToString());
				tmpWriter.WriteElementString("FullName", rdr["FullName"].ToString());
				if (!string.IsNullOrEmpty(rdr["SmallImage"].ToString()))
				{
					tmpWriter.WriteElementString("image", "http://images.supersport.co.za/" + rdr["SmallImage"].ToString());
				}
				else
				{
					tmpWriter.WriteElementString("image", "");
				}
				tmpWriter.WriteElementString("Weight", rdr["Weight"].ToString());
				tmpWriter.WriteElementString("Height", rdr["Height"].ToString());
				tmpWriter.WriteElementString("BirthDate", rdr["BirthDate"].ToString());
				tmpWriter.WriteElementString("BirthPlace", rdr["BirthPlace"].ToString());
				//tmpWriter.WriteElementString("MaritialStatus", rdr["MaritialStatus"].ToString());
				tmpWriter.WriteElementString("Debut", Convert.ToString(rdr["Debut"]));
				tmpWriter.WriteElementString("DebutOpposition", rdr["DebutOpposition"].ToString());
				tmpWriter.WriteElementString("TestCaps", rdr["TestCaps"].ToString());
				tmpWriter.WriteElementString("Tries", rdr["Tries"].ToString());
				tmpWriter.WriteElementString("Conversions", rdr["Conversions"].ToString());
				tmpWriter.WriteElementString("Penalties", rdr["Conversions"].ToString());
				tmpWriter.WriteElementString("DropGoals", rdr["Conversions"].ToString());
				tmpWriter.WriteElementString("Points", rdr["Points"].ToString());
				tmpWriter.WriteElementString("Conversions", rdr["Conversions"].ToString());
				tmpWriter.WriteElementString("FullProfile", "http://www.sarugby.co.za/profile.aspx?id=" + rdr["Id"]);
				tmpWriter.WriteElementString("PlayerWriteUp", rdr["Comments"].ToString().Replace("<br>","<br/>"));
				tmpWriter.WriteEndElement();
			}
			rdr.Close();
			//tmpWriter.WriteEndElement();

		}
		else
		{

		}
	}

	private void getSpringbokSquad(System.Xml.XmlWriter tmpWriter)
	{
		System.Data.SqlClient.SqlDataReader rdr = null;

		sqlQuery = new System.Data.SqlClient.SqlCommand("select e.ID As ID, e.DisplayName, e.Surname, e.NickName, e.FullName, e.SmallImage, e.springbokplayer from Rugby.dbo.squads d inner join Rugby.dbo.Persons e ON e.Id = d.PersonId where d.teamId in (select top 1 c.id from Rugby.dbo.Leagues a inner join Rugby.dbo.Tournaments b on b.id = a.TournamentId inner join Rugby.dbo.TeamsByLeague c on c.LeagueID = a.ID where a.enddate > (GetDate() - 150) and c.name like 'South Africa' and b.CategoryID <> 3 and b.CategoryID <> 4 and b.CategoryID <> 5 and b.CategoryID <> 6 and b.CategoryID <> 7 and b.CategoryID <> 22 and b.CategoryID <> 23 and b.CategoryID <> 24 and b.CategoryID <> 25 order by a.startdate, a.enddate) and e.RoleId = 1 order by e.Surname, e.DisplayName, e.NickName, e.FullName",
		dbcon);

		rdr = sqlQuery.ExecuteReader();

		if (rdr.HasRows)
		{
			tmpWriter.WriteStartElement("profilesList");

			while (rdr.Read())
			{
				tmpWriter.WriteStartElement("profile");
				tmpWriter.WriteAttributeString("ID", rdr["ID"].ToString());
				if (Convert.ToBoolean(rdr["springbokPlayer"]))
				{
					tmpWriter.WriteAttributeString("IsSpringbok", "true");
				}
				else
				{
					tmpWriter.WriteAttributeString("IsSpringbok", "false");
				}
				tmpWriter.WriteElementString("DisplayName", rdr["DisplayName"].ToString());
				tmpWriter.WriteElementString("Surname", rdr["Surname"].ToString());
				tmpWriter.WriteElementString("NickName", rdr["NickName"].ToString());
				tmpWriter.WriteElementString("FullName", rdr["FullName"].ToString());
				if (!string.IsNullOrEmpty(rdr["SmallImage"].ToString()))
				{
					tmpWriter.WriteElementString("image", "http://images.supersport.co.za/" + rdr["SmallImage"].ToString());
				}
				else
				{
					tmpWriter.WriteElementString("image", "");
				}
				tmpWriter.WriteEndElement();
			}
			rdr.Close();
			tmpWriter.WriteEndElement();

		}
		else
		{

		}				
	}
</script>