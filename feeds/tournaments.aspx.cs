﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Xml;

using System.Text;

public partial class tournaments : System.Web.UI.Page
{
    private SqlConnection DatabaseConn = new SqlConnection(ConfigurationManager.ConnectionStrings["connRead"].ConnectionString);
    private SqlCommand SqlQuery;
    private String query;

    protected void Page_Load(object sender, EventArgs e)
    {
        MemoryStream xmlStream = new MemoryStream();
        XmlWriterSettings ws = new XmlWriterSettings();
        Encoding utf8;
        utf8 = Encoding.UTF8;
        ws.CheckCharacters = true;
        ws.CloseOutput = false;
        ws.Encoding = utf8;

        using (XmlWriter tmpWriter = XmlWriter.Create(Response.Output, ws))
        {
            tmpWriter.WriteStartDocument();

            query = "";
            query += "select Id, Name from rugby.dbo.tournaments where not Name = '' order by Name";

            DatabaseConn.Open();

            SqlQuery = new SqlCommand(query, DatabaseConn);

            SqlDataReader RsRec = SqlQuery.ExecuteReader();

            tmpWriter.WriteStartElement("tournaments");

            if (RsRec.HasRows)
            {
                tmpWriter.WriteStartElement("tournamentlist");

                while (RsRec.Read())
                {
                    tmpWriter.WriteStartElement("tournament");

                    tmpWriter.WriteAttributeString("Name", System.Convert.ToString(RsRec["Name"]));
                        tmpWriter.WriteAttributeString("id", System.Convert.ToString(RsRec["Id"]));

                    tmpWriter.WriteEndElement();
                }

                tmpWriter.WriteEndElement();
            }

            tmpWriter.WriteEndElement();

        }
    }
}
