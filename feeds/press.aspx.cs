﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Xml;

using System.Text;

public partial class press : System.Web.UI.Page
{
    private SqlConnection DatabaseConn = new SqlConnection(ConfigurationManager.ConnectionStrings["connRead"].ConnectionString);
    private SqlCommand SqlQuery;
    private String query;

    protected void Page_Load(object sender, EventArgs e)
    {
        MemoryStream xmlStream = new MemoryStream();
        XmlWriterSettings ws = new XmlWriterSettings();
        Encoding utf8;
        utf8 = Encoding.UTF8;
        ws.CheckCharacters = true;
        ws.CloseOutput = false;
        ws.Encoding = utf8;

        using (XmlWriter tmpWriter = XmlWriter.Create( Response.Output, ws))
        {
            tmpWriter.WriteStartDocument();


            query = "";
            query += "SELECT top 10 Id, CategoryName, ArticleDate, Headline, Blurb, Body ";
            query += "FROM articlebreakdown ";
            query += "WHERE (CategoryName='sarugby/pressrelease') AND (Active = 1) ORDER BY ArticleDate DESC";

            DatabaseConn.Open();

            SqlQuery = new SqlCommand(query, DatabaseConn);

            SqlDataReader RsRec = SqlQuery.ExecuteReader();

            tmpWriter.WriteStartElement("news");

            if (RsRec.HasRows)
            {
                tmpWriter.WriteStartElement("articleList");

                while (RsRec.Read())
                {
                    DateTime tmpDate = System.Convert.ToDateTime(RsRec["ArticleDate"]);
                    tmpWriter.WriteStartElement("article");

                    tmpWriter.WriteAttributeString("id", System.Convert.ToString(RsRec["Id"]));

                    tmpWriter.WriteElementString("publicationdate", tmpDate.ToString("yyyy'-'MM'-'dd"));
                    tmpWriter.WriteElementString("headline", System.Convert.ToString(RsRec["Headline"]));
                    tmpWriter.WriteElementString("blurb", System.Convert.ToString(RsRec["Blurb"]));

                    tmpWriter.WriteStartElement("body");
                    tmpWriter.WriteCData(System.Convert.ToString(RsRec["Body"]));
                    tmpWriter.WriteEndElement();

                    tmpWriter.WriteEndElement();
                }
                tmpWriter.WriteEndElement();
            }

            tmpWriter.WriteEndElement();
        }
    }
}
