﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class feeds_rss_video : System.Web.UI.Page
{
    private SqlConnection DatabaseConn = new SqlConnection(ConfigurationManager.ConnectionStrings["connRead"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.ContentType = "text/xml";
        Response.ContentEncoding = Encoding.UTF8;

        int categoryId = 162;

        if (Request.QueryString["cat"] != null)
        {
            bool result = Int32.TryParse(Request.QueryString["cat"].ToString(), out categoryId);
        }

        XmlWriterSettings ws = new XmlWriterSettings();
        ws.CheckCharacters = true;
        ws.CloseOutput = false;
        ws.Indent = true;
        Encoding utf8;
        utf8 = Encoding.UTF8;
        ws.Encoding = utf8;

        DatabaseConn.Open();

        using (XmlWriter tmpWriter = XmlWriter.Create(Response.Output, ws))
        {
            SqlCommand SqlQuery = new SqlCommand("Select Name From SSZGeneral.dbo.broadBandVideoCategories Where (Id = " + categoryId + ")", DatabaseConn);
            string Heading = Convert.ToString(SqlQuery.ExecuteScalar());
            SqlQuery = new SqlCommand("Select Top 1 Created From SSZGeneral.dbo.broadBandVideo Where (Category = 162 And Active = 1 And sarugby = 1) Order By Id Desc", DatabaseConn);
            DateTime Modified = Convert.ToDateTime(SqlQuery.ExecuteScalar());

            tmpWriter.WriteStartDocument();
            tmpWriter.WriteStartElement("rss");
            tmpWriter.WriteAttributeString("version", "2.0");
            tmpWriter.WriteStartElement("channel");

            tmpWriter.WriteElementString("title", "SA Rugby Video | " + Heading);
            tmpWriter.WriteElementString("link", "http://www.sarugby.co.za");
            tmpWriter.WriteElementString("description", "Get all the latest Rugby videos.");
            tmpWriter.WriteElementString("language", "en-gb");
            tmpWriter.WriteElementString("lastBuildDate", Modified.AddHours(-2).ToString("ddd',' dd MMM yyyy HH':'mm':'ss 'GMT'"));
            tmpWriter.WriteElementString("copyright", "Copyright: (C) SA Rugby");
            tmpWriter.WriteElementString("ttl", "15");

            tmpWriter.WriteStartElement("image");
            tmpWriter.WriteElementString("title", "SA Rugby Videos");
            tmpWriter.WriteElementString("url", "http://images.supersport.co.za/rss_sarugby_logo.jpg");
            tmpWriter.WriteElementString("link", "http://www.sarugby.co.za");
            tmpWriter.WriteEndElement();

            SqlQuery = new SqlCommand("Select Top 32 Id, name, shortName, description, thumbnailimage, featureimage, preview, length, created From SSZGeneral.dbo.broadBandVideo Where Category = 162 And Active = 1 And sarugby = 1 Order By Id Desc", DatabaseConn);
            SqlDataReader RsRec = SqlQuery.ExecuteReader();
            while (RsRec.Read())
            {
                DateTime tmpDate = Convert.ToDateTime(RsRec["Created"]);
                tmpWriter.WriteStartElement("item");
                tmpWriter.WriteElementString("title", RsRec["name"].ToString());
                tmpWriter.WriteElementString("description", RsRec["description"].ToString());
                tmpWriter.WriteElementString("link", "http://bb.supersport.co.za/saru_video.asp?play=" + RsRec["Id"]);
                tmpWriter.WriteElementString("pubDate", tmpDate.AddHours(-2).ToString("ddd',' dd MMM yyyy HH':'mm':'ss 'GMT'"));
                tmpWriter.WriteElementString("category", Heading);
                if (Convert.ToString(RsRec["featureImage"]) != "")
                {
                    tmpWriter.WriteStartElement("enclosure");
                    tmpWriter.WriteAttributeString("url", "http://www.supersport.com/videos/images/" + RsRec["featureImage"]);
                    tmpWriter.WriteAttributeString("length", "1");
                    tmpWriter.WriteAttributeString("type", "image/jpeg");
                    tmpWriter.WriteEndElement();
                }

                tmpWriter.WriteEndElement();
            }
            RsRec.Close();

            tmpWriter.WriteEndElement();
            tmpWriter.WriteEndElement();
            tmpWriter.WriteEndDocument();
        }

        DatabaseConn.Close();
    }
}