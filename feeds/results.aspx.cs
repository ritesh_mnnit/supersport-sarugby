﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Xml;

using System.Text;

public partial class results : System.Web.UI.Page
{
    private int TouramentID;
    private int LeagueID;
    private SqlConnection DatabaseConn = new SqlConnection(ConfigurationManager.ConnectionStrings["connRead"].ConnectionString);
    private SqlCommand SqlQuery;
    private String query;

    protected void Page_Load(object sender, EventArgs e)
    {
        TouramentID = Convert.ToInt32(Request.QueryString["tid"]);

        query = "";
        query += "select top 1 tLEA.ID as LeagueID from rugby.dbo.Leagues tLEA where TournamentID=" + TouramentID + " order by tLEA.ID desc";
        DatabaseConn.Open();
        SqlQuery = new SqlCommand(query, DatabaseConn);
        LeagueID = System.Convert.ToInt32(SqlQuery.ExecuteScalar());
        DatabaseConn.Close();

        MemoryStream xmlStream = new MemoryStream();
        XmlWriterSettings ws = new XmlWriterSettings();
        Encoding utf8;
        utf8 = Encoding.UTF8;
        ws.CheckCharacters = true;
        ws.CloseOutput = false;
        ws.Encoding = utf8;

        using (XmlWriter tmpWriter = XmlWriter.Create(Response.Output, ws))
        {
            tmpWriter.WriteStartDocument();

            query = "";
            query += "SELECT a.Id, ";
            query += "a.MatchDateTime, ";
            query += "(SELECT CASE WHEN b.ShortName = '' THEN b.Name ELSE b.ShortName END) AS HomeTeam, ";
            query += "(SELECT CASE WHEN c.ShortName = '' THEN c.Name ELSE c.ShortName END) AS AwayTeam, ";
            query += "d.Name AS League, ";
            query += "e.Name AS Venue, ";
            query += "f.PointsFor as HomeScore, ";
            query += "f.PointsAgainst as AwayScore ";
            query += "From Rugby.dbo.Matches AS a ";
            query += "INNER JOIN Rugby.dbo.TeamsByLeague AS b ON b.Id = a.TeamAID ";
            query += "INNER JOIN Rugby.dbo.TeamsByLeague AS c ON c.Id = a.TeamBID ";
            query += "INNER JOIN Rugby.dbo.Leagues AS d ON d.ID = a.LeagueID ";
            query += "INNER JOIN Rugby.dbo.VenuesByLeague AS e ON e.Id = a.VenueID ";
            query += "INNER JOIN Rugby.dbo.LogDetailsPerMatch AS f ON f.MatchId = a.ID AND a.TeamAID = f.TeamID ";
            query += "WHERE (a.Completed = 1) ";
            query += "AND (a.LeagueID = "+ LeagueID +") ";
            query += "ORDER BY a.MatchDateTime DESC";

            DatabaseConn.Open();

            SqlQuery = new SqlCommand(query, DatabaseConn);

            SqlDataReader RsRec = SqlQuery.ExecuteReader();

            tmpWriter.WriteStartElement("results");

            if (RsRec.HasRows)
            {
                tmpWriter.WriteStartElement("resultslist");

                while (RsRec.Read())
                {
                    DateTime tmpDate = System.Convert.ToDateTime(RsRec["MatchDateTime"]);
                    tmpWriter.WriteStartElement("result");

                    tmpWriter.WriteAttributeString("matchID", System.Convert.ToString(RsRec["Id"]));
                        tmpWriter.WriteAttributeString("matchbreakdownURL", "http://www.sarugby.co.za/matchbreakdown.aspx?id=" + System.Convert.ToString(RsRec["Id"]));
                    tmpWriter.WriteElementString("publicationdate", tmpDate.ToString("yyyy'-'MM'-'dd"));
                    tmpWriter.WriteElementString("HomeTeam", System.Convert.ToString(RsRec["HomeTeam"]));
                    tmpWriter.WriteElementString("AwayTeam", System.Convert.ToString(RsRec["AwayTeam"]));
                    tmpWriter.WriteElementString("HomeScore", System.Convert.ToString(RsRec["HomeScore"]));
                    tmpWriter.WriteElementString("AwayScore", System.Convert.ToString(RsRec["AwayScore"]));
                    tmpWriter.WriteElementString("Venue", System.Convert.ToString(RsRec["Venue"]));

                    tmpWriter.WriteEndElement();
                }

                tmpWriter.WriteEndElement();
            }

            tmpWriter.WriteEndElement();

        }
    }
}
