﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_newspreview.ascx.cs" Inherits="uc_newspreview" %>

<div id="container_news" style="position:relative; height:300px; overflow:hidden;">

    <asp:Repeater ID="rep_newsinfo" runat="server" OnItemDataBound="rep_newsinfo_OnItemDataBound">
    
    <%--ITEMS BUILT IN CODE BEHIND--%>
    
    </asp:Repeater>

    <asp:Repeater ID="rep_newsheadlines" runat="server" OnItemDataBound="rep_newsheadlines_OnItemDataBound">
    
        <HeaderTemplate>
        
            <div class="container_articles_frontpage">
	            
	            <%=design.buildHeading("NEWS HEADLINES", 20, "", "", true)%>
	            
        </HeaderTemplate>
    
        <%-- ITEMS BUILT IN CODE BEHIND --%>
        
        <FooterTemplate>
        
                <div class="article_archive_link txt_green_12"><a href="newsarchive.aspx?<%=archiveURL %>"><strong>...News archive</strong></a></div>
                <%--<div class="article_archive_link txt_green_12"><a href="<%=archiveURL %>/articles"><strong>...News archive</strong></a></div>--%>
            </div>
            
        </FooterTemplate>
    
    </asp:Repeater>

</div>