<%@ Page Language="VB" Debug="true" %>
<%@ OutputCache Duration="60" VaryByParam="id" %>
<%@ Import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlCLient" %>
<%@ import Namespace="System.Configuration" %>
<Script Runat="Server">
	Public Id As String = ""
	Public HomeTeam As String = ""
	Public HomeTeamScore As String = ""
	Public AwayTeam As String = ""
	Public AwayTeamScore As String = ""
	Public MatchStatus As String = ""
	Public Counter As Integer = 0
	Public Reload As Boolean = False
	
	Sub Page_Load
	
		If Not Request.QueryString("id") = Nothing Then
			Id = Request.QueryString("id")
		End If
		
		If Not Request.QueryString("type") = Nothing Then
			Reload = True
		End If
		
        'Dim ConnString As String = ConfigurationSettings.AppSettings("generalstring")
        Dim ConnString As String = "SERVER=repl-sqlcl.supersport.co.za;UID=supersport;PWD=s3@fs#sd2;DATABASE=SupersportZone"
		Dim DatabaseConn As SqlConnection = New SqlCOnnection( ConnString )
		Dim SqlQuery As SqlCommand
		Dim RsRec As SqlDataReader
		Dim KickOff As String = ""
		Dim MatchStatus As String = ""
		Dim HomeTeam As String = ""
		Dim AwayTeam As String = ""
		Dim HomeTeamId As Integer
		Dim AwayTeamId As Integer
		
		DatabaseConn.Open()
		
		SqlQuery = New SqlCommand("SELECT Counter FROM Rugby.dbo.Counter WHERE (Id = 1)", DatabaseConn)
		Counter = SqlQuery.ExecuteScalar()
		
		SqlQuery = New SqlCommand("SELECT a.TeamAId, a.TeamBId, a.MatchDateTime, b.Name AS ATeam, c.Name AS BTeam, d.Name As MatchStatus FROM Rugby.dbo.Matches a INNER JOIN Rugby.dbo.TeamsByLeague b ON b.Id = a.TeamAId INNER JOIN Rugby.dbo.TeamsByLeague c ON c.Id = a.TeamBId INNER JOIN Rugby.dbo.Status d ON d.Id = a.MatchStatus WHERE (a.Id = " & Id & ")", DatabaseConn)
		RsRec = SqlQuery.ExecuteReader
		While RsRec.Read()
			Dim MatchDate As DateTime = RsRec("MatchDatetime")
			Kickoff = MatchDate.ToString("t")
			MatchStatus = RsRec("MatchStatus")
			HomeTeam = RsRec("ATeam")
			AwayTeam = RsRec("BTeam")
			HomeTeamId = RsRec("TeamAId")
			AwayTeamId = RsRec("TeamBId")
		End While
		RsRec.Close()
		
		SqlQuery = New SqlCommand("Execute Rugby.dbo.TeamScore " & Id & ", " & HomeTeamId & "", DatabaseConn)
		RsRec = SqlQuery.ExecuteReader
		While RsRec.Read()
			If IsDbNull(RsRec("Total")) Then
				HomeTeamScore = 0
			Else
				HomeTeamScore = RsRec("Total")
			End If
		End While
		RsRec.Close()
		
		SqlQuery = New SqlCommand("Execute Rugby.dbo.TeamScore " & Id & ", " & AwayTeamId & "", DatabaseConn)
		RsRec = SqlQuery.ExecuteReader
		While RsRec.Read()
			If IsDbNull(RsRec("Total")) Then
				AwayTeamScore = 0
			Else
				AwayTeamScore = RsRec("Total")
			End If
		End While
		RsRec.Close()
		
		Dim retText As String = ""
		
		If MatchStatus = "" Or MatchStatus = "Planned" Then
			retText &= "<table width='300' cellspacing='1' cellpadding='2' border='0' bgcolor='#FFFFFF'>"
			retText &= "<tr>"
			retText &= "<td colspan='12' class='fix1' align='center' style='font-size: 8pt;'><b>" & HomeTeam & " vs " & AwayTeam & "</b><br>Kick off: " & KickOff & "</td>"
			retText &= "</tr>"
			retText &= "</table><br>"
			retText &= "<table width='300' cellspacing='0' cellpadding='0' border='0'>"
			retText &= "<tr><td align='center'><a href=""#1"" onclick=""window.open('live(a).aspx?id=" & Id & "&num=" & Counter & "','rugbylg" & Id & "','width=770,height=500,top=5,left=5,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizeable=yes');"">Full Live Scoring</a></td></tr>"
			retText &= "</table><br>"
			
			Dim HomeTeamPlayers As Boolean = True
			Dim AwayTeamPlayers As Boolean = True
			Dim I As Integer
			For I = 1 TO 2
				If I = 1 Then
					SqlQuery = New SqlCommand("SELECT a.Number, (Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, b.Surname, b.DisplayName FROM Rugby.dbo.LineUps a INNER JOIN Rugby.dbo.Persons b ON b.Id = a.PersonId WHERE (a.MatchId = " & Id & ") AND (a.TeamId = " & HomeTeamId & ") AND (a.Number < 16) ORDER BY a.Number, b.Surname", DatabaseConn)
				Else
					SqlQuery = New SqlCommand("SELECT a.Number, (Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, b.Surname, b.DisplayName FROM Rugby.dbo.LineUps a INNER JOIN Rugby.dbo.Persons b ON b.Id = a.PersonId WHERE (a.MatchId = " & Id & ") AND (a.TeamId = " & AwayTeamId & ") AND (a.Number < 16) ORDER BY a.Number, b.Surname", DatabaseConn)
				End If
				RsRec = SqlQuery.ExecuteReader()
				If RsRec.HasRows() Then
					If I = 2 AND HomeTeamPlayers = True Then
						retText &= "<br><table width='300' cellspacing='1' cellpadding='2' border='0' bgcolor='#FFFFFF'>"
					Else
						retText &= "<table width='300' cellspacing='1' cellpadding='2' border='0' bgcolor='#FFFFFF'>"
					End If
					retText &= "<tr>"
					If I = 1 Then
						retText &= "<td colspan='2' class='fix1'><b>" & HomeTeam & "</b></td>"
					Else
						retText &= "<td colspan='2' class='fix1'><b>" & AwayTeam & "</b></td>"
					End If
					retText &= "</tr>"
					Dim Count As Integer = 1
					While RsRec.Read()
						Dim FullName As String = ""
						If RsRec("DisplayName") = "" Then
							FullName = RsRec("FullName") & " " & RsRec("Surname")
						Else
							FullName = RsRec("DisplayName") & " " & RsRec("Surname")
						End If
						If Count = 1 Then
							retText &= "<tr><td class='fix2' width='300'>"
						End If
						If Count = 1 Then
							retText &= "" & RsRec("Number") & " " & FullName & ""
						Else
							retText &= ", " & RsRec("Number") & " " & FullName & ""
						End If
						Count = Count + 1
					End While
					retText &= "</td>"
					retText &= "</tr>"
					retText &= "</table>"
				Else
					If I = 1 Then
						HomeTeamPlayers = False
					Else
						AwayTeamPlayers = False
					End If
				End If
				RsRec.Close
			Next
		Else
			retText &= "<table width='300' cellspacing='1' cellpadding='2' border='0' bgcolor='#FFFFFF'>"
			retText &= "<tr>"
			retText &= "<td colspan='12' class='fix1' align='center' style='font-size: 8pt;'><b>" & HomeTeam & " " & HomeTeamScore & " - " & AwayTeamScore & " " & AwayTeam & "</b><br>" & MatchStatus & "</td>"
			retText &= "</tr>"
			retText &= "</table><br>"
			retText &= "<table width='300' cellspacing='0' cellpadding='0' border='0'>"
			retText &= "<tr><td align='center'><a href=""#1"" onclick=""window.open('live(a).aspx?id=" & Id & "&num=" & Counter & "','rugbylg" & Id & "','width=770,height=500,top=5,left=5,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizeable=yes');"">Full Live Scoring</a></td></tr>"
			retText &= "</table><br>"
			Dim HomeTeamScoring As Boolean = True
			Dim AwayTeamScoring As Boolean = True
			Dim I As Integer
			For I = 1 To 2
				If I = 1 Then
					SqlQuery = New SqlCommand("Execute Rugby.dbo.EventScoringDetailsByTeam " & Id & ", " & HomeTeamId & "", DatabaseConn)
				Else
					SqlQuery = New SqlCommand("Execute Rugby.dbo.EventScoringDetailsByTeam " & Id & ", " & AwayTeamId & "", DatabaseConn)
				End If
				RsRec = SqlQuery.ExecuteReader()
				If RsRec.HasRows() Then
					If I = 2 AND HomeTeamScoring = True Then
						retText &= "<br><table width='300' cellspacing='1' cellpadding='2' border='0' bgcolor='#FFFFFF'>"
					Else
						retText &= "<table width='300' cellspacing='1' cellpadding='2' border='0' bgcolor='#FFFFFF'>"
					End If
					retText &= "<tr>"
					If I = 1 Then
						retText &= "<td colspan='2' class='fix1'><b>" & HomeTeam & "</b></td>"
					Else
						retText &= "<td colspan='2' class='fix1'><b>" & AwayTeam & "</b></td>"
					End If
					retText &= "</tr>"
					Dim CurrentEvent As String = ""
					Dim OverAllCount As Integer = 1
					Dim Count As Integer = 1
					While RsRec.Read()
						If RsRec("Name") <> CurrentEvent Then
							Count = 1
							CurrentEvent = RsRec("Name")
							If OverAllCount = 1 Then
								retText &= "<tr><td class='fix2' width='100'>" & EventName(RsRec("Name")) & "</td><td class='fix2' width='200'>"
							Else
								retText &= "</td></tr><tr><td class='fix2' width='100'>" & EventName(RsRec("Name")) & "</td><td class='fix2' width='200'>"
							End If
						End If
						If EventName(RsRec("Name")) = "Penalty Tries" Then
							retText &= "Referee"
						Else
							If Count = 1 Then
								If RsRec("Total") > 1 Then
									retText &= "" & Left(RsRec("Fullname"), 1) & " " & RsRec("Surname") & " x " & RsRec("Total") & ""
								Else
									retText &= "" & Left(RsRec("Fullname"), 1) & " " & RsRec("Surname") & ""
								End If
							Else
								If RsRec("Total") > 1 Then
									retText &= ", " & Left(RsRec("Fullname"), 1) & " " & RsRec("Surname") & " x " & RsRec("Total") & ""
								Else
									retText &= ", " & Left(RsRec("Fullname"), 1) & " " & RsRec("Surname") & ""
								End If
							End If
						End If
						OverAllCount = OverAllCount + 1
						Count = Count + 1
					End While
					retText &= "</td>"
					retText &= "</tr>"
					retText &= "</table>"
				Else
					If I = 1 Then
						HomeTeamScoring = False
					Else
						AwayTeamScoring = False
					End If
				End If
				RsRec.Close
			Next
		End If
		
		If Reload = False Then
			SqlQuery = New SqlCommand("SELECT Content FROM ZoneSiteContent WHERE (Site = 16) AND (Type = 'Stats Tag')", DatabaseConn)
			ltl2.Text = SqlQuery.ExecuteScalar()
		End If
		
		DatabaseConn.Close()
		
		ltl1.Text = retText
	
	End Sub
	
	Private Function EventName(ByVal strText As String) As String
	
	Dim retString As String = ""
	
	Select Case Lcase(strText)
		Case "try"
			retString = "Tries"
		Case "conversion"
			retString = "Conversions"
		Case "penalty"
			retString = "Penalties"
		Case "drop goal"
			retString = "Drop Goals"
		Case "penalty try"
			retString = "Penalty Tries"
	End Select
	
	Return retString
	
	End Function
</Script>
<html>
<head>
<title><% =HomeTeamScore %> - <% =AwayTeamScore %></title>
<LINK REL=stylesheet HREF='/External/Styles/livescoring.css' TYPE='text/css'>
<script language="javascript">
	function reloadtimer() {
		timerID = setTimeout('url();',180000);
	}
	function url() {
		location.href = "live.aspx?id=<% =Id %>&type=reload"
	}
</script>
</head>
<body onLoad="reloadtimer();" topmargin="4" leftmargin="4" rightmargin="4" bottommargin="4">
<a name="1"></a>
<Form Runat="Server">
<asp:Literal Id="ltl1" Runat="Server" EnableViewState="False"/>
<asp:Literal Id="ltl2" Runat="Server" EnableViewState="False"/>
</Form>
</body>
<html>