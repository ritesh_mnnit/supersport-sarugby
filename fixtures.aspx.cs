﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class fixtures : System.Web.UI.Page
{
    public String reqCategory = "";

    public int leagueID = 0;

    private String thisURL = "";

    public int pcurr;

    private fixtureItem_list fixList = new fixtureItem_list();
    private List<fixtureItem> fixtureItemList = new List<fixtureItem>();

    private DateTime curDate = Convert.ToDateTime("1900-01-01");

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["categoryid"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        if (func.isNumericType(Request.QueryString["pcurr"], "int"))
        {
            pcurr = Convert.ToInt32(Request.QueryString["pcurr"]);
        }
        else
        {
            pcurr = 1;
        }

        thisURL = func.GetCurrentPageName(Request.Url.Query, true);

        if (!IsPostBack)
        {
            fixtureItemList = fixList.getFixtureItemList(1000, reqCategory, leagueID);

            if (fixtureItemList != null)
            {
                lit_print.Text = "<a href=\"fixtures_print.aspx?category=" + reqCategory + "&leagueid=" + leagueID + "\" target=\"_blank\">Show Printer Friently Version</a>";

                rep_fixtures.DataSource = fixtureItemList;
                rep_fixtures.DataBind();
            }
        }
    }

    protected void rep_fixtures_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            fixtureItem item = (fixtureItem)e.Item.DataItem;

            StringBuilder outStr = new StringBuilder();

            HtmlGenericControl wrapFix = new HtmlGenericControl("div");

            if (curDate.ToString("MMMM") != item.date.ToString("MMMM"))
            {
                curDate = item.date;

                outStr.Append("<tr class=\"txt_black_12\">");
                outStr.Append("     <td colspan=\"7\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">" + item.date.ToString("MMMM yyyy") + "</td>");
                outStr.Append("</tr>");
            }

            outStr.Append("<tr class=\"txt_black_12\">");
            outStr.Append("	<td width=\"30\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.date.ToString("dd") + "</td>");
            outStr.Append("	<td width=\"40\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.date.ToString("HH:mm") + "</td>");
            outStr.Append("	<td align=\"right\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"teams.aspx?id="+ item.homeid +"&category="+ reqCategory +"&leagueid="+ leagueID +"\">" + item.home + "</a></td>");
            outStr.Append("	<td width=\"30\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">vs</td>");
            outStr.Append("	<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"teams.aspx?id=" + item.awayid + "&category="+ reqCategory +"&leagueid="+ leagueID +"\">" + item.away + "</td>");
            outStr.Append("	<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">"+ item.venue +"</td>");
            outStr.Append("	<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.tournament + "</td>");
            outStr.Append("</tr>");

            wrapFix.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapFix);
        }
    }
}