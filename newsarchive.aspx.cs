﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class newsarchive : System.Web.UI.Page
{
    public String reqCategory= "";

    private String thisURL = "";

    public int pcurr;

    private bool isHome = false;

    private newsItem_list list = new newsItem_list();
    private List<newsItem> newsItemList = new List<newsItem>();

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = Request.QueryString["category"];
        if ((reqCategory == "sarugby") || (reqCategory == "") || (reqCategory == null))
        {
            isHome = true;
        }

        if (func.isNumericType(Request.QueryString["pcurr"], "int"))
        {
            pcurr = Convert.ToInt32(Request.QueryString["pcurr"]);
        }
        else
        {
            pcurr = 1;
        }

        thisURL = func.GetCurrentPageName(Request.Url.Query, true);

        if (!IsPostBack)
        {
            btn_prev.Enabled = true;
            btn_next.Enabled = true;

            newsItemList = list.getNewsItemList(500, reqCategory, isHome, System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath).ToLower());

            if (newsItemList != null)
            {
                PagedDataSource tmpSource = new PagedDataSource();

                tmpSource.DataSource = newsItemList;

                tmpSource.AllowPaging = true;

                tmpSource.PageSize = 40;

                if (pcurr < 1) { pcurr = 1; }

                if (pcurr > tmpSource.PageCount) { pcurr = tmpSource.PageCount; }

                f_page.Text = pcurr.ToString();

                tmpSource.CurrentPageIndex = pcurr - 1;

                if (tmpSource.IsFirstPage) { btn_prev.Enabled = false; }

                if (tmpSource.IsLastPage) { btn_next.Enabled = false; }

                lit_totalpages.Text = tmpSource.PageCount.ToString();

                rep_archive.DataSource = tmpSource;

                rep_archive.DataBind();
            }
            else
            {
                pan_page.Enabled = false;
            }
        }
    }

    protected void btn_ok_Click(object sender, EventArgs e)
    {
        Response.Redirect("newsarchive.aspx?category="+ reqCategory +"&pcurr=" + f_page.Text);
    }

    protected void btn_prev_Click(object sender, EventArgs e)
    {
        Response.Redirect("newsarchive.aspx?category=" + reqCategory + "&pcurr=" + (pcurr - 1).ToString());
    }

    protected void btn_next_Click(object sender, EventArgs e)
    {
        Response.Redirect("newsarchive.aspx?category=" + reqCategory + "&pcurr=" + (pcurr + 1).ToString());
    }
}