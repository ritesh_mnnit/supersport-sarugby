﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_statspreview.ascx.cs" Inherits="uc_statspreview" %>

<div style="position:relative;">

	<%=design.buildHeading("MATCH CENTRE", 20, "", "", true)%>
	
	<div class="tabarrow_container">
	
		<div id="tab_MatchData_1" class="tabarrow_active" onmouseover="switchTab('tab_MatchData_',this.id,'info_','info_1','<%=tabCnt %>')">
			<div id="top">FIXTURES
				<div id="bot">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			</div>
		</div>
	    <div class="tabarrow_spacer"></div>
		<div id="tab_MatchData_2" class="tabarrow_inactive" onmouseover="switchTab('tab_MatchData_',this.id,'info_','info_2','<%=tabCnt %>')">
			<div id="top">RESULTS
				<div id="bot">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			</div>
		</div>
		<div class="tabarrow_spacer"></div>
		
		<asp:Panel ID="pan_hometabdata" runat="server">
		
		    <div id="tab_MatchData_3" class="tabarrow_inactive" onmouseover="switchTab('tab_MatchData_',this.id,'info_','info_3','<%=tabCnt %>')">
			    <div id="top">LOGS
				    <div id="bot">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			    </div>
		    </div>
    		
		    <div class="tabarrow_spacer"></div>
		    <div id="tab_MatchData_4" class="tabarrow_inactive" onmouseover="switchTab('tab_MatchData_',this.id,'info_','info_4','<%=tabCnt %>')">
			    <div id="top">CARDS
				    <div id="bot">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			    </div>
		    </div>
		    <div class="tabarrow_spacer"></div>
		
		</asp:Panel>
		
	</div>
	
	<div id="info_1" style="display:block;">
	    <asp:Repeater ID="rep_fixtures" runat="server" OnItemDataBound="rep_fixtures_OnItemDataBound">
	    </asp:Repeater>
	    <div class="stats_more_link txt_green_12"><a href="fixtures.aspx?categoryid=<%=reqCategory %>&leagueid=<%=leagueID %>&fullview=true"><strong>...More fixtures</strong></a></div>
	</div>

	<div id="info_2" style="display:none;">
		<asp:Repeater ID="rep_results" runat="server" OnItemDataBound="rep_results_OnItemDataBound">
	    </asp:Repeater>
		<div class="stats_more_link txt_green_12"><a href="results.aspx?categoryid=<%=reqCategory %>&leagueid=<%=leagueID %>&fullview=true"><strong>...More results</strong></a></div>
	</div>
	
	<asp:Panel ID="pan_homedata" runat="server">
	
	    <div id="info_3" style="display:none">
            <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
	            <tr class="txt_grey_12">
		            <td align="left" valign="top" bgcolor="#F2F2F2"><strong>TEAM</strong></td>
		            <td align="left" valign="top" bgcolor="#F2F2F2"><strong>P</strong></td>
		            <td align="left" valign="top" bgcolor="#F2F2F2"><strong>W</strong></td>
		            <td align="left" valign="top" bgcolor="#F2F2F2"><strong>PTS</strong></td>
	            </tr>
            
		        <asp:Repeater ID="rep_logs" runat="server" OnItemDataBound="rep_logs_OnItemDataBound">
	            </asp:Repeater>
    	    
	        </table>
    	    
		    <div class="stats_more_link txt_green_12"><a href="logs.aspx?categoryid=<%=reqCategory %>&leagueid=<%=leagueID %>&fullview=true"><strong>...Full log</strong></a></div>
	    </div>
    	
    	
	    <div id="info_4" style="display:none">
            <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">

	            <tr class="txt_grey_12">
		            <td align="left" valign="top" bgcolor="#F2F2F2"><strong>PLAYER</strong></td>
		            <td align="center" width="60" valign="top" bgcolor="#F2F2F2"><strong>RED</strong></td>
		            <td align="center" width="60" valign="top" bgcolor="#F2F2F2"><strong>YELLOW</strong></td>
	            </tr>
            
		        <asp:Repeater ID="rep_cards" runat="server" OnItemDataBound="rep_cards_OnItemDataBound">
	            </asp:Repeater>
    	    
	        </table>
    	    
		    <div class="stats_more_link txt_green_12"><a href="cards.aspx?categoryid=<%=reqCategory %>&leagueid=<%=leagueID %>"><strong>...All cards</strong></a></div>
	    </div>
	
	</asp:Panel>
	
</div>