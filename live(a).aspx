<%@ Page Language="VB" Debug="true" %>
<%@ OutputCache Duration="60" VaryByParam="id;num" %>
<%@ Import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlCLient" %>
<%@ import Namespace="System.Configuration" %>
<Script Runat="Server">
	'Public ConnString As String = ConfigurationSettings.AppSettings( "generalstring" )
    Public ConnString As String = "SERVER=repl-sqlcl.supersport.co.za;UID=supersport;PWD=s3@fs#sd2;DATABASE=SupersportZone"
    Public DatabaseConn As SqlConnection = New SqlConnection(ConnString)
	Public SqlQuery As SqlCommand
	Public RsRec As SqlDataReader
	Public HomeTeamScore As String = ""
	Public AwayTeamScore As String = ""
	Public TournamentId As Integer
		
	Sub Page_Load
	
		If Not Request.QueryString("id") = Nothing Then
			Id = Request.QueryString("id")
		End If
				
		DatabaseConn.Open()
		
		Dim MatchDate As DateTime
		Dim MatchStatus As String
		Dim HomeTeam As String = ""
		Dim AwayTeam As String = ""
		Dim HomeTeamId As Integer
		Dim AwayTeamId As Integer
		Dim Venue As String = ""
		Dim LeagueName As String = ""
		Dim LeagueId As Integer
		
		SqlQuery = New SqlCommand("Select b.TournamentId From Rugby.dbo.Matches a INNER JOIN Rugby.dbo.Leagues b ON b.Id = a.LeagueId Where (a.Id = " & Id & ")", DatabaseConn)
		TournamentId = SqlQuery.ExecuteScalar()
		
		SqlQuery = New SqlCommand("SELECT a.TeamAId, a.TeamBId, a.matchDateTime, a.LeagueId, b.Name AS ATeam, c.Name AS BTeam, d.Name As MatchStatus, e.Name As Venue, f.Name As League FROM Rugby.dbo.Matches a INNER JOIN Rugby.dbo.TeamsByLeague b ON b.Id = a.TeamAId INNER JOIN Rugby.dbo.TeamsByLeague c ON c.Id = a.TeamBId INNER JOIN Rugby.dbo.Status d ON d.Id = a.MatchStatus INNER JOIN Rugby.dbo.VenuesByLeague e ON e.Id = a.VenueId INNER JOIN Rugby.dbo.Leagues f ON f.Id = a.LeagueId WHERE (a.Id = " & Id & ")", DatabaseConn)
		RsRec = SqlQuery.ExecuteReader()
		While RsRec.Read()
			MatchStatus = RsRec("MatchStatus")
			HomeTeam = RsRec("ATeam")
			AwayTeam = RsRec("BTeam")
			HomeTeamId = RsRec("TeamAId")
			AwayTeamId = RsRec("TeamBId")
			Venue = RsRec("Venue")
			LeagueName = RsRec("League")
			LeagueId = RsRec("LeagueId")
			MatchDate = RsRec("MatchDateTime")
		End While
		RsRec.Close

		SqlQuery = New SqlCommand("Execute Rugby.dbo.TeamScore " & Id & ", " & HomeTeamId & "", DatabaseConn)
		RsRec = SqlQuery.ExecuteReader()
		While RsRec.Read()
			If IsDbNull(RsRec("Total")) Then
				HomeTeamScore = 0
			Else
				HomeTeamScore = RsRec("Total")
			End If
		End While
		RsRec.Close
		
		SqlQuery = New SqlCommand("Execute Rugby.dbo.TeamScore " & Id & ", " & AwayTeamId & "", DatabaseConn)
		RsRec = SqlQuery.ExecuteReader()
		While RsRec.Read()
			If IsDbNull(RsRec("Total")) Then
				AwayTeamScore = 0
			Else
				AwayTeamScore = RsRec("Total")
			End If
		End While
		RsRec.Close
		
		Dim strText As String = ""
				
		strText &= "<table width='100%' cellspacing='0' border'0' cellpadding='0'>"
		strText &= "<tr>"
		strText &= "<td width='49%' valign='top'>"
		strText &= "<table width='100%' cellspacing='1' cellpadding='2' border='0' class='fixtable'>"
		strText &= "<tr>"
		strText &= "<td width='45%' class='fix1' align='left'>" & MatchDate.ToString("dd MMM HH':'mm") & "</td>"
		If MatchStatus <> "" Then
			strText &= "<td width='10%' class='fix1' align='center'>" & MatchStatus & "</td>"
		Else
			strText &= "<td width='10%' class='fix1' align='center'></td>"
		End If
		strText &= "<td width='45%' class='fix1' align='right'>" & Venue & "</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td width='45%' class='fixheading' align='center'><b>" & HomeTeam & "</b></td>"
		strText &= "<td width='10%' class='fix1' align='center'><b>v</b></td>"
		strText &= "<td width='45%' class='fixheading' align='center'><b>" & AwayTeam & "</b></td>"
		strText &= "</tr>"
		If HomeTeamScore = 0 AND AwayTeamScore = 0 Then
		
		Else
			strText &= "<tr>"
			strText &= "<td width='45%' class='fix2' align='center'>" & HomeTeamScore & "</td>"
			strText &= "<td width='10%' class='fnormal' align='center'></td>"
			strText &= "<td width='45%' class='fix2' align='center'>" & AwayTeamScore & "</td>"
			strText &= "</tr>"
		End If
		strText &= "<tr>"
		strText &= "<td colspan='3' class='fix1' align='center'>" & LeagueName & "</td>"
		strText &= "</tr>"
		
		Dim Tries1 As Integer
		Dim Conversions1 As Integer
		Dim ConAttempts1 As Integer
		Dim Penalties1 As Integer
		Dim PenAttempts1 As Integer
		Dim DropGoals1 As Integer
		Dim DGAttempts1 As Integer
		Dim Yellows1 As Integer
		Dim Reds1 As Integer
		SqlQuery = New SqlCommand("EXECUTE Rugby.dbo.TeamScoringDetails " & Id & "," & HomeTeamId & "", DatabaseConn)
		RsRec = SqlQuery.ExecuteReader()
		While RsRec.Read()
			Tries1 = RsRec("Tries")
			Penalties1 = RsRec("Penalties")
			PenAttempts1 = RsRec("PenAttempts")
			Conversions1 = RsRec("Conversions")
			ConAttempts1 = RsRec("ConAttempts")
			DropGoals1 = RsRec("DropGoals")
			DGAttempts1 = RsRec("DGAttempts")
			Yellows1 = RsRec("Yellows")
			Reds1 = RsRec("Reds")
		End While
		RsRec.Close
		
		Dim Tries2 As Integer
		Dim Conversions2 As Integer
		Dim ConAttempts2 As Integer
		Dim Penalties2 As Integer
		Dim PenAttempts2 As Integer
		Dim DropGoals2 As Integer
		Dim DGAttempts2 As Integer
		Dim Yellows2 As Integer
		Dim Reds2 As Integer
		SqlQuery = New SqlCommand("EXECUTE Rugby.dbo.TeamScoringDetails " & Id & "," & AwayTeamId & "", DatabaseConn)
		RsRec = SqlQuery.ExecuteReader()
		While RsRec.Read()
			Tries2 = RsRec("Tries")
			Penalties2 = RsRec("Penalties")
			PenAttempts2 = RsRec("PenAttempts")
			Conversions2 = RsRec("Conversions")
			ConAttempts2 = RsRec("ConAttempts")
			DropGoals2 = RsRec("DropGoals")
			DGAttempts2 = RsRec("DGAttempts")
			Yellows2 = RsRec("Yellows")
			Reds2 = RsRec("Reds")
		End While
		RsRec.Close
		
		strText &= "<tr>"
		strText &= "<td colspan='3' height='11'></td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td colspan='3' align='center' class='fixheading'>MatchStats</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td class='fix1' align='right'>" & Tries1 & "</td>"
		strText &= "<td class='fix2' align='center'>Tries</td>"
		strText &= "<td class='fix1' align='left'>" & Tries2 & "</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td class='fix1' align='right'>" & Conversions1 & "</td>"
		strText &= "<td class='fix2' align='center'>Conversions</td>"
		strText &= "<td class='fix1' align='left'>" & Conversions2 & "</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td class='fix1' align='right'>" & Penalties1 & "</td>"
		strText &= "<td class='fix2' align='center'>Penalties</td>"
		strText &= "<td class='fix1' align='left'>" & Penalties2 & "</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td class='fix1' align='right'>" & DropGoals1 & "</td>"
		strText &= "<td class='fix2' align='center'>Drop Goals</td>"
		strText &= "<td class='fix1' align='left'>" & DropGoals2 & "</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td colspan='3' align='center' class='fixheading'>Kicking Stats</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td class='fix1' align='right'>" & Conversions1 + Penalties1 & "/" & ConAttempts1 + PenAttempts1 & "</td>"
		strText &= "<td class='fix2' align='center'>Total</td>"
		strText &= "<td class='fix1' align='left'>" & Conversions2 + Penalties2 & "/" & ConAttempts2 + PenAttempts2 & "</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td class='fix1' align='right'>" & Conversions1 & "/" & ConAttempts1 & "</td>"
		strText &= "<td class='fix2' align='center'>Conversions</td>"
		strText &= "<td class='fix1' align='left'>" & Conversions2 & "/" & ConAttempts2 & "</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td class='fix1' align='right'>" & Penalties1 & "/" & PenAttempts1 & "</td>"
		strText &= "<td class='fix2' align='center'>Penalties</td>"
		strText &= "<td class='fix1' align='left'>" & Penalties2 & "/" & PenAttempts2 & "</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td colspan='3' align='center' class='fixheading'>Cards</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td class='fix1' align='right'>" & Yellows1 & "</td>"
		strText &= "<td class='fix2' align='center'>Yellow</td>"
		strText &= "<td class='fix1' align='left'>" & Yellows2 & "</td>"
		strText &= "</tr>"
		strText &= "<tr>"
		strText &= "<td class='fix1' align='right'>" & Reds1 & "</td>"
		strText &= "<td class='fix2' align='center'>Red</td>"
		strText &= "<td class='fix1' align='left'>" & Reds2 & "</td>"
		strText &= "</tr>"
		strText &= "</table><br>"
		
		If TournamentId = 7 Then
			SqlQuery = New SqlCommand("SELECT COUNT(Distinct(Number)) As Number FROM Rugby.dbo.LineUps WHERE (MatchId = " & Id & ") AND (TeamId = " & HomeTeamId & ") AND (Number < 8)", DatabaseConn)
		Else
			SqlQuery = New SqlCommand("SELECT COUNT(Distinct(Number)) As Number FROM Rugby.dbo.LineUps WHERE (MatchId = " & Id & ") AND (TeamId = " & HomeTeamId & ") AND (Number < 16)", DatabaseConn)
		End If
		Dim Team1 As Integer = SqlQuery.ExecuteScalar()
		
		If TournamentId = 7 Then
			SqlQuery = New SqlCommand("SELECT COUNT(Distinct(Number)) As Number FROM Rugby.dbo.LineUps WHERE (MatchId = " & Id & ") AND (TeamId = " & AwayTeamId & ") AND (Number < 8)", DatabaseConn)
		Else
			SqlQuery = New SqlCommand("SELECT COUNT(Distinct(Number)) As Number FROM Rugby.dbo.LineUps WHERE (MatchId = " & Id & ") AND (TeamId = " & AwayTeamId & ") AND (Number < 16)", DatabaseConn)
		End If
		Dim Team2 As Integer = SqlQuery.ExecuteScalar()
		
		Dim TotalRows As Integer
		If Team1 > Team2 Then
			TotalRows = Team1
		Else
			TotalRows = Team2
		End If
		
		If TotalRows > 0 Then
			strText &= "<table width='100%' cellspacing='0' cellpadding='0' border='0' class='fixtable'>"
			strText &= "<tr>"
			strText &= "<td width='50%' class='fixheading' style='padding: 2px; border-right: 1px solid #FFFFFF;'>" & HomeTeam & " Team</td>"
			strText &= "<td width='50%' class='fixheading' style='padding: 2px; border-left: 1px solid #FFFFFF;''>" & AwayTeam & " Team</td>"
			strText &= "</tr>"
			strText &= "<td width='50%' valign='top'>"
			strText &= TeamLineUp(HomeTeamId, TotalRows, 15, "Home")
			strText &= "</td>"
			strText &= "<td width='50%' valign='top'>"
			strText &= TeamLineUp(AwayTeamId, TotalRows, 15, "Away")
			strText &= "</td>"
			strText &= "</tr>"
			strText &= "</table>"
		End If
		
		If TournamentId = 7 Then
			SqlQuery = New SqlCommand("SELECT COUNT(Distinct(Number)) As Number FROM Rugby.dbo.LineUps WHERE (MatchId = " & Id & ") AND (TeamId = " & HomeTeamId & ") AND (Number > 7)", DatabaseConn)
		Else
			SqlQuery = New SqlCommand("SELECT COUNT(Distinct(Number)) As Number FROM Rugby.dbo.LineUps WHERE (MatchId = " & Id & ") AND (TeamId = " & HomeTeamId & ") AND (Number > 15)", DatabaseConn)
		End If
		Team1 = SqlQuery.ExecuteScalar()
		
		If TournamentId = 7 Then
			SqlQuery = New SqlCommand("SELECT COUNT(Distinct(Number)) As Number FROM Rugby.dbo.LineUps WHERE (MatchId = " & Id & ") AND (TeamId = " & AwayTeamId & ") AND (Number > 7)", DatabaseConn)
		Else
			SqlQuery = New SqlCommand("SELECT COUNT(Distinct(Number)) As Number FROM Rugby.dbo.LineUps WHERE (MatchId = " & Id & ") AND (TeamId = " & AwayTeamId & ") AND (Number > 15)", DatabaseConn)
		End If
		Team2 = SqlQuery.ExecuteScalar()
		
		If Team1 > Team2 Then
			TotalRows = Team1
		Else
			TotalRows = Team2
		End If
		
		If TotalRows > 0 Then
			strText &= "<br><table width='100%' cellspacing='0' cellpadding='0' border='0' class='fixtable'>"
			strText &= "<tr>"
			strText &= "<td width='50%' class='fixheading' style='padding: 2px; border-right: 1px solid #FFFFFF;'>" & HomeTeam & " Reserves</td>"
			strText &= "<td width='50%' class='fixheading' style='padding: 2px; border-left: 1px solid #FFFFFF;'>" & AwayTeam & " Reserves</td>"
			strText &= "</tr>"
			strText &= "<td width='50%' valign='top'>"
			strText &= TeamLineUp(HomeTeamId, TotalRows, 22, "Home")
			strText &= "</td>"
			strText &= "<td width='50%' valign='top'>"
			strText &= TeamLineUp(AwayTeamId, TotalRows, 22, "Away")
			strText &= "</td>"
			strText &= "</tr>"
			strText &= "</table>"
		End If
		
		strText &= "</td>"
		strText &= "<td width='2%'></td>"
		strText &= "<td width='49%' valign='top'>"
		
		strText &= "<table width='100%' cellspacing='1' cellpadding='2' border='0' class='fixtable'>"
		SqlQuery = New SqlCommand("SELECT a.MatchTime, (Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, b.Surname As Surname, (Case When c.DisplayName <> '' Then c.DisplayName Else c.FullName End) As FullName1, c.Surname As Surname1, d.Name As Description, a.Comments FROM Rugby.dbo.Commentary a INNER JOIN Rugby.dbo.Persons b ON b.Id = a.Player1Id INNER JOIN Rugby.dbo.Persons c ON c.Id = a.Player2Id INNER JOIN Rugby.dbo.Events d ON d.Id = a.EventId WHERE (a.MatchId = " & Id & ") AND (a.EventId < 12) ORDER BY a.MatchTime DESC, a.Id DESC", DatabaseConn)
		RsRec = SqlQuery.ExecuteReader()
		If RsRec.HasRows() Then
			strText &= "<tr>"
			strText &= "<td colspan='3' class='fixheading' align='center'>Run of Play</td>"
			strText &= "</tr>"
			While RsRec.Read()
				Dim Name As Integer = 0
				Dim Description As Integer = 0
				strText &= "<tr>"
				If RsRec("MatchTime") = 0 OR RsRec("MatchTime") = "0" Then
					strText &= "<td width='20' align='center' class='fix2' valign='top'> - </td>"
				Else
					strText &= "<td width='20' align='center' class='fix2' valign='top'>" & RsRec("MatchTime") & "</td>"
				End If
				strText &= "<td class='fix1'>"
				If RsRec("Surname1") <> "" OR RsRec("FullName1") <> "" Then
					strText &= "" & RsRec("FullName1") & " " & RsRec("Surname1") & " on for " & RsRec("FullName") & " " & RsRec("Surname") & ""
				Else
					If RsRec("Fullname") <> "" OR RsRec("Surname") <> "" Then
						Name = 1
						strText &= "" & RsRec("Fullname") & " " & RsRec("Surname") & ""
					End If
					If RsRec("Description") <> "" AND RsRec("Description") <> "None" Then
						Description = 1
						If Name = 1 Then
							strText &= " " & RsRec("Description") & ""
						Else
							strText &= "" & RsRec("Description") & ""
						End If
					End If
					If RsRec("Comments") <> "" Then
						If Name = 1 OR Description = 1 Then
							strText &= " - " & RsRec("Comments") & ""
						Else
							strText &= "" & RsRec("Comments") & ""
						End If
					End If
				End If
				strText &= "</td>"
				strText &= "</tr>"
			End While
			strText &= "</table>"
		End If
		RsRec.Close
		
		strText &= "</td>"
		strText &= "</tr>"
		strText &= "</table>"
		
		SqlQuery = New SqlCommand("SELECT Content FROM ZoneSiteContent WHERE (Site = 16) AND (Type = 'Stats Tag')", DatabaseConn)
		ltl2.Text = SqlQuery.ExecuteScalar()
		
		DatabaseConn.Close()
		
		ltl1.Text = strText
		
	End Sub

	Private Function TeamLineUp(ByVal TeamId As Integer, ByVal TotalRows As Integer, ByVal Number As Integer, ByVal Team As String) As String
	
		Dim retText As String = ""

		If Number = 15 Then
			If TournamentId = 7 Then
				SqlQuery = New SqlCommand("SELECT a.Number, a.Jersey, (Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, b.Surname FROM Rugby.dbo.LineUps a INNER JOIN Rugby.dbo.Persons b ON b.Id = a.PersonId WHERE (a.MatchId = " & Id & ") AND (TeamId = " & TeamId & ") AND (Number < 8) ORDER BY Number", DatabaseConn)
			Else
				SqlQuery = New SqlCommand("SELECT a.Number, (Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, b.Surname FROM Rugby.dbo.LineUps a INNER JOIN Rugby.dbo.Persons b ON b.Id = a.PersonId WHERE (a.MatchId = " & Id & ") AND (TeamId = " & TeamId & ") AND (Number < 16) ORDER BY Number", DatabaseConn)
			End If
		Else
			If TournamentId = 7 Then
				SqlQuery = New SqlCommand("SELECT a.Number, a.Jersey, (Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, b.Surname FROM Rugby.dbo.LineUps a INNER JOIN Rugby.dbo.Persons b ON b.Id = a.PersonId WHERE (a.MatchId = " & Id & ") AND (TeamId = " & TeamId & ") AND (Number > 7) ORDER BY Number", DatabaseConn)
			Else
				SqlQuery = New SqlCommand("SELECT a.Number, (Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, b.Surname FROM Rugby.dbo.LineUps a INNER JOIN Rugby.dbo.Persons b ON b.Id = a.PersonId WHERE (a.MatchId = " & Id & ") AND (TeamId = " & TeamId & ") AND (Number > 15) ORDER BY Number", DatabaseConn)
			End If
		End If
		RsRec = SqlQuery.ExecuteReader()
		Dim Count As Integer = 0
		Dim CurrentNumber As Integer = 1000
		retText &= "<table width='100%' cellspacing='1' cellpadding='2' border='0' class='fixtable'>"
		While RsRec.Read()
			Dim tmpNumber As Integer = RsRec("Number")
			If TournamentId = 7 Then
				tmpNumber = RsRec("Jersey")
			End If
			If Count = 0 Then
				retText &= "<tr>"
			End If
			If CurrentNumber <> RsRec("Number") AND Count > 0 Then
				retText &= "</td></tr><tr>"
			End If
			If  CurrentNumber = RsRec("Number") Then
				retText &= " / " & RsRec("FullName") & " " & RsRec("Surname") & ""
			Else
				Dim tmpDate As DateTime = New DateTime(2005, 9, 9)
				
				If Team = "Home" Then
					retText &= "<td width='15' class='fix2' align='center' valign='top'>" & tmpNumber & "</td>"
				Else
					retText &= "<td width='15' class='fix2' align='center' valign='top'>" & tmpNumber & "</td>"
				End If
				retText &= "<td class='fix1' valign='top'>" & RsRec("FullName") & " " & RsRec("Surname") & ""
			End If
			Count = Count + 1
			CurrentNumber = RsRec("Number")
		End While
		If Count > 0 Then
			retText &= "</tr>"
		End If
		If Count < TotalRows Then
			Dim I As Integer
			For I = Count + 1 To TotalRows
				retText &= "<tr>"
				retText &= "<td width='15' class='fix2' valign='top'>&nbsp;</td>"
				retText &= "<td class='fix1' valign='top'>&nbsp;</td>"
				retText &= "</tr>"
			Next
		End If
		retText &= "</table>"
		RsRec.Close
		
		Return retText

	End Function
</Script>
<html>
<head>
<title><% =HomeTeamScore %> - <% =AwayTeamScore %></title>
<LINK REL=stylesheet HREF='/External/Styles/livescoring.css' TYPE='text/css'>
<script language=""javascript"">
	function popup (url, name, width, height, scrollbars, toolbar)
	{
		window.open( url, name, "status=no,toolbar=" + toolbar + ",location=no,menubar=no,scrollbars=" + scrollbars + ",resizable=yes,width=" + width + ",height=" + height + ",top=100,left=200");
	}
	function reloadtimer() {
		timerID = setTimeout('self.close()',180000);
	}
</script>
</head>
<body onLoad="reloadtimer();" topmargin="4" leftmargin="4" rightmargin="4" bottommargin="4">
<Form Runat="Server">
<asp:Literal Id="ltl1" Runat="Server" EnableViewState="False"/>
<asp:Literal Id="ltl2" Runat="Server" EnableViewState="False"/>
</Form>
</body>
<html>