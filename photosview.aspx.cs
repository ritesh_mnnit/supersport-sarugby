﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class photosview : System.Web.UI.Page
{
    private String reqCategory = "sarugby/curriecup";

    private String thisURL = "";

    public int pcurr;

    public int galid = -1;

    public int imgid = -1;

    public string galHeading = "";

    private photoItem_list list = new photoItem_list();
    private List<photoItem> photoItemList = new List<photoItem>();

    private StringBuilder outStr = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (func.isNumericType(Request.QueryString["pcurr"], "int"))
        {
            pcurr = Convert.ToInt32(Request.QueryString["pcurr"]);
        }
        else
        {
            pcurr = 1;
        }

        if (func.isNumericType(Request.QueryString["galid"], "int"))
        {
            galid = Convert.ToInt32(Request.QueryString["galid"]);
        }

        if (func.isNumericType(Request.QueryString["imgid"], "int"))
        {
            imgid = Convert.ToInt32(Request.QueryString["imgid"]);
        }

        thisURL = func.GetCurrentPageName(Request.Url.Query, true);

        if (!IsPostBack)
        {
            btn_prev.Enabled = true;
            btn_next.Enabled = true;

            photoItemList = list.getPhotoItemList(galid);

            if (photoItemList != null)
            {
                PagedDataSource tmpSource = new PagedDataSource();

                tmpSource.DataSource = photoItemList;

                tmpSource.AllowPaging = true;

                tmpSource.PageSize = 1;

                if (pcurr < 1) { pcurr = 1; }

                if (pcurr > tmpSource.PageCount) { pcurr = tmpSource.PageCount; }

                f_page.Text = pcurr.ToString();

                tmpSource.CurrentPageIndex = pcurr - 1;

                if (tmpSource.IsFirstPage) { btn_prev.Enabled = false; }

                if (tmpSource.IsLastPage) { btn_next.Enabled = false; }

                lit_totalpages.Text = tmpSource.PageCount.ToString();

                rep_photos.DataSource = tmpSource;

                rep_photos.DataBind();
            }
            else
            {
                pan_page.Enabled = false;
            }
        }
    }

    protected void btn_ok_Click(object sender, EventArgs e)
    {
        Response.Redirect("photosview.aspx?galid=" + galid + "&pcurr=" + f_page.Text);
    }

    protected void btn_prev_Click(object sender, EventArgs e)
    {
        Response.Redirect("photosview.aspx?galid=" + galid + "&pcurr=" + (pcurr - 1).ToString());
    }

    protected void btn_next_Click(object sender, EventArgs e)
    {
        Response.Redirect("photosview.aspx?galid=" + galid + "&pcurr=" + (pcurr + 1).ToString());
    }
}