<%@ Language=VBScript.Encode %>
<html>
<head>
<link REL="stylesheet" HREF="css/general.css" type="text/css">
</head>
<body>
<table align="center" style="border-bottom:1px solid #DEDEDE"><tr><th class="header1">Scrolling Functionality</th></tr></table>
<table><tr style="cursor:hand;">
<td><img class="buttons" src="images/lastprevrec.gif"></td>
<td><img class="buttons" src="images/lastprev.gif"></td>
<td><img class="buttons" src="images/nextrec.gif"></td>
<td><img class="buttons" src="images/nextrecord.gif"></td>
<td><img class="buttons" src="images/update.gif"></td>
<td><img class="buttons" src="images/delete.gif"></td>
<td><img class="buttons" src="images/new.gif"></td>
<td><img class="buttons" src="images/summary.gif"></td>
</tr></table>
<table>
<tr><td colspan=2>The scrolling tool allows the user to scroll between relevant records within a certain screen.</td></tr>
<tr><td width="40"><img class="buttons" src="images/lastprevrec.gif"></td><td><b>Go to previous employee:</b> Clicking on this button will take the user to the record of the previous employee viewed before the currently viewed employee.</td></tr>
<tr><td><img class="buttons" src="images/nextrecord.gif"></td><td><b>Go to next employee:</b>  Clicking on this button will take the user to the next employee's record.</td></tr>
<tr><td><img class="buttons" src="images/lastprev.gif"></td><td><b>Go to the previous record:</b>  This button will take the user to the previous record viewed within the currently viewed employee's portfolio (based on what screen the user is in).</td></tr>
<tr><td><img class="buttons" src="images/nextrec.gif"></td><td><b>Go to next record:</b>  This button will take the user to the next record within the currently viewed employee's portfolio (based on what screen the user is in).</td></tr>
<tr><td><img class="buttons" src="images/update.gif"></td><td><b>Update record:</b>  This button is used to save any changes that have been made on the current record</td></tr>
<tr><td><img class="buttons" src="images/delete.gif"></td><td><b>Delete record:</b>  This button will delete the record currently in view</td></tr>
<tr><td><img class="buttons" src="images/new.gif"></td><td><b>Create new record:</b>  This button will create a new record for the current open screen, for the employee that has already been selected.</td></tr>
<tr><td><img class="buttons" src="images/summary.gif"></td><td><b>Summary view:</b>  This button gives the user the option to view the current screen in a summary view. Clicking on an employee record will open that employee's record in detail view. By clicking on the column heading, the summary view will be ordered descending by the column that has been clicked. Clicking on that column heading again will change the order to ascending.</td></tr>
</table>


</body>
</html>
