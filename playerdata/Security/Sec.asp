<%@ Language=VBScript.encode %>
<!--'===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<%
if Session("SchoolUser") <> "" then
	response.redirect "SchoolSec.asp"
end if

FolderName		=	"Security/"
pathPrefix		=	"../"
Employeeid		=	"UID"
BigOrderBy		=	"FirstName"
UID				=	"UID"
SmallOrderBy	=	"UID"
table				=	"Managers"
CentricTable	=	"Managers"
page				=	"Sec.asp"
ScreenType		=	"Mod"
%>
<!-- #include file="..\includes\asp\nextprevtop.asp" -->
<!-- #include file="..\includes\asp\EditList.asp" -->
<%
'on error resume next
set sql = con1.execute("Select cansee,Functions, Squads from managers where UID='"& rs1("UID") &"'")
if sql(1) = "" or isnull(sql(1)) then:list = "''":else:list = sql(1):end if
if sql("Squads") = "" or isnull(sql("Squads")) then:list12 = "''":else:list12 = sql("Squads"):end if

set rslist = con1.execute("Select * from ManagerFunctions where FunctionID not in ("& list &") and [Function] <> 'Security' and [Function] <> 'Green Squad Update' and location in ('s','t')")
set rslist3 = con1.execute("Select * from ManagerFunctions where FunctionID  in ("& list &") and [Function] <> 'Security' and [Function] <> 'Green Squad Update' and location in ('s','t')")
set rslist4 = con1.execute("Select * from drpSquads where SquadID not in ("& list12 &")")
set rslist5 = con1.execute("Select * from drpSquads where SquadID in ("& list12 &")")

do until rslist3.eof :list2 = list2 & "<option value='"& rslist3(0) &"'>"& rslist3(1) &"</option>":rslist3.movenext:loop
do until rslist5.eof :list3 = list3 & "<option value='"& rslist5(0) &"'>"& rslist5(1) &"</option>":rslist5.movenext:loop
' get list of all the fields
set rsfields= con1.execute("select sysobjects.name,syscolumns.name from syscolumns inner join sysobjects on sysobjects.id= syscolumns.id where sysobjects.name in (Select tablename from ManagerFunctions where  tablename is not null)")
'Variant = recordset.GetString(StringFormat, NumRows, ColumnDelimiter, RowDelimiter, NullExpr)
javafieldsarray = rsfields.getstring(,,",","|")
javafieldsarray = javafieldsarray & "master,Picture|"

function checkblank(val)
	if isnull(val) then:checkblank = "Blank":else:checkblank = val:end if
end function
strColOptions = ""
strColOptions2 = "" 
if not rsList.eof then:Do until rsList.eof:strColOptions=strColOptions & "<Option id='"& checkblank(rslist("tablename")) & "' Value='" & rslist.fields(0) &"'>"  & rslist.fields(1) & "</Option>":rslist.movenext:loop:end if
if not rsList4.eof then:Do until rsList4.eof:strColOptions2=strColOptions2 & "<Option Value='" & rsList4.fields(0) &"'>"  & rsList4.fields(1) & "</Option>":rsList4.movenext:loop:end if
%>
<LINK REL=stylesheet HREF="../includes/css/QBuilder.css" type="text/css">
<script language=javascript>
// JScript source code
	function makestring(array)				{for(x=0;x<array.length;x++){if(array[x]!=''&&array[x]!==undefined){String += array[x] + "|"}}return String;}
	function help(pageType)				{window.open ("Devhelp.asp?pageType="+pageType,"","width=600,height=500,scrollbars=yes,menubar=no,resizable=yes")}
	function addCol(obj,des)				{if (obj.selectedIndex >= 0){var oCloneNode = obj.children[obj.selectedIndex].cloneNode(true);des.appendChild(oCloneNode);obj.children(obj.selectedIndex).removeNode(true);}}

	function removeCol(obj,des,rep)		
	{
		if (des.length > 29)
		{
			alert("The maximum number of squads you may assign a user is 30!");
		}
		else
		{
			if (obj.selectedIndex >= 0)
			{
				var oCloneNode = obj.children(obj.selectedIndex).cloneNode(true);
				if(rep!=false)
				{
					des.insertBefore(oCloneNode);
				}
				obj.children(obj.selectedIndex).removeNode(true);
			}
		};
	}

	function make2DArray(strArray){
		arrTemp = strArray.split("|");
		for (i in arrTemp){arrTemp[i] = arrTemp[i].split(",");}
		return arrTemp;
	}			

	function setdrops1(list,rowID){data = make2DArray(list);objSelect = tblWhere.rows[rowID].cells[2].children[0];objSelect.innerHTML ="";oOption = document.createElement("OPTION");objSelect.appendChild(oOption);oOption.value = "";oOption.text = "Select Value";for(var x=0;x<data.length -1;x++){oOption = document.createElement("OPTION");objSelect.appendChild(oOption);oOption.value = data[x][0];oOption.text = data[x][1]}}

	function submitCols(){
	 	var colsComma = ""
	 	for (i = 0; i < sel2.children.length; i++){colsComma = colsComma + ",'" +sel2.children[i].value + "'" ;}
	 	var colsComma2 = ""
	 	for (i = 0; i < sel3.children.length; i++){colsComma2 = colsComma2 + ",'" +sel3.children[i].value + "'" ;}
	 	strCols = colsComma.substr(1);
	 	updater.txtFunctions.value = strCols;
	 	strCols2 = colsComma2.substr(1);
		if (document.all.sel3.disabled == false)
		{
		 	updater.txtSquads.value = strCols2;
		}
	 	var strSystems = ""
		if (chkSystems1.checked){strSystems=strSystems+"1|";updater.txtDevUserType.value = selDevUserType.value}else{strSystems=strSystems+"0|"}
		if (chkSystems2.checked){strSystems=strSystems+"1|";updater.txtScoutUserType.value = selScoutUserType.value}else{strSystems=strSystems+"0|"}
		if (chkSystems3.checked){strSystems=strSystems+"1";updater.txtRepUserType.value = selRepUserType.value}else{strSystems=strSystems+"0"}
		updater.txtSystems.value = strSystems
	}

	function systems(obj){
		if (eval("lblUserType"+obj+".style.display")=="block"){eval("lblUserType"+obj+".style.display='none'");eval("selUserType"+obj+".style.display='none'");eval("divSpacer"+obj+".style.display='block'");eval("selSpacer"+obj+".style.display='block'");eval("help"+obj+".style.display='none'")}else{eval("lblUserType"+obj+".style.display='block'");eval("selUserType"+obj+".style.display='block'");eval("divSpacer"+obj+".style.display='none'");eval("selSpacer"+obj+".style.display='none'");eval("help"+obj+".style.display='block'")}
		if (obj == '3'){if (eval("lblUserType"+obj+".style.display")=="block"){tblRepresentative.style.display="block"}else{tblRepresentative.style.display="none"}}
	}
	
	function setAll()
	{
		document.all.sel3.disabled = true;
		document.all.sel4.disabled = true;
		document.all.tblSquadAccess.style.display = "none";
		document.all.tblAllSquads.style.display = "";
		document.all.btnSetAll.style.display = "none";
		document.all.btnSetSingle.style.display = "";
	}
	
	function selSingle()
	{
		document.all.sel3.disabled = false;
		document.all.sel4.disabled = false;
		document.all.tblAllSquads.style.display = "none";
		document.all.tblSquadAccess.style.display = "";
		document.all.btnSetAll.style.display = "";
		document.all.btnSetSingle.style.display = "none";
	}
	
	function setAccess()
	{
		if (document.all.sel3.length == 0)
		{
			setAll();
		}
		else
		{
			selSingle();
		};
	}
	
	//Function to make sure that the user has access to the player details screen
	function checkAccess()
	{
		var chkFlag = 1;
		if (chkSystems3.checked)
		{
			chkFlag = 0;
			for (i = 0; i < sel1.children.length; i++)
			{
				if (sel1.children[i].value == 1)
				{
					chkFlag = 1;
				}
			}
		};
		
		if (chkFlag == 1)
		{
			return true;
		}
		else
		{
			alert("A manager with access to the representative system must have access to the player details screen!");
			return false;
		};
	};
	
	parent.frames.Search.close();
</script>
</HEAD>
<body name="MainBody" id="MainBody" topmargin='0' leftmargin='0' background='<%=pathPrefix%>images/eebg.gif' onload="setAccess()">
<td style="width:100%" style='background-color: #F7F7F7;border-top: 1px solid #C0C0C0'>&nbsp;</td></tr></table>
<table class=tblhere border=0 cellpadding=3 cellspacing=0 bordercolor=#999999 style="position:relative;top:10;left:20;width:640px;">
	<tr><td class=headinghere3 style="width:100%;border-left:1px solid #999999;text-align:left">
		<select name="selPage" id="selPage" class="txtboxes" onchange="window.location.href=this.value">
			<option value="sec.asp">Manager Details</option>
			<option value="schoolsec.asp">School Manager Details</option>
		</select>
	</td></tr>
</table><table class=tblhere border=0 cellpadding=3 cellspacing=0 bordercolor=#999999 style="position:relative;left:20;top:12;width:640px;">
	<tr valign="top"><td colspan="3" style="border-left:1px solid #999999;border-top:1px solid #999999;height:92px">
		<table style="width:95%;border-collapse:collapse;position:relative;top:6;" cellpadding='0' cellspacing='0'>
		<form name="updater" id="updater" method=post action=''>
			<tr><td style="width:20%" align='right'>First Name</td><td width='1%'>&nbsp;</td><td style="width:10%"><input name="txtFirstName" ID="txtFirstName" class=txtboxes value="<%=rs1("FirstName")%>"></td><td style="width:20%" align='right'>Email</td><td width='1%'>&nbsp;</td><td style="width:10%"><input name="txtEmail" ID="txtEmail" class=txtboxes value="<%=rs1("Email")%>"></td></tr>
			<tr><td style="width:20%" align='right'>Surname</td><td width='1%'>&nbsp;</td><td style="width:10%"><input name="txtSurname" ID="txtSurname" class="txtboxes" value="<%=rs1("Surname")%>"></td><td style="width:20%" align='right'>Password</td><td width='1%'>&nbsp;</td><td style="width:10%"><input name="txtGSecP" ID="txtGSecP" class=txtboxes value="<%=rs1("GSecP")%>"></td></tr>
			<tr><td colspan='3'></td><td style="width:20%" align='right'>User name</td><td width='1%'>&nbsp;</td><td style="width:10%"><input name="txtGsecU" ID="txtGsecU" class=txtboxes value="<%=rs1("GSecU")%>"></td></tr>
			<tr><td colspan='3'></td><td style="width:20%" align='right'>Province</td><td width='1%'>&nbsp;</td><td style="width:10%"><Select name="selProvince" ID="selProvince" style="height:16px" class=txtboxes><%=getOptions("Province",1)%></Select></td></tr>
			<input type=hidden id="txtFunctions" name="txtFunctions" value="">
			<input type=hidden id="txtCansee" name="txtCansee">
			<input type=hidden id="txtSquads" name="txtSquads">
			<input type=hidden id="txtSystems" name="txtSystems">
			<input type=hidden id="txtDevUserType" name="txtDevUserType">
			<input type=hidden id="txtScoutUserType" name="txtScoutUserType">
			<input type=hidden id="txtRepUserType" name="txtRepUserType">
		</form>
		</table>
	</td>
	<td style="border-top:1px solid #999999;">&nbsp;</td>
	</tr>
</table>
<table class=tblhere border=0 cellpadding=3 cellspacing=0 bordercolor=#999999 style="position:relative;left:20;top:20;width:640px;"><tr><td class=headinghere3 style="width:100%;border-left:1px solid #999999;text-align:left">Has access to the following systems</td></tr></table>
<table class=tblhere border=0 cellpadding=3 cellspacing=0 bordercolor=#999999 style="position:relative;left:20;top:22;width:640px;"><tr><td colspan='4' class=headinghere3><table style="border-collapse:collapse;width:100%;" cellpadding=0 cellspacing=0><tr><td align=left width=30 class=headinghere3 style="border:0px;"><input type="checkbox" id="chkSystems1" name="chkSystems1" style="height:16px;width:13px" onclick="systems('1')"></td><td align=center class=headinghere3 style="Border:0px;width:40%;text-align:left;">Development System</td><td class=headinghere3 style="Border:0px;width:25%"><div style="width:100%;display:block;" id="divSpacer1" name="divSpacer1">&nbsp;</div><div align='right' style="width:100%;display:none;" id="lblUserType1" name="lblUserType1">Type of User</div></td><td class=headinghere3 id="selSpacer1" name="selSpacer1" style="border:0px;display:block;height:20px;">&nbsp;</td><td class=headinghere3 style="Border:0px;display:none;" id="selUserType1" name="selUserType1"><Select name="selDevUserType" ID="selDevUserType" class=txtboxes><%=getOptions("DevUserType",0)%></Select></td><td style="width:1px"><img id="help1" name="help1" src="../images/question.gif" alt="help" style="height:15px;width:15px;display:none;" onclick="help('1')"></td></tr></table></td></tr></table>
<table class=tblhere border=0 cellpadding=3 cellspacing=0 bordercolor=#999999 style="position:relative;left:20;top:24;width:640px;"><tr><td colspan='4' class=headinghere3><table style="border-collapse:collapse;width:100%;" cellpadding=0 cellspacing=0><tr><td align=left width=30 class=headinghere3 style="border:0px;"><input type="checkbox" id="chkSystems2" name="chkSystems2" style="height:16px;width:13px" onclick="systems('2')"></td><td align=center class=headinghere3 style="Border:0px;width:40%;text-align:left;">Scout System</td><td class=headinghere3 style="Border:0px;width:25%"><div style="width:100%;display:block;" id="divSpacer2" name="divSpacer2">&nbsp;</div><div align='right' style="width:100%;display:none;" id="lblUserType2" name="lblUserType2">Type of User</div></td><td class=headinghere3 id="selSpacer2" name="selSpacer2" style="border:0px;display:block;height:20px;">&nbsp;</td><td class=headinghere3 id="selUserType2" name="selUserType2" style="Border:0px;display:none;"><Select name="selScoutUserType" ID="selScoutUserType" class=txtboxes><%=getOptions("ScoutUserType",0)%></Select></td><td style="width:1px"><img id="help2" name="help2" src="../images/question.gif" alt="help" style="height:15px;width:15px;display:none;" onclick="help('2')"></td></tr></table></td></tr></table>
<table class=tblhere border=0 cellpadding=3 cellspacing=0 bordercolor=#999999 style="position:relative;left:20;top:26;width:640px"><tr><td colspan='4' class=headinghere3><table style="border-collapse:collapse;width:100%;" cellpadding=0 cellspacing=0><tr><td align=left width=30 class=headinghere3 style="border:0px;"><input type="checkbox" id="chkSystems3" name="chkSystems3" style="height:16px;width:13px" onclick="systems('3')"></td><td align=center class=headinghere3 style="Border:0px;width:40%;text-align:left;">Representative System</td><td class=headinghere3 style="Border:0px;width:25%"><div style="width:100%;display:block;" id="divSpacer3" name="divSpacer3">&nbsp;</div><div align='right' style="width:100%;display:none;" id="lblUserType3" name="lblUserType3">Access to Security</div></td><td class=headinghere3 id="selSpacer3" name="selSpacer3" style="border:0px;display:block;height:20px;">&nbsp;</td><td class=headinghere3 id="selUserType3" name="selUserType3" style="Border:0px;display:none;"><Select name="selRepUserType" ID="selRepUserType" class=txtboxes><%=getOptions("RepUserType",0)%></Select></td><td style="width:1px"><img id="help3" name="help3" src="../images/question.gif" alt="help" style="height:15px;width:15px;display:none;" onclick="help('3')"></td></tr></table></td></tr></table>
<table id="tblRepresentative" name="tblRepresentative" class=tblhere border=0 cellpadding=3 cellspacing=0 bordercolor=#999999 style="position:relative;left:20;top:26;width:640px;border-top:0px;display:none;">
	<tr><td colspan="2" class=headinghere style="border-top:0px;">Representative Screens</td><td colspan="2" class=headinghere style="border-top:0px;">Representative Squads <input type="button" onclick="setAll()" id="btnSetAll" name="btnSetAll" value="Set All" style="font-size:10px;font-family:Verdana;width:70px;" title="Give the user access to all squads"><input type="button" onclick="selSingle()" id="btnSetSingle" name="btnSetSingle" value="Set Single" style="font-size:10px;font-family:Verdana;width:70px;display:none;" title="Select individual squads the user can access"></td></tr>
	<tr>
		<td colspan="2" class=cellhere align="center" height=60px width="50%">
			<table align="center"><tr><td class=headinghere2>Access</td><td class=headinghere2>No Access</td></tr>
				<tr>
					<td><select class="pulldown1" style="width:150px" name=sel1 id=sel1 size=8 onDblClick="addCol(this,sel2)" onkeydown="if(event.keyCode==13){addCol(this,sel2);};"><%=strColOptions%></select></td>
					<td><select class="pulldown1"  style="width:150px"  name=sel2 id=sel2 size=8 ondblclick="removeCol(this,sel1);" onkeydown="if(event.keyCode==13){removeCol(this,sel1);};"><%=list2%></select></td>
				</tr>
			</table>
		</td>
		<td colspan="2" class=cellhere align="center" height=60px width="50%">	
			<table align="center" id="tblSquadAccess" name="tblSquadAccess" style="display:none">
				<tr><td class=headinghere2>Access</td><td class=headinghere2>No Access</td></tr>
				<tr>
					<td><select class="pulldown1" style="width:150px" name=sel3 id=sel3 size=8 onDblClick="addCol(this,sel4)" onkeydown="if(event.keyCode==13){addCol(this,sel4);};"><%=list3%></select></td>
					<td><select class="pulldown1"  style="width:150px"  name=sel4 id=sel4 size=8 ondblclick="removeCol(this,sel3);" onkeydown="if(event.keyCode==13){removeCol(this,sel3);};"><%=strColOptions2%></select></td>
				</tr>
			</table>
			<table align="center" id="tblAllSquads" name="tblAllSquads" style="display:none">
				<tr>
					<td>This user has access to all the squads.</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="4" class=headinghere>
			<table width=100% bgcolor="#CcCcCc" border=0  cellpadding=0 cellspacing=0 ID="Table12">
			<!------------------------------ WHERE CRITERIA -------------------------------------------->
			<tr>
				<td colspan=2 class=headinghere>
					<table width=100% cellspacing=0 cellpadding=0 ID="Table1"><tr><td><input type="checkbox" value="Search" id="Checkbox1" name="chkWhere" onClick="WhereVis()"  <%=checked%>></td><td align=center>Manager has access to these employees:</td><td style="text-align:right;"><img src="../images/plus.gif" onclick="addSearch();" style='height:9px;width:9px;'>&nbsp;</td></tr></table>
				</td>
			</tr>	
			<tr name=rowWhere id=rowWhere  style="display:<%if checked ="" then response.Write "none" else response.Write "block" %>;">
				<td colspan=2 class=cellhere style="text-align:left">
					<table width=100% id=tblWhere name=tblWhere>
						<%=newrow%>
					</table>
				</td>
			</tr>
			<!---------------------------- END WHERE CRITERIA ------------------------------------------>
			</table>
		</td>
	</tr>
</table>
<form name=gd id=gd method=post action="../includes/asp/GetDrop.asp" target=pin>
	<input type=hidden name=table value="" ID="Hidden1">
	<input type=hidden name=row value="" ID="Hidden2">
	<input type=hidden name=destination value="" ID="Hidden3">
</form>

<iframe name=pin id=pin width=580 height=100 border=0 cellspacing=0 cellpadding=0 style="display:none" src="GetDrop.asp"></iframe>
<script>
spnPageHeader.innerHTML = "Security"
var tabcontrol=false
var Sumcontrol=false
</script>
<!-- #include file="..\includes\asp\xscroll.asp" -->
<script language=javascript>
disabler('<%=rs1("backwards")%>,xx,xx,<%=rs1("Forwards")%>,1,1,1,1')
control.style.top = "142"
control.style.left = "122"
control.style.display = "block"
<%
strVBSystems = rs1("Systems")
strVBArSystems = split (strVBSystems,"|")
if ubound(strVBArSystems) <> 0 then
	for object = 0 to ubound(strVBArSystems)
		if strVBArSystems(object) = "1" then Response.write "chkSystems"&object+1&".checked='True';lblUserType"&object+1&".style.display='block';selUserType"&object+1&".style.display='block';divSpacer"&object+1&".style.display='none';selSpacer"&object+1&".style.display='none';help"&object+1&".style.display='block';"
		if object = "2" then
			if strVBArSystems(object) = "1" then Response.write "tblRepresentative.style.display='block'"
		end if
	next
end if
%>
</script>
</BODY>
</HTML>