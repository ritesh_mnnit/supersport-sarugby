<%@ Language=VBScript.Encode %>
<%#@~^NwAAAA==@#@&wCT+:XwP{~I;;+kY }!+DHjYMkxT~cJalT+PX2nr#@#@&@#@&mQ8AAA==^#~@%>

<html>
<head>
<LINK REL=stylesheet HREF="../includes/css/QBuilder.css" type="text/css">
<title>Genesys Player Manager Security Help - - - - - - - - - - </title>
</head>
<body>
<table class="tblhere" align="center" style="border-collapse:collapse;width:95%;border-right:1px solid #999999;border-bottom:1px solid #999999">
<%#@~^FgAAAA==r6P2moKXa+,xPrFEPDtnU7wYAAA==^#~@%>
<tr><td class=headinghere style="font-size:12px;width:100%;height:36px;" colspan="3"><b>Development System Help</b></td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:36px">This screen describes the different levels of security in the Development (Mass&nbsp;capture) system. Security access that each level has is also described.</td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Superuser</b></td></tr>
<tr><td class=headinghere style="text-align:right">Access any player</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Access Security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Access Reports on all players</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Provincial Administrator</b></td></tr>
<tr><td class=headinghere style="text-align:right">Access any player within the province assigned to the Provincial Administrator</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Access Security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">No</td></tr>
<tr><td class=headinghere style="text-align:right">Access Reports on players within their province</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>School Coach</b></td></tr>
<tr><td class=headinghere style="text-align:right">Access players created by themself only</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Access Security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">No</td></tr>
<tr><td class=headinghere style="text-align:right">Access Reports on players created by themself only</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">No</td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Player (Self help, not available on security screen)</b></td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">Each player that has been added to the database with an email address will be able to login using their email and an auto-generated password.  Their user rights are as follows.</td></tr>
<tr><td class=headinghere style="text-align:right">Access (update only) their own biographical information.</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Change their passwords</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Access Security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">No</td></tr>
<tr><td class=headinghere style="text-align:right">Access Reports</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">No</td></tr>
<%#@~^GgAAAA==n^/nb0,wlT+:zwPxPr E~Dt+	mQgAAA==^#~@%>
<tr><td class=headinghere style="font-size:12px;width:100%;height:36px;" colspan="3"><b>Scout System Help</b></td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Administrator</b></td></tr>
<tr><td class=headinghere style="text-align:right">Access Security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Access Green Squad history / Scouting history reports on all players</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Accept / Deny scouting lists created by the scouts that have been submitted</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Scout</b></td></tr>
<tr><td class=headinghere style="text-align:right">Access Security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">No</td></tr>
<tr><td class=headinghere style="text-align:right">Access Green Squad history / Scouting history reports on all players</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Create, modify, submit scout lists</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<%#@~^GgAAAA==n^/nb0,wlT+:zwPxPr&E~Dt+	mggAAA==^#~@%>
<tr><td class=headinghere style="font-size:12px;width:100%;height:36px;" colspan="3"><b>Representative System Help</b></td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Normal User</b></td></tr>
<tr><td class=headinghere style="text-align:right">Access security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">No</td></tr>
<tr><td class=headinghere style="text-align:right">Access specific squads and screens</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Normal Admin User</b></td></tr>
<tr><td class=headinghere style="text-align:right">Access security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Access specific squads and screens</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Provincial User</b></td></tr>
<tr><td class=headinghere style="text-align:right">Access security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">No</td></tr>
<tr><td class=headinghere style="text-align:right">Access all squads for a specific province</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Provincial Admin User</b></td></tr>
<tr><td class=headinghere style="text-align:right">Access security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Access all squads for a specific province</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere colspan="3" style="text-align:left; height:26px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Super User</b></td></tr>
<tr><td class=headinghere style="text-align:right">Access security screen</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<tr><td class=headinghere style="text-align:right">Access all squads and screens</td><td style="width:5px;border-left:0px;" class=headinghere>&nbsp;</td><td class=headinghere style="border-left:0px;">Yes</td></tr>
<%#@~^BgAAAA==n	N~b0JgIAAA==^#~@%>
<tr><td class=headinghere colspan="3" >&nbsp;</td></tr>
</table>


</body>
</html>
