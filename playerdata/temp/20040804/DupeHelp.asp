<%@ Language=VBScript.Encode %>
<html>
<head>
	<link REL='stylesheet' HREF='includes/css/querybuilder.css' type='text/css'></head>
</head>
<body>

<table width='100%' cellspacing=0 style='BORDER-BOTTOM: 1px solid;BORDER-RIGHT: 1px solid;position:relative;top:-10px;left:-5px' align="center"><tr><td>
<Table width='100%' cellpadding=4 cellspacing=0 border=0 bordercolor=#999999 style='BORDER-BOTTOM: 1px solid;BORDER-RIGHT: 1px solid' ID="Table2">
<tr><th colspan='2' class=cellhere style="background-color:#4A667A;color:#EEEEEE;text-align:left">&nbsp;&nbsp;Help</th></tr>
<tr><td colspan='2'><br><br>Hi,<br>Three options are available to you when a duplication is detected.<br><b>Please note: Clicking on anything in this help file will have no effect on anything.</b> The illustrations on the right are for demonstration purposes only.<br><br></td></tr>
<tr><td><b>1.) Go back and change the data.</b><br><br>In the top left corner of the window you should be able to see the text "Go back and change Data for insert". <br><br>Clicking on this area will prompt you whether you <u>really</u> want to go back and make changes. If you choose Yes, the screen displaying the Duplication summary will hide itself, and the data that you have prepared for insertion / update will be visible again. Once you are ready to resubmit the data, click the applicable insert or update button. If there are no duplications detected. The data will be posted to the database.<br><br></td><td><table style="border-collapse:collapse;"><tr><th class=cellhere style="background-color:#4A667A;color:#DDDDDD;height:45;cursor:hand;text-align:center;" onmouseover="this.style.color='#FFFFFF';this.style.backgroundColor='#758FA5';" onmouseout="this.style.color='#DDDDDD';this.style.backgroundColor='#4A667A';"><<--Go back to change Data for insert</th></tr></table></td></tr>
<tr><td><b>2.) Continue with your Update / Insert.</b><br><br>Above the second column in the Duplication summary, you should see the text "Continue with Insert" (if you're doing an insert) or "Continue with Update" (if you're doing an update).<br><br>If you select this step the following will happen: If there was a matching ID Number or Passport Number (you would see these matching records displayed in the duplication summary screen), the records where the ID Numbers or Passport Numbers match will have their ID Numbers / Passport Numbers reset to blank. In other words, this step assumes that your data is correct, and therefore the other data must be incorrect, and is reset.<br><br></td><td><table style="border-collapse:collapse;"><tr><td onmouseover='mouv(this)'  onmouseout='mout(this)' style='background-color:#BFCAD2;text-align:right;color:#000000;cursor:hand' class=cellhere>CONTINUE WITH UPDATE / INSERT<br>(Matching&nbsp;critical&nbsp;fields will&nbsp;be&nbsp;reset&nbsp;to&nbsp;BLANK)</td></tr></table></td></tr>
<tr><td><b>3.) Overwrite this player's data.</b><br><br>Above the third, fourth... columns, you should see the text "Overwrite this Player's Data"<br><br>Be careful when selecting this option. If you select this step, then the data you have prepared for insert / update is the correct and more current data for the player's data that you choose to overwrite. <u>This overwrite cannot be undone.</u><br><br></td><td><table style="border-collapse:collapse"><tr><td onmouseover='mouv(this)'  onmouseout='mout(this)' style='background-color:#BFCAD2; text-align:center;color:#000000;cursor:hand' class=cellhere>OVERWRITE THIS PLAYER'S DATA</td></tr></table></td></tr>
<tr><td colspan=2>&nbsp;</td></tr>
<tr><td>NOTE:<br><br>If you have matching FIRST NAME, SURNAME + DOB data, the following message should be displayed above the second column, and you should only be able to overwrite the the matched data (FIRST NAME, SURNAME, DOB combinations are considered unique) or go back to change the data. If you come across this and you are sure you data is correct and more current, then overwrite the matching player's record. The matching player's entire record will be overwritten with the data you wish to insert. If you have made a mistake in the name or DOB, then go back and change these fields to the correct data.<br><br></td><td><table style="border-collapse:collapse;"><tr><td onmouseover='mouv(this)'  onmouseout='mout(this)' style='background-color:#BFCAD2;cursor:default;text-align:right;' class=cellhere>Cannot insert this data. Either overwrite or go back and change the data to insert</td></tr></table></td></tr>
</table></td></tr></table>

<script language="javascript">
	function mouv(cell){cell.style.backgroundColor= "#ECF2F2"}
	function mout(cell){cell.style.backgroundColor= "#BFCAD2"}
</script>
</body>
</html>
