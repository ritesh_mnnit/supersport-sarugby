<%@ Language=VBScript.Encode %>
<html>
<head>
<link REL="stylesheet" HREF="css/general.css" type="text/css">
</head>
<body>
<table align="center" style="border-bottom:1px solid #DEDEDE"><tr><th class="header1">Search Functionality</th></tr></table>
<table><tr><td><img src="images/searchbox.gif"></td></tr></table>
<table>
<tr><td>To open up the search tool the user has to <font style="color:blue;font-weight:bold;">click</font> on this button <img src="images/search1.gif"> which can be found in the top right hand corner of the open screen.</td></tr>
<tr><td style="height:10px"></td></tr>
<tr><td>To close the search tool just <font style="color:blue;font-weight:bold;">click</font> on the green button again.</td></tr>
<tr><td style="height:10px"></td></tr>
<tr><td>The search tool allows the user <b>to locate records within the system</b> based on the following criteria:</td></tr>
<tr><td style="height:10px"></td></tr>
<tr><td>
<ul>
<li>Surname</li>
<li>First name</li>
<li>Employee No</li>
<li>Company No</li>
<li>Department</li>
<li>Employee Status</li>
</ul>
</td></tr>
<tr><td>Search can be done on <b>three different levels</b> allowing for detailed searches. If for instance the Surname of 'Smith' is searched for on the first search box, a First Name of 'John' is searched for on the second search box and a Department of 'Buying' is searched for on the third search box, a group of John Smith's in the buying deparment will be displayed in the details area.</td></tr>
<tr><td style="height:10px"></td></tr>
<tr><td>An option exists for each search box to do a partial or exact search. An exact search means that the word you are looking for, for instance a surname of 'Mocke' would need to be stored in the database exactly as typed in. If the user is not sure what the exact spelling of the surname 'Mocke' is, he/she can type in 'Mo' and do a partial search. This will display a group of employee records that are all conected to an employee with a surname that starts with 'Mo'.</td></tr>
<tr><td style="height:10px"></td></tr>
<tr><td>Once a search has been done, which sets up a group of records according to the criteria of that search, the user can clear the search in order to display all records again. This is done by <font style="color:blue;font-weight:bold;">clicking</font> on the icon for the relevant screen in the navigation bar on the left hand side of the screen.</td></tr>
</table> 

</body>
</html>
