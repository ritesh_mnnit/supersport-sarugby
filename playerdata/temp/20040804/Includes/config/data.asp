<%
data = session("data")
if isarray(data) then
	strDataUserID		= data(0)
	strDataUserName		= data(1)
	strSystems			= data(2)
	strUserEmail		= data(3)
	DevUserType			= data(4)
	RepUserType			= data(5)
	ScoutUserType		= data(6)
	strSquads			= data(7)
	strProvince			= data(8)
	strCansee			= data(9)
	screens				= data(10)
	
	ScreenAccess		= " and Page in (" & screens & ") "
	
	if RepUserType = "1" then strRepUserSecurity = " and [Function] <> 'Security' " else strRepUserSecurity = ""
	'--------Checks whether this user has access to more than one system, if so, displays the home icon.
	arrData2 = split(strSystems,"|")
	systemCounter = 0
	for item = 0 to ubound(arrData2)
		if arrData2(item) = "1" then systemCounter = systemCounter + 1
	next
	
	if incFrom = "nextPrevTop" then
		' Set security 
		if session("User") <> True  then
			response.redirect pathPrefix &"expLogin.asp?page=" & FolderName & page
		end if
		'See if person has access to screen																								'
		if page <> "TestsIfr.asp" and page <> "SkillsIfr.asp" and page <> "IsoLegs.asp" and page <> "IsoArms.asp" then																	'
			if instr(ucase(data(10)),ucase(page)) = 0 then																			'
				if instr(ucase(data(10)),"EMPLOYEEDATA.ASP") = 0 then															'
					scr = split(ucase(data(10)),",")																						'
					page = replace(scr(0),"'","")																							'
				else																																'
					page = "Default.asp"																										'
				end if																			   													'
				response.redirect page													   													'
			end if																				   													'
		end if																						   												'
	end if																						   													'
																																					'
	if strCansee <> "" then																													'
		secwhere = replace(strCansee,"'","''")																							'
	End if																				   															'
	if strSquads <> "" then																													'
		dim SquadArray															   															'
		SquadArray = split(replace(strSquads,"'",""),",")					 															'
		for each element in SquadArray                                          															'
			Squadwhere = Squadwhere & " or employeeData.Squads like ''%|" & element & "|%'' "						'
		next																																		'
		Squadwhere = "(" & mid(Squadwhere, 4) & ")"											   									'
	End if																										   									'
																																					'
	if screens <> "" then																														'
		if instr(ucase(screens),ucase("EmployeeDetails.asp")) = 0 then															'
			scr = split(screens,",")										   																	'
			screen = replace(scr(0),"'","")										   															'
		else										   																								'
			screen = "screens/EmployeeDetails.asp"										   											'
		end if 										   																							'
	end if 										   																								'
else																																				'
	response.Redirect PathPrefix & "default.asp"					                                               						'
	'response.write "Error with array!"
end if																			   										   	   						'
%>