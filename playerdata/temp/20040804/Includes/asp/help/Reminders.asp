<html>
<head><link REL="stylesheet" HREF="css/general.css" type="text/css">
<title>Help with reminders</title>
</head>

<body bgcolor=#f5f5f5>
<table align="center" style="border-bottom:1px solid #DEDEDE">
<tr><th class="header1">Reminders</th></tr>
</table>
<table>
<tr><td>To open up the Reminder tool, click on this icon <img src="images/reminder.gif"> which can be found in the top left-hand corner of the screen.</td></tr>
</table>
<table>
<tr><td>With the reminders tool users are able to set reminders on certain fields within the system.</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>If the user deems it necessary to be reminded when any field changes or is about to change, then a reminder can be set up on that field to remind the user of the change.</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>The reminder tool can also be used to remind anyone who is on the system. In this way managers can set up reminders to remind certain employees if they feel that it is necessary for a reminder to be set up.</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>For example, a reminder can be set up to remind a user three months after any new employee's date of join if there is a probation period; or a reminder can be set up to remind a user a week after the date of join of a new employee to see how their induction is proceeding.</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>How to set up reminders</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>Users can either:</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>
<ol>
<li><span style="width:180px">Create new reminders</span><img src="images/remind1.gif"></li>
<li><span style="width:180px">Update existing reminders</span><img src="images/remind2.gif"></li>
</ol>
</td></tr>
<tr><td><u>Create new reminders:</u></td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>
<ol>
<li>Click on Create new</li>
<li>Select the source table or the screen on which the field is which the user wants to be reminded on from the drop down list. A list of possible fields will the appear in the Trigger Field window.</li>
<li>Highlight the field which will be the trigger field</li>
<li>In the Remind me section, type and select the time period before or after the change in the field, the reminder will be sent.</li>
<li>In the Reminder section type the message which will accompany the reminder</li>
<li>Finally, from the Remind drop down list, select who the reminder will be sent to.</li>
</ol>
</td></tr>
<tr><td><u>Update existing reminders:</u></td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>
<ol>
<li>Click on Update Existing. A list will appear with all the current reminders which have been set up on the system.</li>
<li>In this list the user is able to edit the following fields:</li>
</ol>
</td></tr>
<tr><td>
<li>Time interval</li>
<li>Time period</li>
<li>The reminder message</li>
<li>Whether to be reminded before or after</li>
<li>Who to remind</li>
<li>Or the reminder can be deleted</li>
</td></tr>
<tr><td style="height:5px"></td></tr>
<tr><td>
<ol start='3'>
<li>Click on Submit and the reminder will be updated</li>
</ol>
</td></tr>
</table>
<%#@~^JwAAAA==~b0~M+$E+kYvEtV2YHwnE*P',E/bxo^nEPDtxKg0AAA==^#~@%>
<br><center><a href="#" onclick="javascript:parent.window.close()" style="font-size:10px; font-weight:bold">Close Window</a></center>
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
</body>
</html>