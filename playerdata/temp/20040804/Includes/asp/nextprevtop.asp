<!--===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<%	incFrom = "nextPrevTop"	%>
<!-- #include file="../config/DB_Connectivity.asp" -->
<!-- #include file="../config/data.asp" -->
<%
'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'--------Build the Where string -------------------------------------------------------------------------------------------------------------------------------------------------------------
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'wheresql posted from Search screen if blank not been searched
'	response.Write "whereSQL: " & request("wheresql") & "<BR>"
Dim dbFlag
dbFlag = 0

if Page = "Tests.asp" then
	If Request("from") = "Summary" then
		session("LE") = ""
		CurEmployee = Session("LE")
		if ScreenType = "Mod" then where = ""
	elseif ScreenType = "Mod" then
		CurEmployee = request ("hidCurEmpID")
		Session("LE") = ""
		where = ""
	else
		CurEmployee = Session("LE")
	end if
end if

if request("wheresql") <> "" then
	session("LE") = ""
	where = split(request("wheresql"),"and")
	for each element in where
		if instr(1,element,"Squads") > 1 then
			strwhere = strwhere & " and " & replace(replace(replace(	replace(element,"=","like"),"%","")," ''"," ''%|")&" ","'' ","|%'' ")
		else
			strwhere = strwhere & " and " & element
		end if
	next
	where = right(strwhere,len(strwhere)-4)
	if secwhere <> "" then where = secwhere & " and " & where
	if Squadwhere <> "" then where = Squadwhere & " and " & where
	where = " where GenID in(Select Employeedata.GenID from Employeedata where " & where & ")"
else
	if request("hidSQL") <>"" then
		where = request("hidSQL")
	else
		where = ""
		if secwhere <> "" then where = " and " & secwhere
		if Squadwhere <> "" then where = " and " & Squadwhere & where
		if where <> "" then where = " where GenID in(Select Employeedata.GenID from Employeedata where " & mid(where,5) &")"
	end if
end if
'=========================================================================================================================================
'==============SQL STRINGS CREATED FROM THE SCROLL STORED PROC ================================================================================
'=========================================================================================================================================
if Page <> "Tests.asp" then
	If Request("from") = "Summary" then
		session("LE") = ""
		CurEmployee = Session("LE")
		if ScreenType = "Mod" then where = ""
	elseif ScreenType = "Mod" then
		CurEmployee = request ("hidCurEmpID")
		Session("LE") = ""
		where = ""
	else
		CurEmployee = Session("LE")
	end if
end if

Direction = request("direction")
Typescroll= request("typescroll")

'Response.Write "exec scroll '"& table &"','"& CentricTable &"','" &  request("UID") &"','" & CurEmployee &"','"& Direction &"','"& Typescroll &"','"& where &"','"& EmployeeID &"','"& BigOrderBy &"','"& UID &"','"& SmallOrderBy &"'"
'Response.End

set rs1 =con1.execute("exec scroll '"& table &"','"& CentricTable &"','" &  request("UID") &"','" & CurEmployee &"','"& Direction &"','"& Typescroll &"','"& where &"','"& EmployeeID &"','"& BigOrderBy &"','"& UID &"','"& SmallOrderBy &"'")


'This flag is used to check if there are no employees in the database, it will allow the user to input one
intCleanFlag = 0

if Session("SchoolUser") <> "" and rs1.eof = true and page = "PlayerRep.asp" then
	if Session("data")(9) = "" or isnull(Session("data")(9)) then
		strTempCanSee = ""
	else
		strTempCanSee = " where " & Session("data")(9)
	end if
	Set rsTempData = Con1.Execute("Select * from EmployeeData" & strTempCanSee)
		if rsTempData.eof = true then
			intCleanFlag = 1
			Set rs1 = Con1.Execute("select '0' 'backwards', '0' 'backsmall', '0' 'forwardssmall', '0' 'forwards', '' 'linkid', '' 'employeeno', '' 'addresstype', '' 'address1', '' 'address2', '' 'address3', '' 'address4', '' 'agegroup', '' 'bodymass', '' 'cellphone', '' 'DOB', '' 'Email', '' 'EthnicOrigin', '' 'Firstname', '' 'Gender', '' 'GsecP', '' 'HomePhone', '' 'HomePhoneArea', '' 'IDNumber', '' 'Institution', '' 'KnownAs', '' 'ManagerID', '' 'MiddleNames', '' 'PassportNo', '' 'PassportIssued', '' 'Password', '' 'PlayingLevel', '' 'Position', '' 'PostCode', '' 'Province', '' 'RugbyUnion', '' 'Surname', '' 'Username', '' 'WorkPhone', '' 'WorkPhoneArea', '' 'Squad1', '' 'Year1', '' 'Squad2', '' 'Year2', '' 'Squad3', '' 'Year3', '' 'Squad4', '' 'Year4', '' 'Squad5', '' 'Year5', '' 'Squad6', '' 'Year6', '' 'Squad7', '' 'Year7', '' 'Squad8', '' 'Year8', '' 'Squad9', '' 'Year9', '' 'Squad10', '' 'Year10', '' 'Squads', null 'LatestInsert', '' 'GenID', '' 'EmployeeNo'")
		end if
	Set rsTempData = nothing
end if

'==========================================================================================================
'========Set Last employee Session ==============================================================================
'==========================================================================================================
if not rs1.eof and ScreenType <> "Mod" then
	Session("LE") = rs1(EmployeeID)
end if
'==================================================================================================================================
'========This little bit is for the case that a screen is accessed that doesn't have a record for that employee.===============================================
'==================================================================================================================================
dim NotCurEmpNotScrolled
dim LastEmp
NotCurEmpNotScrolled = False
if not rs1.eof and ScreenType <> "Mod" then
	if trim(session("LE")) <> trim(rs1("GenID")) then
		if Direction = "" and Typescroll = "" and where = "" then
			NotCurEmpNotScrolled = True
		end if 
	end if
end if


'==================================================================================================================================
'========If there is not a record for the employee. Because of the way this is handled  ================================================================
'========with NotCurEmpNotScrolled, this should never fire.=====================================================================================
'==================================================================================================================================
if rs1.eof and request("wheresql") <> "" then 
	%><html><head><body background="<%=pathPrefix%>images/eebg.gif" bgcolor="#FFFFFF" onload="javascript:alert('No Players matched your search');javascript:history.go(-1);"></head></html><%
	response.end
end if
'===================================================================================================================================
'========Functions.===================================================================================================================
'===================================================================================================================================
'Set values for the record counter

if not rs1.eof then
	if table <> "EmployeeData" and ScreenType <> "Mod" then
		sn = rs1(0) + 1
		rn = rs1(1) + 1
		rfe= rs1(2) + rn
		sfe = rs1(3) + sn
	else
		sn = rs1(0) + 1
		rn = rs1(0) + 1
		rfe= rs1(3) + rn
		sfe = rs1(3) + sn
	end if
end if

'Get list names array "employeeno,Longname|"
'if not rs1.eof then		
'	namesql ="select isnull(Surname,'') + ' ' + isnull([knownas],'') + ' (' + employeedata.Employeeno +') ',employeedata.GenID from employeedata order by Surname"
'	set namez = con1.execute(namesql)	
'	datanamez = namez.getstring(, ,",","|")		
'else
'	dbFlag = 1
'end if

'25 March 2004 - Added this to replace above stuff
if rs1.eof = true then
	dbFlag = 1
end if

'if not rs1.eof then		
'	namesqlCurrent ="select isnull(Surname,'') + ' ' + isnull([knownas],'') + ' (' + employeedata.Employeeno +') ',employeedata.GenID from employeedata order by Surname"
'	set namezCurrent = con1.execute(namesqlCurrent)	
'	datanamezCurrent = namezCurrent.getstring(, ,",","|")		
'end if

' Check and creates the drop options
set rsDropDowns = con1.execute("exec droploop '" & table &"'")
dim arrDropDowns()
redim arrDropDowns(1,0)
if not rsDropDowns.eof then
	tempMasterCol = rsDropDowns(1)
	arrDropDowns(1,ubound(arrDropDowns,2)) = tempMasterCol
	do until rsDropDowns.eof
		if rsDropDowns(1) <> tempMasterCol then
			tempMasterCol = rsDropDowns(1)
			redim preserve arrDropDowns(1,ubound(arrDropDowns,2)+1)
			arrDropDowns(1,ubound(arrDropDowns,2)) = tempMasterCol
		end if
			arrDropDowns(0,ubound(arrDropDowns,2)) = arrDropDowns(0,ubound(arrDropDowns,2)) & rsDropDowns(0)
		rsDropDowns.movenext
	loop
end if

'Finds the right code for the column of the main table, blank = 1 or 0 1 adds a blank option
function getOptions(masterCol,blank)
	x = 0
	strOptions = ""
	do until x > ubound(arrDropDowns,2)
		if ucase(masterCol) = ucase(arrDropDowns(1,x)) then 
			strOptions = arrDropDowns(0,x)
			strOptions = replace(strOptions,"value='"&Ucase(rs1(arrDropDowns(1,x)))&"'","value='"&Ucase(rs1(arrDropDowns(1,x)))&"' selected ")
			exit do
		end if
		x = x + 1
	loop
	if blank = 0 then
		getOptions = strOptions
	else
		getOptions = "<option></option>" & strOptions
	end if
end function

'Function to return the various institutions, using hard coded values for school, teriary and club
function getOptionsIns(field)
	Dim intIns, strOptions
	intIns = 0
	strOptions = "<option></option>"
	select case lcase(field)
		case "school"
			intIns = 1
		case "tertiary"
			intIns = 2
		case "club"
			intIns = 3
	end select
	strSQL = "select InstitutionID, Institution from drpInstitution where [Level] = " & intIns
	Set rsTempData = Con1.Execute(strSQL)
		if rsTempData.eof = false then
			do while not rsTempData.eof
				strOptions = strOptions & "<option value='" & rsTempData(0) & "'>" & rsTempData(1) & "</option>"
				rsTempData.movenext
			loop
		end if
	Set rsTempData = nothing
	getOptionsIns = strOptions
end function

function getDrpValue(masterTable, masterCol, field)
	strReturnString = ""
	Set rsTempData = Con1.Execute("Select [" & masterCol & "] from [" & masterTable & "] where [" & masterCol & "ID] = '" & rs1(field) & "'")
	if rsTempData.eof = false then
		strReturnString = rsTempData(0)
	end if
	getDrpValue = strReturnString
	'response.end
end function

function formatDate(strDate)
	if not strDate="" and not isnull(strDate) then
		formatDate = day(strDate) & " " & monthname(month(strDate)) & " " & year(strDate)
	else
		formatDate = ""
	end if
end function

Function fieldsec(droptable,field,datatype,dropsetting,additionalHtml,validation,Title)
	if validation <> "" then 
		if field = "DOB" then
			validation = " onBlur="&""""&"check(this,'"& validation  &"'); calcAge(this,'selAgeGroup');"& """"  & " "
		else
			validation = " onBlur="&""""&"check(this,'"& validation  &"')"& """"  & " "
		end if
	end if
    select case datatype
		case  "inputbox"
			fieldsec = "<tr><td><div align='right'>" & title & "</div></td><td width='5'>&nbsp;</td><td width='170'><input  name='txt"&  field  &"'" & validation & " class=txtboxes "& additionalHtml &" onchange='c(this)' value=" & """" &   rs1(field) & """" & " maxlength='30'></td></tr>"  & vbcrlf 
		case "datetype" 
			fieldsec = "<tr><td><div align='right'>" & title & "</div></td><td width='5'>&nbsp;</td><td width='170'><input title='Double click to show calendar' "& additionalHtml &" name='txt"&  field & "'" & validation  & " class=txtboxes  onchange='c(this)' ondblclick='gfPop.fPopCalendar(this)' value=" & """" &  formatDate(rs1(field)) & """" & "></td></tr>"  & vbcrlf 
		case "dropdown"
			button =""
			if droptable <> "" then button = "<td id='custdd' style='display:block'><Input value='::' type=Button class=but onMouseOver='butover(this)' onMouseOut='butout(this)' onclick="&"""" &"ddl('"& droptable &"')" &""""& "></td>"
			fieldsec = "<tr><td><div align='right'>" & title & "</div></td><td width='5'>&nbsp;</td><td width='170'><select name='sel" & field & "' "& additionalHtml &" class=txtboxes onchange='c(this)'>" & getOptions(field,dropsetting)  & "</select></td>"& button &"</tr>"& vbcrlf 
		case  "textarea"
			fieldsec = "<tr><td><div align='right'>" & title & "</div></td><td width='5'>&nbsp;</td><td width='170'><textarea name='txt"&  field  &"'" & validation & " " & additionalHtml &" onchange='c(this)'>"& rs1(field) &"</textarea></td></tr>"  & vbcrlf 
		case "dropdownIns"
			if Session("SchoolUser") = "1" then
				button = ""
			else
				button = "<td style='display:block; width:16px;'><Input value='::' type=Button class=but onMouseOver='butover(this)' onMouseOut='butout(this)' onclick=""openDrpWin(document.all.selPlayingLevel.value);"" id='custdd" & field & "' name='custdd" & field & "'></td>"			
			end if
			fieldsec = "<tr id=""ins" & field & """ name=""ins" & field & """ style=""display:none""><td><div align='right'>" & title & "</div></td><td width='5'>&nbsp;</td><td width='170'><select name='sel" & field & "' "& additionalHtml &" class=txtboxes onchange='c(this)' disabled>" & getOptionsIns(field)  & "</select></td>"& button &"</tr>"& vbcrlf 
    end select
end function

Function fieldsec2(droptable, dropfield, field, field2, datatype, dropsetting, additionalHtml, validation, Title)
    select case datatype
		case  "dropDownSqauds"
			button = "<td id='custdd' style='display:block'><Input value='::' type=Button class=but onMouseOver='butover(this)' onMouseOut='butout(this)' onclick="&"""" &"ddlSquad('"& field &"')" &""""& "></td>"
			fieldsec2 = "<tr><td><div align='right'>" & title & "</div></td><td width='5'>&nbsp;</td><td width='200' nowrap valign='top'><input name='bktxt"&  field  &"' " & validation & " class=txtboxes "& additionalHtml &" onchange='c(this)' value=" & """" &   getDrpValue(dropTable, dropField, field) & """" & " maxlength='30' style='width:145px;' disabled><input type='hidden' name='txt" & field & "' value='" & rs1(field) & "'> <select name='sel" & field2 & "' "& additionalHtml &" class=txtboxes onchange='c(this)' style='width:50px;'>" & getOptions(field2,dropsetting)  & "</select></td>" & button & "</tr>"  & vbcrlf 
    end select
end function

' Function for displaying the summary view
function sumfields(arrayname)
	templist = ""
	z = 0
	for each  element in arrayname
		templist =templist & "," & arrayname(z)
		z =z+1
	next
	if templist <> "" then
	sumfields = right(templist,len(templist)-1)	
	end if
end function

function summary(fields)
	set tabstring = con1.execute("exec Query '"& table &"','nopage','"&UID&"','','','" & table &"." & Employeeid &"=''"&rs1(Employeeid)  &"''',',"&fields&",','"&strPageFilter&"','"&strSec&"','"&SmallOrderBy&"'")
	if not tabstring.eof then
		for each whatever in tabstring.fields
			strHeader= strHeader &"<th class='head'>" & "&nbsp;" & replace(whatever.name," ","&nbsp;") & "&nbsp;" & "</th>"
			strFooter = strFooter &  "<th class='head'>" & "&nbsp;" & "</th>"
		next
		strSummaryBody = tabstring.GetString(,,"</td><td class='cellrep'>","</td></tr><tr style='cursor:hand' onclick='setrec(this)' onmouseover='select(this)' onmouseout='deselect(this)'><td class='cellrep'>","&nbsp;") 
	end if
	summary ="<div id=subDet style='display:none;width: 310; height: 323; overflow: auto;position:absolute;left:335px;top:62px'><table id=Summary width=100% border=0 cellspacing=0 cellpadding=0 ><tr>" & strHeader & "</tr><tr style='cursor:hand' onclick='setrec(this)' onmouseover='select(this)' onmouseout='deselect(this)'><td class='cellrep'>"& strSummaryBody &"</td></tr><tr>" & strFooter & "</tr></table></div>"
end function
'==========================================================================================================
'========End Functions.=======================================================================================
'==========================================================================================================
if dbFlag = 1 then
	session("LE") = ""
	response.redirect page
else
	if ucase(table) <> "MANAGERS" and ucase(table) <> "SCHOOLMANAGERS" then set rsPlayerData = con1.execute("Select top 1 * from EmployeeData where GenID = '" & rs1("GenID") & "'")
end if


%>
<!-- #include file="PageHeader.asp" -->