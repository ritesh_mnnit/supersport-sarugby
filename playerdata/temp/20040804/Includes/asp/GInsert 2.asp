<%@ Language=VBScript.Encode%>
<!--===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<!-- #include file="../config/DB_Connectivity.asp" -->
<!-- #include file="../config/Email.asp" -->
<!-- #include file="../config/data.asp" -->
<%
Dim original
original = SetLocale("en-gb")

Dim DevDB,RepDB
DevDB			= "[PlayerDev]":RepDB = "[PlayerManager]"
changedfields	= request("changedfields")																															' comma delimited list of fields changed 
table				= request("TableName")
ID					= request("ID")																																		' not used this page ID of current record
UKEY				= request("Ukey")																																		' Unique column name

'---------------------------------------------------------------------------------------------
'---------------PASSWORD GENERATOR-------------------------------------------------------
'---------------------------------------------------------------------------------------------
function genPass()
	dim strAlphabet,strPass
	strAlphabet = "BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz123456789"
	strPass = ""
	Randomize()
	i = 0
	do while i <= 6
		strPass = strPass & mid(strAlphabet,cstr(Int(51 * Rnd + 1)),1)
		i = i + 1
	loop
	genPass = strPass
end function

Dim strPassword
strPassword = genPass()
	
Sub EmailPlayer()
	strPlayerName = request.form("txtFirstName") & "&nbsp;" & request.form("txtMiddleNames") & "&nbsp;" & request.form("txtSurname")
	strPlayerEmail = request.form("txtEmail")
	strPlayerPassword = strPassword
	strHtmlHead = "<HTML><HEAD><Style>BODY{FONT-SIZE: xx-small;FONT-FAMILY: Verdana, Arial, Helvetica}TH.cell{BORDER-TOP: 1px solid #999999;BORDER-LEFT: 1px solid #999999;font-size=10px;text-align:center;background-color:#F5F5F5;text-transform:capitalize;color:#666666;}TD.cell{BORDER-TOP: 1px solid #999999;BORDER-LEFT: 1px solid #999999;font-size=10px;text-align:left;background-color:#F5F5F5;height:25px;color:#333333}.THd{BORDER-TOP: 1px solid #999999;BORDER-LEFT: 1px solid #999999;font-size=10px;text-align:center;background-color:#cccccc;text-transform:capitalize;color:#333333;}A{text-decoration:none;color:#333333}A:hover{font-weight:bold;color:#666666}</style></HEAD><Body>"
	strLeaveTable = "<table align=center width='500' align='left' cellpadding='4' cellspacing='0' border='0' style='BORDER-BOTTOM: 1px solid #999999; BORDER-RIGHT: 1px solid #999999;'><tr height='25'><th class='thd' colspan=2>Login information</th></tr><tr><th width=130 class='cell' style='text-align:right'>Link to Player Database login:</th><td class='cell'> <A  HREF=""http://www.playerdata.co.za"">http://www.playerdata.co.za</A></td></tr><tr><th width=130 class='cell' style='text-align:right'>Data manager's Email:</th><td class='cell'>"& ManEmail &"</td></tr><tr><th width=130 class='cell' style='text-align:right'>Player's Name:</th><td class='cell'>"& strPlayerName &"</td></tr><tr><th width=130 class='cell' style='text-align:right'>Username (Email Address):</th><td class='cell'>"& strPlayerEmail &"</td></tr><tr><th class='cell' style='text-align:right'>Password:</th><td class='cell'>"& strPlayerPassword &"</td></tr></table><br>"
	strBody = strHtmlHead & "<table align=center width='500' align='left' cellpadding='4' cellspacing='0' border='0' style='BORDER-BOTTOM: 1px solid #999999; BORDER-RIGHT: 1px solid #999999;'><tr height='25'><th class='thd' colspan=2>SA Rugby�s National Player Database</th></tr><tr><td class='cell'>Your details have been inserted into SA Rugby�s national player database. You can view or update your details through any internet access point by going to :<BR>http://196.36.178.136/playermanager<BR>and logging in. Your information will be used for the tracking of players, for talent identification and for statistical information. This information can only be accessed by the player, using the username and password below and by SA Rugby management. This information will not be used for any purpose other than rugby administration.</td></tr><tr height='25'><th class='thd' colspan=2>How to login</th></tr><tr><td class='cell'>Your login information can be seen below. Log in with your email address and your password. In case of a change in email address you may login using your previous email address and change the email address in your record � remembering that the next time you login you must use your new email address, but the same password. Should you have lost or forgotten your password � you can go to the login screen enter your email address into the username box and click on resend password. <BR> Should you have any other problems please would you email your data-manger (the data-managers email address is included in the information below)</td></tr></table><Br>" & strLeaveTable
	sEmail strUserEmail,strPlayerEmail,"Rugby Database Password",strBody,strUserName
End Sub

function nuller(value)
	if value ="" then
		value = "null"
	else
		value = "'" & value & "'"
	end if
	nuller =value
end function
'---------------------------------------------------------------------------------------------
'---------------END PASSWORD GENERATOR---------------------------------------------------
'---------------------------------------------------------------------------------------------
Dim strSQLCols, strSQLVals
strSQLCols = ""
strSQLVals = ""
for each element in request.Form
	if ucase(element) <> "HIDLATESTINSERT" then
		if trim(request.Form(element)) = "" then value = "null" else value = "'"& trim(replace(request.Form(element),"'","''")) &"'"
		strSQLCols = strSQLCols & "," & "[" & mid(element,4) &"]"
		strSQLVals = strSQLVals & "," & value
		if instr(changedfields,element) <> 0 then
			logstring  = logstring & "<tr><Td>" & mid(element,4)&  "</td><td>No Previous Value</td><td>"& replace(value,"'","") &"</td></tr>"
		end if
	end if
next
strSQL = " insert " & table & "(" & mid(strSQLCols,2) & ") values (" & mid(strSQLVals,2) & ")"

	if ucase(request("tablename")) = "EMPLOYEEDATA" then
		'This is where the duplicate check will go, before the insert so as to ascertain whether or not the record
		'already exists
		
		'Although this check is supposed to be done for the player rep and player dev databases, for now it will simply
		'be done for the player rep database alone
		
		'There are three checks to do:
		'1. Check with the player name, surname and date of birth
		'2. Check with the ID number
		'3. Check with the passport number
		
		'1. Check - Name, Surname, DOB
		
		strCheckSQL = "select GenID from EmployeeData where Surname = '" & request("txtSurname") & "' and Firstname = '" & request("txtFirstName") & "' and DOB = '" & request("txtDOB") & "'"
		Set rsCheck1 = con1.execute(strCheckSQL)
		if rsCheck1.eof = false then
			%>
				<script Language="JavaScript">
					alert("You are attempting to create a duplicate record, there is already a player in the \ndatabase with the same:\n\n- Firstname\n- Surname\n- Date of Birth")
					history.back()
				</script>
			<%
			response.end
		end if
		reCheck1.close
		Set rsCheck1 = nothing
		
		'2. Check the ID number
		strCheckSQL = "select GenID from EmployeeData where IDNumber = '" & request("txtIDNumber") & "'"
		Set rsCheck1 = con1.execute(strCheckSQL)
		if rsCheck1.eof = false then
			%>
				<script Language="JavaScript">
					alert("You are attempting to create a duplicate record, there is already a player in the \ndatabase with the same:\n\n- ID ")
					history.back()
				</script>
			<%
			response.end
		end if
		reCheck1.close
		Set rsCheck1 = nothing
	end if
	con1.execute(strSQL)
	'response.write strSQL
	'response.end
	if ucase(request("tablename")) = "EMPLOYEEDATA" then
		set rsLastRec = con1.execute("select top 1 * from EmployeeData order by GenID desc")
		con1.execute "insert Kit (GenID) values ('" & rsLastRec("GenID") & "') "
		Session("LE") = rsLastRec("GenID")
	end if

if err.number <> 0 then
	%>
		<html><head><body background="../images/eebg.gif" bgcolor="#FFFFFF" onload="javascript:alert('There has been an error trying to insert this information into the database.\nPlease retry or contact your system administrator.');javascript:history.go(-1);"></head></html>
	<%
	response.End
end if
%><!-- #include file="log.asp" --><%
cleanstring = request.ServerVariables("http_referer")
pos = instr(cleanstring,"?")-1
if pos <> -1 then cleanstring = left(request.ServerVariables("http_referer"),pos)
Response.Redirect cleanstring&"?hidSQL="&server.URLEncode(where)
%>