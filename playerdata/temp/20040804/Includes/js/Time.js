/*
-----------------------------------------------
Time Script
-----------------------------------------------
*/
var undefined; // = void 0;

function _asTime() {
	var oTime = new Time(this.value);
	if (typeof(oTime.value) == 'undefined') {
		return undefined;
	}
	else {
		return oTime;
	}
}
_asTime.errorMessage = ' can not be understood as a valid time';

parser = new Object
parser.readValue = function(oField) {
	if (typeof oField.parse == 'function')
		oField.underlyingValue = oField.parse();
	else
		oField.underlyingValue = new String(oField.value);
	if (oField.underlyingValue == undefined && oField.value != '')
		{
			alert('\"' + oField.value + '\"' + oField.parse.errorMessage);
			oField.focus();
			oField.value = "";
			return false;
		}
		else
		{
			parser.showFormatted(oField);
		}
}
parser.showUnderlying = function(oField) {
	// don't use alert statements in this function
	if (oField.underlyingValue != undefined) {
		oField.underlyingValue.format = oField.underlyingValue._baseFormat;
		oField.value = oField.underlyingValue.toString();
	}
}
parser.showFormatted = function(oField) {
	if (oField.underlyingValue != undefined) {
		if (typeof oField.format == 'string')
			oField.underlyingValue.format = oField.format;
		oField.value = oField.underlyingValue.toString();
	}
}

function Time(sTimeVal) {
	if (typeof sTimeVal == 'string') {
		var sTime = sTimeVal;
		var TimeVal;
		var intCounter;
		var items = new Array();
		items[0] = "";
		
		var lHour = 0;
		var lMinute = 0;
		var lSecond = 0;
		var fValid = false;
		
		for (intCounter = 0; intCounter < sTime.length; intCounter++) {
			if (Time.Delims.indexOf(sTime.charAt(intCounter)) > -1) {
				//intCounter++; // what if seperator is at end of strTime??
				if (items[items.length - 1] != "") 
					items[items.length] = "";
					// only add new item if last item is not null
			}
			else if (sTime.charAt(intCounter)=='0' && items[items.length - 1] == "") {
				//Strip leading zeros, unless 0
				if (intCounter + 1 < sTime.length) {
					if (Time.Delims.indexOf(sTime.charAt(intCounter+1)) > -1)
						items[items.length - 1]='0';
				}
				else if (intCounter + 1 == sTime.length) {
					items[items.length-1]='0';
				}
			}
			else
				items[items.length-1] += sTime.charAt(intCounter);
		}
		
		if (items.length==3) {
			if (parseInt(items[0])!=items[0] || parseInt(items[1])!=items[1]) {
				fValid = false;
			}
			else if (parseInt(items[0])<0 || parseInt(items[0])>23) {
				fValid = false;
			}
			else if (parseInt(items[1])<0 || parseInt(items[1])>59) {
				fValid = false;
			}
			else if (parseInt(items[2]) == items[2]) {
				if (parseInt(items[2])<0 || parseInt(items[2])>59) {
					fValid = false;
				}
				else {
					//items[3] = Seconds
					lHour = parseInt(items[0]);
					lMinute = parseInt(items[1]);
					lSecond = parseInt(items[2]);
					fValid = true;
				}
			}
			else if (items[2] == "PM" || items[2] == "pm" || items[2] == "P" || items[2] == "p") {
				if (parseInt(items[0])<1 || parseInt(items[0])>12) {
					fValid = False;
				}
				else if (parseInt(items[0])==12) {
					//PM
					lHour = 12;
					lMinute = parseInt(items[1]);
					lSecond = 0;
					fValid = true
				}
				else {
					//PM
					lHour = (parseInt(items[0])+12);
					lMinute = parseInt(items[1]);
					lSecond = 0;
					fValid = true
				}
			}
			else if (items[2] == "AM" || items[2] == "am" || items[2] == "A" || items[2] == "a") {
				if (parseInt(items[0])<1 || parseInt(items[0])>12) {
					fValid = false;
				}
				else if (parseInt(items[0])==12) {
					//AM
					lHour = 0;
					lMinute = parseInt(items[1]);
					lSecond = 0;
					fValid = true;
				}
				else {
					//AM
					lHour = parseInt(items[0]);
					lMinute = parseInt(items[1]);
					lSecond = 0;
					fValid = true;
				}
			}
			else {
				fValid = false;
			}
			
		}
		else if (items.length==2) {
			if (parseInt(items[0])!=items[0]) {
				fValid = false;
			}
			else if (parseInt(items[0])<0 || parseInt(items[0])>23) {
				fValid = false;
			}
			else if (parseInt(items[1])==items[1]) {
				if (parseInt(items[1])<0 || parseInt(items[1])>59) {
					fValid = false;
				}
				else {
					lHour = parseInt(items[0]);
					lMinute = parseInt(items[1]);
					lSecond = 0;
					fValid = true;
				}
			}
			else if (items[1] == "PM" || items[1] == "pm" || items[1] == "P" || items[1] == "p") {
				if (parseInt(items[0])<1 || parseInt(items[0])>12) {
					fValid = false;
				}
				else if (parseInt(items[0])==12) {
					//PM
					lHour = 12;
					lMinute = 0;
					lSecond = 0;
					fValid = true;
				}
				else {
					//PM
					lHour = (parseInt(items[0])+12);
					lMinute = 0;
					lSecond = 0;
					fValid = true;
				}
			}
			else if (items[1] == "AM" || items[1] == "am" || items[1] == "A" || items[1] == "a") {
				if (parseInt(items[0])<1 || parseInt(items[0])>12) {
					fValid = false;
				}
				else if (parseInt(items[0])==12) {
					//AM
					lHour = 0;
					lMinute = 0;
					lSecond = 0;
					fValid = true;
				}
				else {
					//AM
					lHour = parseInt(items[0]);
					lMinute = 0;
					lSecond = 0;
					fValid = true;
				}
			}
			else {
				fValid = false;
			}
			
		}
		else if (items.length==1) {
			if (parseInt(items[0])!=items[0]) {
				fValid = false;
			}
			else if (parseInt(items[0])<0 || parseInt(items[0])>23) {
				fValid = false;
			}
			else {
				lHour = parseInt(items[0]);
				lMinute = 0;
				lSecond = 0;
				fValid = true;
			}
		}
		else {
			fValid = false;
		}
		
		if (fValid) {
			TimeVal = lHour + ':';
			if (lMinute < 10) TimeVal += '0';
			TimeVal += lMinute + ':';
			if (lSecond < 10) TimeVal += '0';
			TimeVal += lSecond;
		}
		else if (sTimeVal == '')
			TimeVal = '';
		else
			TimeVal = undefined;
		
		this.value = TimeVal;
	}
	else
		this.value = sTimeVal;
}
Time.Delims = ", .-:";

Time.prototype._baseFormat = 'HH:nn:ss';
Time.prototype.format = 'hh:nn tt';
Time.prototype.valueOf = function() {return this.value;};

Time.prototype.getHours = function() {
	var sTime = this.value;
	var timeVal;
	var intCounter;
	var items = new Array();
	items[0] = "";
	for (intCounter = 0; intCounter < sTime.length; intCounter++) {
		if (Time.Delims.indexOf(sTime.charAt(intCounter)) > -1) {
			if (items[items.length - 1] != "") 
				items[items.length] = "";
		}
		else if (sTime.charAt(intCounter)=='0' && items[items.length - 1] == "") {
			//Strip leading zeros, unless 0
			if (intCounter + 1 < sTime.length) {
				if (Time.Delims.indexOf(sTime.charAt(intCounter+1)) > -1)
					items[items.length - 1]='0';
			}
			else if (intCounter + 1 == sTime.length) {
				items[items.length-1]='0';
			}
		}
		else
	 		items[items.length-1] += sTime.charAt(intCounter);
	}
	if (items.length == 3 && parseInt(items[0]) == parseInt(items[0])) {
		return parseInt(items[0]);
	}
	else {
		return undefined;
	}
}
Time.prototype.getMinutes = function() {
	var sTime = this.value;
	var timeVal;
	var intCounter;
	var items = new Array();
	items[0] = "";
	for (intCounter = 0; intCounter < sTime.length; intCounter++) {
		if (Time.Delims.indexOf(sTime.charAt(intCounter)) > -1) {
			if (items[items.length - 1] != "") 
				items[items.length] = "";
		}
		else if (sTime.charAt(intCounter)=='0' && items[items.length - 1] == "") {
			//Strip leading zeros, unless 0
			if (intCounter + 1 < sTime.length) {
				if (Time.Delims.indexOf(sTime.charAt(intCounter+1)) > -1)
					items[items.length - 1]='0';
			}
			else if (intCounter + 1 == sTime.length) {
				items[items.length-1]='0';
			}
		}
		else
	 		items[items.length-1] += sTime.charAt(intCounter);
	}
	if (items.length == 3 && parseInt(items[1]) == parseInt(items[1])) {
		return parseInt(items[1]);
	}
	else {
		return undefined;
	}
}
Time.prototype.getSeconds = function() {
	var sTime = this.value;
	var timeVal;
	var intCounter;
	var items = new Array();
	items[0] = "";
	for (intCounter = 0; intCounter < sTime.length; intCounter++) {
		if (Time.Delims.indexOf(sTime.charAt(intCounter)) > -1) {
			if (items[items.length - 1] != "") 
				items[items.length] = "";
		}
		else if (sTime.charAt(intCounter)=='0' && items[items.length - 1] == "") {
			//Strip leading zeros, unless 0
			if (intCounter + 1 < sTime.length) {
				if (Time.Delims.indexOf(sTime.charAt(intCounter+1)) > -1)
					items[items.length - 1]='0';
			}
			else if (intCounter + 1 == sTime.length) {
				items[items.length-1]='0';
			}
		}
		else
	 		items[items.length-1] += sTime.charAt(intCounter);
	}
	if (items.length == 3 && parseInt(items[2]) == parseInt(items[2])) {
		return parseInt(items[2]);
	}
	else {
		return undefined;
	}
}

Time.prototype.toString = function() {
	if (typeof(this.getHours()) == 'undefined' || typeof(this.getMinutes()) == 'undefined' || typeof(this.getSeconds()) == 'undefined') {
		return this.value;
	}
	else if (typeof(this.value) != 'undefined' && typeof(this.format) == 'string') {
		var sTime = this.format;
		var sRes = this.format;
		
		// Replace the hour code
		while (sTime.search(/(HH)|(hh)|(H)|(h)/) >= 0) {
			if (RegExp.$1 != '') {
				sRes = sRes.replace(/HH/, longHour24(this.getHours()));
				sTime = sTime.replace(/HH/, '');
			}
			else if (RegExp.$2 != '') {
				sRes = sRes.replace(/hh/, longHour12(this.getHours()));
				sTime = sTime.replace(/hh/, '');
			}
			else if (RegExp.$3 != '') {
				sRes = sRes.replace(/H/, shortHour24(this.getHours()));
				sTime = sTime.replace(/H/, '');
			}
			else if (RegExp.$4 != '') {
				sRes = sRes.replace(/h/, shortHour12(this.getHours()));
				sTime = sTime.replace(/h/, '');
			}
		}
		
		// Replace the minutes code
		while (sTime.search(/(nn)|(n)/i) >= 0) {
			if (RegExp.$1 != '') {
				sRes = sRes.replace(/nn/i, longMinute(this.getMinutes()));
				sTime = sTime.replace(/nn/i, '');
			}
			else if (RegExp.$2 != '') {
				sRes = sRes.replace(/n/i, shortMinute(this.getMinutes()));
				sTime = sTime.replace(/n/i, '');
			}
		}

		// Replace the seconds code
		while (sTime.search(/(ss)|(s)/i) >= 0) {
			if (RegExp.$1 != '') {
				sRes = sRes.replace(/ss/i, longSecond(this.getSeconds()));
				sTime = sTime.replace(/ss/i, '');
			}
			else if (RegExp.$2 != '') {
				sRes = sRes.replace(/s/i, shortSecond(this.getSeconds()));
				sTime = sTime.replace(/s/i, '');
			}
		}
		
		// Replace the AM/PM code
		if (sTime.search(/(TT)|(tt)|(T)|(T)/) >= 0) {
			if (RegExp.$1 != '') {
				sRes = sRes.replace(/TT/, longAPC(this.getHours()));
				sTime = sTime.replace(/TT/, '');
			}
			else if (RegExp.$2 != '') {
				sRes = sRes.replace(/tt/, longAPL(this.getHours()));
				sTime = sTime.replace(/tt/, '');
			}
			else if (RegExp.$3 != '') {
				sRes = sRes.replace(/T/, shortAPC(this.getHours()));
				sTime = sTime.replace(/T/, '');
			}
			else if (RegExp.$4 != '') {
				sRes = sRes.replace(/t/, shortAPL(this.getHours()));
				sTime = sTime.replace(/t/, '');
			}
		}
		
		// Return the code-replace string
		return sRes;
	}
	else
		return this.value;
}

function longMinute(iMinute) {
	if (iMinute < 10)
		return '0' + iMinute;
	else
		return iMinute;
}
function shortMinute(iMinute) {
	return iMinute;
}

function longSecond(iSecond) {
	if (iSecond < 10)
		return '0' + iSecond;
	else
		return iSecond;
}
function shortSecond(iSecond) {
	return iSecond;
}

function longHour24(iHour) {
	if (iHour < 10)
		return '0' + iHour;
	else
		return iHour;
}
function shortHour24(iHour) {
	return iHour;
}

function longHour12(iHour) {
	if (iHour == 0)
		return 12;
	else if (iHour < 10)
		return '0' + iHour;
	else if (iHour < 13)
		return iHour;
	else
		return (iHour - 12);
}
function shortHour12(iHour) {
	if (iHour == 0)
		return 12;
	else if (iHour < 13)
		return iHour;
	else
		return (iHour - 12);
}

function longAPC(iHour) {
	if (iHour < 12)
		return 'AM';
	else
		return 'PM';
}
function shortACP(iHour) {
	if (iHour < 12)
		return 'A';
	else
		return 'P';
}
function longAPL(iHour) {
	if (iHour < 12)
		return 'am';
	else
		return 'pm';
}
function shortACL(iHour) {
	if (iHour < 12)
		return 'a';
	else
		return 'p';
}
