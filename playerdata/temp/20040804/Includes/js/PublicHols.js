/*=============================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================*/
var intLastRunYear

function PublicHols(Y) {

arrPublicHols = new Array('1 January '+Y,'21 March '+Y,'27 April '+Y,'1 May '+Y,'16 June '+Y,'9 August '+Y,'24 September '+Y,'16 December '+Y,'25 December '+Y,'26 December '+Y)
arrMonths = new Array('January','February','March','April','May','June','July','August','September','October','November','December')
arrPHD = ['New Year','Human Rights Day','Good Friday','Family Day','Freedom Day','Workers Day','Youth Day','National Women�s Day','Heritage Day ','Day of Reconciliation','Christmas Day','Day of Goodwill']

//Loop through each public holiday, if it lands on a sunday move it to the next monday.
for (var i in arrPublicHols) {
	dateTemp = new Date(arrPublicHols[i])
	if (dateTemp.getDay()==0){
		dateTemp = new Date(Date.parse(dateTemp)+86400000)
		arrPublicHols[i] = dateTemp.getDate() + ' ' + arrMonths[dateTemp.getMonth()]+' '+dateTemp.getYear();
		//alert(arrPublicHols[i])	
	}	
	
}



//Calculate easter for this year and add 1 day on for Easter Mon and Subtract 2 day for Good Friday.
    var C = Math.floor(Y/100);
    var N = Y - 19*Math.floor(Y/19);
    var K = Math.floor((C - 17)/25);
    var I = C - Math.floor(C/4) - Math.floor((C - K)/3) + 19*N + 15;
    I = I - 30*Math.floor((I/30));
    I = I - Math.floor(I/28)*(1 - Math.floor(I/28)*Math.floor(29/(I + 1))*Math.floor((21 - N)/11));
    var J = Y + Math.floor(Y/4) + I + 2 - C + Math.floor(C/4);
    J = J - 7*Math.floor(J/7);
    var L = I - J;
    var M = 3 + Math.floor((L + 40)/44);
    var D = L + 28 - 31*Math.floor(M/4);

	parsed = Date.parse(arrMonths[M-1] + ' ' + D + ', ' + Y)
	
	dateEasterFri = new Date(parsed-(86400000*2))
	dateEasterMon = new Date(parsed+86400000)
	
	arrPublicHols[arrPublicHols.length] = dateEasterFri.getDate() + ' ' + arrMonths[dateEasterFri.getMonth()] + ' ' + Y
	arrPublicHols[arrPublicHols.length] = dateEasterMon.getDate() + ' ' + arrMonths[dateEasterMon.getMonth()] + ' ' + Y






//Re-order array into Chronological Order
	function compareDates(a, b){
		return Date.parse(a) - Date.parse(b)
	}
	arrPublicHols = String(arrPublicHols.sort(compareDates)).split(",")

	return arrPublicHols
}
