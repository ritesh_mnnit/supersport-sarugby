// JScript source code

	var data = ""
	var String = ""
	function WhereVis()						{if (chkWhere.checked==false){rowWhere.style.display='none';}else{rowWhere.style.display='block';}}
	function addSearch()					{var oNodeWhere = tblWhere.childNodes[0].childNodes[0].cloneNode(true);tblWhere.childNodes[0].insertBefore(oNodeWhere);}
	function remSearch()					{var tblRows = tblWhere.children[0].children.length-1;if (tblRows>0){tblWhere.childNodes[0].childNodes[tblRows].removeNode(true);}}
	function makestring(array)				{for(x=0;x<array.length;x++){if(array[x]!=''&&array[x]!==undefined){String += array[x] + "|"}}return String;}
	function help(pageType)				{window.open ("Devhelp.asp?pageType="+pageType,"","width=600,height=500,scrollbars=yes,menubar=no,resizable=yes")}
	function addCol(obj,des)				{if (obj.selectedIndex >= 0){var oCloneNode = obj.children[obj.selectedIndex].cloneNode(true);des.appendChild(oCloneNode);obj.children(obj.selectedIndex).removeNode(true);}}
	function removeCol(obj,des,rep)		{if (obj.selectedIndex >= 0){var oCloneNode = obj.children(obj.selectedIndex).cloneNode(true);if(rep!=false){des.insertBefore(oCloneNode);}obj.children(obj.selectedIndex).removeNode(true);}}

	var strWhere =""
	function sql(){
		if(chkWhere.checked){
			var WhereComma=""
			for (i = 0; i < tblWhere.children[0].children.length; i++){
				Opp = tblWhere.children[0].children[i].children[1].children[0].value
				switch (Opp) {
					case 'like' :strCritVal = "'%"+tblWhere.children[0].children[i].children[2].children[0].value+"%'";break;
					case 'not like' :strCritVal = "'%"+tblWhere.children[0].children[i].children[2].children[0].value+"%'";break;
					case 'beginning' :Opp = 'like';strCritVal = "'"+tblWhere.children[0].children[i].children[2].children[0].value+"%'";break;
					case 'not beginning' :Opp = 'not like';strCritVal = "'"+tblWhere.children[0].children[i].children[2].children[0].value+"%'";break;
					case 'between' :arrVals = (tblWhere.children[0].children[i].children[2].children[0].value).split(",");if(arrVals.length != 2){alert('Please supply only 2 values seperated by a comma.');return false;};strCritVal = "'"+arrVals[0]+"' and '"+arrVals[1]+"'";break;
					case 'not between' :arrVals = (tblWhere.children[0].children[i].children[2].children[0].value).split(",");if(arrVals.length != 2){alert('Please supply only 2 values seperated by a comma.');return false;};strCritVal = "'"+arrVals[0]+"' and '"+arrVals[1]+"'";break;
					case 'is null' :strCritVal = "";break;
					case 'is not null' :strCritVal = "";break;
					case 'in' :re = new RegExp(",","g"); strCritVal = "('"+tblWhere.children[0].children[i].children[2].children[0].value.replace(re,"','")+"')";if(strCritVal.indexOf(",") == -1){alert('Please supply multilple values seperated by commas.');return false;};break;
					case 'not in' :re = new RegExp(",","g") ; strCritVal = "('"+tblWhere.children[0].children[i].children[2].children[0].value.replace(re,"','")+"')";if(strCritVal.indexOf(",") == -1){alert('Please supply multilple values seperated by commas.');return false;};break;
					default :strCritVal = "'"+tblWhere.children[0].children[i].children[2].children[0].value+"'";
				} 
				WhereComma = WhereComma +"and " + tblWhere.children[0].children[i].children[0].children[0].value +" "+ Opp +" "+strCritVal+" "
			}
			strWhere = WhereComma.substr(3)
			updater.txtCansee.value = strWhere
		}else{
			updater.txtCansee.value=""
		}
		return true;
	}

	//Returns the row the object is in.
	function witchrow(Si){
		var row = 0
		for(var y=0;y<tblWhere.rows.length;y++){																		// loops through all rows
			if (tblWhere.rows[y].id!='rowEditList'){																			// if not an EditList row
				for(var x=0;x<tblWhere.rows[0].children.length;x++){													// loops through all cells in each row
					if(tblWhere.rows[y].cells[x].children[0].sourceIndex==Si)row = y ;							// checks the sourceindex against sourceindex Si passed to function
				}
			}
		}
		return row;
	}

	// changes the value box (third box) to either a drop down containing a list from the linked relavant table, or a text box.
	function ged(obj){
		row = witchrow(obj.sourceIndex)
		hide()																																									// hide the EditListbox
		tblWhere.rows[row].cells[3].children[0].style.display = "none"																						// makes the button visible
		// name of the options in the first box holds the droptable ( referenced table ) if there is one.
		var name = tblWhere.rows[row].cells[0].children[0].options[tblWhere.rows[row].cells[0].children[0].selectedIndex].name		// retrieve the field list.name value
		if(name!=''){																																						// if no value ( no referenced table )
																																												// sets evaluation criteria select list with a onchange event
			tblWhere.rows[row].cells[1].innerHTML = "<select class='pulldown' style='width:180px' name=selCritOpp onchange='inlist(this)'><option value='='>Is Equal To</option><option value='<>'>Is Not Equal To</option><option value='>'>Is Greater Than</option><option value='in'>In list</option><option value='not in'>Not in list</option></select>" 
			tblWhere.rows[row].cells[2].innerHTML = "<select class=txtboxes name=txtCritVal id=txtCritVal><select>"						// resets the value select list
			gd.table.value = name																																		// assigns values to gd form
			gd.row.value = row
			gd.submit()																																						// submits to repopulate the value select box
		}else{
			tblWhere.rows[row].cells[1].innerHTML = "<select class='pulldown' style='width:180px' name=selCritOpp><option value='='>Is Equal To</option><option value='<>'>Is Not Equal To</option><option value='>'>Is Greater Than</option><option value='<'>Is Less Than</option><option value='>='>Is Greater Than Or Equal To</option><option value='<='>Is Less Than Or Equal To</option><option value='like'>Contains</option><option value='not like'>Does not Contain</option><option value='in'>In list</option><option value='not in'>Not in list</option></select>" 
			tblWhere.rows[row].cells[2].innerHTML = "<input class=txtboxes name=txtCritVal id=txtCritVal>"									// if name == "" & value <> Reports to , then it's a text field
		}
	}

	// this function called if the second box's "in list" or "not in list" option is selected, and the first box has a droptable linked 
	var strListOptionValues = ""
	function inlist(obj){
		row = witchrow(obj.sourceIndex)																													// returns the row currently worked on
		if (tblWhere.rows[row].cells[0].children[0].options[tblWhere.rows[row].cells[0].children[0].selectedIndex].name!=""){	// if the selected field.name not = ""
			if(obj.options[obj.selectedIndex].text=="In list"||obj.options[obj.selectedIndex].text=="Not in list"){						// is selected evaluation criteria = "in list" / "Not in List"
				//this code will remove options that don't have the name attribute set to inlist from the dropdown list.
				for(var i=0;i<tblWhere.rows[row].cells[2].children[0].children.length;i++){														// for each option in search value select list
					if(tblWhere.rows[row].cells[2].children[0].children[i].name!="inlist"){															// is option.name = "inlist"
						tblWhere.rows[row].cells[2].children[0].children[i].removeNode(true)														// if not, remove node
						i=-1																																			// reset counter of loop
					}else{
						strListOptionValues = strListOptionValues + tblWhere.rows[row].cells[2].children[0].children[i].value + ","		// else add to comma del. string variable
					}
				}
				strListOptionValues = "," + strListOptionValues																							// adds the leading "," to the comma del. string variable
				tblWhere.rows[row].cells[3].children[0].style.display = "block"																		// makes the button visible
			}else if(tblWhere.rows[row].cells[3].children[0].style.display=="block"){																// checks if there is a button ( the select list would have also been altered then, and needs to be reverted. )
				hide()																																					// hides the EditList tables
				tblWhere.rows[row].cells[3].children[0].style.display = "none"																		// hides the button
				gd.table.value = tblWhere.rows[row].cells[0].children[0].options[tblWhere.rows[row].cells[0].children[0].selectedIndex].name		// assigns values to the gd form to repopulate the select list
				gd.row.value = row
				gd.destination.value = ''
				gd.submit()
			}
		}
	}

	// creates a row directly after the row which contains the pressed button
	function editList(obj){
		hide()																							// hides all EditList tables
		row = witchrow(obj.sourceIndex)													// gets which row is current
		tblWhere.rows[row].cells[3].children[0].disabled = true;						// disables the button
		tblWhere.rows[row].cells[3].children[0].style.cursor = "default"				// sets the cursor
		myNewRow = document.all.tblWhere.insertRow(row+1)						// inserts a row after current row
		myNewRow.id = 'rowEditList'															// sets inserted row's id
		myNewCell = myNewRow.insertCell()												// inserts a cell into the new row
		myNewCell.colSpan = "4"																// new cell to span all 4 cells of the table
		myNewCell.style.textAlign="center"													// style attribute
																										// inserts a table with some select boxes and buttons into the new cell
		myNewCell.innerHTML = "<table cellspacing='0' cellpadding='0' style='border-right:1px solid #AAA;border-bottom:1px solid #AAA;'><tr><td class=cellhere height=60px width=10px><table><tr><td class=headinghere2>Access</td><td rowspan='2'><table style='text-align:center;'><tr><td><input class='but' type=button value='>' title='Move selected items to the right' onclick='onClick(EditList1,EditList2)'></td></tr><tr><td><input class='but' type=button value='>>' title='Move all items to the right' onclick='allClick(EditList1,EditList2)'></td></tr><tr><td><input class='but' type=button value='<' title='Move selected items to the left' onclick='onClick(EditList2,EditList1)'></td></tr><tr><td><input class='but' type=button value='<<' title='Move all items to the left' onclick='allClick(EditList2,EditList1)'></td></tr></table></td><td class=headinghere2>No Access</td></tr><tr><td><select multiple class='pulldown' style='width:150px' name='EditList1' id='EditList1' size=7 onDblClick='addCol(this,EditList2);' onkeydown='if(event.keyCode==13){addCol(this,EditList2);};'></select></td><td><select multiple class='pulldown'  style='width:150px' name=EditList2 id=EditList2 size=7 ondblclick='addCol(this,EditList1);' onkeydown='if(event.keyCode==13){addCol(this,EditList1);};'></select></td><td><table><tr><td><input type=button class=but value='close' style='width:40px;height:20px;' onclick='hide()'></td></tr><tr><td><input type=button class=but value='submit' style='width:40px;height:20px;' onclick="+""+"submitEditList('"+row+"')"+""+"></td></tr></table></td></tr></table></td></table>"
																										// assigns values to the gd to populate select lists; especially note the destination.value = 'EditList'; this causes a different function to run, populating the inserted tables select lists, not the search values' select lists.
		gd.table.value = tblWhere.rows[row].cells[0].children[0].options[tblWhere.rows[row].cells[0].children[0].selectedIndex].name
		gd.row.value = row
		gd.destination.value = 'EditList'
		gd.submit()
	}

	var data = ""
	function PopulateEditList(list,rowID){
		// loop through the search values' options, creating a comma delimited string of the options' values
		for(var x=0;x<tblWhere.rows[rowID].cells[2].children[0].children.length;x++){
			strListOptionValues = strListOptionValues + tblWhere.rows[rowID].cells[2].children[0].children[x].value + ","
		}
		strListOptionValues = "," + strListOptionValues;								// insert a comma at the beginning of the string.
		alert(list)
		data = make2DArray(list);															// create the array from the list value passed to this function
		//Create Options
		for(var x=0;x<data.length-1;x++){
			if (strListOptionValues.search(","+data[x][0]+",")>=0) {				// if the value in the array is in the comma del. list, then 
				oOption = document.createElement("OPTION");						// add options to the first select box
				EditList1.appendChild(oOption)
				oOption.value = data[x][0]
				oOption.text = data[x][1]
			}else{
				oOption = document.createElement("OPTION");						// else
				EditList2.appendChild(oOption)											// add options to the second select box
				oOption.value = data[x][0]
				oOption.text = data[x][1]
			}
		}
		strListOptionValues = ""																// reset the comma del. list to ""
	}

	// default function run when the gd form submitted to Getdrop.asp ; populates the search value select list.
	function setdrops(list,rowID){
		for(var i=0;i<tblWhere.rows[rowID].cells[2].children[0].children.length;i++){		// loops through the search's value select list
			if(tblWhere.rows[rowID].cells[2].children[0].children[i].name!="inlist"){			// if the name not = 'inlist' ( a previously selected item from EditList )
				tblWhere.rows[rowID].cells[2].children[0].children[i].removeNode(true);		// removes node
				i=-1																								// and resets counter
			}else{																								// else
				strListOptionValues = strListOptionValues + tblWhere.rows[rowID].cells[2].children[0].children[i].value + ","		// adds option's value to comma del. list
			}
		}
		strListOptionValues = "," + strListOptionValues												// adds a "," to the beginning of the comma del. list
		data = make2DArray(list);																			// create the array from the list value passed to this function
		objSelect = tblWhere.rows[rowID].cells[2].children[0]										// assigns the search's value select list object to the variable objSelect
		//Create Options														
		oOption=document.createElement('OPTION');												// add a "Select Value" option to the dropdown list.
		oOption.text = "Select Value";									
		objSelect.add(oOption,0);										   
		objSelect.selectedIndex = 0;																		// Sets focus on the "Select Value" option
		for(var x=0;x<data.length-1;x++){																// loops through array
			if (strListOptionValues.search(","+data[x][0]+",")<0){									// searches to see whether data to be added is in comma del. list
				oOption = document.createElement("OPTION");										// if not, adds options for each one
				oOption.value	= data[x][0];
				oOption.text	= data[x][1];
				objSelect.add(oOption);
			}
		}
		strListOptionValues = ""																				// reset the comma del. list to ""
	}

	// Populates the list from the selected options when the EditList tables
	function submitEditList(row){
		objSelect = tblWhere.rows[row].cells[2].children[0]										// assigns the search's value select list object to the variable objSelect
		objSelect.innerHTML = ""																			// clears all options
		for(var y=0;y<EditList1.children.length;y++){												// loops through the EditList1 options
			oOption = document.createElement("OPTION");											// for each one, creates the options
			objSelect.appendChild(oOption)																// and appends
			oOption.value = EditList1.children[y].value												// setting value
			oOption.text = EditList1.children[y].text													// and text
			oOption.name = 'inlist'
		}
		hide()																										// hides all the EditList tables
	}

	// removes all other created editlists
	function hide(){
		for(var y=0;y<tblWhere.rows.length;y++){													// loops through all rows
			if (tblWhere.rows[y].id=='rowEditList'){													// checks the rows' id = 'rowEditList' ?
				tblWhere.rows[y-1].cells[3].children[0].disabled = false							// enables the button
				tblWhere.rows[y-1].cells[3].children[0].style.cursor = "hand"					// resets the button cursor to hand 
				tblWhere.rows[y].removeNode(true)													// removes the EditList node
			}
		}
	}

	function make2DArray(strArray){
		arrTemp = strArray.split("|");
		for (i in arrTemp){arrTemp[i] = arrTemp[i].split(",");}
		return arrTemp;
	}			

	function setdrops1(list,rowID){data = make2DArray(list);objSelect = tblWhere.rows[rowID].cells[2].children[0];objSelect.innerHTML ="";oOption = document.createElement("OPTION");objSelect.appendChild(oOption);oOption.value = "";oOption.text = "Select Value";for(var x=0;x<data.length -1;x++){oOption = document.createElement("OPTION");objSelect.appendChild(oOption);oOption.value = data[x][0];oOption.text = data[x][1]}}

	function submitCols(){
	 	var colsComma = ""
	 	for (i = 0; i < sel2.children.length; i++){colsComma = colsComma + ",'" +sel2.children[i].value + "'" ;}
	 	var colsComma2 = ""
	 	for (i = 0; i < sel3.children.length; i++){colsComma2 = colsComma2 + ",'" +sel3.children[i].value + "'" ;}
	 	strCols = colsComma.substr(1);
	 	updater.txtFunctions.value = strCols;
	 	strCols2 = colsComma2.substr(1);
	 	updater.txtSquads.value = strCols2;
	 	var strSystems = ""
		if (chkSystems1.checked){strSystems=strSystems+"1|";updater.txtDevUserType.value = selDevUserType.value}else{strSystems=strSystems+"0|"}
		if (chkSystems2.checked){strSystems=strSystems+"1|";updater.txtScoutUserType.value = selScoutUserType.value}else{strSystems=strSystems+"0|"}
		if (chkSystems3.checked){strSystems=strSystems+"1";updater.txtRepUserType.value = selRepUserType.value}else{strSystems=strSystems+"0"}
		updater.txtSystems.value = strSystems
	}

	function systems(obj){
		if (eval("lblUserType"+obj+".style.display")=="block"){eval("lblUserType"+obj+".style.display='none'");eval("selUserType"+obj+".style.display='none'");eval("divSpacer"+obj+".style.display='block'");eval("selSpacer"+obj+".style.display='block'");eval("help"+obj+".style.display='none'")}else{eval("lblUserType"+obj+".style.display='block'");eval("selUserType"+obj+".style.display='block'");eval("divSpacer"+obj+".style.display='none'");eval("selSpacer"+obj+".style.display='none'");eval("help"+obj+".style.display='block'")}
		if (obj == '3'){if (eval("lblUserType"+obj+".style.display")=="block"){tblRepresentative.style.display="block"}else{tblRepresentative.style.display="none"}}
	}