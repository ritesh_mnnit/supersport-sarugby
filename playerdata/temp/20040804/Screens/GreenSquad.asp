<%@ Language=VBScript.Encode %>
<%#@~^4gAAAA==@#@&nCT+,'PrMMn+	?5EmNi29lYEdiddi77didid7d77id7B,2lT+~-mDkC8^+@#@&hCT+Hm:P',JV.+x~?$EC[,jw9 Jiddi77didid7d77idvPgC:PGW,wlLn@#@&d+^#mV\/kloP{~J^kdY,W2ObWxkEd,PPi77B,jk+[PGU,Y4+,-l^k[CDkWU~K0~Y4n,/n^+1YP^kkO/,WUPkE8hbY@#@&CTwAAA==^#~@%>
<%#@~^FAAAAA==~alO4nM+0b6,xPrR zrPpwUAAA==^#~@%>
<!-- #include file="../includes/config/DB_Connectivity.asp" -->
<!-- #include file="../includes/config/data.asp" -->
<html>
<head>
	<link REL="stylesheet" HREF="../includes/css/qbuilder.css" type="text/css">
	<link REL=stylesheet HREF="../includes/css/general.css" type="text/css">
	<script LANGUAGE='javascript' SRC='../includes/js/jsValidator.js'></script>
	<script language="javascript">
		//this closes the search frame if that frame was open.
		if (parent.parent.document.all.sclick.rows == "63,*")
		{
			parent.parent.document.all.sclick.rows = "19,*";
			parent.parent.frames.Search.document.all.searchystuffy.style.display='none';	
			parent.parent.frames.Search.document.all.gb.src="../../images/down.gif";
			parent.parent.frames.Search.document.all.gb.alt = "Display Search Screen"
		}
		
		//Function to check that the squads have been updated and then submit the form
		function checkForm(strForm)
		{
			var msgSquads;
			var colsComma1;
			var strCols;

			var intFlag = 0;
			var intSubmitFlag = 0;

			msgSquads = "";

			if ((strForm.sel1GreenU16.length != 0) && (strForm.sel2GreenU16.length == 0) && (strForm.sel3GreenU16.length == 0))
			{
				msgSquads = msgSquads + "\n- Green U16";
				intFlag = 1;
			};

			if ((strForm.sel1GreenU17.length != 0) && (strForm.sel2GreenU17.length == 0) && (strForm.sel3GreenU17.length == 0))
			{
				msgSquads = msgSquads + "\n- Green U17";
				intFlag = 1;
			};

			if ((strForm.sel1GreenU18.length != 0) && (strForm.sel2GreenU18.length == 0) && (strForm.sel3GreenU18.length == 0))
			{
				msgSquads = msgSquads + "\n- Green U18";
				intFlag = 1;
			};

			if ((strForm.sel1GreenU19.length != 0) && (strForm.sel2GreenU19.length == 0) && (strForm.sel3GreenU19.length == 0))
			{
				msgSquads = msgSquads + "\n- Green U19";
				intFlag = 1;
			};

			if ((strForm.sel1GreenU20.length != 0) && (strForm.sel2GreenU20.length == 0))
			{
				msgSquads = msgSquads + "\n- Green U20";
				intFlag = 1;
			};

			if (intFlag == 1)
			{
				if (confirm("Are you sure you want to update the squads, you have not selected players to be moved for the following squads:\n" + msgSquads))
				{
					intSubmitFlag = 1;
				};
			}
			else
			{
				if (confirm("If you are sure all the data you have selected is correct, select OK to continue. If not, select CANCEL to return and check!"))
				{
					intSubmitFlag = 1;
				}
			};

			if (intSubmitFlag == 1)
			{
				colsComma1 = "";
				for (i = 0; i < strForm.sel1GreenU16.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel1GreenU16.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU161.value = strCols;
				
				colsComma1 = "";
				for (i = 0; i < strForm.sel2GreenU16.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel2GreenU16.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU162.value = strCols;
				
				colsComma1 = "";
				for (i = 0; i < strForm.sel3GreenU16.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel3GreenU16.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU163.value = strCols;

				colsComma1 = "";
				for (i = 0; i < strForm.sel1GreenU17.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel1GreenU17.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU171.value = strCols;
				
				colsComma1 = "";
				for (i = 0; i < strForm.sel2GreenU17.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel2GreenU17.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU172.value = strCols;

				colsComma1 = "";
				for (i = 0; i < strForm.sel3GreenU17.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel3GreenU17.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU173.value = strCols;

				colsComma1 = "";
				for (i = 0; i < strForm.sel1GreenU18.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel1GreenU18.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU181.value = strCols;
				
				colsComma1 = "";
				for (i = 0; i < strForm.sel2GreenU18.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel2GreenU18.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU182.value = strCols;

				colsComma1 = "";
				for (i = 0; i < strForm.sel3GreenU18.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel3GreenU18.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU183.value = strCols;

				colsComma1 = "";
				for (i = 0; i < strForm.sel1GreenU19.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel1GreenU19.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU191.value = strCols;
				
				colsComma1 = "";
				for (i = 0; i < strForm.sel2GreenU19.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel2GreenU19.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU192.value = strCols;

				colsComma1 = "";
				for (i = 0; i < strForm.sel3GreenU19.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel3GreenU19.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU193.value = strCols;

				colsComma1 = "";
				for (i = 0; i < strForm.sel1GreenU20.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel1GreenU20.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU201.value = strCols;
				
				colsComma1 = "";
				for (i = 0; i < strForm.sel2GreenU20.length; i++)
				{
	 				colsComma1 = colsComma1 + "," + strForm.sel2GreenU20.children[i].value;
				}
				strCols = colsComma1.substr(1);
				frmSaveData.txtGreenU202.value = strCols;

				frmSaveData.submit();
			};

			return false;
		};
		
		//Function to display the selected squad information when selected from Status div
		function setSel(strSquadSelected)
		{
			document.all.selSquad.value = strSquadSelected;
			dispSquad();
		};
		
		//Function to display the appropriate status of the upgrade in the status boxes
		function hideStatus()
		{
			if ((document.all.sel2GreenU16.length == 0) && (document.all.sel3GreenU16.length == 0))
			{
				if (document.all.sel1GreenU16.length == 0)
				{
					document.all.GreenU16P.style.display = "none";
					document.all.GreenU16N.style.display = "";
				}
				else
				{
					document.all.GreenU16P.style.display = "";
					document.all.GreenU16N.style.display = "none";
				};
				document.all.GreenU16C.style.display = "none";
				document.all.GreenU16I.style.display = "none";
			}
			else
			{
				document.all.GreenU16P.style.display = "none";
				document.all.GreenU16N.style.display = "none";
				document.all.GreenU16C.style.display = "";
				document.all.GreenU16I.style.display = "none";
			};
			
			if ((document.all.sel2GreenU17.length == 0) && (document.all.sel3GreenU17.length == 0))
			{
				if (document.all.sel1GreenU17.length == 0)
				{
					document.all.GreenU17P.style.display = "none";
					document.all.GreenU17N.style.display = "";
				}
				else
				{
					document.all.GreenU17P.style.display = "";
					document.all.GreenU17N.style.display = "none";
				};
				document.all.GreenU17C.style.display = "none";
				document.all.GreenU17I.style.display = "none";
			}
			else
			{
				document.all.GreenU17P.style.display = "none";
				document.all.GreenU17N.style.display = "none";
				document.all.GreenU17C.style.display = "";
				document.all.GreenU17I.style.display = "none";
			};
			
			if ((document.all.sel2GreenU18.length == 0) && (document.all.sel3GreenU18.length == 0))
			{
				if (document.all.sel1GreenU18.length == 0)
				{
					document.all.GreenU18P.style.display = "none";
					document.all.GreenU18N.style.display = "";
				}
				else
				{
					document.all.GreenU18P.style.display = "";
					document.all.GreenU18N.style.display = "none";
				};
				document.all.GreenU18C.style.display = "none";
				document.all.GreenU18I.style.display = "none";
			}
			else
			{
				document.all.GreenU18P.style.display = "none";
				document.all.GreenU18N.style.display = "none";
				document.all.GreenU18C.style.display = "";
				document.all.GreenU18I.style.display = "none";
			};
			
			if ((document.all.sel2GreenU19.length == 0) && (document.all.sel3GreenU19.length == 0))
			{
				if (document.all.sel1GreenU19.length == 0)
				{
					document.all.GreenU19P.style.display = "none";
					document.all.GreenU19N.style.display = "";
				}
				else
				{
					document.all.GreenU19P.style.display = "";
					document.all.GreenU19N.style.display = "none";
				};
				document.all.GreenU19C.style.display = "none";
				document.all.GreenU19I.style.display = "none";
			}
			else
			{
				document.all.GreenU19P.style.display = "none";
				document.all.GreenU19N.style.display = "none";
				document.all.GreenU19C.style.display = "";
				document.all.GreenU19I.style.display = "none";
			};

			if (document.all.sel2GreenU20.length == 0)
			{
				if (document.all.sel1GreenU20.length == 0)
				{
					document.all.GreenU20P.style.display = "none";
					document.all.GreenU20N.style.display = "";
				}
				else
				{
					document.all.GreenU20P.style.display = "";
					document.all.GreenU20N.style.display = "none";
				};
				document.all.GreenU20C.style.display = "none";
				document.all.GreenU20I.style.display = "none";
			}
			else
			{
				document.all.GreenU20P.style.display = "none";
				document.all.GreenU20N.style.display = "none";
				document.all.GreenU20C.style.display = "";
				document.all.GreenU20I.style.display = "none";
			};
		}

		//Function to display the selected squad information
		function dispSquad()
		{
			//First set everything to be hidden
			document.all.GreenU16.style.display = "none";
			document.all.GreenU17.style.display = "none";
			document.all.GreenU18.style.display = "none";
			document.all.GreenU19.style.display = "none";
			document.all.GreenU20.style.display = "none";
			document.all.GreenU16Drop.style.display = "none";
			document.all.GreenU17Drop.style.display = "none";
			document.all.GreenU18Drop.style.display = "none";
			document.all.GreenU19Drop.style.display = "none";
			
			//Now find out which squad was selected, and display the appropriate squad
			switch (document.all.selSquad.value)
			{
				case "GreenU16":
					document.all.GreenU16.style.display = "";
					document.all.GreenU16Drop.style.display = "";
					hideStatus()
					document.all.GreenU16P.style.display = "none";
					document.all.GreenU16C.style.display = "none";
					document.all.GreenU16N.style.display = "none";
					document.all.GreenU16I.style.display = "";
					break;
				case "GreenU17":
					document.all.GreenU17.style.display = "";
					document.all.GreenU17Drop.style.display = "";
					hideStatus()
					document.all.GreenU17P.style.display = "none";
					document.all.GreenU17C.style.display = "none";
					document.all.GreenU17N.style.display = "none";
					document.all.GreenU17I.style.display = "";
					break;
				case "GreenU18":
					document.all.GreenU18.style.display = "";
					document.all.GreenU18Drop.style.display = "";
					hideStatus()
					document.all.GreenU18P.style.display = "none";
					document.all.GreenU18C.style.display = "none";
					document.all.GreenU18N.style.display = "none";
					document.all.GreenU18I.style.display = "";
					break;
				case "GreenU19":
					document.all.GreenU19.style.display = "";
					document.all.GreenU19Drop.style.display = "";
					hideStatus()
					document.all.GreenU19P.style.display = "none";
					document.all.GreenU19C.style.display = "none";
					document.all.GreenU19N.style.display = "none";
					document.all.GreenU19I.style.display = "";
					break;
				case "GreenU20":
					document.all.GreenU20.style.display = "";
					hideStatus()
					document.all.GreenU20P.style.display = "none";
					document.all.GreenU20C.style.display = "none";
					document.all.GreenU20N.style.display = "none";
					document.all.GreenU20I.style.display = "";
					break;
			};
		};
		
		//Used to move objects from the one drop down to the other. By Double clicking
		function addCol(obj,des)
		{
			if (obj.selectedIndex >= 0)
			{
				var oCloneNode = obj.children[obj.selectedIndex].cloneNode(true);
				des.appendChild(oCloneNode);
				obj.children(obj.selectedIndex).removeNode(true);
			}
		}
		
		//Used to move objects from the one drop down to the other. By Double clicking
		function removeCol(obj,des,rep)
		{
			if (obj.selectedIndex >= 0)
			{
				var oCloneNode = obj.children(obj.selectedIndex).cloneNode(true);
				if(rep!=false)
				{
					des.insertBefore(oCloneNode);
				}
				obj.children(obj.selectedIndex).removeNode(true);
			}
		}
		
		//Used to show/hide rows
		function showRow(obj)
		{
			for(var i=1;i<3 ;i++)
			{
				eval("row"+i+"[0].style.display='block'");eval("row"+i+"[1].style.display='none'");eval("row"+i+"[2].style.display='none'")
			}
			eval("row"+obj+"[0].style.display='none'");eval("row"+obj+"[1].style.display='block'");eval("row"+obj+"[2].style.display='block'")
		}
		
		//Function used to check whether or not the year has been selected
		function checkYearForm(form)
		{
			if (form.strYear.value == "")
			{
				alert("Please select a year!");
				return false;
			}
		}
	</script>
</head>
<body background='../images/eebg.gif' topmargin='0' leftmargin='0' <%#@~^JAAAAA==~b0~M+$E+kYvE/DDe+mDEb,@!@*,EJ,YtU~hAoAAA==^#~@%> onload="dispSquad()" <%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>>

	<!--ASTHETICS-->
	<iframe width=168 height=202 name='gToday:normal:agenda.js' id='gToday:normal:agenda.js' src='../includes/js/ipopeng.htm' scrolling='no' frameborder='0' style='visibility:hidden; z-index:65535; position:absolute; top:0px; border:2px ridge;'></iframe>
	<table width="100%" bgcolor="#CcCcCc" border="0" cellpadding="0" cellspacing="0"><tr height="9"><td></td></tr><tr height="1" bgcolor="#eeeeee"><td></td></tr></table>
	<font face="Verdana, Arial, Helvetica, sans-serif" color="#333333" style="FONT-SIZE: 12px"><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff"><tr bgcolor="#dcdcdc"><td rowspan="2" width="181" height="20" valign="top" style="BORDER-BOTTOM: #c0c0c0 1px solid"><font face="Verdana, Arial, Helvetica, sans-serif" size="4" COLOR="#999999">&nbsp;<span name='spnPageHeader' id='spnPageHeader'><%=#@~^CgAAAA==~hlL1m:+,PgMAAA==^#~@%></span></font></td><td width="19">&nbsp;</td><td>&nbsp;</td></tr><tr><td height="20"><img src="../images/corner2.gif" WIDTH="19" HEIGHT="20"></td><td style="BORDER-TOP: #c0c0c0 1px solid; BACKGROUND-COLOR: #f7f7f7">&nbsp;</td></tr></table>
	<!--END ASTHETICS-->
	
<%#@~^IQAAAA==~b0~M+$E+kYvE/DDe+mDEb,'PrEPDt+	~RwoAAA==^#~@%>

	<%#@~^QQAAAA==@#@&d7k+DPDkKhwGlOl,'~/KxFc36mEDncJU+^+^Y~M,0.Ws~NMwenmDJb@#@&dgxIAAA==^#~@%>
	<form name="frmYearSelect" id="frmYearSelect" action="GreenSquad.asp" method="post" onsubmit="return checkYearForm(this);">
		<table style="position:absolute;left:40px;top:55px;">
			<tr>
				<td><div align="right">Select a year</div></td>
				<td>
					<select name="strYear" id="strYear" class="txtboxes" style="width:60px;">
						<option></option>
						<%#@~^DAEAAA==@#@&d7ididd9W,AtbVnP	WO~M/KhwGlYm nW6@#@&d7d77id7k6~DkKnhaflOCv!bP@!@*,J0rPDt+	@#@&7did7did7./wKU/RhMrO+,J@!W2YrG	P-l^;+{BE~LPDdP:2fmOm`EI+mDqGJ*~[,Jv@*rP'~M/KhwGlYmcE5lMJbP'~r@!&WaOkKx@*E@#@&d77id7din	N~b0@#@&didi7did./:+h2GlYm :K\+	naY@#@&id7d77iVGWa@#@&id77idhz4AAA==^#~@%>
					</select>
				</td>
				<td><input type="submit" value="Go" style="font-size:8pt"></td>
			</tr>
		</table>
	</form>
	<%#@~^IwAAAA==@#@&d7k+DPDkKhwGlOl,'~UKYtbUo@#@&dHgkAAA==^#~@%>

	
<%#@~^BgAAAA==~VdP6QEAAA==^#~@%>

	<table style="position:absolute;left:40px;top:55px;">
		<tr>
			<td><div align="right">Select a squad</div></td>
			<td>
				<select name="selSquad" id="selSquad" class="txtboxes" onchange="dispSquad()" style="width:100px;">
					<option value="GreenU16">Green U16</option>
					<option value="GreenU17">Green U17</option>
					<option value="GreenU18">Green U18</option>
					<option value="GreenU19">Green U19</option>
					<option value="GreenU20">Green U20</option>
				</select>
			</td>
		</tr>
	</table>

<%#@~^4QIAAA==@#@&dvwE	mYbW	~YKPL+DPO4P/$;l9Pk9vd@#@&d6EUmOrKx~0	j;!l[(G`/O.U;;l9b@#@&7i?YPM/:n:afCYmPx~1Wx8 +X+m!On`r?VnmO~U;;l9(f,0.GsPN.2U;;l9d,h4DP?$Em[P{PvJ,[~dDD?$;l9P[,EvJ*@#@&d7drW,DdKhwGlOCc+WW~{POD!n,Y4x@#@&didi.+kwGxk+ AMkY~JwbKzJ~2"I}I~C)j,r/Z`]2G~~Kd2bj3,Z61:);K~V2g2?I?,jjhn6I:"ZZr@#@&i7diD+k2Gxk+c+UN@#@&id7+^d+@#@&77id0Uj$ECN&9,'~M/:+:afmOlv!b@#@&d77xN,r0@#@&dijnY,DkKn:29mYCP{~xKY4r	o@#@&7x[P6;	mObW	@#@&@#@&idYM5nlMPx~M+;!n/D`JkO.5lMJb@#@&7@#@&7BUnY!w~C,/Y.r	o~YK~9kdaVmXPDt~Xl.Pk+sn1Y+9@#@&i/YMenlMfb/2Px~rJ@#@&id+DP.d:+:29mYCP{~;WU8RA6+1EDn`r/nVmO~]5+m.T,0DKh~NMwI+CD~A4+.+,e+mD(9,'PvE,[~/D.I+CMPLPJEJ*@#@&idr0,DdP:wGCYmR+KW~',0mVd+~O4+U@#@&7di/O.I+l.9b/2P{~M/P:aflDlvT#@#@&7dx[~b0@#@&7/YPMdP+swGlOl~x,xGY4rxT@#@&ctIAAA==^#~@%>	
	
<form name="updater" id="updater" method="post" action="" onsubmit="return checkForm(this);">
<%#@~^zgAAAA==@#@&dvG+1VlMk	LPmx[Pk+OObxo,;w,\lMrC4^+kPWW.~9kdw^CXbxL~Dt+~[b0W+Mn	Y~k;!lNk@#@&7fb:~?$EC[gl:SP	+6Dj5EmNglh+@#@&i@#@&dE9kkwsCHkxL~6k./D~k;;mN@#@&dU;!CNglh+,'~EVD+UP`Fvr@#@&d	+XYj;;C91C:~',J!.+x~i8GE@#@&WD4AAA==^#~@%>
<!--  #include file="../Includes/asp/gsuHeader.asp" -->

<%#@~^XQAAAA==@#@&dvGkkwVmXbUo,/nmKx[~k;Em[@#@&d?$;CNgls+~'~EVDn+	~j8GE@#@&dxnaD?5Em[glhP{PJVDnx,jq%r@#@&aBkAAA==^#~@%>
<!--  #include file="../Includes/asp/gsuHeader.asp" -->

<%#@~^XQAAAA==@#@&dvGkkwVmXbUo,/nmKx[~k;Em[@#@&d?$;CNgls+~'~EVDn+	~j8%E@#@&dxnaD?5Em[glhP{PJVDnx,jq,r@#@&ahkAAA==^#~@%>
<!--  #include file="../Includes/asp/gsuHeader.asp" -->

<%#@~^XQAAAA==@#@&dvGkkwVmXbUo,/nmKx[~k;Em[@#@&d?$;CNgls+~'~EVDn+	~j8,E@#@&dxnaD?5Em[glhP{PJVDnx,j+!r@#@&YxkAAA==^#~@%>
<!--  #include file="../Includes/asp/gsuHeader.asp" -->

<%#@~^WwAAAA==@#@&dvGkkwVmXbUo,/nmKx[~k;Em[@#@&d?$;CNgls+~'~EVDn+	~jy!E@#@&dxnaD?5Em[glhP{PJGDK2wNE@#@&YRkAAA==^#~@%>
<!--  #include file="../Includes/asp/gsuHeader.asp" -->

<div id="divUpdateStatus" name="divUpdateStatus" style="position:absolute; top:90px; left:470px; width:190px; text-align:center;">
	<table class="tblhere" cellpadding="3" cellspacing="0" bordercolor="#999999" width="185px">
		<tr>
			<td class="headinghere" colspan="2">Update Status</td>
		</tr>
		<tr>
			<td class="headinghere" style="width:100px;">Squad</td>
			<td class="headinghere" style="width:82px;">Status</td>
		</tr>
		<tr>
			<td class="cellhere" style="cursor:hand;" onclick="setSel('GreenU16');">Green U16</td>
			<td class="cellhere" style="color:red; display:" id="GreenU16P" name="GreenU16P">Pending</td>
			<td class="cellhere" style="color:green; display:none" id="GreenU16C" name="GreenU16C">Complete</td>
			<td class="cellhere" style="color:black; display:none" id="GreenU16N" name="GreenU16N">No Players</td>
			<td class="cellhere" style="color:blue; display:none" id="GreenU16I" name="GreenU16I">In Progress</td>
		</tr>
		<tr>
			<td class="cellhere" style="cursor:hand;" onclick="setSel('GreenU17');">Green U17</td>
			<td class="cellhere" style="color:red; display:" id="GreenU17P" name="GreenU17P">Pending</td>
			<td class="cellhere" style="color:green; display:none" id="GreenU17C" name="GreenU17C">Complete</td>
			<td class="cellhere" style="color:black; display:none" id="GreenU17N" name="GreenU17N">No Players</td>
			<td class="cellhere" style="color:blue; display:none" id="GreenU17I" name="GreenU17I">In Progress</td>
		</tr>
		<tr>
			<td class="cellhere" style="cursor:hand;" onclick="setSel('GreenU18');">Green U18</td>
			<td class="cellhere" style="color:red; display:" id="GreenU18P" name="GreenU18P">Pending</td>
			<td class="cellhere" style="color:green; display:none" id="GreenU18C" name="GreenU18C">Complete</td>
			<td class="cellhere" style="color:black; display:none" id="GreenU18N" name="GreenU18N">No Players</td>
			<td class="cellhere" style="color:blue; display:none" id="GreenU18I" name="GreenU18I">In Progress</td>
		</tr>
		<tr>
			<td class="cellhere" style="cursor:hand;" onclick="setSel('GreenU19');">Green U19</td>
			<td class="cellhere" style="color:red; display:" id="GreenU19P" name="GreenU19P">Pending</td>
			<td class="cellhere" style="color:green; display:none" id="GreenU19C" name="GreenU19C">Complete</td>
			<td class="cellhere" style="color:black; display:none" id="GreenU19N" name="GreenU19N">No Players</td>
			<td class="cellhere" style="color:blue; display:none" id="GreenU19I" name="GreenU19I">In Progress</td>
		</tr>
		<tr>
			<td class="cellhere" style="cursor:hand;" onclick="setSel('GreenU20');">Green U20</td>
			<td class="cellhere" style="color:red; display:" id="GreenU20P" name="GreenU20P">Pending</td>
			<td class="cellhere" style="color:green; display:none" id="GreenU20C" name="GreenU20C">Complete</td>
			<td class="cellhere" style="color:black; display:none" id="GreenU20N" name="GreenU20N">No Players</td>
			<td class="cellhere" style="color:blue; display:none" id="GreenU20I" name="GreenU20I">In Progress</td>
		</tr>
		<tr>
			<td colspan="2" align="center" class="headinghere"><input class="txtboxes" type="submit" value="Update Squads"></td>
		</tr>
	</table>
</div>

</form>

<form name="frmSaveData" id="frmSaveData" style="display:none" action="../includes/asp/gsuSave.asp" method="post">
	<input type="hidden" name="txtGreenU161" id="txtGreenU161">
	<input type="hidden" name="txtGreenU162" id="txtGreenU162">
	<input type="hidden" name="txtGreenU163" id="txtGreenU163">
	<input type="hidden" name="txtGreenU171" id="txtGreenU171">
	<input type="hidden" name="txtGreenU172" id="txtGreenU172">
	<input type="hidden" name="txtGreenU173" id="txtGreenU173">
	<input type="hidden" name="txtGreenU181" id="txtGreenU181">
	<input type="hidden" name="txtGreenU182" id="txtGreenU182">
	<input type="hidden" name="txtGreenU183" id="txtGreenU183">
	<input type="hidden" name="txtGreenU191" id="txtGreenU191">
	<input type="hidden" name="txtGreenU192" id="txtGreenU192">
	<input type="hidden" name="txtGreenU193" id="txtGreenU193">
	<input type="hidden" name="txtGreenU201" id="txtGreenU201">
	<input type="hidden" name="txtGreenU202" id="txtGreenU202">
	<input type="hidden" name="txtYear" id="txtYear" value="<%=#@~^CQAAAA==~kY.I+mDPKgMAAA==^#~@%>">
</form>

<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>


<!--  #include file="../Includes/asp/gsuFooter.asp" -->

