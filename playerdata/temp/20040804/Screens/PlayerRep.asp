<%@ Language=VBScript.Encode %>


<!--'===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<%
FolderName		=	"Screens/"
pathPrefix		=	"../"
Employeeid		=	"GenID"
BigOrderBy		=	"Surname"
UID				=	"GenID"
SmallOrderBy	=	"GenID"
table			=	"EmployeeData"
CentricTable	=	"EmployeeData"
page			=	"PlayerRep.asp"
'if Session("SchoolUser") = "" then
	'ValFields			=	"selSquad1,Squad|txtFirstName,FirstName|txtSurname,Surname|txtDOB,DateofBirth"
'else
	ValFields			=	"txtFirstName,FirstName|txtSurname,Surname|txtDOB,DateofBirth"
'end if
%>
<!-- #include file="..\includes\asp\nextprevtop.asp" -->
<%
	'With NavType, 0 means the navigation is off, 1 means the navigation is on
	if session("NavType") = "1" and request("nnFunc") = "" then
	%>
		<script Language="JavaScript">
			newWin = window.open('newnav.asp?openhlp=<%= request("openhlp") %>','newWin','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=750,height=550');
			newWin.focus()
			parent.frames.Search.hideSearchHelp()
			parent.frames.Search.close();
		</script>
		
	<%
	end if
	%>
			<script language="javascript">
				function setIns()
				{
	<%
	select case rs1("PlayingLevel")
		case "1"
			'It's a school
			%>
					document.all.insSchool.style.display = "";
					document.all.selSchool.value = "<%= rs1("Institution") %>";
			<%
		case "2"
			'It's tertiary
			%>
					document.all.insTertiary.style.display = "";
					document.all.selTertiary.value = "<%= rs1("Institution") %>";
			<%
		case "3"
			'It's a club
			%>
					document.all.insClub.style.display = "";
					document.all.selClub.value = "<%= rs1("Institution") %>";
			<%
	end select
%>
				};
				
				function dispIns()
				{
					switch (document.all.selPlayingLevel.value)
					{
						case "1":
							document.all.insClub.style.display = "none";
							document.all.insTertiary.style.display = "none";
							document.all.insSchool.style.display = "";
							document.all.custddClub.style.display = "none";
							document.all.custddTertiary.style.display = "none";
							document.all.custddSchool.style.display = "";
							document.all.selSchool.value = document.all.txtInstitution.value;
							document.all.selClub.value = document.all.txtInstitution.value;
							document.all.selTertiary.value = document.all.txtInstitution.value;
							break;
						case "2":
							document.all.insClub.style.display = "none";
							document.all.insSchool.style.display = "none";
							document.all.insTertiary.style.display = "";
							document.all.custddClub.style.display = "none";
							document.all.custddSchool.style.display = "none";
							document.all.custddTertiary.style.display = "";
							document.all.selSchool.value = document.all.txtInstitution.value;
							document.all.selClub.value = document.all.txtInstitution.value;
							document.all.selTertiary.value = document.all.txtInstitution.value;
							break;
						case "3":
							document.all.insSchool.style.display = "none";
							document.all.insTertiary.style.display = "none";
							document.all.insClub.style.display = "";
							document.all.custddSchool.style.display = "none";
							document.all.custddTertiary.style.display = "none";
							document.all.custddClub.style.display = "";
							document.all.selSchool.value = document.all.txtInstitution.value;
							document.all.selClub.value = document.all.txtInstitution.value;
							document.all.selTertiary.value = document.all.txtInstitution.value;
							break;
						default:
							document.all.custddSchool.style.display = "none";
							document.all.custddTertiary.style.display = "none";
							document.all.custddClub.style.display = "none";
							document.all.selSchool.value = "";
							document.all.selClub.value = "";
							document.all.selTertiary.value = "";
					};
				};
				
				//Function to take the value returned from the drpsearch page and populate the appropriate
				//text boxes with info
				function drpAssign(strId, strField)
				{
					document.all.txtInstitution.value = strId;
					switch (document.all.selPlayingLevel.value)
					{
						case "1":
							document.all.selSchool.value = strId;
							break;
						case "2":
							document.all.selTertiary.value = strId;
							break;
						case "3":
							document.all.selClub.value = strId;
							break;
					};
				};
				
				//Function to add new values to institution drop downs
				function drpAdd(strValue, strID, strType)
				{
					var oOption = document.createElement("OPTION");
					switch (strType)
					{
						case "1":
							document.all.selSchool.options.add(oOption);
							break;
						case "2":
							document.all.selTertiary.options.add(oOption);
							break;
						case "3":
							document.all.selClub.options.add(oOption);
							break;
						case "4":
						
							<% for i = 1 to 9 %>
							var oOption<%= i %> = document.createElement("OPTION");
							document.all.selSquad<%= i %>.options.add(oOption<%= i %>);
							oOption<%= i %>.innerText = strValue;
							oOption<%= i %>.value = strID;
							<% next %>

							document.all.selSquad10.options.add(oOption);
							break;
					}
					oOption.innerText = strValue;
					oOption.value = strID;
				};

				//Function to changes values in institution drop downs
				function drpChange(strValue, strID, strType)
				{
					switch (strType)
					{
						case "1":
							for (i = 0; i < document.all.selSchool.options.length; i++)
							{
								if (document.all.selSchool.options[i].value == strID)
								{
									document.all.selSchool.options[i].removeNode(true);
									i = document.all.selSchool.options.length;
								};
							};
							break;
						case "2":
							for (i = 0; i < document.all.selTertiary.options.length; i++)
							{
								if (document.all.selTertiary.options[i].value == strID)
								{
									document.all.selTertiary.options[i].removeNode(true);
									i = document.all.selTertiary.options.length;
								};
							};
							break;
						case "3":
							for (i = 0; i < document.all.selClub.options.length; i++)
							{
								if (document.all.selClub.options[i].value == strID)
								{
									document.all.selClub.options[i].removeNode(true);
									i = document.all.selClub.options.length;
								};
							};
							break;
						case "4":
							var arrSelected = ""
							var intStFlag = 0
							<% for i = 1 to 10 %>
							for (i = 0; i < document.all.selSquad<%= i %>.options.length; i++)
							{
								if (document.all.selSquad<%= i %>.options[i].value == strID)
								{
									if (document.all.selSquad<%= i %>.options[document.all.selSquad<%= i %>.selectedIndex].value == strID)
									{
										if (intStFlag == 0)
										{
											arrSelected = arrSelected + "<%= i %>," + strID
											intStFlag = 1;
										}
										else
										{
											arrSelected = arrSelected + ",<%= i %>," + strID
										};
										//alert(arrSelected);
									};
									document.all.selSquad<%= i %>.options[i].removeNode(true);
									i = document.all.selSquad<%= i %>.options.length;
									//Sub.Dropvalues.
								};
							};
							<% next %>
							break;
					};
					drpAdd(strValue, strID, strType);
					//alert(arrSelected);
					if (arrSelected)
					{
						drpSelect(arrSelected);
					};
				};
				
				//Function to set the squads drop downs back to the squads they were set on after an update
				function drpSelect(arrSelected)
				{
					strSelSquads = arrSelected.split(",");
					for (i=0;i<strSelSquads.length;i++)
					{
						eval("document.all.selSquad"+strSelSquads[i]+".value = " + strSelSquads[i+1] + ";")
						i = i + 1;
					};
				};
				
				function squadDrop(strID)
				{
					<% for i = 1 to 10 %>
					for (i = 0; i < document.all.selSquad<%= i %>.options.length; i++)
					{
						if (document.all.selSquad<%= i %>.options[i].value == strID)
						{
							document.all.selSquad<%= i %>.options[i].removeNode(true);
							i = document.all.selSquad<%= i %>.options.length;
						};
					};
					<% next %>
				}

			</script>

<!--Tabs-->
<td id='tabs' align='center' style='font-size: 11px' onclick="fnTabClick('0');"><a class='clsTabLink' href='javascript:void();'onclick= " this.blur(); return fnTabClick('0');">&nbsp;&nbsp;Contact&nbsp;&nbsp;</a></td>
<td id='tabs' align='center' style='font-size: 11px; <% if Session("SchoolUser") <> "" then %> display:none; <% end if %>' onclick="fnTabClick('1');"><a class='clsTabLink' href='javascript:void();'onclick= " this.blur(); return fnTabClick('1');">&nbsp;&nbsp;Squads&nbsp;&nbsp;</a></td>
	<td style="width:100%" style='background-color: #F7F7F7;border-top: 1px solid #C0C0C0'>&nbsp;</td></tr></table>
</tr></table>

<div id="addhelpdiv01" style="display:none;position:absolute;top:90px;left:310px;background-color:#FF3300;border: 1px solid black;color:white;width:70px">
	<img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"> 1. Start here and fill in all the relevant details. You can use the TAB key or your mouse to move to the next field.
</div>

<div id="addhelpdiv02" style="display:none;position:absolute;top:30px;left:330px;background-color:#FF3300;border: 1px solid black;color:white;width:320px">
	<img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"> 2. Before you save, don't forget to click on the squads tab and select the squads the player belongs to.
</div>

<div id="addhelpdiv03" style="display:none;position:absolute;top:374px;left:125px;background-color:#FF3300;border: 1px solid black;color:white;width:150px;">
	<table width="255px">
		<tr>
			<td valign="top" align="center"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px"></td>
		</tr>
		<tr>
			<td valign="top" style="color:white"><% if Session("SchoolUser") = "" or isNull(Session("SchoolUser")) then response.write "3." else response.write "2." end if %> Once you are done, and are ready to save, click the green tick.</td>
		</tr>
	</table>
</div>

<div id="addhelpdiv04" style="display:none;position:absolute;top:325px;left:0px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td align="right" style="color:white">To <strong>CANCEL</strong> saving, simply <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a> to return to the Navigation Wizard.</td>
			<td align="right" width="14px" valign="top"><br><br><img src="../images/Navigation/arrr.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
	</table>
</div>

<div id="search2helpdiv01" style="display:none;position:absolute;top:340px;left:0px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td align="right" style="color:white">2. You can now scroll through your search results, modify, delete or view a summary of all your search results. If you need help with this, <a href="#" onclick="window.open('../includes/asp/help/scrolling.asp?helptype=single','','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=650,height=500')" style="color:white; text-decoration:underline">click here.</a></td>
			<td align="right" width="14px" valign="top"><br><img src="../images/Navigation/arrr.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
	</table>
</div>

<div id="search2helpdiv02" style="display:none;position:absolute;top:394px;left:145px;background-color:#FF3300;border: 1px solid black;color:white;width:150px;">
	3. To return back and be able to view all the players again, you can click on the Player Details icon on the left hand side menu under the Players tab. If you cannot find it, <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a> to return to the Navigation Wizard.
</div>

<div id="search2helpdiv03" style="display:none;position:absolute;top:0px;right:120px;background-color:#FF3300;border: 1px solid black;color:white;width:250px">
	<table>
		<tr>
			<td style="color:white" align="right">This shows the available player records.</td>
			<td align="right" width="14px"><img src="../images/Navigation/arrr.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
	</table>
</div>

<form name=updater id=updater method=post action=''>
<table  width=290 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:17px;top:78px" ID="Table1">
	<input type='hidden' id='hidSQL1' name='hidSQL1' value="<%=where%>">
	<input type='hidden' id='txtGenID' name='txtGenID' disabled value='<%=rs1("GenID")%>'>
	<input type='hidden' id='txtLinkID' name='txtLinkID' disabled value='<%=rs1("LinkID")%>'>
	<input type='hidden' id='hidLatestInsert' name='hidLatestInsert' value="<%=rs1("LatestInsert")%>">
	<input id='txtemployeeno' name='txtemployeeno' disabled type='hidden' value='<%=rs1("EmployeeNo")%>'><%
	Response.write fieldsec("","Surname","inputbox",1,"","","Surname")
	Response.write fieldsec("","FirstName","inputbox",1,"","","First&nbsp;Name")
	Response.write fieldsec("","MiddleNames","inputbox",1,"","","Middle&nbsp;Names")
	Response.write fieldsec("","KnownAs","inputbox",1,"","","Known&nbsp;As")
	Response.write fieldsec("","Gender","dropdown",1,"","","Gender")
	Response.write fieldsec("","EthnicOrigin","dropdown",1,"","","Ethnic&nbsp;Origin")
	Response.write fieldsec("","DOB","datetype",1,"","date","Date&nbsp;Of&nbsp;Birth")
	Response.write fieldsec("","RugbyUnion","dropdown",1,"","","Rugby&nbsp;Union")
	if Session("SchoolUser") = "" then
		Response.write fieldsec("","PlayingLevel","dropdown",1,"onchange='dispIns()';","","<b>Institution:</b>&nbsp;&nbsp;&nbsp;&nbsp;<i>Level</i>")
	else
		Response.write fieldsec("","PlayingLevel","dropdown",1,"disabled","","<b>Institution:</b>&nbsp;&nbsp;&nbsp;&nbsp;<i>Level</i>")
	end if
	%>
		<input type="hidden" name="txtInstitution" id="txtInstitution" value="<%= rs1("Institution") %>">
	<%
	Response.write fieldsec("","School","dropdownIns",0,"","","<i>Name</i>")
	Response.write fieldsec("","Tertiary","dropdownIns",1,"","","<i>Name</i>")
	Response.write fieldsec("","Club","dropdownIns",1,"","","<i>Name</i>")
	Response.write fieldsec("","AgeGroup","dropdown",1,"disabled","","Level/Age&nbsp;Group")
	Response.write fieldsec("","Position","dropdown",1,"","","Normal&nbsp;Position")
%></table><table id=subDet width=270 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:350px;top:62px;display:block">
	    <tr><td>&nbsp;</td><td>&nbsp;</td><td style="position:relative;top:2;"><div>Area code&nbsp;&nbsp;&nbsp;&nbsp;Number</div></td></tr>
		<tr>
			<td><div align='right' title="Please type in the player's home telephone number">Home Phone</div></td>
	        <td>&nbsp;</td>
	        <td>
			<table border=0  cellspacing=0 cellpadding=0 style="position:relative;left:0;">
				<tr>
				<td align=right><input name="txtHomePhoneArea" onblur="check(this,'number')" class=shorttxtboxes value="<%=rs1("HomePhoneArea")%>" ID="txtHomePhoneArea" maxlength="10"></td>
				<td>&nbsp;</td>
				<td><input name="txtHomePhone" onblur="check(this,'number')" class=mediumtxtboxes value="<%=rs1("HomePhone")%>" ID="txtHomePhone" maxlength="20"></td>
				<td>&nbsp;</td>
				</tr>
			</table>
			</td>
	    </tr>
	    <tr>
			<td><div align='right' title="Please type in the player's work telephone number">Work Phone</div></td>
	        <td>&nbsp;</td>
	        <td>
			<table border=0  cellspacing=0 cellpadding=0 style="position:relative;left:0;">
				<tr>
				<td align=right><input name="txtWorkPhoneArea" onblur="check(this,'number')" class=shorttxtboxes value="<%=rs1("WorkPhoneArea")%>" ID="txtWorkPhoneArea" maxlength="10"></td>
				<td>&nbsp;</td>
				<td><input name="txtWorkPhone" onblur="check(this,'number')" class=mediumtxtboxes value="<%=rs1("WorkPhone")%>" ID="txtWorkPhone" maxlength="20"></td>
				<td>&nbsp;</td>
				</tr>
			</table>
			</td>
	    </tr>
	    <%
	Response.write fieldsec("","CellPhone","inputbox",1," maxlength=""20""","number","Cell&nbsp;Phone")
	Response.write fieldsec("","AddressType","dropdown",1,"","","Address&nbsp;Type")
	Response.write fieldsec("","Address1","inputbox",1,"","","Address")
	Response.write fieldsec("","Address2","inputbox",1,"","","")
	Response.write fieldsec("","Address3","inputbox",1,"","","")
	Response.write fieldsec("","Address4","inputbox",1,"","","")
	Response.write fieldsec("","Province","dropdown",1,"","","Province")
	Response.write fieldsec("","PostCode","inputbox",1," maxlength=""10""","number","Post&nbsp;Code")
	Response.write fieldsec("","Email","inputbox",1," maxlength=""50""","email","Email&nbsp;Address")
	Response.write fieldsec("","IDNumber","inputbox",1,"","id","SA&nbsp;ID&nbsp;Number")
	Response.write fieldsec("","PassportNo","inputbox",1,"","","<b>Passport:</b>&nbsp;&nbsp;&nbsp;&nbsp;<i>Number</i>")
	Response.write fieldsec("","PassportIssued","inputbox",1,"","","<i>Country Issued</i>")

%></table>

<%
	if Session("SchoolUser") = "" then
%>
<table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:356px;top:78px;display:block">
	<tr>
		<td>
			<div align='right'>Member<br>of</div>
		</td>
		<td width='5'>&nbsp;</td>
		<td width='160'>
			<select name='selSquad1' class=txtboxes onchange='c(this)' style='width:160px;'>
				<%= getOptions("Squad1","1") %>
			</select>
		</td>
		<td width='50'>
			<select name='selYear1' class=txtboxes onchange='c(this)' style='width:50px;'>
				<%= getOptions("Year1","1") %>
			</select>
		</td>
		<td id='custdd' style='display:block'><Input value='::' type=Button class=but onMouseOver='butover(this)' onMouseOut='butout(this)' onclick="ddl('drpSquads')"></td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='160'>
			<select name='selSquad2' class=txtboxes onchange='c(this)' style='width:160px;'>
				<%= getOptions("Squad2","1") %>
			</select>
		</td>
		<td width='50'>
			<select name='selYear2' class=txtboxes onchange='c(this)' style='width:50px;'>
				<%= getOptions("Year2","1") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='160'>
			<select name='selSquad3' class=txtboxes onchange='c(this)' style='width:160px;'>
				<%= getOptions("Squad3","1") %>
			</select>
		</td>
		<td width='50'>
			<select name='selYear3' class=txtboxes onchange='c(this)' style='width:50px;'>
				<%= getOptions("Year3","1") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='160'>
			<select name='selSquad4' class=txtboxes onchange='c(this)' style='width:160px;'>
				<%= getOptions("Squad4","1") %>
			</select>
		</td>
		<td width='50'>
			<select name='selYear4' class=txtboxes onchange='c(this)' style='width:50px;'>
				<%= getOptions("Year4","1") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='160'>
			<select name='selSquad5' class=txtboxes onchange='c(this)' style='width:160px;'>
				<%= getOptions("Squad5","1") %>
			</select>
		</td>
		<td width='50'>
			<select name='selYear5' class=txtboxes onchange='c(this)' style='width:50px;'>
				<%= getOptions("Year5","1") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='160'>
			<select name='selSquad6' class=txtboxes onchange='c(this)' style='width:160px;'>
				<%= getOptions("Squad6","1") %>
			</select>
		</td>
		<td width='50'>
			<select name='selYear6' class=txtboxes onchange='c(this)' style='width:50px;'>
				<%= getOptions("Year6","1") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='160'>
			<select name='selSquad7' class=txtboxes onchange='c(this)' style='width:160px;'>
				<%= getOptions("Squad7","1") %>
			</select>
		</td>
		<td width='50'>
			<select name='selYear7' class=txtboxes onchange='c(this)' style='width:50px;'>
				<%= getOptions("Year7","1") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='160'>
			<select name='selSquad8' class=txtboxes onchange='c(this)' style='width:160px;'>
				<%= getOptions("Squad8","1") %>
			</select>
		</td>
		<td width='50'>
			<select name='selYear8' class=txtboxes onchange='c(this)' style='width:50px;'>
				<%= getOptions("Year8","1") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='160'>
			<select name='selSquad9' class=txtboxes onchange='c(this)' style='width:160px;'>
				<%= getOptions("Squad9","1") %>
			</select>
		</td>
		<td width='50'>
			<select name='selYear9' class=txtboxes onchange='c(this)' style='width:50px;'>
				<%= getOptions("Year9","1") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='160'>
			<select name='selSquad10' class=txtboxes onchange='c(this)' style='width:160px;'>
				<%= getOptions("Squad10","1") %>
			</select>
		</td>
		<td width='50'>
			<select name='selYear10' class=txtboxes onchange='c(this)' style='width:50px;'>
				<%= getOptions("Year10","1") %>
			</select>
		</td>
	</tr>
</table>
<%
	else
%>
<table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:356px;top:78px;display:block"></table>
<%
	end if
%>

</form>

<iframe name=pin id=pin width='100%' height='100%' border=0 cellspacing=0 cellpadding=0 style="position:absolute;top:0;left:0;display:none;z-index:100" src=""></iframe>
<script>
	spnPageHeader.innerHTML = "Player Details"
	var tabcontrol=true
	var Sumcontrol=false
	if(Sumcontrol==true&&tabcontrol==false){subDet.style.display='Block'}
</script>
	<!-- #include file="..\includes\asp\Xscroll.asp" -->
<script>
	disabler('<%=rs1("backwards")%>,xx,xx,<%=rs1("Forwards")%>,1,1,1,1')
	control.style.left='122'
	control.style.display = "block"
	
	function showAddHelpDivs()
	{
		document.all.addhelpdiv01.style.display = "";
		<% if Session("SchoolUser") = "" or isNull(Session("SchoolUser")) then %>
		document.all.addhelpdiv02.style.display = "";
		<% else %>
		document.all.addhelpdiv02.style.display = "none";
		<% end if %>
		document.all.addhelpdiv03.style.display = "";
		document.all.addhelpdiv04.style.display = "";
	}
	
	function showSearch2Help()
	{
		document.all.search2helpdiv01.style.display = "";
		document.all.search2helpdiv02.style.display = "";
		document.all.search2helpdiv03.style.display = "";
	}

</script>

<%
if session("NavType") = "1" then
	select case request("nnFunc")
		case "add"
			%>
				<script Language="JavaScript">
					clean()
					updater.txtSurname.focus()
					parent.frames.leftFrame.fnTabClick('0');
					showAddHelpDivs()
				</script>
			<%
		case "search"
			%>
				<script Language="JavaScript">
					parent.frames.Search.openSearchScreen();
					parent.frames.Search.showSearchHelp()
					parent.frames.leftFrame.fnTabClick('0');
					showSearch2Help()
				</script>
			<%
		case "search2"
			%>
				<script Language="JavaScript">
					parent.frames.Search.close();
					parent.frames.leftFrame.fnTabClick('0');
					showSearch2Help()
				</script>
			<%
	end select
end if

%>


</body>
</html>