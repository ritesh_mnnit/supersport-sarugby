<%@ Language=VBScript.Encode %>
<!-- #include file="..\includes\config\DB_Connectivity.asp" -->
<html>
	<head>
	<LINK REL="stylesheet" HREF="../includes/css/QBuilder.css" type="text/css">
	<script language="javascript">
		function printer()
		{
			<%#@~^igAAAA==@#@&d7idb0PU+kdkKxcJgl-PHw+rbP{PJ8E~Y4+	@#@&d77id.+k2W	/n SDkOn,J[W1;s+UDRmVVcD2WMY4+^w[r7!FcdYHV+c[r/aVmX~'~v	WU+EE@#@&d77i+x[~b0@#@&i7iCSYAAA==^#~@%>
			document.all.printtab.style.display='none'
			print()
			<%#@~^hgAAAA==@#@&d7idb0PU+kdkKxcJgl-PHw+rbP{PJ8E~Y4+	@#@&d77id.+k2W	/n SDkOn,J[W1;s+UDRmVVcD2WMY4+^w[r7!FcdYHV+c[r/aVmX~'~vEJ@#@&i7di+U[,k0@#@&id7WSQAAA==^#~@%>
			document.all.printtab.style.display=''
		}
		
		//If a user submitted a report, and then clicked on the save buttons before the entire report had been displayed
		//he or she would get a javascript error. Hence I setup the page to be hidden, until the page had finished loading
		//when it would be shown again.
		function showBody()
		{
			document.all.divLoading.style.display = "none";
			document.all.body01.style.display = "";
		}
		
		function goBackRep()
		{
			window.location.href = "<%=#@~^KQAAAA==~M+5!+kYRU+M-+M.CDbl8s/`r4YDw{MnW+M+MJbPJg8AAA==^#~@%>"
		}
	</script>
	</head>
<body onload="showBody()">

<div id="divLoading" name="divLoading" style="display:; text-align:center; width:100%;">
<br><br>
<img src="../images/loading.gif" alt="" width="76" height="27" border="0">
</div>

<div id="body01" main="body01" style="display:none">

<!-- 
	This form is to make available a form on this page to indicate to the topbar that it must disable the back button.
	This had to be done because of an error that would occur if you saved two reports on the report results page, and
	tried to press back, it would give you a JavaScript error
!-->
<form name="repCheckTrue" id="repCheckTrue"><input type="hidden" name="emptyField" id="emptyField"></form>

<%#@~^cwIAAA==@#@&NCDl,'Pk+kdkKxcJ9lOCr#@#@&r0,k/m..lH`9lOlb~Dtnx@#@&dkY.9mYlidD(fi7{P[mYm`!*@#@&nx9Pr0@#@&@#@&b0PMn;!+/DcEDwwEUmEb,@!@*Prjbj2E~Dt+U@#@&d]+k2KxdRqDkD+,E@!Dl8VPCsbox{vmxY.vPbN{B2DrUDYC4E~xm:nxEwDrUDYC4E~1+s^/almbxTx!,/OX^+xv9k/aslH)4^G^3pA}I92]R~rPK}\),F2a,/Wsr9i$r"9AIR"qVCK=P826,/GVbNI2K/kDrW	)DsCYb\iOW2l FTwXIV0Ol *waISk[Y4ly!YE@*@!YD@*@!D[@*@!kUw!Y~OHw+{v4!YYKUvP7l^En'vKMkUYE~m^ldd{BYaO(Wa+kv,/OHV'BSk9Ot=F{!a6I^!D/K.)4lx9IvPKx1VrmVxEw.k	O+M`bv@*@!zO[@*@!ON@*@!bx2!Y,YXa+{v4!YOW	B~-mVExB@!OO~C^3EP1VC/dxEYaY(G6/v~kYXsn{BAk9O4)qF!a6i1EMdWM)4l	NIv,Wx1sk13'ELGAmm0InwcbE@*@!zD[@*@!zO.@*@!zOC(Vn@*r@#@&+U9Pb0@#@&ocIAAA==^#~@%>

<center>
<div id="reporthelpdiv01" style="display:none;position:relative;top:0px;left:0px;background-color:#FF3300;border: 1px solid black;color:white;width:550px">
	<table>
		<tr>
			<td align="center" style="color:white">
				<table width="350px">
					<tr>
						<td align="center"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
						<td align="center"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="color:white">You can now print your report, by selecting the Print button, or go back by pressing the Back button. Should you wish to save your report, you can scroll to the right of the table, and you will have the option to Save it as a Static or Dynamic report. Saving as a Static report means the data as displayed in the report at the time you saved it will be stored exactly as it is. A Dynamic report contains all the fields you selected (e.g. Players, Squads etc.) with the data being updated by the current information in the database.</td>
		</tr>
	</table>
</div>
</center>

<%#@~^0wAAAA==@#@&dr6PU+/kkKU`r1C\:X2nr#P{~J8JPD4nx@#@&id.+d2Kxd+cADbYn~r@!/^.bwOPdC	o;mo'Bxl7C?1DrwDB@*E@#@&di.+kwW	dnRSDbYnPE[Km;:UYclsscD+2GMY4+^29k-ZFc/YHV Nb/2VmX~x,BBr@#@&idDd2W	/RADrOPE@!JdmMk2O@*J@#@&7x[PbW@#@&qj4AAA==^#~@%>
			
<%#@~^7QoAAA==@#@&/OMn^lXDk7d{P]+$EndD`JDaYhVlHn./r#@#@&dY.Kmondi7',In5!+/OcrnCoE*@#@&kYM?YmDD9lD+7',In5!+/DcJ4kNUOCDDfmYnJb@#@&/ODAUNGlOnid'~];;+kOvJ4bNAxNGlDnJ*@#@&@#@&frh,kxDPl(V+;UOdiB&xO+.LD~YK~ob\n~mPEUr$EnPb[xOb0b+D,YK~+mm4PDl8sPNbdw^lX[SPDtb/~x;h(+.PSrV^P8n,E/n[,0GD,O4+~M+aWDDPkC\@#@&fb:~dDDCtPSUYWMn7BUYMkUo~Wb+sN,OW,/C-PY4n,CPHd~9kdaVmX+9~,Otb/~hbVs~(+P!d+9PYK~dl7+,Y4+~u:HJPbUPmPWr^+@#@&@#@&kUY:C(Vn;xDP',!@#@&/DDuKtSjOKD+,xPrJ@#@&@#@&k6PkY.?OCMY9lDnP@!@*~ErPY4n	PdYMjDl.DfmY+,',^NmYn`kY.jDlDD9lD+#@#@&r0,/DD3x[9mYnP@!@*PrJ~O4+x~dDD3x99mYn,',mNmYc/DD3x9fCO#@#@&@#@&b0PkO.?DlMY9lOni@!@*PrEPDtnU,@#@&7dDD9lDn,'~rPmxN,YdY9lO+,@*x~EJPL~/DD?DC.YGlD+~[~EEPE@#@&nx9PrW@#@&@#@&r6PdYM3	N9mYP@!@*PrEPDtnx@#@&7r6P4M^s^lo,x~!,Y4+U@#@&7i/ODGCYPx~kYD9CD+~[,E,lU9PD+/DNmO+,@!xPEJ~',/YM3x9flDn~[,JEJ@#@&77(D^s^Co,'~+@#@&dnsk+@#@&i7kY.GlD+P{PkODGlO+,[~E,lx9~Y/Y9CO+,@!{PvJ~',/ODAUNGlOn,[PEvr@#@&di8Mmo^lTP', @#@&dx[Pb0@#@&xN,r0@#@&@#@&dODGlD+~'~.wsl1n`kY.9mY+S~rBE~,EEBE*@#@&@#@&kYM!DKE2AHd77{PI5E/YvE4k9MMW;w$zr#@#@&bWPkY.!MWE2$Hd7',ErPO4+	P/DDV.W!w$X,'~EhW/bOkKxJ@#@&r0,/DD!DG;aAzdixPrnGdbYkGUrPOtU@#@&7VDKEw~X"GhgW7',c@#@&V/r0,/YM!.W!w~X7'~EU;;l9EPDtnU@#@&d!.KE2AH]KhHKd{P*@#@&s/kWPkY.!MWEa$Xi'Pr];o(X`xrWUE,Y4+	@#@&iM.G!wAz]KhHWix,v@#@&+	NPb0@#@&@#@&B9EPOG,Yt~0mmY,O4lDPDtnPd5!l[/,OtP;dDPdn^+^Y[,hnM+,xWDP9r/aVCXbxL~aDWanD^X~,rOPSlkP[+^r9+[PDGP^kdO,Ytn~k;;l9d@#@&vbx,YtP4nl9kUo,WW~Dt+,Ol(V+c~PtP6WsVGAbxLP6;x1YrG	PonOkPOt~k;;mNkPqGP1GN/~h4k^4,lD~wm//[~0MWsPOtn~aDn\bGEk@#@&v6WDhS,lUN,;k+d,Y4+:,YK~DY.k\n~Dt+,Uls+/,GWPDtPd;;C9/~0MG:,Y4n,NlOC(ld+B~Str1t,k/,EknN,hrY4PO4PtCNbxo,GWPl1t~YC8^+@#@&kODU;;C9/Px~kwskDcM+5!+kY`rtb[?$ECNkJbS,J~rb@#@&0VmLokM/DInm~x,!@#@&6GD,k~x,!POG,E8W!U9`dDDU;EmNkb@#@&ddYM?5;mN4nDP',Ej+^+1Y~?5;mN~0MG:,N.2U;EC[kPAt.Pj$EmNqGP{~BrP'PkY.j$El9d`b#PL~EBr@#@&dj+O~M/P+s2?$EC[,'P^G	F +Xn1EO`kYDU;!CNqtnD#@#@&idk6~DkK+s2j;!l9RnWW~{PWl^d+,Y4n	@#@&77ikWP6smoobDkYIm,xPZPOtx@#@&iddidYMK+s2j;!l9/~'~.kKn:aj;!l[cZ#@#@&7id70^CTsrM/DI+1P{~F@#@&7di+sd@#@&i7di/YMPn:a?$ECNd~{PdYMP+swj5!lNd~,[~JB~rP',DkK+swU5EmNc!*@#@&7id+	[Pb0@#@&77+	N,kW@#@&7U+OPMdK:2j$El[~{PUWD4bxL@#@&	+6D@#@&@#@&b0~E1ldnv/YMKlT+#,x~E1lk+cJ]naW.YnrYr#~O4+x@#@&i/ODwrV[kdid',D2Vmmn`"+5;/YvEYXY?sqJ*~rBE~EE*@#@&d4rN;W;UDskns9/7',];;/D`JD6Dj+^ E#@#@&@#@&ik0,;4KEx9cdw^kD`dY.ob+sNkSJBJbb,@!Pq,Y4+	@#@&d7kYMhtD7d{PEh4+.n,2fc!+	qf,rUPvJ,[~In2^l^+vdYMnsCH+DdSrBE~rvEJb,[,J#,J,'PM+2VmmnckYDGCY~JDndY9lD+E~E9mYnja[lD+[E*@#@&77kY.?5Jidx,J6+1P:.l13rxTIn2KDYk~BnkYESvJ,[,/ODA4DnPL~JE~v~KDNn.,4zPA9cJ'M+aVl1+vdYMM.W!w$zBJ?$;l9J~rj5EmN8Jb[ESAf ?!.xm:n~E~BSELPdYMob+s9/,[Pr~EE@#@&d7k6Pjnk/kKU`r?m4GGV`/DE#~x,JEPD4+	@#@&7idIn2glh+ix,JFbY,q/kE[P"+2WMY~WKDPr~[,/YMPn:a?$ECNd@#@&d7+^d+@#@&77iI+2Hm:nd{~r|rDP&//!+9~IwGDDJ@#@&id+	[Pb0@#@&7vd"+kwGxdnc.kDnPkY.j5S@#@&7Ed]+k2KxdRAxN@#@&i7NzcDAA==^#~@%><!-- #include file= "includes/tableRender.asp" --><%#@~^Lw4AAA==@#@&dn	N,k0@#@&idYMh4+M+77i'PrCx9P3bO oxbN~kU~vJ'/D.n^lznM/[Eb,J~[,.wsmm`Dw^Cm`dYMfCO~JDn/DNlDnE~rfmYnj2[mYnNrb~rBvEBJBEb@#@&7/D.UpJidi'Pr/s+1Y~e,0.GsP`knVmY,vPDmm0/;kOv,BVkDv~,$[.a?ErOUk"+ODc$j!kD?k.+Y~BDDCm0/;rDB~,^W!xYvMbPEx!:8+.v,0.Ws~3bY~s0Y~%KkUP][Mwj!kD?k.+Y~$9D2?!kOjby+ODPKxP]FrYYR]K.l^VkErYY~',$[.a?ErOUk"+ODc$j!kD?k.+&9T,Vn0DPG;D+D,%WbxPh2VKX+[lOC,WUPhw^WznNlOCconxb[,'~0kDRoxb[PStnDP,FbYTc,KMlm0d;kDT,kdPUGDPUE^sPr[dOMhtn.[EPT.KE2,4HP$9DajEbYjk.+1Dc$?!rYUkyD~E	kKx~/nsmOPE!W^0j4bDYv~E3rYES,$[MwUEkD?b"+8!DR]?;rD?k.nT,BMKsW?4kMYv~~^KEUYvM#,BU;s4+.v,0.Ws~0kO,V0Y,LKrx,$[Da?;rD?k.nT,$NM2jEbYUk"+qTYPGx,,|bYD ]MWsWUtrDDD,'~]NMw?!kDjk.+q!YR,j!kYUryqfY~s+6Y,W;Yn.,LGk	~+swsGH++[CDl~W	~:2^WH++9lDCRT+Uk9Px~0kYcL+	kN,A4+M+,$FkODc$!W^W?4k.OYPkd~	WOP	;^V~r[kYDSt.+LJ~oMW;2,4X,,NMw?!rO?byFTT ,UErYUryT~;	kWU~k+s+1O,B]!o(XPx+Md+HBSP]N.2UEkDjk.+F8D $UEbYjk"nYPvI!L4HPBnM/+zvBP^W!UD`M*PExEs4.B,0.WsPVrDPVWY,LWbU~$9Da?;kOjbynT,,NMwj;bY?r"FqT,G	P,nkDTR]I!L4H9nDk+zD,'P][Da?EbOjk.+8FDR,j!kO?b"+&fD~^+0O~KEO+M~NWr	P:w^WHn+9lOl,WU~:w^GX+NmOCRT+	k[Px~0kORTnxbN~A4+Dn~]|rYY ]I;T4H9+M/zT,kdP	WO~	EV^~JL/YMA4+M+LJ~o.G!w~4H~$9D2j!kYjr.+qFY ]?;bYUkyT,;xbWUPk+sn1YPE]ET4X,j4WMYkBSP,[MwjtK.Yk?r"F D ]?4WMOk?r.+YPB"ET8X,?4WMYdvBPmK;xD`e*~vx!:(+.B~WMWhP0rY,VnWDPLGr	P,NM2UtGMYk?k.+Y~$9D2?4W.Ok?k.nFyTPKU~$nkDT $];T4z?4GDD/D~{P$[.a?4WMOk?r.+8 Tc$U4WMYd?byn(GTP^n0DPW!OnD,LKkUPnhaVGXnNmYC~KxPnhaVGXn9lOmRT+xbN,xP0kORT+Ur9Ph4nDP$nrOTc$"EL4zj4W.YkDPb/~UKYPU;^V~JLdDDA4+M+[rPT.W!w~4HP,[Mw?4GDD/?b"nFyTc$jtG.D/jk.nT,EUrKxPdn^+^Y,v~VC.+MB~,P][DaAsl.+./!YFWDR]AVm"nD;EDT~_~v,O~B,QP]N.2~Vl"nM?ryq2T ]A^lyDUryTSP1W;UD`e*~B	E:(n.B,0MWhPVrDPs+6OPNWrU,$N.2~VCy.;EOYP]NDaA^CyD/EDF*D,Wx,,|bYTc,$VmyD/EOD,'~$9.w~VC"DZ;O8cDR]$^l"D;EY&fY~V0OPNWrU,$NM2A^ly.jk.+YP,N.2~VCy.?bynq2TPGU,$FkDDc$$^l.+DUk.nT,'~$9D2$^ly.?by+8fDR]A^l"+.jbynqGDP^+WO,WEOnMP%WbU,+haVKX+NmOl,WUP:2sKX+[lDlRTnUk9P{PVkO T+Uk9~h4+.n,$|rOYR,A^C.+.;EDTPb/,UWDPUE^V~C	NP]FkDTR]$sl.+M?rynD,kdP	GY,x;s^PJ'dDDAt.[E,oMWEaP(zP]N.w~VC"DZ!OFWTR]$sl.+MZ;YDS]N.w~sl.+.jby+qfYR,A^C.+.Uk.+T,E	rW	Pd+^+^O,BKMGEk+DES~$9DaCkdOUk"+8XTc$	Cb/Yjr.+D~,^KEUD`C#PEx!h4DvP6DGh,3kD~V0Y,%Gk	P]N.w	Cb/O?b"+YP,[MwCrkYjk.n8*D,W	P$nkDDR]K.W!/n.qlkkOT,'P][.wqlb/O?r"FXTc,mkdOUkyn(GT~VWDPG!YDPNWbUP:2VKXnn9lYm~W	P+s2sWH+NCYC T+Uk9~',3rOco+Ur9PAt.P,nkDTR]KMGEk+.mkdOYPkk~xKYP	;sV,JL/ODA4Dn[r~oMW;2,4X~,9D2mrkYjbyF*YR]	lb/O?bynD,ExbGx,/+^n^Y,BUtG+vS,$[DajtK+jr.+F0Dc$jtKnUk"TBPmKE	O`C#~B	Eh8DB,WDK:P0rOP^+6Y~LGr	P,NM2?4Wnjby+D~]N.wU4K+jbyF%YPKUP]|rYYR,j4W+Y~',$NM2jtK+Uk"+q0YR,?4G+Uk"n&fT~s0OPK;D+.,LKkx,+s2VKXn+9lOC,Wx,n:aVWHnnNmYmRL+Ur9PxP0rYconUbNPA4DnP]FbYDc$UtWT,r/,xGY,x;s^PJLdYMht.n[rPTDGE2~(X~$9.wUtGnUkynqRT $U4K+jbyTP!xbGx,/nVmO~EAWKOBBP$9.2?4W?rynqFT $U4W?r"TPQv,O~B3,9D2~WKYZ!YyTTc$$WKY/;DT_E~O,B_][.w~WKYjY;[k qTc,AKWOjDENdDBP^W!UD`M*PExEs4.B,0.WsPVrDPVWY,LWbU~$9DaAGWO/!YDP][DaAGGDZEO+ZT~W	~]|rDTc$AKWD/EDT~',$[.aAWKOZ!Y ZD $~WKY/EO(GT~VWY,LGr	P$[.aAGWDjDE[kT,$NMw~GWD?OE9/+qYPW	~$nkYY ,AKWD?OE[dYPxP][DaAGGD?Y;[k qTc,~WGD?DENkqGDP^+WY,LGr	P$9.wUtWjryT,$[D2j4Wn?b"+8GD~KxP,FbYDR]PMlr	k	o?4WDP{P,NMwj4K+?b"+8GTc,jtK+Uk"+(9YPs+6OPKEOnMPLGr	Pn:asKXnNmYl,W	~+swsWH+n[mYlcL+	kN,x~3bYconxr[,h4+MnP]|rOYR$$GKY/EDD,kd,xKYP	E^sPmx[P]|rOYR$~GWD?Y![dT,kkPUWO~	EsV,Cx9P,FbYT ,:DCk	r	oj4WTPb/,UWDPUE^V~EL/YMAtD+LE~oMW!w~4z~]N.wU4W?r"FGD ]?4WjbynY~]NDaAKGY;EO ZT ,~WWD/EDT~][.w~WKYjY;[k qTc,AKWOjDENdD*PVkDq,h4DP3bY8 3bY~k	PcE,[P4rN;WE	OokV9/~[~E*J@#@&idYM?OCMYd77{Pq@#@&7"+2gls+di',EZKEUY,|rO,I+aGDDJ@#@&7dYMMMW;w$zidxPrVkDJ@#@&iMDG;aAzIKAgW7{PZ@#@&ik6~VxctbN/G!xYwr+^N/*~@*P8PDtnx@#@&iB7IdwKxdncDrOPdYMj5S@#@&dEdI/aGxk+ 2	N@#@&id0e0EAA==^#~@%><!-- #include file= "includes/tableRender.asp" --><%#@~^rAQAAA==@#@&d7@#@&i+x9PbW@#@&@#@&+^/nr6PE1C/`/D.KlT+*PxP;^m/n`r]+aW.O:+/Odr#~Y4n	@#@&i/DDK/DddidxPM+2smm+v]+$E+kOcJD6D?nVqE*~EBrSJr#@#@&i/Y.j0ksVk7idx,DwVmmcI;;+kYcED6YUnVyJ#BEvJBJr#@#@&7dDD9lDnK:2~{P/O.GlO+@#@&@#@&7mDMK+kYk~',/2VbYcdDDKdYk~JBEb@#@&dmD.?Vr^VdP{~/aVrOv/Y.j0ksVkSr~E*@#@&d@#@&dkODStnDPx~rM+	(f,kx,cEPLPkY.nsCH+./,'Pr#~E@#@&@#@&76W.PC1t~D+kYPbx,CDMKn/D/@#@&idlM.K/Yk+~',/aVrYcO/O~rkJ*@#@&7i/Y.Pm4s+:nsw~{PmDD:+kO/y`T#,[~EcY+kONmY+r@#@&di/DD9lOn:+hw,xPM+2smm+cdDD9lDnBPED+kYNmYE~,/OD:l8sK+s2#@#@&didODGlD+P+h2,'~D2VmmnckYD9CD+P+s2BPEEBr~PrBrb@#@&d7k6P;^m/+vCDMK+kOd v!*#~'~E&?6b"\J,W.~!mldnvl.D:nkYdy`Z##,',EqUrJ2VJ~O4+x,@#@&iddS4nDP{PEPA4DnP]EPLPC.MK+dOk c!*~LPEYRV+x&f,rx,`EPLPdOMnVmz+M/PL~E#,J,[~/O.GlO+:n:a@#@&7i+Vdn@#@&7diA4+.P{PJ,h4nDP,J,[~CMDKdYk `Zb~[,JYREP'~kY.h4nDP'~kYD9CD+P+s2@#@&7i+	NPb0@#@&di/ODUpJ~{PJa+1PKMC^3bxTInwG.D/~Br~[,l..:+/Ody`T#,'rBSEJ,[PM+asl1+ch4+.nBJBrSJEBJ*~'PrBBB~W.[D~4H~2GRE'M+wsC1+c/D.VDG!w~X~r?$;l9JSJU;;C9FJ*'JB2fcj;D	ls+~BSvEJ@#@&i7k6Pjnk/kGUvJjm4GKVik+MJ#,',EJ,Y4+	@#@&7idI21m:+,x~lMD:+dYd+vFbPL~J,0G.,JP'~kY.Kha?5!l9/@#@&dinVk+@#@&id7]w1mh+i'Pm..K/D/+`qb@#@&7dUN,kW@#@&ddv]wHlsn,'~mDMK+kYk+`8#~[,J~WKDPr~[,/YMPn:a?$ECNd@#@&d7vkkBAA==^#~@%><!-- #include file= "includes/tableRender.asp" --><%#@~^rgIAAA==@#@&dU6D@#@&@#@&iWWMPnl1t~j0kV^~k	PlM.j3bV^/@#@&77mD.?0rV^/+~{P/2sbYc?0r^VSrur#@#@&didYMKC4^+PnswP{~lMD?0rsVk v!bP'~rRO+kONmYnE@#@&d7dDD9lDn:+haP{PDw^Cm`dYMfCO~PrO+kYNmOnJBPkY.KC8^+P+s2#@#@&77kYD9CD+P+s2,'~M+aVl1+vdYMfCYKnha~PrvBr~PrvE#@#@&idAtn.PxPr~h4+.n,JP'~mD.?0r^Vdy`Z#PLPr J,[~/DDA4D+,'PkYDGCO+:+sw@#@&77kY.?5JP{PEnX+m~PMl^3bUTInaWMY/,Br~[,l.DU3rs^/ vT#,[JESvJ,[,DnwsC1+ch4nD~Evr~Jvvr#~[,EE~v,WMN+MP(zPAf JLDn2^lmc/DDMMG;w~XBJj;;C9JSJU5EmNqE*[JS3GRjEMUm:n,BBBBr@#@&7db0~?/drKx`rjm4WW^id+MJ*PxPEE,Y4+	@#@&id7]w1ChPxPm.M?VbV^/ vF*~[,J~0KD~E,[PkOD:+:aj5EmNk@#@&d7n^/n@#@&7diIn2gl:n7{PCDMj0ks^/y`F*@#@&7dx[Pb0@#@&idB"nwgl:~xPmDM?Vkssk cF*~[,J~WKDPE~LPdYMP:2U;!lNk@#@&7dr78AAA==^#~@%><!-- #include file= "includes/tableRender.asp" --><%#@~^myEAAA==@#@&dU6D@#@&Vknk6P;mm/nckYDhCo#P{~;mm/`EIn2KDO?DCYbmEb,YtnU@#@&@#@&idDDPm4^+K:a~',JPfmYnqcY+kONmY+r@#@&dkYMfCYnP:2P{~DwsC1+`dOMfCYS,JO/DNlD+rSPkY.Km4sn:+:ab@#@&d/D.9lD+:+hw~x,Dnw^Cm`dOMflOn:+hwB~rBvr~,JBr#@#@&dkY.h4+.n,'Pr~h4+D~3fcMx(f~r	PcJ,'PkY.K^lXn.kP'Prb,J~LPkYDGlDnK:2@#@&dv./wKU/RhMrO+,/DD	tn.@#@&dE.+kwGUk+RnU9@#@&dUn^+^DP1l/P"n;!+dYvJ4r9I+aGDDJ#@#@&7d1lk+~F@#@&id7/D.?5S77{PJjn^+^Y,obDdD1m:+BPU;D	lh+BP[.anWkrYbWxcKG/bYbWU~~[Mwj;!CNkRj5!lNS~9D2I!L(Xi	kKxR"ET8X`xrW	~~PGlYqR:+/D9CYPEKn/O~GlO+ESP~+U^4nDndkR]+k;^Y~EAxm4Ph.+k/~b(/Gs!Y+ESP~+x14KD/kR]+sCDk-+"n/!VO~EA+U^4PKDdkP]VmYk7+ESPt2KE^V;2kRIdE^YPEK;V^O!wdBS~t2KEk4Ea/ ]/EsO,BKEk4 E2kBBPsKDCDsM.kaR]nkEVDJPEMDb2~?DDxLY4~z4dW^;YPcJ0YbvBPoWMnmDhVDbwR"+k;VDI~BVDr2,?YMnxTYt,)8/KV!YnPc]bo4Y*v~,sj5!lYq "+dE^O,Bq"H,sE^V,j;!lOPz4dG^EYv~,s?$;CY8R"+slOr7+]+k;VDPvq"HPo;^V~?$;mY~"+^lYb\v~,sj;!lOfcI+k;VDPB2]\PwE^V~?5;mY~b(dW^EOnE~Poj$ECY2 "+smYb\+"+k;VDPv&"H~o!VV,j;!lY,]nVmYb\nBS~d+LnMn/kF ]/EsO,BqIt~d+L,nM+/kPz8/KV;YBS~d+oh.+k/Fc]nVmYb\nInd!VOPEqItPJnTPn.nk/~IsmYr7+E~Pd+TKD/d&cInd!VY,v&"HPdnLPhD/dP)8kWsEDnBBPJnTnDndk& IsmYr7+"+/!VD~B2I\Pd+L~hD+kdP"+VmOr\BBPK?5;mYqR"n/!VO~EFI\~hl.l^sV~U;!lY,b(dW^EO+E~~KU;EmOFcI+^COk7+"+dEsO,BqIt~nmDCs^+V~j$ECY,]VCDk7+BBPhj;!lO&cInd!VY,v&"HPhC.l^VV~?5;mY~b(dW^EOnE~PKj$ECY2 "+smYb\+"+k;VDPv&"H~KmDl^s+^P?$;CY,IVCYr-BSPx;:a?5;mY T "+dE^O,BB!:aP?$EmOPSkOt, TVTPb(dW^EYvSP;W!xO+.\K\n:UYcInd!VY~v;W;xDnMPhK\:+	Y,BEswv~,nGADZ^nl	RId;VDPEF]H~KKhnD,/VlUv,0DGh,2hw^GH+nGlDlPAf,s+6Y~W!Yn.,LWbUP9Dw";L4Hj	kGx~G	P3fc]ET4zi	kWU~{P[Da]!o8Hj	kW	R";o(XixbWU(GPVWY,WEDn.PNWbx~N.2U;;l9dPKx~3GR?5;mNqP{~9D2U;!lNkRU5EmN(f,VnWDPW!O+MPLKrUP9DanG/rObWUPKUPAf KK/kOrKx~',[MwKK/bYkKxcKWkkOkKx(9,V+6OPKEY.~LKk	Pc?nsmOPVnx&fS~:+/O9mYnP6.K:~~+	mthDd/,EUkKx~jV+1OPV+x&9SP:+kY9lOn,0.Ws~sKDnCM:M.raP;xbG	PjVmY,MUqG~~K/O9mY+,WDK:PdnLnM+k/fP;UbWUPUnVmO~V+x(9BPP+kOGlOP6DWsPwj;!lOF,EUrKxPUnVmY,!nx&fBPP+dOGlO+,WDK:~oU;ECO2P;xbG	PjVmY,MUqG~~K/O9mY+,WDK:Phj5EmY8P;xrG	Pj+^nmDP!n	qfS~:+dYGCD+~6DK:Ph?$;lD&~E	kGU,?+^nmDPMU(fBP:+dY9CD+~0MG:,9;ha?;;CD TP!UbWU,?V+1Y,!+	q9~,KndDflDnP6DWs~/W!xD+.HG-:nxD~E	kGU,?+sn1Y~MU&fS,K/YGlDnP6DG:,nGADZ^nl	PE	rGx,?VnmO~V+UqGSP:+dOGlYn~6DG:,\An;^V!w/,E	rW	Pj+^+^O,M+	(fBPKdOfmYPWDGh,H3n!dt!wd~!xkGU,?nV^DP!x&f~,KdYGlO+,0.GsPSLnM+/kqbP:fmYnF~G	PPfmO+8R!n	qf~x,29RVn	q9,V0Y,W!O+MP%Wbx~$xm4KD//,GUPAfcMnx(9,'~AUm4n.nk/R!n	q9PmU9PPGlD+FcKdYGlO+,'~$xm4KD//cPn/DfmYnPsn6Y~W!O+MP%GbxPoGM+CDs!Mk2,W	P2GRVnx&f~',sG.lDs!DbwRVnUqGPmx[PP9mYnFcP+kY9CD+Px~wW.+m.sM.bwcK+kYGCYPs+6Y~G!Y+M~LKkx,JnohD/dF~G	P3fc!+	q9~{PSnLhDn/kqcMn	qGPl	N,PfmYnFcKndDflDnP{PSLKD/kF KndDfCY~V0O~KEYn.,LGk	~d+LhD//2PKUPAf Mx(9,'PdnohD+kdfRV+	q9PCU9PPfmO+8RPnkYfCOPxPdnTn./k&R:+kOfmYnP^+WO,WEDnD,LWbU~sU;!lOF~G	P3fc!+	q9~{Psj5!lOFc!x(GPmxN,KGCYF K/O9mY+,xPw?;!COFcK/OfCOPs+6OPKEOnMPLGr	Po?$;mYf,W	P2GRVnx&f~',sj5!lY2 MxqG~Cx9P:fCYnqcKn/D9lD+~x,s?5;mYfR:nkY9mYPV0D~W!YnD,LGr	PnU5EmYF,GUPAfcMnx(9,'~nU5EmYq V+x(9,lUN,PGlOFcK+kYGCYPxPh?5;mYFcP+kYfmOnP^+6Y~W;OD~LKrx,nj5!lYf~Kx~2G V+U&f,'Ph?$;lD& Mx(9,lx9~KGlYq K/DfCYn~{PK?$;lD& P/Y9CD+~VWDPG!YDPNWbUPxEhwU;;CD !,Gx,2fc!nx&f,'~9;ha?5EmO ZR!n	qf~C	N~KGCD+qcK/YGlDnP{PBEswj5!lYyTR:+/D9CYP^+WY~G!YnD,%Wbx~/KExOnMHG\hxO,W	P2GRVnx&f~',ZG;	Y+M\W7+:UORV+	q9PCU9PPfmO+8RPnkYfCOPxP;G!xODtW\:UYcKn/DfCOPVWY,WEDn.PNWbx~nGAD/VCx,WU~AfR!n	q9P{~hWAD;V+mxc!+	q9Pmx[~:flDnFcK+kO9lD+,'~nGAD/VCxcKndDflOn,Vn0D~KEOD,LWbx,\2hEsV!wd~KxPA9RV+x&9~',HAn;Vs;a/ MUqGPCU9PK9CD+qR:nkY9mYP',HAKE^V;wkRPnkYfmO+,V+6O~W!YD~LGr	P\2h;/4E2d,Wx~3GR!+	(GPx,HAnEkt!2/cMnx&f~C	NP:9lD+FcPn/DfmYnPx~t2KEk4Ea/ P/Y9CD+~JLdDDA4+M+[rPK.ND~4HP39cJ[Mnw^lmcdYMMMW;w$zBJj;!CNr~Ej$El[qr#'JB~UE.	ls+~,KGCYF K/O[mY+,[+kmJ@#@&@#@&idikWPjnk/rW	cJUm4GKVjdnMJbP{~rJ~Dtx@#@&di7d"+21m:n7{PJt;/1V+,j^WM+kJ~[~E,0GD,EPLPdOMK+h2U;;l9d@#@&7idV/@#@&7did]+a1Chd',EH!/m^n~?1WM+dJ@#@&id7+	[Pb0@#@&iddv]wHlsni'~rH!/m^+,jmKDn/rP'~rP0K.PrP[,dOD:+swj;;C9/@#@&i7mm/n~y@#@&77i/ODU}dd7{Pr?+^+1OPwk./D1Ch~PU;D	l:S~NMwhWdkOrKx nKdkDkGUBPN.2U;;l9dc?5!l9~P9Da]ET4zj	kGUcIET8X`xkKUSP:fmYnF P/OfmO+,BPnkYP9CD+v~,ja+n9FZ:R"+k;VDPvFZ:~ja++9v~,?wn[cZ:cIn/;sDPvcZhPUwnn9B~~)TkskDz&VsbxKk/cIdE^Y~BzorsbYX,(V^kxKrdP"+kEsYvS,bLk^rYHKPnkYR]nkEsY,vzor^kDXP:O:n/DP]+kEsOE~P"nwUwDbUOR"+kEsY~v"+2+mO+9Pj2MkxO~"+dE^OE~~;sUtEDY^nI!x I/;sDPBU4EDYV~]E	P"+dEsOE~~C&\?cICOI+^G7+.PEu&Hj,ImY+,D^W7+.XEPW.K:PAhw^WXn9lDl,29Psn6Y~W!O+MP%GbxPcjVnmD~V+U&fBPK/D9lD+~0MWh~Uw+[FZ:P!UrW	PU+s+^O,Mnx&9~,KndDflOn,0.Ws~UwnNW!:,E	rW	Pj+^+^O,M+	(fBPKdOfmYPWDGh,bLk^rYHqssbxWrd,EUkKU,?n^+1YPV+	(fBPP+kY9CD+P6.WsPbTrskDX:Kn/O~!xrW	~?Vn^DPMnU&fSP:nkY9mYP0MWs~IwjwMkUO,ExbGx,?+^n^Y,Mx(fS~:+dYGCYPW.K:P/oUt;YDsI;	P!xkKx,j+^+^Y,MnU&f~,P+kYfmOnP6DK:~C(\U#~KGCYF~G	PK9CD+qRVn	q9,',2fcMUqGPs+6Y~G!Y+M~LKkx,[.w"ET4zjUrKx~W	~2GR];T4XiUbWUP{~9D2"ET4X`xbGxcI;o(XiUbWx&9P^+0D~GED+MP%WrU,N.wU5EmNd~KxP39c?5Em[8Px,NMw?$Em[/c?5EmN(9,V+6OPKEY.~LKk	P[D2KK/rYbGx,WU~AfRKGkkOkKU,'~9DanWkkDrW	RKWkkOrKxqG~V0Y,G;YD,LGkU~Uwn+9q!sPGU,2f !x(f,x,?2+9F!sRVnx&f~l	N~PGlYqR:+/D9CYP{Pjwnn9FT:cP+kY9CD+Psn6Y~W!OD~NWbxPUwnNW!hPKx~3GRMUqGP',j2+NW!hR!n	q9PmUN,K9CD+F P/OfmOPx,?a++9cZhR:+dYGlOn,V+6OPKEY.~LKk	P)orsbYzq^sk	Wrd,Wx~3GR!+	(GPx,bTkVbYH(V^kUWb/ !xqG~l	NP:9CYFcKn/O9mYnP{~bTksrDXqssbxGkk :+dDfmY+,VWY,W;YD~%Kkx,)obVkDzPK/DPGx~3GR!+	(f,'~)TkVrOHKP+kOcMn	qGPl	N,PfmYnFcKndDflDnP{PbTrskDX:Kn/O :+dYGCYPsn6YPG;D+.PNGbx~"+a?wMk	OPKx~2GR!n	qf,xP"+wU2.k	YcMnx(9,lUN,PfmYnqcK+dOGlO+,x,Ina?aDk	YcP+kY9lD+~s0Y,GED+D,%Gk	P;sjt;ODVnI!UPKx~3GRMnU&f~',/w?4!YDV+"E	 Mx(f,lU[,KfmO+8RKdOfmYPxP/oUt;YDs+"EU :+/O9mYnP^n6Y~KED+D,LKrx,C(HUPGU,2fc!+	qf,x~C&HUR!+U(GPCx9~KGlOn8RKndDfCY~{Pu&HURK/D9lD+~JL/O.St+Mn[rPWM[nD,4HP3f ELDnw^Cm`dOMMDG;aAz~rj$EC9JBJ?$Em[Fr#'JBPj;Mxlsn~,KfmOnFcK/ONCOP[+k^J@#@&@#@&iddrW,?n/krKxcr?1tWKV`d+MJbP{PEE,YtU@#@&ddi7]+a1m:ndx~r?2+[Pmx[~zoksrDXEPL~rPWKD,JPLPkOD:+hwU;;C9/@#@&7di+Vkn@#@&idid]+2Hm:nd{~JUwnn9PlU[,bLk^rDXE@#@&iddx9~k6@#@&didv]w1mh+i'Prj2+N,lUN~)TkskDzJ,[~E,0W.~rP'PkOMKnswU;EmNk@#@&id^lk+~f@#@&di7/DD?5J7d{Pr?nVn^DPokMdYglhnBP?;.	lh+B~9D2hWkkYbW	 nK/rYbWUS,NDaj;!lNk j;!l9~~N.2"EL4HixbWU "Eo8z`xrW	S,K9mYFR:+kOfmYnPEKndDPfmO+E~PUO.lbo4YJ+L]mkd+c]+kEsOdPBjJ"PJBB~UY.mkTtYd+T]lb/nR"+d;^YI,v?dIP"vSPtW9K4Wh(^VrWa I/;sDSPv(^VrWadKld,SE~PtW9PtK:(V^kG2cI+k;VDIPE(sVbWa/Gld~"BSPtGN:tGh5El[ "+dE^OdPv5EmNPdBB~HKNPtK:};mNR"n/!VY"~vp!l9P]B~~6DG:,3:aVGz+fCOmP3f,s0O,W!Y+MPNGk	Pc?Vn^DPMUqG~P:ndYGlD+~0.GsPjYMCkTtOJoICrk+~E	rKx~U+^+mDPVnx&fSP:+dOGlY~0MW:,\GN:tK:(VsrKw~E	rW	Pjn^+mO~V+UqGS,KnkYGlYP6.WsP\W9K4GspEm[#,KfmOnF,W	PPfCOF MUqGPx~AfR!n	q9P^n6Y~KED+D,LKrx,N.w"EL8HjxbGx,Wx,39R"ET4zjUrKx~',[DaI;L(XjUrKx I!L(Xi	kKxqGP^n0DPGED+.~NWk	~NMw?$;CNkPKx~29 U;;l9qP{P[.a?;;C9/ ?$;mN(GP^+0DPK;YD~LKkU~9DwhG/bYkKU~W	PAf nGdbYrW	~',N.2hW/rObWURhGkkObW	qf,VWY,W;YD~%Kkx,jYMlkT4OSo"lr/n~Kx~2G Mx(9,'PjOMlro4Od+L"lb/+cMUqGPCx9PP9mY+8 K/YGCO+,',?ODCrTtOSLImkdncK+dOGlO+,s0O,W!Y+MPNGk	P\W9K4GsqV^rWaPW	~3fcMx(f~x,HGN:4WsqssbWw !x(f,C	N~:fmY+8R:n/DfCYPx~tWN:4WsqV^rGwcK/OfCOPs+6OPKEOnMPLGr	P\W9P4Wh5EmNPKx,3fcMnx&f~x,HW9PtK:p!C[RV+	q9PCU9PPfmO+8RPnkYfCOPxPtG9K4K:5El9R:n/DfCYPE'kYDS4+M+[r~GD9+MP8X~3GRE[Mnw^l^nv/Y.!MW;w~zBJj$EmNJBJU5EmNqJ*[ES,?EMUls+~,P9lD+8RP+dO9lO+,[+kmE@#@&@#@&77ikWPUnk/rKxvJ?1tKGV`/nDr#~x,JJ,Otx@#@&77diIwHlhni'~Jws+Xk8r^kYzE,[~J,WKD~rPLP/DD:n:a?5EmNd@#@&ddinVk+@#@&77diIwHlhni'~Jws+Xk8r^kYzE@#@&7din	N~b0@#@&didE]+a1C:dx~rsVak(kVbOzJ,[,J~0G.,J~[,dYMKnha?;;C9/@#@&i71ldPW@#@&ididYM4+M+~x,D+asl1+`kO.4+M+SPEPGlO+8 Y/O[mY+ES,J)xD4MW2K:YDHR][lD+DJ*@#@&7id/D.?5Sdix~JU+^+^Y~obDdYgC:~~j!DxCh~~NM2hWdbYbWxcnKdkDkGxBP[.a?;!CNkR?$;CNBP9D2I;L(XixbGxcI;L(XjUrKxSPzUDt.KwK:+DDH fmYnPEbUO4DWaG:YDH~P+kY,fCYnvBP)xD4DKwGhYDz UYCY!.Pv_+botDBB~b	Y4DKwGhYDH AKNXtCd/,Bq+ro4OE~~b	OtMW2Gs+Y.zc$jEs~U+-x,?3bx,oW^NdT,Bj;sP?-+	P?0rUPwW^NdBS~zxOtMGwK:nOMXRKnMmnxDr^+\!/1V+,Bu~H!/^VBS~zxY4.WaW:O.XcnD^+UObVnAK[XwlO~E]P$G9X~smOE~~zxDtDKwKh+DDzRAx[GsWDa4X,B2	[G:KDatzBS~zxOtMGwK:nOMXR\nkWhWM24X~EH/WsWM2tHBSPzxO4MWwKh+DDXc3^YK:KD2tz~E2^YKhWMw4zEP0.GsP3:asKXnfmYl,2G~V0OPKEOnMPLKrx,NDa];o(X`xrWU~Kx~2G I!o8z`xkGU,'~NM2"EL(X`xkKxc]ET4zj	kGU&fP^n0DPW!OnD,LKkUP[.a?5Em[/,WU~AfRj5!l[F,x,N.a?$El9/cj;!l[qGPsn6YPK;YDPNGrx,NMwKWdrDkGx,Gx,29 hW/rObWUP{~9D2hWkkYbW	 nK/rYbWU(GPVWY,WEDn.PNWbx~bUO4DGwKh+DDz~KxP39cMnx&9,'~zxDtDKwKh+DDzRV+U(GPJLdYMht.n[rPKD[+.~(X~2G JLDn2^lmnckY.MMG!w$H~r?;!l9E~r?5EmNqE*[JB~?!DxmhnJ@#@&@#@&7d7r6Pj+kdkKxcEUmtGG^jd+ME*Px,JrPY4+	@#@&id7d"+2Hm:+ixPrbxD4.WaWs+ODzE,[~J,WWMPE~LP/O.:+hwU5!l[k@#@&ddi+^d+@#@&7did]na1lsnd{PJzUOtMWaWh+O.HJ@#@&i7dx[~b0@#@&7idvI2glhd{PJzxD4DKwG:Y.zrP[,EP6WD,E~[,/DDP+h2U;;l9d@#@&dnU9P?nsmO@#@&7kY.UYmDYidixPZ@#@&dVDG;aAX"GhgWd{~!DKEaAzIGAgW~O,+@#@&d7iigLAA==^#~@%><!-- #include file= "includes/tableRender.asp" --><%#@~^NwAAAA==@#@&dvM+kwW	/ hMkO+,/O.UpS@#@&dED+k2Gxk+c+UN@#@&x[PbW0A8AAA==^#~@%>

<form name="frmReportSave" id="frmReportSave" method="post" action="reportrender.asp?repFunc=SAVE" target="ifrsaveReport">
	<input type="hidden" name="txtPlayers" id="txtPlayers" value="<%=#@~^FwAAAA==~M+5!+kY`rYXOn^lz+M/Eb,HggAAA==^#~@%>">
	<input type="hidden" name="Page" id="Page" value="<%=#@~^EQAAAA==~M+5!+kY`rnmL+r#~WwUAAA==^#~@%>">
	<input type="hidden" name="hidStartDate" id="hidStartDate" value="<%=#@~^GQAAAA==~M+5!+kY`rtb[?Dl.YGlOnr#PnwgAAA==^#~@%>">
	<input type="hidden" name="hidEndDate" id="hidEndDate" value="<%=#@~^FwAAAA==~M+5!+kY`rtb[2	N9lD+Eb,qAcAAA==^#~@%>">
	<input type="hidden" name="hidGroupBy" id="hidGroupBy" value="<%=#@~^FwAAAA==~M+5!+kY`rtb[MMW;w~XEb,2wcAAA==^#~@%>">
	<input type="hidden" name="txtSel1" id="txtSel1" value="<%=#@~^FAAAAA==~M+5!+kY`rYXO?VqJ*PkwYAAA==^#~@%>">
	<input type="hidden" name="txtSel2" id="txtSel2" value="<%=#@~^FAAAAA==~M+5!+kY`rYXO?V+J*PlAYAAA==^#~@%>">
	<input type="hidden" name="hidReport" id="hidReport" value="<%=#@~^FgAAAA==~M+5!+kY`rtb[IwGDDJb~jwcAAA==^#~@%>">
	<input type="hidden" name="intTableCnt" id="intTableCnt">
	<input type="hidden" name="intReportType" id="intReportType">
	<input type="hidden" name="strReportName" id="strReportName">
	<input type="hidden" id="hidSavedFrom" name="hidSavedFrom" value="<%=#@~^DwAAAA==.;;/D`JalTnJ*OwUAAA==^#~@%>">
</form>

<iframe name="ifrsaveReport" id="ifrsaveReport" style="display:none;width:300px;height:300px;" src="about:blank"></iframe>

<script language="javascript">
	//Returns the row the object is in.
	function whichrow(Si){
		var row = 0
		for(var y=0;y<document.all.save.length;y++){														// loops through all rows
			if(document.all.save[y].sourceIndex==Si)row = y ;												// checks the sourceindex against sourceindex Si passed to function
		}
		return row;
	}
	
	//Function to save the report. It gets the table number, the report type and and the report name passed to it
	//by the calling string.
	function saveRep(intTableCnt, intReportType, strReportName)
	{
		if (intReportType == "1")
		{
			strName = prompt("Save this report with the following name.","Static-" + strReportName + " <%=#@~^HgAAAA==WKDhmY9lYYbh+vxGhB\8sKxo9CY#zQsAAA==^#~@%>")
		}
		else
		{
			strName = prompt("Save this report with the following name.","Dynamic-" + strReportName + " <%=#@~^HgAAAA==WKDhmY9lYYbh+vxGhB\8sKxo9CY#zQsAAA==^#~@%>")
		}
		frmReportSave.intTableCnt.value = intTableCnt;
		frmReportSave.intReportType.value = intReportType;
		if (strName != null)
		{
			frmReportSave.strReportName.value = strName;
			frmReportSave.submit();
		}
	}

</script>

</div>

</body>

</html>
