<%@ Language=VBScript.Encode %>
<!--'===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<%#@~^DQEAAA==@#@&sG^ND1m:7d{dE?1Dnn	/zr@#@&alY4K.+6kXd7'7EcR&J@#@&2swsGH++r[idxdr!x(GJ@#@&Abo}.ND$Xidx7r?EMUls+J@#@&iqGdid7'7E`q9J@#@&?slss}DNn.~X7'iE`q9r@#@&Yl(V7didxdr1;OMkYbGxr@#@&;nUYMk1KC4sni'7JAhw^WznflOCr@#@&wmLd7id{dJgED.kDkGxcld2r@#@&jCVwk+^[ddid{dEYaO9lO+B9lD+E@#@&sEQAAA==^#~@%>
<%#@~^lAAAAA==@#@&dvqkDtPgl7PXa+SPZPhnmx/,OtPxm-romYbWUPrd,WW0B~F,:nC	/PO4PUl7rTlObW	PkkPKU@#@&dr0,/ndkkW	cJgl\:z2+r#,'~JqE,lUN,.+$EndD`JUUwEUmrb,'~rJ,Ytx@#@&dZywAAA==^#~@%>
		<script Language="JavaScript">
			newWin = window.open('newnav.asp?openhlp=<%=#@~^FAAAAA==~M+5!+kY`rWanx4V2J*P1AYAAA==^#~@%>','newWin','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=750,height=550');
			newWin.focus()
			parent.frames.Search.hideSearchHelp()
			parent.frames.Search.close();
		</script>
	<%#@~^DwAAAA==@#@&dn	N,k0@#@&XQIAAA==^#~@%>
<!-- #include file="..\includes\asp\nextprevtop.asp" -->
	<!--Tabs-->
	<td id='tabs' align='center' onclick="fnTabClick('0');"><a class='clsTabLink' href='javascript:void();' onclick= " this.blur(); return fnTabClick('0');" style='font-size: 9px'>&nbsp;Meals&nbsp;</a></td>
	<td id='tabs' align='center' onclick="fnTabClick('1');"><a class='clsTabLink' href='javascript:void();' onclick= " this.blur(); return fnTabClick('1');" style='font-size: 9px'>&nbsp;Protein&nbsp;</a></td>
	<td id='tabs' align='center' onclick="fnTabClick('2');"><a class='clsTabLink' href='javascript:void();' onclick= " this.blur(); return fnTabClick('2');" style='font-size: 9px'>&nbsp;Dairy&nbsp;</a></td>
	<td id='tabs' align='center' onclick="fnTabClick('3');"><a class='clsTabLink' href='javascript:void();' onclick= " this.blur(); return fnTabClick('3');" style='font-size: 9px'>&nbsp;Carbs&nbsp;</a></td>
	<td id='tabs' align='center' onclick="fnTabClick('4');"><a class='clsTabLink' href='javascript:void();' onclick= " this.blur(); return fnTabClick('4');" style='font-size: 9px'>&nbsp;Fruit/veg&nbsp;</a></td>
	<td id='tabs' align='center' onclick="fnTabClick('5');"><a class='clsTabLink' href='javascript:void();' onclick= " this.blur(); return fnTabClick('5');" style='font-size: 9px'>&nbsp;Fluid&nbsp;</a></td>
	<td id='tabs' align='center' onclick="fnTabClick('6');"><a class='clsTabLink' href='javascript:void();' onclick= " this.blur(); return fnTabClick('6');" style='font-size: 9px'>&nbsp;Supp&nbsp;</a></td>
	<td id='tabs' align='center' onclick="fnTabClick('7');"><a class='clsTabLink' href='javascript:void();' onclick= " this.blur(); return fnTabClick('7');" style='font-size: 9px'>&nbsp;Alcohol&nbsp;</a></td>
	<td id='tabs' align='center' onclick="fnTabClick('8');"><a class='clsTabLink' href='javascript:void();' onclick= " this.blur(); return fnTabClick('8');" style='font-size: 9px'>&nbsp;Fastfood&nbsp;</a></td>
	<td id='tabs' align='center' onclick="fnTabClick('9');"><a class='clsTabLink' href='javascript:void();' onclick= " this.blur(); return fnTabClick('9');" style='font-size: 9px'>&nbsp;Summary&nbsp;</a></td>
	<td style="width:100%" style='background-color: #F7F7F7;border-top: 1px solid #C0C0C0'>&nbsp;</td>
	</tr></table>
	
<div id="searchhelpdiv01" style="display:none;position:absolute;top:359px;left:300px;background-color:#FF3300;border: 1px solid black;color:white;width:210px">
	<img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px" align="left">
	2. Using the scroll bar, you can now scroll through your search results, add records to for players, update records, delete records or view a summary of the records. If you need help with this, <a href="#" onclick="window.open('../includes/asp/help/scrolling.asp?helptype=single','','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=650,height=500')" style="color:white; text-decoration:underline">click here</a>.
</div>

<div id="searchhelpdiv02" style="display:none;position:absolute;top:394px;left:105px;background-color:#FF3300;border: 1px solid black;color:white;width:150px;">
	4. To return back to the Navigation Wizard <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a>.
</div>

<div id="searchhelpdiv03" style="display:none;position:absolute;top:7px;right:210px;background-color:#FF3300;border: 1px solid black;color:white;">
	<table>
		<tr>
			<td style="color:white" align="right">This shows the available player records.</td>
			<td align="right" width="14px"><img src="../images/Navigation/arrr.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
	</table>
</div>

<div id="searchhelpdiv04" style="display:none;position:absolute;top:33px;left:20px;background-color:#FF3300;border: 1px solid black;color:white;width:203px">
	<img src="../images/Navigation/arrr.gif" alt="" border="0" style="width: 14px; height: 8px" align="right">
	More information can be added or seen by clicking on these tabs.
</div>

<div id="searchhelpdiv05" style="display:none;position:absolute;top:220px;left:100px;background-color:#FF3300;border: 1px solid black;color:white;width:150px;">
	<table>
		<tr>
			<td style="color:white">3. To view a report of the players nutrition details, click on the <strong>VIEW REPORT</strong> button.</td>
		</tr>
		<tr>
			<td align="center"><img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
	</table>
</div>

<form name=updater id=updater method=post action=''>
<table  width=190 border=0 cellspacing=0 cellpadding=0 style='position:absolute;left:98px;top:72px;'>
<tr><td><div align='right'>Player</div></td><td width='5'>&nbsp;</td><td><input name="txtemployeeno" class=txtboxes disabled ID="txtemployeeno" value="<%#@~^SAAAAA==./2Kxk+RqDbO+,Ddn^lznMflDC`rskMdO1m:JbP'~rPEPL~DknsCH+D9CDlcJU;MxCs+r#HhgAAA==^#~@%>"></td></tr>
<%#@~^BQEAAA==]/2Kxk+RSDbO+,J@!k	w;O,kN{vYXYMU(fEP	lh+xvD6OMUqGB~OHw+xv4k[NUEP-mV!+'EJL./8`EMx(9r#[rv@*r@#@&i]n/aW	/nRA.bYnP6r+^Ndn1`JESrNCYEBJ[mYYXa+rSFBJE~rNCOJ~r9lD+J*@#@&dEI/2WUdRADbO+,0rn^N/n^vJE~r[mYnr~rNlD+DzwJSFBJESrNlDnJBJfmOn[	4kwIrW'	4dwp$kMY4E*@#@&@#@&/E0AAA==^#~@%></table><table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:382px;top:72px;display:none"><%#@~^LAIAAA==@#@&d]/aWxk+c	DbYnPr@!O.@*@!Y9~mKV/aCU'E&EPdYzs'vhb[Y4)qTZ]B@*@!9k-@*_GSPhmxHPNmXk~@!(@*CPS+nV@!z4@*~NKPXK;~+mY=@!&Nr-@*@!&Y9@*@!JY.@*r@#@&7]/2W	dR	MkD+Pr@!D.@*@!Y[P1Wsdalx{v&EP/Dzs+{BSk[Y4l8!T]E@*[	4d2p@!zO[@*@!&YM@*r@#@&iI/wKxknRSDrYPWrVNknmvJJBE\+mVwDn;$.lV0rSJ9DG29WhUEB!SJkOHVn{BSkNDt=q ZBE~rJSELx4k2iLx4k2I[	4kwI[U8kwI[	8/ai'U(/wI'	4dwp$M+C00m/Yr#@#@&d"+dwKxdnchDbO+,0ks[/mvJE~E\lssMn;dEU^4J~E[MW2NKA	JSZ~r/YHVxBSk[Y4)q+ZBJBEJBJS!U^tr#@#@&7IndaWU/ hMkOn,0kns9/nmvEr~Et+mVsM+$jEawnDr~E[MWw9Gh	J~ZSE/DX^+xBAr9Y4)8+!EJSEr~Jj;awnDrb@#@&7@#@&raQAAA==^#~@%></table><table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:382px;top:72px;display:none"><%#@~^rwUAAA==@#@&d]/aWxk+c	DbYnPr@!O.@*@!Y9~mKV/aCU'E&EPdYzs'vhb[Y4)qTZ]B@*@!9k-@*_GSPhmxHPNmXk~@!(@*CPS+nV@!z4@*~hKEV9~zW!PlO)@!&9k-@*@!&Y9@*@!&DD@*E@#@&d]+k2KxdRqDkD+,E@!DD@*@!DN~^KV/aCx{B&E~dYHV'vhr[DtlFZT]E@*'U(/wI@!JY[@*@!&DD@*r@#@&dI/aGxk+ hMkOn,0ksNk+mvEE~rnMW/tr^0hrY4j3bxESrNDG29WAxrSZ~EkYHV+{BSrNDtlFy!vEBJJBEZ4km0nU[	4kwI`Q'	4dwpd3bxbE*@#@&7]/2W	dRAMkD+P6ksNk+^`rJSEhDW;4k13hbO4W!YU3rxESrN.Wa[WSxESZ~JdOHVn'EAbNO4)8 !EJBEJBJ/tbmVn	[x(dwp`OLU8/aik3rxbE*@#@&d"n/aWUdRh.rD+~0bn^NdmvJJBJh.Wt+CYSkO4wlYrSJ9DWa[Gh	JB!SJdOHVn'EAk9Y4l8 !vEBJE~r\lOLx(/wp`3'x(/2i6lObr#@#@&7I/wKUd+chMkO+~Wb+sNknmvJESrnDG\lOhbO4W;DsmYJBJ9.WaNGh	JSTBJ/DzV'BSr[Y4)8 TBESrJSJtnlD[U8kwicRLx8/aI6lO*J*@#@&iIdwKxd+ch.rD+P6r+^N/^cJr~rn.WorktE~r[DKw[GSxJSTBJdYHs'vSk9Yt=FyTBr~EJBJorktzDrx	+Nrb@#@&iI/2WUdRADbO+,0rn^N/n^vJE~rKMW3TokhkDtK;YwlOJBJ[.KwNKAxr~!BEdYHV'vhr[DtlFyTBr~EEBJ2LLk[U4k2p`RLx(/wpWbsz6lO#r#@#@&iI+k2W	/+cA.kD+,0r+s[k+^`rE~rn.GAoodAbY4smOr~E9DKwNKh	E~Z~E/DXsn{Bhb[Y4)FyTvJBJr~E2LLk[U4k2ivw.naBN'U(/2i3'	4dai6lY*J*@#@&iIn/aWUdRhMrYP0bnsNk+1`EJSEhDGZ4n+k+urTtsCOr~ENMGaNGSxr~!BJkOX^+xBSk[O4)FyTBr~JrSEZ4+/n[U8kwI`4ro4[U8kwiWCD#E#@#@&iInkwKx/RS.kD+~0b+s[k+mvEJBJnMG/t+k+JWAomYE~r[DKw[GSxJSTBJdYHs'vSk9Yt=FyTBr~EJBJ/4+/'x(/wpcsWS[	4dwIWmYbJ*@#@&iIndaWxdnch.kDn,0rV9/+1`rE~rn.Wd+L;s+/rSJ9DWa[Gh	JB!SJdOHVn'EAk9Y4l8 !vEBJE~rJo;s+kJ#@#@&i]+kwGxk+ AMkY~0b+V9dnmvJr~En.GhDGmd/N\nmYJSE9DGw9GSxEB!BJ/DX^n'EhrNDtlqy!BrSJr~Jh.Gm/k+[[U8kwI:CYr#@#@&i@#@&Y8EBAA==^#~@%></table><table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:382px;top:72px;display:none"><%#@~^wgIAAA==@#@&d]/aWxk+c	DbYnPr@!O.@*@!Y9~mKV/aCU'E&EPdYzs'vhb[Y4)qTZ]B@*@!9k-@*_GSPhmxHPm!wk~W6POtPWG^VWSrxTPNmr.X,0KW[/~@!(D@*NK~XKE~4m\+~@!(@*CP9CHg@!J4@*`F,m!2P{P+*Z:sb@!zNb-@*@!zY9@*@!zDD@*J@#@&7]/2W	d+c.rD+PE@!DD@*@!D[,mG^/alx{B2vPkYzV'vAbNY4lFZ!]E@*'x(/ai@!zO[@*@!&YM@*J@#@&7]/wGUk+ hMrD+~6kVNk+1cJr~Efmk\r^3s!sV;D+mhE~rNMW2NGA	JS!BE/DXsn{Bhr[DtlFyTEJSrJBJHbV0'x(/2iv0;s^mDC:*J#@#@&7I/aWU/n SDrY~0b+s[k+mcEr~Efmrtks0?0k:r~r[DKw[WSxESZ~JkOX^+'EArNDt=F+!vEBJE~r\k^3'U(/wIcy]S[	8kwIk3b:#r#@#@&d"+dwKxdnchDbO+,0ks[/mvJE~E9mkeWT4EMYESrNDG29WAxrSZ~EkYHV+{BSrNDtlFy!vEBJJBE5Kot!.OJ*@#@&d]+d2Kxd+cADbYn~6k+s[k+^`rEBJ9mktllkJBENMW2NKhUEB!~rdYHV+{vAk9Y4)q Tvr~EJBE[	4d2p[x8dai\lmdJ[U(/aiA!YDnDsks3r#@#@&@#@&K9MAAA==^#~@%></table><table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:382px;top:72px;display:none"><%#@~^DwMAAA==@#@&d]/aWxk+c	DbYnPr@!O.@*@!Y9~mKV/aCU'E&EPdYzs'vhb[Y4)qTZ]B@*@!9k-@*_GSPhmxHPNmXk~@!(@*2+MPAn3@!J8@*,NW,zGE,+mYl@!&[b\@*@!JON@*@!&OM@*J@#@&iIn/aG	/ncMkYPr@!YM@*@!Y9P^G^/wmU'E&B,dOX^+{BAk[O4)q!ZYB@*[U8kwi@!&DN@*@!JOM@*E@#@&iI+kwKU/RADbYn~6k+^[/m`rESJ;lMA.+C[r~ENMGw9WAUr~!SEkYzVxEhr9Y4)Fy!EE~rJSJ~DnC9J#@#@&d"+/aGU/RSDrYn~6knV9d+1`EEBJZC.hW.Db[T+EBJ9DWaNKAxr~T~r/Oz^+'EAk9Yt=q+!EJBJE~E'	4dwp'x(/2ILx4d2pnGDMr9onJmD+mVrb@#@&d]+kwGUk+RS.kD+P6rnV9/mcJESrZCDhGYmYGEBJN.GaNGh	EB!Sr/DXV'EAk9Y4)8 Tvr~JrSJhWYmOGJ*@#@&d]+d2Kxd+cADbYn~6k+s[k+^`rEBJ/mD"kmJBENMW2NKhUEB!~rdYHV+{vAk9Y4)q Tvr~EJBEIbmnE*@#@&7]/2W	dRAMkD+P6ksNk+^`rJSE;lDhC/DlJBE[DKw9WAxESZ~E/DzV'vAbNY4l8 TBrSrJSrnm/YmJ*@#@&iIn/aWUdRhMrYP0bnsNk+1`EJSE;l.?mhwr~E[MWw[GSxE~ZSr/OHV'BSk9Ot=F+!EJSEr~JUC:aJ#@#@&@#@&uOcAAA==^#~@%></table><table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:382px;top:72px;display:none"><%#@~^0QEAAA==@#@&d]/aWxk+c	DbYnPr@!O.@*@!Y9~mKV/aCU'E&EPdYzs'vhb[Y4)qTZ]B@*@!9k-@*_GSPhmxHPW6PD4+k+~NKPzG!P+mOP@!4@*-nDHP9lzg@!&(@*@!z9r\@*@!&O9@*@!&OM@*E@#@&7"+daW	/+cMrYPE@!DD@*@!DNP1GVkwl	xv&EPkYzVnxEhrND4)8!TYE@*[U8kwI@!JO9@*@!JYM@*J@#@&i]+kwGxk+ AMkY~0b+V9dnmvJr~Es.nktoD!rYr~E[MWw[GSxE~ZSr/OHV'BSk9Ot=F+!EJSEr~Jw.+kt[	8dwp0MErYEb@#@&7IdwKxdnchDrOPWks9/n1`rJ~rsMn/4.nor~E[MWw9Gh	J~ZSE/DX^+xBAr9Y4)8+!EJSEr~J'U(/2iLU(/2p[	4/aiLU4kwI[	4d2p[x(dwp.+TnOl(V/E#@#@&@#@&TYkAAA==^#~@%></table><table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:342px;top:72px;display:none"><%#@~^VQYAAA==@#@&d]/aWxk+c	DbYnPr@!O.@*@!Y9~mKV/aCU'E&EPdYzs'vhb[Y4)qTZ]B@*'	4dwp@!JY[@*@!JYD@*J@#@&d"+dwKxdncDbO+,J@!D.@*@!DN,mGVd2mxxB2vPkYzs'BAr9Y4)8TZ]v@*@!9k\@*CKAPslUX, XT,:V,2WMYkKUdPK0,Y4+~WKVsWSrxTP[G,XW;~9Drx0~@!4@*a+MPh+0@!z(@*l@!JNr-@*@!zD[@*@!zYM@*E@#@&d"+dwGUk+ hMrYPWrVNdn1`EJBEwV;bN:+l;W6W+JSJbx2;D4WXE~Z~JkOzV'EhrNO4=F+!E~:m6sn	oY4xrJ*JrEBJr	Yr~J:+m'x(/2iKD'U(/wp^W60+Eb@#@&d"+dwGUk+ hMrYPWrVNdn1`EJBEwV;bNUEomDanD;E2JBJrUaEY(G6r~!BEdYHV'vhr[DtlFyTB,:Ca^+xLO4'EJWErJSrk	YJBJgGRLx8/aiGWLx4k2iD+lk2GW	/Lx8/2IkELlM&m!wEb,@#@&7]/2W	dRAMkD+P6ksNk+^`rJSEwVEb[HbV3rSEk	w!Y8WaEB!SJkOX^+xvSkNO4=F+!E~sla^+	oY4'rEcrJE~rkUOr~JtrV0J#@#@&7I/aWU/n SDrY~0b+s[k+mcEr~Es^;bN\bV0/tm3E~rkUw!Y8GXJ~ZSJkYX^nxBSk9Y4)q+ZB~:maVxLO4'JE*rJE~rr	YEBJtkV0/4C3Jb@#@&d]nkwW	d+chDbOnP6kV[/n^vJE~roV!k[/K3+ESrkUw!O(War~Z~JkYHs+{BAk9Y4l8 !E~:m6VULY4'rJ*JEEBJrxDE~rPjG6YO[.bxV[	8kwIvmK3+BP6CxDl~+Dm br#@#@&7I/wKUd+chMkO+~Wb+sNknmvJESrsV;r9KC4rSrkUaED4WXJBT~r/OX^+xvSkND4)8 !E~hlXVxLY4xrJ*JrE~rkUOr~JJGSO^l^GMknLx(/wp/KWY N.k	3'U(/wpcYm4#rb@#@&iI/2WUdRADbO+,0rn^N/n^vJE~ro^Er9s^l\KEM	lD+.JBJrUaEY(G6r~!BEdYHV'vhr[DtlFyTB,:Ca^+xLO4'EJWErJSrk	YJBJw.EbY'x(/2I6Vl7GEM+NLU8/aiskU+.C^[U4k2iSlOnMJ#@#@&iIn/aG	/nchMkYP6r+^Nd+1`EEBJs^;k9?wK.O/GDbxVJSEbx2ED8WXJSTBJ/Oz^+xBSr9Y4=Fy!B,:maVxLY4'EEWJJrSJbxYrSE?aWMYd[U8kwIWM'x(/2Ix+.LH[U4k2pN.bx0J#@#@&i]+kwGxk+ AMkY~0b+V9dnmvJr~Ess;bN	lDnDr~Er	wEO8K6E~ZSr/OHV'BSk9Ot=F+!EPhCXV+	LY4'Jr*EJr~rkUYESrCY.~Lx8dai:rUDCVLU(/2pWM[x(/aI/KNC[	4d2phlDnDr#@#@&7]+kwKxd+ AMkO+,WkV[dm`EEBJoV!r9s.!kD9EbmE~rkUw!Y8GXJ~ZSJkYX^nxBSk9Y4)q+ZB~:maVxLO4'JE*rJE~rr	YEBJwDEbYLU4kwIL!k^nr#P@#@&@#@&@#@&2v4BAA==^#~@%></table><table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:342px;top:72px;display:none"><%#@~^cgUAAA==@#@&d]/aWxk+cADbYnP6kns9/+1cJr~JU;2w^+s+UYdEBJ[DK2NKhUEB!~EdDXs+{vSk[Dt=F ZBrSJr~EbM+'U(/wpzW![x(d2i1EMDnxOsH@!8D@*Pl0kULLx4d2plUXLU(/2p?!ww^+snxD/_J*@#@&7"+/aGxk+Rq.rYPr@!OD@*@!DN~mKs/alUxE&B~dDXs+{vSk[Dt=F!Z]E@*[	4dwp@!&O9@*@!JOD@*J@#@&7]+kwKxd+ 	MkO+,E@!DD@*@!DNP^G^/2l	xE&v,/DXV'EAk9Y4)8!TYE@*@!9r\@*q0,zn/BPaVnldn,/nV^Y,Y4n,lw2.Kw.kmOPG	+kP4VKAg@!z[k7@*@!&DN@*@!&YM@*J@#@&7I/aWU/n qDrY~J@!Y.@*@!YN~^KVdwmU{BfEPkYX^+{vhbNOt=FTTuB@*LU4kwi@!&ON@*@!JY.@*E@#@&d]+k2W	/n SDkOn,0r+^[k+^vJr~JUEa2nMWO+bxj4m3+kE~rNDK2[WSxr~T~EdDXs+{vhbNO4=F Tvr~EJBEhDGD+bx[	4k2iktC3/'U(/wpGDLx4k2IwMWD+rxR8m/nNLU4kwI.mW-nMX'x(dai[Mk	3/r#@#@&d"+dwKxdnchDbO+,0ks[/mvJE~Ej!w2bsrxKb^r9/JSE9DGw9GSxEB!BJ/DX^n'EhrNDtlqy!BrSJr~JzhrxK[	4dwIC1k[+JuH~zLs!Ylhr	+E#@#@&iInkwKx/RS.kD+~0b+s[k+mvEJBJ?!22ZM+mYrxnEBJ[DK2NKhUEB!~EdDXs+{vSk[Dt=F ZBrSJr~EZM+CObx+rb@#@&dId2W	/RADrOPWksNk+^crJ~Ej!w2.bOm:r	/r~J9DK2NKhUJB!SEkYX^n'Ehk9O4)8 ZBE~EEBJ#kDC:bxd'	4/2Imx[[	8kwItk	+DmVkE#@#@&7I/2G	/+cADbY+,Wr+^Nk+^`EEBJjEa2ZmD8Gr~J[.Kw[WSUr~TBJkYX^+{vhbNOt=F+TEJ~rE~rZlM8GtHNMlO+Eb@#@&7IdwKxdnchDrOPWks9/n1`rJ~r?!2wt+CV"+2smm+rSJ9DWa[Gh	JB!SJdOHVn'EAk9Y4l8 !vEBJE~r\lsLx(/wpI2Vmmn:xOdr#@#@&7I/wKUd+chMkO+~Wb+sNknmvJESr?E22wlOSKdkJSrNMWw9WSUJB!SJkYzs'BSrNDt)8+TBr~rJSJoCD[U4k2i^WddLx4d2p/;was:n	YkJ#@#@&i]+kwGxk+ AMkY~0b+V9dnmvJr~E?;2arOt.JBJ[.KwNGA	JS!BEkYz^+{BhbND4)8 TBr~EEBJ?!2w^+:UO/Lx(/2i6O4+.J*@#@&@#@&@#@&XrcBAA==^#~@%></table><table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:342px;top:72px;display:none"><%#@~^gwQAAA==@#@&d]/aWxk+c	DbYnPr@!O.@*@!Y9~mKV/aCU'E&EPdYzs'vhb[Y4)qTZ]B@*@!9k-@*w.K:~DtPVb/D~4VGhBPrU9kmmO+,tWS~hl	X@!4.@*[.bxV/,zW!P4C7+P@!8@*wnD,A+Vc@!J4@*@!z9r\@*@!&Y9@*@!&DD@*r@#@&iI+k2Gxk+c.kOn,J@!YM@*@!DN~^KV/2C	'v&E~kYz^+{BhbND4)8!T]E@*'U(/wp@!zDN@*@!&OD@*J@#@&7IndaWU/ hMkOn,0kns9/nmvEr~EzV1A+DrSJ9DGw9WAUr~!BE/DXVxvhbNDtlF+TEJSJrSJ~+n.Lx4d2pO'x(dai2DLx4kwp^l	JbP@#@&7]/wKU/RhMrO+,0b+sNdn1`EJBEb^mJrD+AnnMJSJ9.Kw[Kh	J~Z~rdYHVn'Ehr[Dt)8+!EJ~rESJLx(/2i'U(/2iLU4kwI'	4/2ILx8/aILx8kwp[x(/aI[	4dwp[U8kwiLU4kwiLU8/aiLx8/2ILx8/aISbYn'	4/2I~+nDLU(/2pOLx4kwp2+M[U4kwI^mxJ*@#@&iI+k2Gxk+ch.kOn,0r+^[/mcEr~J)s1rxEBJ[MWaNWSxrS!BJdYHVnxEhk9Ot=F ZvE~rJBJ	kUnLx8/aIOLx8daiwn.Lx8/aITVCk/r#P@#@&i]+kwGxk+ AMkY~0b+V9dnmvJr~Ebs^UwrDbO/r~E[MWw[GSxE~ZSr/OHV'BSk9Ot=F+!EJSEr~JU2kMkYk'U4kwpO'x8dai2+M'x(/2IDWYEb@#@&7IdaWUk+chDbY~0b+sNk+^crJ~r)V1Sk$;nEMJBJ[DG29WAxrS!BJdOHV+xvSk[Y4l8 TEJBJJBJdr;!+;DLx8daiOLU4kwian.[	4kwIYGOr#@#@&i]+kwGUk+R	.bYnPr@!DD@*@!Y9PmKVk2l	'v&EPdOHV+{vhbNY4lq!Z]E@*'x8dai@!zD[@*@!zO.@*J@#@&7"+dwKUk+ qDbY+,J@!OD@*@!ON,mGskwl	xB2BPkOzV'EhrNO4=FT!uv@*@!Nr-@*[x8dai'x(dai'	4kwiLx(dwp[U4kwI'	4/aI`8PYKO~', lPhVb@!JNr\@*@!zDN@*@!JYD@*E@#@&@#@&UF8BAA==^#~@%></table><table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:342px;top:72px;display:none"><%#@~^nAYAAA==@#@&d]/aWxk+c	DbYnPr@!O.@*@!Y9~mKV/aCU'E&EPdYzs'vhb[Y4)qTZ]B@*@!9k-@*&U,Y4P@!4@*alkOPDt.+PhG	YtkS@!J4@*,4Gh,W6Ynx~[bN~XK;PlO~mxX~G6POt~6Ws^WSkxT),8DlV0m/OS,VE	^t,WD,[rx	+MPGD@!&9k-@*@!&Y9@*@!&DD@*E@#@&d]+k2KxdRSDkD+,WkV[/mcEr~J:C3bhmzdJBJ9DGw[GSxE~ZSJkYzs'BAr9Y4)8+ZBEBJr~JDl0nOKEOP6DGh,lPMn/DlEMCUY,WMPWldO,0GW9~+kYC8^k/4hxORrb,@#@&iI/wKxknRqDrYPE@!DD@*@!ON,mW^d2l	'E&vPdOHVn'EAk9Y4l8!!Yv@*[U4k2p@!&DN@*@!zDD@*E@#@&d]+kwGUk+RS.kD+P6rnV9/mcJESr?Ul1V/r~E[MWw[GSxE~ZSr/OHV'BSk9Ot=F+!EJSEr~JGG[	4/aIzW![	4dwId	l^3LU4kwI8Yhnn	[U4k2p:nmVkgJ*@#@&7I/2W	/n qDkDnPr@!YM@*@!Y9P1Ws/2C	'v&E~/DXsn{Bhr[DtlFZTuB@*Lx(/wp@!JON@*@!&YM@*E@#@&dIdwKx/ 	DbYPE@!O.@*@!ON,^W^/2C	'Bfv,/OX^n{BAbNDt)8!ZYB@*@![k7@*P4bx3,G0,Yt~WWKN,XGE~nmY~k	~l,hnn0R@!8.@*PuWS~slUHP@!4@*9lHdPmPA+3@!&(@*P9GPHWE,nCY,Y4+d+_@!JNr\@*@!zDN@*@!JYD@*E@#@&7IdaWUk+chDbY~0b+sNk+^crJ~rjxmnlkO.k/r~EN.GaNGh	E~Z~EdDXVnxEhrND4=F+ZBr~Jr~rKlkY.k/S'	4/aImm3+rb@#@&iI/2WUdRADbO+,0rn^N/n^vJE~rj	luKY;tka/rSJ9DGw9WAUr~!BE/DXVxvhbNDtlF+TEJSJrSJ_WO'	4/2I1trwkE*@#@&iI/wKxknRSDrYPWrVNknmvJJBEjxmZMkdwdEBJ[DK2NKhUEB!~EdDXs+{vSk[Dt=F ZBrSJr~EnKYCOK[x(dwpmtb2dJ*@#@&d]+d2Kxd+cADbYn~6k+s[k+^`rEBJj	lgEYkJBENMW2NKhUEB!~rdYHV+{vAk9Y4)q Tvr~EJBE1!YdE*@#@&7]/2W	dRAMkD+P6ksNk+^`rJSEUxl&^+1D+mhE~rNMW2NGA	JS!BE/DXsn{Bhr[DtlFyTEJSrJBJq1+ ^Dlhz;DnCsJ#@#@&d"+/aGU/RSDrYn~6knV9d+1`EEBJ?UC;tGmKsmYnkJBJNMWa[WSxE~Z~EdDXVxBSkND4lFy!EJSJESrZ4W1GVmYndB[x8daiWE9L[U(/aiWM[	8/aiOW60nnkJ#@#@&d"+/aGU/RSDrYn~6knV9d+1`EEBJ?UCt+CYhrJSrNMWw9WSUJB!SJkYzs'BSrNDt)8+TBr~rJSJ\nmY'x(dwpwrnLx4d2pW.[	8kwIkl!/lT+LU4kwIDKVsE*@#@&i]+kwW	dnRSDbYnPWrV[/^`rJSEUxl$r^YGxTEBJ[MWaNWSxrS!BJdYHVnxEhk9Ot=F ZvE~rJBJ$ksOKxL[	8/aiG.Lx4d2pN.WAKDdr#@#@&@#@&iAwCAA==^#~@%></table>
</form>
<table style="Border:1px solid #CDCDCD;position:absolute;top:300;left:82;width:180;cursor:hand;background-color:#F3F7F8" onclick="showReport()" onmouseover="this.style.backgroundColor='#FFFFFF'" onmouseout="this.style.backgroundColor='#F3F7F8'"><tr><td><div style="text-align:center">View report</div></td></tr></table>
<%#@~^SAAAAA==./2Kxk+RSDbO+,/;:sl.zvJfmO+BI+aG.Y,ZmD8W4z9DCYSIwG.DPHnC^PoD5!+U1Xr#whoAAA==^#~@%>
<script>
	function showReport(){
		page='NutritionReport.asp?GenID=<%=#@~^DAAAAA==.kFcrMxqGJ*UgMAAA==^#~@%>&UID=<%=#@~^CgAAAA==.kFcrj&fJ*jQIAAA==^#~@%>'
		NewWin = window.open(page,'NutritionReport','menubar=no,resizable,scrollbars=yes,status=no,width=800,height=600');
		NewWin.focus();
	}
	spnPageHeader.innerHTML = "Nutrition"
	var tabcontrol=true
	var Sumcontrol=true
	if(Sumcontrol==true&&tabcontrol==false){subDet.style.display='Block'}
</script>
	<!-- #include file="..\includes\asp\Xscroll.asp" -->
<script>
	disabler('<%=#@~^EAAAAA==.kFcr4mm3SlM[/r#XQUAAA==^#~@%>,<%=#@~^EAAAAA==.kFcr4mm3k:msVr#VQUAAA==^#~@%>,<%=#@~^FAAAAA==.kFcr0KDhmD9d/slsVr#LAcAAA==^#~@%>,<%=#@~^DwAAAA==.kFcrsKDhmD9dJ*8wQAAA==^#~@%>,1,1,1,1')
	control.style.display = "block"
</script> 

<%#@~^YAAAAA==@#@&kW,///bW	cJgl-KHwnE*P',EFrPY4nU@#@&dk+s+^O,mC/~D;;nkY`EU	s;x1E*@#@&id1l/Prd+mD^tr@#@&7idehkAAA==^#~@%>
				<script Language="JavaScript">
					parent.frames.Search.openSearchScreen();
					parent.frames.Search.showSearchHelp()
					document.all.searchhelpdiv01.style.display = "";
					document.all.searchhelpdiv02.style.display = "";
					document.all.searchhelpdiv03.style.display = "";
					document.all.searchhelpdiv04.style.display = "";
					document.all.searchhelpdiv05.style.display = "";
				</script>
			<%#@~^GwAAAA==@#@&d71lk+Pr/CD1t+J@#@&77iAwUAAA==^#~@%>
				<script Language="JavaScript">
					parent.frames.Search.openSearchScreen();
					parent.frames.Search.showSearchHelp()
					document.all.searchhelpdiv01.style.display = "";
					document.all.searchhelpdiv02.style.display = "";
					document.all.searchhelpdiv03.style.display = "";
					document.all.searchhelpdiv04.style.display = "";
					document.all.searchhelpdiv05.style.display = "";
				</script>
			<%#@~^HQAAAA==@#@&dn	N,/+^+1O@#@&+UN,kW@#@&SwYAAA==^#~@%>

</body>
</html>
