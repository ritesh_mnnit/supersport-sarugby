<%@ Language=VBScript.Encode %>

<!-- #include file="..\includes\config\DB_Connectivity.asp" -->
<!-- #include file="..\includes\config\data.asp" -->
<%
Dim original
original			= SetLocale("en-gb")
table				= request("table")
UID				= request("UID")
Page				= request("Page")
ScreenType		= request("ScreenType")
intStartRec = request("intStartRec")

if intStartRec = "" then
	intStartRec = 0
end if

if inStr(lcase(Page),"playerrep.asp") then
	uKey = "GenID"
else
	uKey = "UID"
end if

'These next four are used to manage the order functionality.
OrderBy = request("OrderBy")

'ASC / DESC
OrderByDirection = request("OrderByDirection")
if OrderByDirection = "" then
	OrderByDirection = "ASC"
end if

'The name of the heading that you want to order by.
OrderByElement = request("OrderByElement")
if OrderByElement = "" then
	OrderByElement = "Player"
end if

'where statement built here
whereSQL = Request("whereSQL")

'where statement to pass
PasswhereSQL = whereSQL

scrollID = request("hidScrollID")
scrollRecord = request("hidScrollRec")

if scrollRecord = "" then
	scrollRecord = "Player"
end if

ScrollDirection = request("direction")

'response.write "EXEC [dbo].[SUMMARYVIEW] '"& scrollID &"','"& scrollRecord &"','20','"& OrderByElement &"','"& OrderByDirection &"','"& ScrollDirection &"','"& table &"','','"& whereSQL &"'"
'response.end

'ScrollID - Ukey of the previous page (very first recs actual value) e.g. "12345"
'ScrollRecord - First record value of the order by column eg. "Smith"
'ScrollOrder - E.g. "Surname"
'ScrollOrderType - E.g. "ASC" (default is ASC)
'ScrollDirection - E.g. "forward"
'Table
'StrSQLWhere

strSQL = "EXEC [dbo].[SUMMARYVIEW] '"& scrollID &"','"& scrollRecord &"','20','"& OrderByElement &"','"& OrderByDirection &"','"& ScrollDirection &"','"& table &"','','"& whereSQL &"'"

set rsCols = con1.execute(strSQL)
if rsCols.eof = false then
	scrollID = rsCols("ID")
	if OrderByElement <> "" then
		scrollRecord = rsCols(OrderByElement)
	end if
end if
%>
<HTML>
<HEAD>
	<script LANGUAGE='javascript' SRC='../Includes/js/jsValidator.js'></script>
	<link REL='stylesheet' HREF='../includes/css/querybuilder.css' type='text/css'>
<style>
	@media print {
	    TABLE{width:100%}
	}
</style>
</HEAD>
<BODY>

<form name=frmScroll id=frmScroll method=post action="<%=page%>?from=Summary">
	<input id="hidCurEmpID" name='hidCurEmpID' type='hidden' value="">
	<input id="hidSQL" name='hidSQL' type='hidden' value="<%=Request("whereSQL")%>">
	<input id='hidTab' name='hidTab' type='hidden' value="">
	<input id='direction' name='direction' type='hidden' value=''>
	<input id='typescroll' name='typescroll' type='hidden' value=''>
	<input id='UID' name='UID' type='hidden' value=''>
	<input id='hidValue' name='hidValue' type='hidden' value=''>
	<input type="hidden" name="nnFunc" id="nnFunc" value="<%= request("nnFunc") %>">
</form>

<form name="frmSummary" id="frmSummary" method="post" action="summary.asp" style="display:none">
	Table
	<input type="text" id="table" name="table" value="<%=table%>">
	Page
	<input type="text" id="Page" name="Page" value="<%=Page%>">
	ScreenType
	<input type="text" id="ScreenType" name="ScreenType" value="<%=ScreenType%>">
	OrderBy
	<input type="text" id="OrderBy" name="OrderBy" value="<%= OrderBy %>">
	OrderByDirection
	<input type="text" id="OrderByDirection" name="OrderByDirection" value="<%= OrderByDirection %>">
	OrderByElement
	<input type="text" id="OrderByElement" name="OrderByElement" value="<%= OrderByElement %>">
	whereSQL
	<input type="text" id="whereSQL" name="whereSQL" value="<%=PasswhereSQL%>">
	UID
	<input type="text" id="UID" name="UID" value="<%=UID%>">
	nnFunc
	<input type="text" name="nnFunc" id="nnFunc" value="<%= request("nnFunc") %>">
	hidCurEmpID
	<input type="text" id="hidCurEmpID" name="hidCurEmpID" value="<%= request("hidCurEmpID") %>">
	hidValue
	<input type="text" id="hidValue" name="hidValue" value="<%= request("hidValue") %>">
	hidScrollID
	<input type="text" id="hidScrollID" name="hidScrollID" value="<%= scrollID %>">
	hidScrollRec
	<input type="text" id="hidScrollRec" name="hidScrollRec" value="<%= scrollRecord %>">
	direction
	<input type="text" id="direction" name="direction" value="">
	intStartRec
	<input type="text" id="intStartRec" name="intStartRec" value="<%= intStartRec %>">
</form>

<script language="javascript">
	function mouv(cell){cell.style.backgroundColor= "#ecf2f2"}
	function mout(cell){cell.style.backgroundColor= ""}
</script>
<%
if rsCols.eof then
	strFromUrl = request("Page")
	Response.Write "<table align=center onclick=""window.location='" & strFromUrl & "'"" style='cursor:hand;'><tr><td style='text-align:center' class=cellhere style='border:0px'>No records have been created for this screen.</td></tr><tr><td style='text-align:center;border:0px;' class=cellhere>Click here to go back to Details Screen.</td></tr></table>"
	Response.End
else
	'response.write "SELECT COUNT("& uKey &") FROM "& table &" Empdata "& Replace(whereSQL,"''","'")
	'response.end
	Dim totalRecords
	Set countData = Con1.Execute("SELECT COUNT("& uKey &") FROM "& table &" Empdata "& Replace(whereSQL,"''","'"))
	totalRecords = countData(0)
	Set countData = Nothing
end if
alldata=rsCols.getrows
numcols=ubound(alldata,1)
numrows=ubound(alldata,2)




if Session("NavType") = "1" then
	intTop = "20px"
	%>
		<div id="summaryhelpdiv01" style="background-color:#FF3300;border: 1px solid black;color:white;width:540px">
			Welcome to the summary view page. This page displays a summary of your search results, or a summary of all the records available, depending
			on when you selected to view the page. These are the things you can do from this page:
			
			<ol>
				<li><strong>Selecting a record to view / edit</strong> - when you move your mouse over a record, it gets highlighted and you can select to view the details of that specific record by simply clicking on it.</li>
				<li><strong>Sorting the results</strong> - To sort the results on this page simply click on one of the headings, the results will then be displayed sorted by that heading. By clicking the heading again, the results will then be shown sorted in descending order for that heading.</li>
			</ol>
			
			To return back to the screen you came from either select a player or click on the Back button on the menu bar directly above this box. To return to the Navigation <a href="playerrep.asp" style="color:white; text-decoration:underline"><strong>click here</strong></a>.
		</div>
	<%
else
	intTop = "-10px"
end if
%>

<table cellspacing=0 style='BORDER-BOTTOM: 1px solid;BORDER-RIGHT: 1px solid;position:relative;top:<%= intTop %>;left:-5px' align="center"><tr><td>
<Table cellpadding=4 cellspacing=0 border=0 bordercolor=#999999 style='BORDER-BOTTOM: 1px solid;BORDER-RIGHT: 1px solid'>
<tr><th class=cellhere style="background-color:#4A667A;color:#EEEEEE;text-align:left" colspan=<%=numcols%>>&nbsp;&nbsp;Summary View &nbsp; 

<%
	if intStartRec + 20 > totalRecords then
		intEndRec = totalRecords
	else
		intEndRec = intStartRec + 20
	end if
%>

<% if intStartRec > 0 then %>
<a href="#" onclick="jsScroll('backward');" style="color:white; text-decoration:none;">&laquo;</a> 
<% end if %>

Records <%= intStartRec + 1 %> to <%= intEndRec %> of <%= totalRecords %> 

<% if intStartRec + 20 < totalRecords then %>
<a href="#" onclick="jsScroll('forward');" style="color:white; text-decoration:none;">&raquo;</a> 
<% end if %>

</th></tr>
<tr>
<%
' Now lets grab all the records
for each whatever in rsCols.fields
	if ucase(whatever.name) <> "GENID" and ucase(whatever.name) <> "UID" then
		strImageName = replace (whatever.name," ","") 
		response.Write "<th onmouseover='mouv(this)'  onmouseout='mout(this)' style='cursor:hand' class=cellhere onclick=""clicky('"& whatever.name &"')"" valign='top'>"
		response.Write ucase(whatever.name)
		response.Write "<img name='" & strImageName & "Arrow' id='" & strImageName & "Arrow' src='../images/arrow.blank.gif'></Th>"
	end if
next
Response.Write "</tr>"

shownull = "&nbsp;"

FOR rowcounter=0 TO numrows
	Response.Write "<tr style='cursor:hand' onmouseover=this.style.backgroundColor='#FFFCFC' onmouseout=this.style.backgroundColor='' onclick=""fe('" & alldata(1,rowcounter) & "')"">"
	FOR colcounter = 1 to numcols
		thisfield=alldata(colcounter,rowcounter)
		if isnull(thisfield) or trim(thisfield)="" then thisfield=shownull
		response.write "<td>"
		response.Write left(replace(thisfield, chr(10), "<Br>"), 100)
		response.Write "</td>"
	NEXT
	response.write "</tr>"
NEXT
response.write "<tr><td style='background-color:#BFCAD2; text-align:right' class=cellhere colspan='"&numcols+1&"'>"& numrows + 1 &" Records</td></tr></table></td></tr></table><br><br>" 
%>
</Body>
<script language=javascript>
var intOrderByFlag  = 0;
<%
	if OrderByDirection = "ASC" then
		response.write "intOrderByFlag  = 0"
	else
		response.write "intOrderByFlag  = 1"
	end if
%>

function fe(uid){
<%
if Request("From")="View" then
%>
	frmScroll.hidSQL.value=''
	frmScroll.wheresql.value=''
<%
end if
%>
	alert(uid);
	frmScroll.UID.value=uid
	frmScroll.submit()
}

function clicky (type)
{
	if (type=="ID")
	{
		alert("You cannot order by the ID column, please select another column to order by!");
	}
	else
	{
	    var element = removeSpaces(type);
		if (type == frmSummary.OrderByElement.value)
		{
			if (intOrderByFlag == 0)
			{
				frmSummary.OrderByDirection.value = "DESC";
				intOrderByFlag = 1;
			}
			else
			{
				frmSummary.OrderByDirection.value = "ASC";
				intOrderByFlag = 0;
			};
		}
		else
		{
			frmSummary.OrderByDirection.value = "ASC";
			intOrderByFlag = 0;
		};
		frmSummary.OrderBy.value = type
		frmSummary.OrderByElement.value = element
		frmSummary.direction.value = "";
		frmSummary.intStartRec.value = "";
		frmSummary.hidScrollID.value = "";
		frmSummary.hidScrollRec.value = "";
		frmSummary.UID.value = "";
		frmSummary.submit()
	};
}


function ArrowImage(){
	if ("<%=OrderByDirection%>"=="DESC"){
		<%=OrderByElement%>Arrow.src = "../images/arrow.up.gif"
	}
	else
	{
		<%=OrderByElement%>Arrow.src = "../images/arrow.down.gif"
	}
}
//this closes the search frame if that frame was open.
if (parent.parent.document.all.sclick.rows == "63,*"){
	parent.parent.document.all.sclick.rows = "19,*";
	parent.parent.frames.Search.document.all.searchhelpdiv01.style.display = "none";
	parent.parent.frames.Search.document.all.searchystuffy.style.display='none';	
	parent.parent.frames.Search.document.all.gb.src="../../images/down.gif";
	parent.parent.frames.Search.document.all.gb.alt = "Display Search Screen"
}

function jsScroll(strDirection)
{
	if (strDirection == 'backward')
	{
		frmSummary.intStartRec.value = "<%= intStartRec - 20 %>";
	}
	else
	{
		frmSummary.intStartRec.value = "<%= intStartRec + 20 %>";
	};
	frmSummary.direction.value = strDirection;
	//alert(frmSummary.direction.value);
	frmSummary.submit();
};

<%if OrderByElement <> "" then response.write"ArrowImage()" %>
</script>

</HTML>