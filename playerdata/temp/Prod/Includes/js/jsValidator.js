/*=============================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================*/
var id = false
function check(objName,inpType){
	switch (inpType) {
		case "date":
			if (chkdate(objName) == false) {
				strAlert = "That date is invalid.  Please try again.";
			}
			else {
				return true;
			}
			break;

		case "id":
			if (chkIDNum(objName) == false) {
				strAlert = "That ID Number is invalid.  Please try again.";

			}
			else {
			    
		    objName.title =""
			objName.style.backgroundColor=""
				return true;
			}
			break; 

		case "int":  
			if (chkInt(objName) == false) {
				strAlert = "The value you entered is not a whole number. Please try again.";
			}
			else {
				return true;
			}
			break;

		case "float":  
			if (chkFloat(objName) == false) {
				strAlert = "The value you entered is not a number.  Please try again.";
			}
			else {
				return true;
			}
			break;

		case "cell":  
			if (cellnmber(objName) == false) {
				strAlert = "The value you entered is not a valid cellphone number number.  Please try again.";
			}
			else {
				return true;
			}
			break;

		case "number":  
			if (gn(objName) == false) {
				strAlert = "The value you entered is not a valid number.  Please try again.";
			}
			else {
				return true;
			}
			break;
		case "lengt5000":  
			if (len5000(objName) == false) {
				strAlert = "The value you entered is too long ("+objName.value.length+"). \n \n Only 5000 characters can be saved in this field.";
			}
			else {
				return true;
			}
			break;

		case "lengt500":  
			if (len500(objName) == false) {
				strAlert = "The value you entered is too long ("+objName.value.length+"). \n \n Only 500 characters can be saved in this field.";
			}
			else {
				return true;
			}
			break;

		case "lengt50":  
			if (len50(objName) == false) {
				strAlert = "The value you entered is too long ("+objName.value.length+"). \n \n Only 50 characters can be saved in this field.";
			}
			else {
				return true;
			}

			break;
		case "email":  
			if (chkEmail(objName) == false) {
				strAlert = "The value you entered is not a valid email address.  Please try again.";
			}
			else {
				return true;
			}
			break;
		
	}
		if (strAlert.length > 0){
			objName.select();
			alert(strAlert);
			objName.focus();
			return false;
		}
}

function removeSpaces(s)
{   
	var whitespace = " \f\n\r\t";
	var i;
    for (i = 0; i < s.length; i++)
		{   
			var c = s.charAt(i);
			if (whitespace.indexOf(c) != -1) {s = s.slice(0,i)+s.slice(i+1);i=i-1};
		}
    return s;
}


/*--------------------------------------------
-------------- Email Validator ---------------
--------------------------------------------*/

function chkEmail(objName)
{
        var strEmail = removeSpaces(objName.value)
       	objName.value = strEmail
       	if (strEmail.length == 0) return true;
        var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$/;
        if (filter.test(strEmail)) return true;
        else return false;
}


/*--------------------------------------------
------------- IDNumber validator -------------
--------------------------------------------*/

function chkIDNum(objName) {
	var strIDNum = removeSpaces(objName.value)
	objName.value = strIDNum
	if (strIDNum.length == 0) return true;
	if(strIDNum.length != 13){
		return false;
	}
	var row1 = 0
	var row2 = 0
	for (i = 0; i < strIDNum.length-1; i++) {
		row2 = row2 + parseInt(strIDNum.charAt(i))

		i = i + 1

		intCurr = strIDNum.charAt(i)*2
		if (eval("'"+intCurr+"'").length == 2){
			intCurr = parseInt(eval("'"+intCurr+"'").charAt(0)) + parseInt(eval("'"+intCurr+"'").charAt(1))
		}
		row1 = row1 + parseInt(intCurr)
	}
	
	if (parseInt(strIDNum.charAt(12)) != ((Math.ceil(((row1+row2)/10))*10)-(row1+row2))){
		return false;
	}
}

/*--------------------------------------------
------------- Integer validator --------------
--------------------------------------------*/

function chkInt(objName) {
	var strNum = removeSpaces(objName.value)
	objName.value = strNum
	if (strNum.length == 0) return true;
	if (isNaN(parseInt(strNum))||(parseInt(strNum))!=strNum){
		return false;
	}else{
		objName.value = parseInt(strNum);
	}
}

/*--------------------------------------------
--------- Floating Number validator ----------
--------------------------------------------*/

function chkFloat(objName) {
	var strNum = removeSpaces(objName.value)
	if ((strNum.indexOf(",") == strNum.lastIndexOf(","))&&(strNum.length - strNum.indexOf(",")<= 3)){
			strNum = strNum.replace(/,/g,'.');
	}
	if ((strNum.length - strNum.lastIndexOf(",")> 3)){
		strNum = strNum.replace(/,/g,'');
	}
	objName.value = strNum
	if (strNum.length == 0) return true;
	if (isNaN(parseFloat(strNum))||(parseFloat(strNum))!=strNum){
		return false;
	}else{
		objName.value = parseFloat(strNum);
	}
}


/*-------------------------------------------
---------Field Length Validation-------------
---------------------------------------------*/

function len5000(objName){
	var strTemp=objName.value
	if (strTemp.length>5000){			
		return false;	
	}else{
		objName.value = strTemp
		return true;	
	}
}

function len500(objName){
	var strTemp=objName.value
	if (strTemp.length>500){			
		return false;	
	}else{
		objName.value = strTemp
		return true;	
	}
}

function len50(objName){
	var strTemp=objName.value
	if (strTemp.length>50){			
		return false;	
	}else{
		objName.value = strTemp
		return true;	
	}
}

/*-------------------------------------------
-------------Cellnumber Validation-----------
---------------------------------------------*/

function cellnmber(objName){
	var number  =removeSpaces(objName.value)
	var chck = true		
	if (isNaN(number)){			
		chck = false	
	}
	if(number.length != 10&&number.length!= 0){
		chck = false			
	}
	if(chck==false){
		return false;
	}else{
		objName.value =number
		return true	;	
		}
	}



/*---------------------------------------------
-------------- General Number------------------
-----------------------------------------------*/
function gn(objName){
	var number  =removeSpaces(objName.value)
	if (isNaN(number)){			
		return false;	
	}else{
		objName.value = number
		return true;	
	}
}


/*--------------------------------------------
---------------- Date validator --------------
--------------------------------------------*/

function chkdate(objName) {
	//var strDatestyle = "US"; //United States date style
	var strDatestyle = "EU";  //European date style
	var strDate;
	var strDateArray;
	var strDay;
	var strMonth;
	var strYear;
	var intday;
	var intMonth;
	var intYear;
	var datefield = objName;
	var strSeparatorArray = new Array("-"," ","/",".");
	var intElementNr;
	var err = 0;
	var strMonthArray = new Array(12);
	strMonthArray[0] = "January";
	strMonthArray[1] = "February";
	strMonthArray[2] = "March";
	strMonthArray[3] = "April";
	strMonthArray[4] = "May";
	strMonthArray[5] = "June";
	strMonthArray[6] = "July";
	strMonthArray[7] = "August";
	strMonthArray[8] = "September";
	strMonthArray[9] = "October";
	strMonthArray[10] = "November";
	strMonthArray[11] = "December";
	strDate = datefield.value;
	if (strDate.length < 1) {
		return true;
	}
	strDateArray = new Array()
	for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++) {
		if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1) {
			strDateArray = strDate.split(strSeparatorArray[intElementNr]);
			if ((strDateArray.length != 2)&&(strDateArray.length != 3)) {
				err = 1;
				return false;
			}else if(strDateArray.length == 2){
				var now = new Date();
				strDateArray[2] = now.getFullYear();
				strDay = strDateArray[0];
				strMonth = strDateArray[1];
				strYear = String(strDateArray[2]);
			}else{
				strDay = strDateArray[0];
				strMonth = strDateArray[1];
				strYear = strDateArray[2];
			}
		}
	}
	
	if((strDateArray.length == 0)&&(strDate.length>5)){
			strDay = strDate.substr(0, 2);
			strMonth = strDate.substr(2, 2);
			strYear = strDate.substr(4);
	}else if (strDateArray.length != 3) {
		err = 1;
		return false;
	}
	
	if (strYear.length == 2) {
		strYear = '20' + strYear;
	}else if(strYear.length != 4){
		return false;
	}
// US style
	if (strDatestyle == "US") {
		strTemp = strDay;
		strDay = strMonth;
		strMonth = strTemp;
	}
	intday = parseInt(strDay, 10);
	if (isNaN(intday)) {
		err = 2;
		return false;
	}
	intMonth = parseInt(strMonth, 10);
	if (isNaN(intMonth)) {
		for (i = 0;i<12;i++) {
			if (strMonth.substr(0,3).toUpperCase() == strMonthArray[i].substr(0,3).toUpperCase()) {
				intMonth = i+1;
				strMonth = strMonthArray[i];
				i = 12;
			}
		}
		if (isNaN(intMonth)) {
			err = 3;
			return false;
		}
	}
	intYear = parseInt(strYear, 10);
	if ((isNaN(intYear))||(intYear!=strYear)) {
		err = 4;
		return false;
	}
	if (intMonth>12 || intMonth<1) {
		err = 5;
		return false;
	}
	if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) {
		err = 6;
		return false;
	}
	if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) {
		err = 7;
		return false;
	}
	if (intMonth == 2) {
		if (intday < 1) {
			err = 8;
			return false;
		}
		if (LeapYear(intYear) == true) {
			if (intday > 29) {
				err = 9;
				return false;
			}
		}
		else {
			if (intday > 28) {
				err = 10;
				return false;
			}
		}
	}
	
	if((intYear < 1900)||(intYear > 2079)||(intYear == 2079 && intMonth > 6)||(intYear == 2079 && intMonth == 6 && intday > 6)){
		err = 1;
		return false;
	}
		
	if (strDatestyle == "US") {
		datefield.value = strMonthArray[intMonth-1] + " " + intday+" " + intYear;
	}
	else {
		datefield.value = intday + " " + strMonthArray[intMonth-1] + " " + intYear;
	}
	return true;
}

function LeapYear(intYear) {
	if (intYear % 100 == 0) {
		if (intYear % 400 == 0) { return true; }
	}
	else {
		if ((intYear % 4) == 0) { return true; }
	}
		return false;
}

