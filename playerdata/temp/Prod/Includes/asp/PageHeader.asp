<script language='JavaScript'>
function butover(obj){obj.style.backgroundColor='#F5F5F5'}
function butout(obj){obj.style.backgroundColor='#FFFFFF'}

function ddl(table){
	page='../includes/asp/Dropdown.asp?Listboxes=' + table
	NewWin = window.open(page,'DDList','toolbar=no,menubar=no,0,status=no,width=276,height=285');
	NewWin.focus();
}

function ddlSquad(strSquad){
	page='../includes/asp/SquadSearch.asp?squadno=' + strSquad
	NewWin = window.open(page,'DDList','toolbar=no,menubar=no,resizable=no,status=no,width=405,height=176');
	NewWin.focus();
}

function limitLength(obj,intLength){
	if (obj.value.length > intLength){
		obj.value = obj.value.substring(0,intLength)
	}
}

function calcAge(tocheck,toreturn)
{
	if (chkdate(tocheck) == true)
	{
		if (tocheck.value != "")
		{
			var strSepArr = new Array("-"," ","/",".");
			var intElmtNo;
			var strDateArr;
			var strCurrDate = "<%=#@~^EgAAAA==~GlO.mVE`gGhv#bPkQUAAA==^#~@%>";
			var strDOB = tocheck.value;
			var intDOBDay;
			var intDOBMonth;
			var intDOBYear;
			var strDOBYear;
			var intCurrDay;
			var intCurrMonth;
			var intCurrYear;
			var intAgeDay;
			var intAgeMonth;
			var intAgeYear;
			var intDays;
			var intFlag = 0;
			
			strDOBDateArr = new Array();
			strCurrDateArr = new Array();

			for (intElmtNo = 0; intElmtNo < strSepArr.length; intElmtNo++)
			{
				if (strDOB.indexOf(strSepArr[intElmtNo]) != -1)
				{
					strDOBDateArr = strDOB.split(strSepArr[intElmtNo]);
					//alert(strDateArr.length)
					if (strDOBDateArr.length == 3) 
					{
						intDOBDay = parseInt(strDOBDateArr[0]);
						strDOBMonth = strDOBDateArr[1].toLowerCase();
						intDOBYear = parseInt(strDOBDateArr[2]);
					};
				};
			};

			//alert(intDOBDay + " - " + strDOBMonth + " - " + intDOBYear);
			
			switch (strDOBMonth)
			{
				case "january":
					intDOBMonth = 1;
					intDays = 31;
					break;
				case "february":
					intDOBMonth = 2;
					intDays = 28;
					break;
				case "march":
					intDOBMonth = 3;
					intDays = 31;
					break;
				case "april":
					intDOBMonth = 4;
					intDays = 30;
					break;
				case "may":
					intDOBMonth = 5;
					intDays = 31;
					break;
				case "june":
					intDOBMonth = 6;
					intDays = 30;
					break;
				case "july":
					intDOBMonth = 7;
					intDays = 31;
					break;
				case "august":
					intDOBMonth = 8;
					intDays = 31;
					break;
				case "september":
					intDOBMonth = 9;
					intDays = 30;
					break;
				case "october":
					intDOBMonth = 10;
					intDays = 31;
					break;
				case "november":
					intDOBMonth = 11;
					intDays = 30;
					break;
				case "december":
					intDOBMonth = 12;
					intDays = 31;
					break;
			};

			for (intElmtNo = 0; intElmtNo < strSepArr.length; intElmtNo++)
			{
				if (strCurrDate.indexOf(strSepArr[intElmtNo]) != -1)
				{
					strCurrDateArr = strCurrDate.split(strSepArr[intElmtNo]);
					//alert(strDateArr.length)
					if (strCurrDateArr.length == 3) 
					{
						intCurrDay = parseInt(strCurrDateArr[1]);
						intCurrMonth = parseInt(strCurrDateArr[0]);
						intCurrYear = parseInt(strCurrDateArr[2]);
					};
				};
			};

			//intCurrDay = 1;
			//intCurrMonth = 1;

			//alert(intDOBDay + " - " + intDOBMonth + " - " + intDOBYear);
			//alert(intCurrDay + " - " + intCurrMonth + " - " + intCurrYear);
			
			intFlag = 0;
			
			if (intDOBYear > intCurrYear)
			{
				intFlag = 1;
			}
			else
			{
				if (intDOBYear == intCurrYear)
				{
					if (intDOBMonth > intCurrMonth)
					{
						intFlag = 1;
					}
					else
					{
						if (intDOBMonth == intCurrMonth)
						{
							if (intDOBDay > intCurrYear)
							{
								intFlag = 1;
							};
						};
					};
				};
			};
			
			if (intFlag == 1)
			{
				updater.elements[toreturn].value = "";
			}
			else
			{
				intAgeYear = intCurrYear - intDOBYear;
				intAgeMonth = intCurrMonth - intDOBMonth;
				intAgeDay = intCurrDay - intDOBDay;
				
				if (intAgeDay < 0)
				{
					intAgeMonth = intAgeMonth - 1;
					intAgeDay = intAgeDay + intDays;
				};
				
				if (intAgeMonth < 0)
				{
					intAgeYear = intAgeYear - 1;
					intAgeMonth = intAgeMonth + 12;
				};
				
				//alert(intAgeYear);
				
				switch (intAgeYear)
				{
					case 8:
						updater.elements[toreturn].value = "2";
						break;
					case 9:
						updater.elements[toreturn].value = "3";
						break;
					case 10:
						updater.elements[toreturn].value = "4";
						break;
					case 11:
						updater.elements[toreturn].value = "5";
						break;
					case 12:
						updater.elements[toreturn].value = "6";
						break;
					case 13:
						updater.elements[toreturn].value = "7";
						break;
					case 14:
						updater.elements[toreturn].value = "8";
						break;
					case 15:
						updater.elements[toreturn].value = "9";
						break;
					case 16:
						updater.elements[toreturn].value = "10";
						break;
					case 17:
						updater.elements[toreturn].value = "11";
						break;
					case 18:
						updater.elements[toreturn].value = "12";
						break;
					case 19:
						updater.elements[toreturn].value = "13";
						break;
					case 20:
						updater.elements[toreturn].value = "14";
						break;
					default:
						updater.elements[toreturn].value = "";
				};

				if (intAgeYear < 8)
				{
					updater.elements[toreturn].value = "1";
				};
				
				if ((intAgeYear > 20)&&(intAgeYear < 23))
				{
					updater.elements[toreturn].value = "15";
				};

				if (intAgeYear > 22)
				{
					updater.elements[toreturn].value = "16";
				};

			};
			
		};
	};
};

function openDrpWin(strType)
{
	page='../includes/asp/drpSearch.asp?type=' + strType;
	NewWin = window.open(page,'DDList','toolbar=no,menubar=no,resizable=no,status=no,width=405,height=176');
	NewWin.focus();
}

//alert(location.href.toLowerCase().search("ifr.asp"))
if(location.href.toLowerCase().search("ifr.asp") == -1)
{
	parent.frames.Search.close();
};

</script>
	<link REL='stylesheet' HREF='<%=#@~^CgAAAA==2mY4hD0kXGwQAAA==^#~@%>Includes/css/tabs.css' type='text/css'>
	<link REL='stylesheet' HREF='<%=#@~^CgAAAA==2mY4hD0kXGwQAAA==^#~@%>Includes/css/general.css' type='text/css'>
	<script LANGUAGE='javascript' SRC='<%=pathPrefix%>Includes/js/jsValidator.js'></script>
	<%#@~^SgAAAA==r6PoMls+P@!@*,EsMlh+rPCU9PYm8VP@!@*~EqkWd+LJ~C	N~Ym8VP@!@*,JqdGzDhJ,O4+UEhUAAA==^#~@%>
	</head>
	
<body style="display:none" name="MainBody" id="MainBody" topmargin='0' leftmargin='0' background='<%=#@~^CgAAAA==2mY4hD0kXGwQAAA==^#~@%>/../images/eebg.gif' <%#@~^IAAAAA==~b0~alT+P{PrKVmXnD"+2 m/wr~Y4+x,YgoAAA==^#~@%> onload="setIns(); <%#@~^OgAAAA==~b0~bxDZVl	oVmo~',F~O4+x,.+kwW	dnRSDbYnPE^^+CxvbirPnU9PkWVRMAAA==^#~@%>" <%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>>
	<%#@~^WAAAAA==r6P2moP@!@*Prj+1RC/aJ~C	NPaCoP@!@*~EHm/kSGl[nMRC/aEPmx[~alon~@!@*~JU^4WG^?mRm/aEPDtnxVBoAAA==^#~@%>
	<!-- Background Squares-->
	<table width='315' height='326' border='0' cellpadding='10' style='border: 1px #CCCCCC solid' style='position:absolute;top:60px;left:8px;display:block;' ID="backSquare1"><tr><td></td></tr></table>
	<table width='315' height='326' border='0' cellpadding='10' style='border: 1px #CCCCCC solid' style='position:absolute;top:60px;left:335px;display:block;' ID="backSquare2"><tr><td></td></tr></table>
	<!--Record Count-->
		<%#@~^HwAAAA==r6P2moP@!@*PrWk^+Ul7RCdaJPD4+	sAkAAA==^#~@%>
		<span id="spnRecSet" name="spnRecSet" style="<%#@~^QgAAAA==~b0~alT+P{PrP+kYdRm/2E,YtUPM+/aGU/RSDrYn~rNr/aslH)UG	+iE~x[PbW,jhYAAA==^#~@%>position:absolute;right:5px;top:12px;font-size: 9px;color='#777777'">Player: <%=#@~^AgAAAA==d	4QAAAA==^#~@%> of <%=#@~^AwAAAA==d6+PgEAAA==^#~@%>&nbsp;&nbsp;<%#@~^BwEAAA==(6POm4^+P@!@*,E2swsWH+n9mYlr~l	NPU^.+x:X2+~@!@*PEHK[J,lU[,Kl8sP@!@*,E"+dbo	lYbW	/tmVVb/OE,lx9~Km4V~@!@*,JA:2Z4n13JkkOJ,lU[,Kl8sP@!@*,Eh+.6WM:l	m]+7knhrPCU9PKm8VP@!@*~E|bYrPOtnU,Dn/aGxk+ 	MkYn~ru'x(dai'	4kwi"+1GD9)~JLDU'rPW6~JLD0'E[	4kwI[U8kwI[	8/ai'U(/wIERVIAAA==^#~@%></span>
		<%#@~^EQAAAA==n	N~b0@#@&dx9~k6bAQAAA==^#~@%>
	<!--Gray Block and white line for header-->
	<table width='100%' bgcolor='#CcCcCc' border='0' cellpadding='0' cellspacing='0' ID="Table1"><tr height='9'><td></td></tr><tr height='1' bgcolor='#eeeeee'><td></td></tr></table>
	<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#F7F7F7' ID="Table2">
	<!--top row of header-->
	<tr bgcolor='#dCdCdC'><td rowspan='2' width='181' height='20' valign='top' style='border-bottom: 1px solid #C0C0C0'><table width='181' cellspacing='0' ID="Table3"><tr><td style='color:#999999;font-size:18px'>&nbsp;<span name='spnPageHeader' id='spnPageHeader'></span></td></tr></table><td>&nbsp;</td><td width='100' height='20' valign='top'><table width='25' ID="Table6"></table></td><td colspan=12></td><td width='100%'></td></tr>
	<tr><td><img src='<%=#@~^CgAAAA==2mY4hD0kXGwQAAA==^#~@%>/../images/corner2.gif' WIDTH='19' HEIGHT='20'></td>
	<td style='background-color: #F7F7F7;border-top: 1px solid #C0C0C0'>&nbsp;</td>
	<!--Tabs-->
<%#@~^BgAAAA==n	N~b0JgIAAA==^#~@%>