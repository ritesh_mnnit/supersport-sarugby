<html>
<head>
<link REL="stylesheet" HREF="css/general.css" type="text/css">
<title>Help with mail merge</title>
</head>
<body bgcolor=#f5f5f5>
<table align="center" style="border-bottom:1px solid #DEDEDE"><tr><th class="header1">Mail Merge: Saved and New letters</th></tr></table>
<table><tr><td>To open the Mail Merge tool click on this <img src="images/mail.gif"> 
 icon which can be found in the top left-hand corner of the screen.</td></tr></table>
<table><tr><td>Mail Merge is a word processor document management tool. It allows users to automatically generate letters to any (or all) of the employees in the system. In Mail Merge users can:</td></tr></table>
<table><tr><td>
<li>Generate letters that have been saved in the system</li>
<li>Create and save new letters (using MSWord)</li>
<li>Edit saved letters (using MSWord)</li>
<br><br>
There are three steps to generating saved letters:
<ol>	
<li>First the user must select the folder in which the letter has been saved.</li>	
<li>Then the user must select a letter from that folder</li>	
<li>Finally the user must designate to which employees the letter will be addressed. This is done by using a search tool similar to the one found within the main system. Fields that the user can use to designate to which employees the letters are addressed are all from the Employee Details (master) screen. The first step is to select a field; then to select an operator (e.g. is equal to, is greater than etc.); and the final step is to put in a value.</li>	
<li>Users also have the option to send letters to employees as an attachment to an e-mail and to save letters in employees' file folders.</li>	
</ol>
The user needs to click on Merge Letters and the system will generate the required document.
</td></tr></table>
<hr>
<table><tr><td>
To create new letters the user has to follow the following steps:
<ol>	
<li>First the user has to select to create a new letter by clicking on New Letter.</li>
<li>The user will then be able to decide whether to Upload or Create a new letter. If a letter is to be uploaded it means that it has to have been created. It is possible to upload letters that have not been created through the system (by clicking on Upload), but then that letter wont be dynamically be linked to the fields within the system. When a new letter is created (by clicking on Create) through the system, then the user has the option to dynamically put any fields from the Employee details (master) screen in the new letter.</li>
<li>Once the user has decided what fields to put in the letter, it will be created by clicking on Create Template. The system will then open up MSWord and the user will have the option to save the document straight away before editing it. This is must be done if the user is to proceed with creating the letter.</li>
</ol>
</td></tr>
<tr><td align="center"><b><u>Important</u></b></td></tr>
<tr><td>
It is important to know that a letter will not be able to be uploaded to system if it has not first been saved on the user's computer.
<ol start='4'>
<li>The user will now be in MSWord. This is where all detail is added and formatting is done. Once the detail of the letter has been done the user closes the window. The system the immediately goes back to the Upload Letter/Create Letter screen.</li>
<li>The next step is to upload the letter. The user must click on Upload Letter The system then gives the user the option to either create a New Folderor to use an Existing folder. Selecting to create a new folder will allow the user to type a name for that folder before uploading a letter to it. Once a folder has been selected or created, the user will have the option to give the letter a unique name. This will be the name that appears in the system. Next the user has to browse to locate the document. By clicking on the document and then clicking on OK, the document will be uploaded and the system will go back to the main Mail Merge screen.</li>
</ol>
</td></tr></table>
<%#@~^JwAAAA==~b0~M+$E+kYvEtV2YHwnE*P',E/bxo^nEPDtxKg0AAA==^#~@%>
<br><center><a href="#" onclick="javascript:parent.window.close()" style="font-size:10px; font-weight:bold">Close Window</a></center>
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
</body>
</html>