<%@ Language=VBScript.Encode %>
<html>
<head>
<link REL="stylesheet" HREF="css/general.css" type="text/css">
<title>Help with search functionality</title>
</head>
<body bgcolor=#f5f5f5>
<table  align="center"style="border-bottom:1px solid #DEDEDE"><tr><th class="header1">Search Functionality</th></tr></table>
<br>
<table>
<tr><td>To open up the search tool you the user has to click on this button <img src="images/search1.gif"> which can be found in the top right hand corner of the screen.</td></tr>
<tr><td style="height:5px"></td></tr>
<tr><td style="align:center;"><img src="images/SSearch.gif"></td></tr>
</table>
<br>
<table>
<tr><td>To close the search tool just click on the green button again.</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>The search tool allows you the user to locate specific records within the system based on the following criteria:</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>
<ul>
<li>Surname</li>
<li>First Name</li>
<li>ID Number</li>
<li>Squad</li>
</ul>
</td></tr>
<tr><td>You can search using from one to three of the above criteria to narrow down your search results.</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>So if you were to search using a 'First Name' of 'John' in the first search box, a 'Surname' of 'Smith' in the second search box and a 'Squad' of 'Springboks' in the third search box, then you would get all the player records where there first name is equal to 'John', the surname is equal to 'Smith' and they play in the 'Springboks' squad. Otherwise if there are no records that match your search criteria then a box will pop up in the middle of your screen with the message "No players matched your search"</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>An option exists for each search box to do a partial or exact search. An exact search means that the word you are looking for, for instance a surname of 'Mocke' would need to be stored in the database exactly as typed in. If the user is not sure what the exact spelling of the surname 'Mocke' is, he/she can type in 'Mo' and do a partial search. This will show a set of records where the players' surname begins with 'Mo'.</td></tr>
<tr><td style="height:2px"></td></tr>
<tr><td>Once a search has been done, which sets up a group of records according to the criteria of that search, the user can clear the search in order to display all records again. This is done by clicking on the icon for the relevant screen in the navigation bar on the left hand side of the screen.</td></tr>
</table> 
<%#@~^JwAAAA==~b0~M+$E+kYvEtV2YHwnE*P',E/bxo^nEPDtxKg0AAA==^#~@%>
<br><center><a href="#" onclick="javascript:parent.window.close()" style="font-size:10px; font-weight:bold">Close Window</a></center>
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
</body>
</html>