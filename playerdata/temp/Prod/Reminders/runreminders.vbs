' VBScript source code
'Create reminder facility
'________________________________________________________________________
' get reminder fields 
'timeinterval	TimePeriod	Reminder	BeforeAfter
'4	1	No specific reason	2
'<!--===========================================================================================
'========================EVERYTHING on this site is copyrighted to Genesys=================================
'==============Any use without written approval by Genesys is considered infringment.============================
'=============================================================================================-->
Const ForReading = 1, ForWriting = 2,ForAppending =8
Dim fso
dim MyFile
Set fso = CreateObject("Scripting.FileSystemObject")
dim logfile
logfile = fso.GetAbsolutePathName("..\log")& "\reminderlog.csv"
Set MyFile= fso.OpenTextFile(logfile,ForAppending, True)
'Get connection string from the include file
includefile = fso.GetAbsolutePathName("..\includes")& "\DB_Connectivity.asp"
set f = fso.GetFile(includefile) 
set ts = f.OpenAsTextStream(ForReading, -2) 
dim constring

Do While not ts.AtEndOfStream
    tempcon = ts.ReadLine
    if instr(tempcon,"con1.ConnectionString") then
	    constring = tempcon
    end if 
Loop 

constring =  replace(right(constring,len(constring)- instr(constring,"=")),"""","")
Dim con1
set con1 = createobject("adodb.connection")
con1.ConnectionString = constring
con1.open
set mainloop = con1.Execute("select tablename from reminder group by tablename")
'start main loop
do until mainloop.eof
	TableName = mainloop(0)
	set getrm = con1.execute("SELECT dbo.Timeperiods.TimePeriod AS Timeperiods, dbo.Reminder.ReminderId, dbo.Reminder.Tablename,dbo.Reminder.timeinterval, dbo.Reminder.Fieldname,dbo.Reminder.Reminder, dbo.Reminder.BeforeAfter, dbo.Reminder.Remind as email FROM dbo.Timeperiods RIGHT OUTER JOIN dbo.Reminder ON dbo.Timeperiods.TimeperiodID = dbo.Reminder.TimePeriod where tablename ='"& TableName &"'")

	if not getrm.eof then
		do until getrm.eof
			if getrm("Timeperiods") = "days" THEN
				PER ="d"
			elseif getrm("Timeperiods") = "weeks" then
				PER="ww"
			elseif getrm("Timeperiods") = "months" then
				PER="m"
			elseif getrm("Timeperiods") = "years" then
				PER="yyyy"
			end if

			if getrm("BeforeAfter") ="1" then
				interval = getrm("timeinterval") * -1
			else
				interval = getrm("timeinterval") 
			end if

			'check if it is in the future
			constr = "select " &  getrm("fieldname") & ",Employeeno,"& getrm("fieldname")&" as dname from " & TableName & " where cast(datepart(" & """"  &  "yyyy" & """" &  "," & "dateadd(" & PER & "," & interval & "," & getrm("fieldname") & ")" &  ") as varchar) +  cast(datepart("&""""&"mm"&""""& ","  & "dateadd(" & PER & "," & interval & "," & getrm("fieldname") & ")" &   ") as varchar) +  cast(datepart(" &""""&    "d"   &""""& "," & "dateadd(" & PER & "," & interval & "," & getrm("fieldname") & ")" &  ") as varchar) = cast(datepart(" &""""&  "yyyy" &""""&  ",getdate()) as varchar) +  cast(datepart("&""""& "mm"&""""&",getdate()) as varchar) +  cast(datepart("&""""&"d"&""""&",getdate()) as varchar)"
			set sertr = con1.execute(constr)
			if not sertr.eof then 'employee loop
				do until sertr.eof
					'get time period 
					if not isnull(sertr(0)) then
						'check who to send the email to
						'we know the Employeeno
						'get his department
						set gd = con1.Execute("select department,Firstname +' ' + surname from Employeedata where Employeeno='" & sertr(1)& "'")
						if not gd.eof then
							employeename = gd(1)
							emailto = getrm("email")
							table = TableName
							field = getrm("fieldname")
							dateevent = sertr("dname")
							period = getrm("Timeperiods")
							timeperiod = getrm("BeforeAfter")
							interv = getrm("timeinterval")

							if timeperiod ="1" then 
								timeperiod ="Before"
							else
								timeperiod="After"
							end if
						end if
						strBody = ""
						strBody = strBody & "<HTML><HEAD><Style>BODY{FONT-SIZE: xx-small;FONT-FAMILY: Verdana, Arial, Helvetica}TH.cell{BORDER-TOP: 1px solid #999999;BORDER-LEFT: 1px solid #999999;font-size=10px;text-align:center;background-color:#F5F5F5;text-transform:capitalize;color:#666666;}TD.cell{BORDER-TOP: 1px solid #999999;BORDER-LEFT: 1px solid #999999;font-size=10px;text-align:center;background-color:#F5F5F5;height:25px;color:#333333}.THd{BORDER-TOP: 1px solid #999999;BORDER-LEFT: 1px solid #999999;font-size=10px;text-align:center;background-color:#cccccc;text-transform:capitalize;color:#333333;}A{text-decoration:none;color:#333333}A:hover{font-weight:bold;color:#666666}</Style></HEAD><Body>"
						strBody = strBody & "<table align='center' width='500' align='center' cellpadding='4' cellspacing='0' border='0' style='BORDER-BOTTOM: 1px solid #999999; BORDER-RIGHT: 1px solid #999999;' ID='Table1'>"
						strBody = strBody & "<tr height='25'><th class='thd' colspan='2'>Reminder</th></tr>"
						strBody = strBody & "<tr><th width='130' class='cell' style='text-align:right'>Employee:</th><td class='cell'>"& employeename &"</td></tr>"
						strBody = strBody & "<tr><th width='130' class='cell' style='text-align:right'>Table:</th><td class='cell'>"& table &"</td></tr>"
						strBody = strBody & "<tr><th width='130' class='cell' style='text-align:right'>Field:</th><td class='cell'>"& field &"</td></tr>"
						strBody = strBody & "<tr><th width='130' class='cell' style='text-align:right'>Reminder set:</th><td class='cell'>"& interv & " " & period & " " & timeperiod &"</td></tr>"
						strBody = strBody & "<tr><th width='130' class='cell' style='text-align:right'>Trigger date:</th><td class='cell'>"& formatdatetime(dateevent,vblongdate) &"</td></tr>"
						strBody = strBody & "<tr><th width='130' class='cell' style='text-align:right'>Reminderdate:</th><td class='cell'>"& formatdatetime(date(),vblongdate) &"</td></tr>"
						strBody = strBody & "<tr><th width='130' class='cell' style='text-align:right'>Reason:</th><td class='cell'>"& getrm(5) &"</td></tr></table></Body></HTML>"

						set objEMail = CreateObject("CDONTS.NewMail")
						objEMail.To = emailto
						objEMail.From = "Reminders@spur.co.za"
						objEMail.subject = "Reminder."
						objEMail.BodyFormat = 0 
						objEMail.MailFormat = 0 
						objEMail.body = strBody
						objEMail.send
						set objEMail = nothing

						MyFile.WriteLine date() &"," & time()& "," & employeename & "," & emailto & "," & table & "," & field & "," & dateevent & "," & interv & "," &  period & "," &timeperiod

					end if
					sertr.movenext ' end employeeloop
				loop
			end if 'for employee loop
			getrm.movenext
		loop
	end if
	mainloop.movenext
loop
MyFile.Close
'end mainloop
'___________________________________________________________________________