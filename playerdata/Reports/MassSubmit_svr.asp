<%@ Language=VBScript.Encode%>

<% 
pathPrefix		=	"../"
%>

<!-- #include file="../includes/config/db_connectivity.asp" -->
<!-- #include file="../includes/config/Email.asp" -->
<%
strPlayerID = Request("hidPlayerID")
arrPlayerID = split(strPlayerID,",")
NumPlayers = ubound(arrPlayerID)

Dim val1, val2
			

dim array1()
dim j
j = 0

function nuller(value)
	if trim(value) ="" then
		value = "null"
	else
		value = "'" & trim(value) & "'"
	end if
	nuller =value
end function

function populateArray1(obj)
	if NumPlayers = 0 then
		strTemp = request(obj)
		array1(j,0) = nuller(strTemp)
	else
		strTemp = split(request(obj),",")
		for i = 0 to NumPlayers
			array1(j,i) = nuller(strTemp(i))
		next
	end if
	j = j + 1
end function

array2 = split("hidPlayerID,txtSASquad,txtAgeGroup,txtDate,txtHeight,txtWeight,txtCmUp,txtInjured,txtTricep,txtBicep,txtSubscapularis,txtSuprailliac,txtAbdominal,txtMidThigh,txtCalf,txtMidThighGirth,txtCalfGirth,txtForearmGirth,txtContractedArmGirth,txtSubGlutealGirth,txtKneeGirth,txtHumerousdiameter,txtFemurdiameter,txtSLRLeft,txtSLRRight,txtIlliopsisLeft,txtIlliopsisRight,txtQuadLeft,txtQuadRight,txtSitReach,txt1RMBenchPressAbsolute,txtBenchThrow40,txtBenchThrow50,txtBenchThrow60,txtBenchThrow70,txtBenchThrow80,txtBenchThrowwith20kgAbsolute,txtForearmGripStrengthAbsoluteLeft,txtForearmGripStrengthAbsoluteRight,txt1RMFullSquatAbsolute,txt3RMFullSquatAbsolute,txt1RMLegPressAbsolute,txt3RMLegPressAbsolute,txt1RMParallelSquatAbsolute,txt3RMParallelSquatAbsolute,txtSquatJump20,txtSquatJump40,txtSquatJump60,txtSquatJump80,txtSquatJump100,txtSquatJumpwith20kgAbsolute,txtCountermovementjumpStanding,txtCountermovementjumpJump,txtVerticaljumpStanding,txtVerticaljumpJump,txt1RMPowerCleanAbsolute,txtFlexedArmHang,txtPullups,txtPushups,txtSitups120,txtSitups60,txt10mSpeed,txt40mSpeed,txtAgilityItest,txtAgilityTtest,txtRepeatedSprint,txtMultistageShuttleRun,txtModifiedMultistageShuttle,txtCooperTest,txt2-4kmTimeTrial,txt3-0kmTimeTrial,txtHIMS",",")
redim array1(ubound(array2),NumPlayers)
for each item in array2
	populateArray1(item)
next

if NumPlayers = 0 then
	i = 0
	'Anthropometry insert
	if not (array1(8,0)="null" and array1(9,0)="null" and array1(10,0)="null" and array1(11,0)="null" and array1(12,0)="null" and array1(13,0)="null" and array1(14,0)="null" and array1(15,0)="null" and array1(16,0)="null" and array1(17,0)="null" and array1(18,0)="null" and array1(21,0)="null" and array1(22,0)="null" and array1(19,0)="null" and array1(20,0)="null" and array1(6,0)="null") then
		strSQL = strSQL & "insert Anthropometry(GenID,SASquadComparative,AgeGroup,[Date],Stature,BodyMass,SFTriceps,SFBiceps,SFSubscapular,SFSupraIliac,SFAbdominal,SFThigh,SFCalf,GMidThigh,GCalf,GForearm,GContractedArm,DHumerus,DFemur,GSubGluteal,GKnee,LengthSubGluteKnee) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(4,i)&","&array1(5,i)&","&array1(8,i)&","&array1(9,i)&","&array1(10,i)&","&array1(11,i)&","&array1(12,i)&","&array1(13,i)&","&array1(14,i)&","&array1(15,i)&","&array1(16,i)&","&array1(17,i)&","&array1(18,i)&","&array1(21,i)&","&array1(22,i)&","&array1(19,i)&","&array1(20,i)&","&array1(6,i)&")"
	end if
	'StraightLegRaise insert
	if not (array1(23,0)="null" and array1(24,0)="null") then
		strSQL = strSQL & "insert StraightLegRaise(GenID,SASquadComparative,AgeGroup,TestDate,ResultL,ResultR) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(23,i)&","&array1(24,i)&")"
	end if
	'ModThomIlliop insert
	if not (array1(25,0)="null" and array1(26,0)="null") then
		strSQL = strSQL & "insert ModThomIlliop(GenID,SASquadComparative,AgeGroup,TestDate,ResultL,ResultR) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(25,i)&","&array1(26,i)&")"
	end if
	'ModThomQuad insert
	if not (array1(27,0)="null" and array1(28,0)="null") then
		strSQL = strSQL & "insert ModThomQuad(GenID,SASquadComparative,AgeGroup,TestDate,ResultL,ResultR) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(27,i)&","&array1(28,i)&")"
	end if
	'SitReach insert
	if not (array1(29,0)="null") then
		strSQL = strSQL & "insert SitReach(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(29,i)&")"
	end if
	'BenchPress insert
	if not (array1(30,0)="null") then
		if array1(30,i) = "null" or array1(5,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(30,i),"'","")/(replace(array1(5,i),"'","")*0.57))
		end if
		strSQL = strSQL & "insert BenchPress(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(30,i)&",'"& val1 &"')"
	end if
	'BenchThrow insert
	if not (array1(31,0)="null" and array1(32,0)="null" and array1(33,0)="null" and array1(34,0)="null" and array1(35,0)="null") then
		strSQL = strSQL & "insert BenchThrow(GenID,SASquadComparative,AgeGroup,TestDate,Result40,Result50,Result60,Result70,Result80) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(31,i)&","&array1(32,i)&","&array1(33,i)&","&array1(34,i)&","&array1(35,i)&")"
	end if
	'BenchThrow20 insert
	if not (array1(36,0)="null") then
		strSQL = strSQL & "insert BenchThrow20(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(36,i)&")"
	end if
	'ForearmGrip insert
	if not (array1(37,0)="null" and array1(38,0)="null") then
		if array1(5,i) = "null" or array1(38,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(38,i),"'","")/replace(array1(5,i),"'",""))
		end if
		if array1(5,i) = "null" or array1(37,i) = "null" then
			val2 = "null"
		else
			val2 = (replace(array1(37,i),"'","")/replace(array1(5,i),"'",""))
		end if
		strSQL = strSQL & "insert ForearmGrip(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,ResultL,ResultR,RelativeResultL,RelativeResultR) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(37,i)&","&array1(38,i)&",'"& val2 &"','"& val1 &"')"
	end if
	'FSquat1 insert
	if not (array1(39,0)="null") then
		if array1(5,i) = "null" or array1(39,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(39,i),"'","")/(replace(array1(5,i),"'","")*0.6))
		end if
		strSQL = strSQL & "insert FSquat1(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(39,i)&",'"& val1 &"')"
	end if
	'FSquat3 insert
	if not (array1(40,0)="null") then
		if array1(5,i) = "null" or array1(40,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(40,i),"'","")/(replace(array1(5,i),"'","")*0.6))
		end if
		strSQL = strSQL & "insert FSquat3(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(40,i)&",'"& val1 &"')"
	end if
	'LegPress1 insert
	if not (array1(41,0)="null") then
		if array1(5,i) = "null" or array1(41,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(41,i),"'","")/replace(array1(5,i),"'",""))
		end if
		strSQL = strSQL & "insert LegPress1(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(41,i)&",'"& val1 &"')"
	end if
	'LegPress3 insert
	if not (array1(42,0)="null") then
		if array1(5,i) = "null" or array1(42,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(42,i),"'","")/replace(array1(5,i),"'",""))
		end if
		strSQL = strSQL & "insert LegPress3(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(42,i)&",'"& val1 &"')"
	end if
	'PSquat1 insert
	if not (array1(43,0)="null") then
		if array1(43,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(43,i),"'","")/(replace(array1(43,i),"'","")*0.6))
		end if
		strSQL = strSQL & "insert PSquat1(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(43,i)&",'"& val1 &"')"
	end if
	'PSquat3 insert
	if not (array1(44,0)="null") then
		if array1(44,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(44,i),"'","")/(replace(array1(44,i),"'","")*0.6))
		end if
		strSQL = strSQL & "insert PSquat3(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(44,i)&",'"& val1 &"')"
	end if
	'JumpSquat insert
	if not (array1(45,0)="null" and array1(46,0)="null" and array1(47,0)="null" and array1(48,0)="null" and array1(49,0)="null") then
		strSQL = strSQL & "insert JumpSquat(GenID,SASquadComparative,AgeGroup,TestDate,Result20,Result40,Result60,Result80,Result100) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(45,i)&","&array1(46,i)&","&array1(47,i)&","&array1(48,i)&","&array1(49,i)&")"
	end if
	'JumpSquat20 insert
	if not (array1(50,0)="null") then
		strSQL = strSQL & "insert JumpSquat20(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(50,i)&")"
	end if
	'CounterMovement insert
	if not (array1(51,0)="null" and array1(52,0)="null") then
		if array1(51,i) = "null" or array1(52,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(51,i),"'","")-replace(array1(52,i),"'",""))
		end if
		strSQL = strSQL & "insert CounterMovement(GenID,SASquadComparative,AgeGroup,TestDate,ReachHeight,JumpHeight,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(51,i)&","&array1(52,i)&",'"& val1 &"')"
	end if
	'VertJump insert
	if not (array1(53,0)="null" and array1(54,0)="null") then
		if array1(53,i) = "null" or array1(54,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(53,i),"'","")-replace(array1(54,i),"'",""))
		end if
		strSQL = strSQL & "insert VertJump(GenID,SASquadComparative,AgeGroup,TestDate,StandReach,StandJump,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(53,i)&","&array1(54,i)&",'"& val1 &"')"
	end if
	'PowerClean insert
	if not (array1(55,0)="null") then
		if array1(55,i) = "null" then
			val1 = "null"
		else
			val1 = (replace(array1(55,i),"'","")/(replace(array1(55,i),"'","")*0.58))
		end if
		strSQL = strSQL & "insert PowerClean(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(55,i)&",'"& val1 &"')"
	end if
	'MEFlexedArm insert
	if not (array1(56,0)="null") then
		strSQL = strSQL & "insert MEFlexedArm(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(56,i)&")"
	end if
	'MEPullups insert
	if not (array1(57,0)="null") then
		strSQL = strSQL & "insert MEPullups(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(57,i)&")"
	end if
	'MEPushups insert
	if not (array1(58,0)="null") then
		strSQL = strSQL & "insert MEPushups(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(58,i)&")"
	end if
	'MESitups insert
	if not (array1(59,0)="null") then
		strSQL = strSQL & "insert MESitups(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(59,i)&")"
	end if
	'MESitupsU12 insert
	if not (array1(60,0)="null") then
		strSQL = strSQL & "insert MESitupsU12(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(60,i)&")"
	end if
	'Speed10m insert
	if not (array1(61,0)="null") then
		strSQL = strSQL & "insert Speed10m(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(61,i)&")"
	end if
	'Speed40m insert
	if not (array1(62,0)="null") then
		strSQL = strSQL & "insert Speed40m(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(62,i)&")"
	end if
	'AgilityIllinois insert
	if not (array1(63,0)="null") then
		strSQL = strSQL & "insert AgilityIllinois(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(63,i)&")"
	end if
	'AgilityTTest insert
	if not (array1(64,0)="null") then
		strSQL = strSQL & "insert AgilityTTest(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(64,i)&")"
	end if
	'RepSprint insert
	if not (array1(65,0)="null") then
		strSQL = strSQL & "insert RepSprint(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(65,i)&")"
	end if
	'CFShuttleRun insert
	if not (array1(66,0)="null") then
		strSQL = strSQL & "insert CFShuttleRun(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(66,i)&")"
	end if
	'CFShuttleRun insert
'	if not (array1(69,0)="null") then
'		strSQL = strSQL & "insert RepSprint(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(69,i)&")"
'	end if
	'CFCoopers insert
	if not (array1(68,0)="null") then
		strSQL = strSQL & "insert CFCoopers(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(68,i)&")"
	end if
	'CF2400Trial insert
	if not (array1(69,0)="null") then
		strSQL = strSQL & "insert CF2400Trial(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(69,i)&")"
	end if
	'CF3000Trial insert
	if not (array1(70,0)="null") then
		strSQL = strSQL & "insert CF3000Trial(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(70,i)&")"
	end if
	'HIMS insert
	if not (array1(71,0)="null") then
		strSQL = strSQL & "insert HIMS(GenID,SASquadComparative,AgeGroup,TestDate,RateRecover) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(71,i)&")"
	end if
else
	for i = 0 to NumPlayers
		'Anthropometry insert
		if not (array1(8,i)="null" and array1(9,i)="null" and array1(10,i)="null" and array1(11,i)="null" and array1(12,i)="null" and array1(13,i)="null" and array1(14,i)="null" and array1(15,i)="null" and array1(16,i)="null" and array1(17,i)="null" and array1(18,i)="null" and array1(21,i)="null" and array1(22,i)="null" and array1(19,i)="null" and array1(20,i)="null" and array1(6,i)="null") then
			strSQL = strSQL & "insert Anthropometry(GenID,SASquadComparative,AgeGroup,[Date],Stature,BodyMass,SFTriceps,SFBiceps,SFSubscapular,SFSupraIliac,SFAbdominal,SFThigh,SFCalf,GMidThigh,GCalf,GForearm,GContractedArm,DHumerus,DFemur,GSubGluteal,GKnee,LengthSubGluteKnee) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(4,i)&","&array1(5,i)&","&array1(8,i)&","&array1(9,i)&","&array1(10,i)&","&array1(11,i)&","&array1(12,i)&","&array1(13,i)&","&array1(14,i)&","&array1(15,i)&","&array1(16,i)&","&array1(17,i)&","&array1(18,i)&","&array1(21,i)&","&array1(22,i)&","&array1(19,i)&","&array1(20,i)&","&array1(6,i)&")"
		end if
		'StraightLegRaise insert
		if not (array1(23,i)="null" and array1(24,i)="null") then
			strSQL = strSQL & "insert StraightLegRaise(GenID,SASquadComparative,AgeGroup,TestDate,ResultL,ResultR) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(23,i)&","&array1(24,i)&")"
		end if
		'ModThomIlliop insert
		if not (array1(25,i)="null" and array1(26,i)="null") then
			strSQL = strSQL & "insert ModThomIlliop(GenID,SASquadComparative,AgeGroup,TestDate,ResultL,ResultR) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(25,i)&","&array1(26,i)&")"
		end if
		'ModThomQuad insert
		if not (array1(27,i)="null" and array1(28,i)="null") then
			strSQL = strSQL & "insert ModThomQuad(GenID,SASquadComparative,AgeGroup,TestDate,ResultL,ResultR) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(27,i)&","&array1(28,i)&")"
		end if
		'SitReach insert
		if not (array1(29,i)="null") then
			strSQL = strSQL & "insert SitReach(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(29,i)&")"
		end if
		'BenchPress insert
		if not (array1(30,i)="null") then
			Dim relResult
			if array1(30,i) = "null" or array1(5,i) = "null" or array1(5,i) = "'0'" or array1(30,i) = "'0'" then
				relResult = "null"
			else
				relResult = (replace(array1(30,i),"'","")/(replace(array1(5,i),"'","")*0.57))
			end if
			strSQL = strSQL & "insert BenchPress(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(30,i)&",'"& relResult &"')"
			
		end if
		
		'BenchThrow insert
		if not (array1(31,i)="null" and array1(32,i)="null" and array1(33,i)="null" and array1(34,i)="null" and array1(35,i)="null") then
			strSQL = strSQL & "insert BenchThrow(GenID,SASquadComparative,AgeGroup,TestDate,Result40,Result50,Result60,Result70,Result80) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(31,i)&","&array1(32,i)&","&array1(33,i)&","&array1(34,i)&","&array1(35,i)&")"
		end if
		
		'BenchThrow20 insert
		if not (array1(36,i)="null") then
			strSQL = strSQL & "insert BenchThrow20(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(36,i)&")"
		end if
		
		'ForearmGrip insert
		if not (array1(37,i)="null" and array1(38,i)="null") then

			if array1(5,i) = "'0'" or array1(5,i) = "null" or array1(38,i) = "'0'" or array1(38,i) = "null" then
				val1 = "null"
				val2 = "null"
			else
				val1 = (replace(array1(37,i),"'","")/replace(array1(5,i),"'",""))
				val2 = (replace(array1(38,i),"'","")/replace(array1(5,i),"'",""))
			end if
			
			strSQL = strSQL & "insert ForearmGrip(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,ResultL,ResultR,RelativeResultL,RelativeResultR) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(37,i)&","&array1(38,i)&",'"& val1 &"','"& val2 &"')"
		end if
		'FSquat1 insert
		if not (array1(39,i)="null") then
			if array1(5,i) = "'0'" or array1(5,i) = "null" or array1(39,i) = "'0'" or array1(39,i) = "null" then
				val1 = "null"
			else
				val1 = (replace(array1(39,i),"'","")/(replace(array1(5,i),"'","")*0.6))
			end if
			strSQL = strSQL & "insert FSquat1(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(39,i)&",'"& val1 &"')"
		end if
		'FSquat3 insert
		if not (array1(40,i)="null") then
			if array1(5,i) = "'0'" or array1(5,i) = "null" or array1(40,i) = "'0'" or array1(40,i) = "null" then
				val1 = "null"
			else
				val1 = (replace(array1(40,i),"'","")/(replace(array1(5,i),"'","")*0.6))
			end if
			strSQL = strSQL & "insert FSquat3(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(40,i)&",'"& val1 &"')"
		end if
		'LegPress1 insert
		if not (array1(41,i)="null") then
			if array1(5,i) = "'0'" or array1(5,i) = "null" or array1(41,i) = "'0'" or array1(41,i) = "null" then
				val1 = "null"
			else
				val1 = (replace(array1(41,i),"'","")/replace(array1(5,i),"'",""))
			end if
			strSQL = strSQL & "insert LegPress1(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(41,i)&",'"& val1 &"')"
		end if
		'LegPress3 insert
		if not (array1(42,i)="null") then
			if array1(5,i) = "'0'" or array1(5,i) = "null" or array1(42,i) = "'0'" or array1(42,i) = "null" then
				val1 = "null"
			else
				val1 = (replace(array1(42,i),"'","")/replace(array1(5,i),"'",""))
			end if
			strSQL = strSQL & "insert LegPress3(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(42,i)&",'"& val1 &"')"
		end if
		'PSquat1 insert
		if not (array1(43,i)="null") then
			if array1(5,i) = "'0'" then
				val1 = "null"
			else
				val1 = (replace(array1(43,i),"'","")/(replace(array1(43,i),"'","")*0.6))
			end if
			strSQL = strSQL & "insert PSquat1(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(43,i)&",'"& val1 &"')"
		end if
		'PSquat3 insert
		if not (array1(44,i)="null") then
			if array1(5,i) = "'0'" then
				val1 = "null"
			else
				val1 = (replace(array1(44,i),"'","")/(replace(array1(44,i),"'","")*0.6))
			end if
			strSQL = strSQL & "insert PSquat3(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(44,i)&",'"& val1 &"')"
		end if
		'JumpSquat insert
		if not (array1(45,i)="null" and array1(46,i)="null" and array1(47,i)="null" and array1(48,i)="null" and array1(49,i)="null") then
			strSQL = strSQL & "insert JumpSquat(GenID,SASquadComparative,AgeGroup,TestDate,Result20,Result40,Result60,Result80,Result100) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(45,i)&","&array1(46,i)&","&array1(47,i)&","&array1(48,i)&","&array1(49,i)&")"
		end if
		'JumpSquat20 insert
		if not (array1(50,i)="null") then
			strSQL = strSQL & "insert JumpSquat20(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(50,i)&")"
		end if
		'CounterMovement insert
		if not (array1(51,i)="null" and array1(52,i)="null") then
			if array1(51,i) = "null" or array1(52,i) = "null" then
				val1 = "null"
			else
				val1 = (replace(array1(51,i),"'","")-replace(array1(52,i),"'",""))
			end if
			strSQL = strSQL & "insert CounterMovement(GenID,SASquadComparative,AgeGroup,TestDate,ReachHeight,JumpHeight,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(51,i)&","&array1(52,i)&",'"& val1 &"')"
		end if
		'VertJump insert
		if not (array1(53,i)="null" and array1(54,i)="null") then
			if array1(53,i) = "null" or array1(54,i) = "null" then
				val1 = "null"
			else
				val1 = (replace(array1(53,i),"'","")-replace(array1(54,i),"'",""))
			end if
			strSQL = strSQL & "insert VertJump(GenID,SASquadComparative,AgeGroup,TestDate,StandReach,StandJump,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(53,i)&","&array1(54,i)&",'"& val1 &"')"
		end if
		'PowerClean insert
		if not (array1(55,i)="null") then
			if array1(55,i) = "null" then
				val1 = "null"
			else
				val1 = (replace(array1(55,i),"'","")/(replace(array1(55,i),"'","")*0.58))
			end if
			strSQL = strSQL & "insert PowerClean(GenID,SASquadComparative,AgeGroup,TestDate,BodyWeight,Result,RelativeResult) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(5,i)&","&array1(55,i)&",'"& val1 &"')"
		end if
		'MEFlexedArm insert
		if not (array1(56,i)="null") then
			strSQL = strSQL & "insert MEFlexedArm(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(56,i)&")"
		end if
		'MEPullups insert
		if not (array1(57,i)="null") then
			strSQL = strSQL & "insert MEPullups(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(57,i)&")"
		end if
		'MEPushups insert
		if not (array1(58,i)="null") then
			strSQL = strSQL & "insert MEPushups(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(58,i)&")"
		end if
		'MESitups insert
		if not (array1(59,i)="null") then
			strSQL = strSQL & "insert MESitups(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(59,i)&")"
		end if
		'MESitupsU12 insert
		if not (array1(60,i)="null") then
			strSQL = strSQL & "insert MESitupsU12(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(60,i)&")"
		end if
		'Speed10m insert
		if not (array1(61,i)="null") then
			strSQL = strSQL & "insert Speed10m(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(61,i)&")"
		end if
		'Speed40m insert
		if not (array1(62,i)="null") then
			strSQL = strSQL & "insert Speed40m(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(62,i)&")"
		end if
		'AgilityIllinois insert
		if not (array1(63,i)="null") then
			strSQL = strSQL & "insert AgilityIllinois(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(63,i)&")"
		end if
		'AgilityTTest insert
		if not (array1(64,i)="null") then
			strSQL = strSQL & "insert AgilityTTest(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(64,i)&")"
		end if
		'RepSprint insert
		if not (array1(65,i)="null") then
			strSQL = strSQL & "insert RepSprint(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(65,i)&")"
		end if
		'CFShuttleRun insert
		if not (array1(66,i)="null") then
			strSQL = strSQL & "insert CFShuttleRun(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(66,i)&")"
		end if
		'CFShuttleRun insert
	'	if not (array1(69,i)="null") then
	'		strSQL = strSQL & "insert RepSprint(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(69,i)&")"
	'	end if
		'CFCoopers insert
		if not (array1(68,i)="null") then
			strSQL = strSQL & "insert CFCoopers(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(68,i)&")"
		end if
		'CF2400Trial insert
		if not (array1(69,i)="null") then
			strSQL = strSQL & "insert CF2400Trial(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(69,i)&")"
		end if
		'CF3000Trial insert
		if not (array1(70,i)="null") then
			strSQL = strSQL & "insert CF3000Trial(GenID,SASquadComparative,AgeGroup,TestDate,Result) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(70,i)&")"
		end if
		'HIMS insert
		if not (array1(71,i)="null") then
			strSQL = strSQL & "insert HIMS(GenID,SASquadComparative,AgeGroup,TestDate,RateRecover) values ("&array1(0,i)&","&array1(1,i)&","&array1(2,i)&","&array1(3,i)&","&array1(71,i)&")"
		end if
	next
end if

sEmail "heath@genesys.co.za","heath@genesys.co.za","Mass Insert","<html><body><font style='font-family:verdana;font-size:10px;'>"&strSQL&"</font></body></html>","Joshua Freislich"
if len(strSQL) > 1 then
	con1.execute(replace(strSQL, "'null'", "null"))
end if
%>
<script language="javascript">
	alert("Data successfully inserted to database")
	location.href="MassLoader.asp"
</script>
