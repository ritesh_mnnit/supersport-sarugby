<%#@~^FAAAAA==~alO4nM+0b6,xPrR zrPpwUAAA==^#~@%>

<!-- #include file="../../includes/config/DB_Connectivity.asp" -->
<!-- #include file="../../includes/config/data.asp" -->
<html>
<head>
	<link REL="stylesheet" HREF="../includes/css/qbuilder.css" type="text/css">
	<link REL=stylesheet HREF="../includes/css/general.css" type="text/css">
	<script LANGUAGE='javascript' SRC='../includes/js/jsValidator.js'></script>
</head>
<body background='../images/eebg.gif' topmargin='0' leftmargin='0'>

<%#@~^WgAAAA==@#@&dr	Y;xYgW,xPW@#@&db0~Kmo+gC:P',EjYmYbm~In2KDO/r~Y4+U@#@&ddrUDZUYgG,'~2@#@&d+	N,r0@#@&dRYAAA==^#~@%>

<div id="reporthelpdiv01" style="display:none;position:absolute;top:0px;left:135px;background-color:#FF3300;border: 1px solid black;color:white;width:360px">
	<table>
		<tr>
			<td style="color:white" colspan="2">Start by selecting the squads you want in the report. <br>To select a squad double click on it in the <strong>Not Selected</strong> area.<br> To unselect a squad double click on it in the <strong>Selected</strong> area.</td>
		</tr>
		<tr>
			<td align="left" style="color:white">&nbsp; <img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"> Not Selected</td>
			<td align="right" style="color:white">Selected <img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
	</table>
</div>

<div id="reporthelpdiv02" style="display:none;position:absolute;top:396px;left:190px;background-color:#FF3300;border: 1px solid black;color:white;width:250px">
	<table>
		<tr>
			<td align="center"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
		<tr>
			<td style="color:white">Once you have selected the squads you want in the report, click the submit button.</td>
		</tr>
	</table>
</div>

<div id="reporthelpdiv03" style="display:none;position:absolute;top:24px;left:114px;background-color:#FF3300;border: 1px solid black;color:white;width:395px">
	<table>
		<tr>
			<td style="color:white" colspan="2">Select the players (and the fields) you want to see in the report.</td>
		</tr>
		<tr>
			<td align="left" style="color:white">&nbsp; <img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"> Not Selected</td>
			<td align="right" style="color:white">Selected <img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
	</table>
</div>

<div id="reporthelpdiv04" style="display:none;position:absolute;top:175px;left:595px;background-color:#FF3300;border: 1px solid black;color:white;width:82px">
	<table>
		<tr>
			<td style="color:white">Click this button, and select the fields you want in the report.</td>
		</tr>
		<tr>
			<td align="left" style="color:white"><img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
	</table>
</div>

<div id="reporthelpdiv05" style="display:none;position:absolute;top:271px;left:595px;background-color:#FF3300;border: 1px solid black;color:white;width:82px">
	<table>
		<tr>
			<td align="left" style="color:white"><img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
		<tr>
			<td style="color:white">Then click this button, and select the fields you want in the report.</td>
		</tr>
	</table>
</div>

<div id="reporthelpdiv06" style="display:none;position:absolute;top:415px;left:40px;background-color:#FF3300;border: 1px solid black;color:white;width:250px">
	<table>
		<tr>
			<td align="center"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
		<tr>
			<td style="color:white" colspan="2"><strong>Optional</strong> - Enter a start date and an end date (only data created within the dates will be displayed),as well as the field you want the report to be grouped by.</td>
		</tr>
	</table>
</div>

<div id="reporthelpdiv07" style="display:none;position:absolute;top:415px;left:352px;background-color:#FF3300;border: 1px solid black;color:white;width:240px">
	<table>
	
		<tr>
			<td align="right"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
			<td width="30px">&nbsp;</td>
		</tr>
		
		<tr>
			<td style="color:white" colspan="2">Click <strong>View Report</strong> to generate the report, click <strong>Clear Selections</strong> to unselect all the selected fields on this screen, or click <strong>Back</strong> to return to the previous page.</td>
		</tr>
		
	</table>
</div>

<div id="reporthelpdiv12" style="display:none;position:absolute;top:396px;left:40px;background-color:#FF3300;border: 1px solid black;color:white;width:250px">
	<table>
		<tr>
			<td align="center"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
		<tr>
			<td style="color:white" colspan="2"><strong>Optional</strong> - Enter a start date and an end date (only data created within the dates will be displayed),as well as the field you want the report to be grouped by.</td>
		</tr>
	</table>
</div>

<div id="reporthelpdiv13" style="display:none;position:absolute;top:396px;left:352px;background-color:#FF3300;border: 1px solid black;color:white;width:240px">
	<table>
	
		<tr>
			<td align="right"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
			<td width="30px">&nbsp;</td>
		</tr>
		
		<tr>
			<td style="color:white" colspan="2">Click <strong>View Report</strong> to generate the report, click <strong>Clear Selections</strong> to unselect all the selected fields on this screen, or click <strong>Back</strong> to return to the previous page.</td>
		</tr>
		
	</table>
</div>

<div id="sreporthelpdiv01" style="display:none;position:absolute;top:310px;left:594px;background-color:#FF3300;border: 1px solid black;color:white;width:87px">
	<table>
		<tr>
			<td style="color:white">Click the <strong>saved reports</strong> button.</td>
		</tr>
		<tr>
			<td align="left" style="color:white"><img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
	</table>
</div>

<div id="sreporthelpdiv02" style="display:none;position:absolute;top:400px;left:70px;background-color:#FF3300;border: 1px solid black;color:white;width:490px">
	<table>
		<tr>
			<td align="center" style="color:white"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
		<tr>
			<td style="color:white">
				There are two types of Saved Reports,  <a href="#" onclick="window.open('../includes/asp/help/static.asp?helptype=single','','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=600,height=230')" style="color:white; text-decoration:underline">Static Reports</a> and  <a href="#" onclick="window.open('../includes/asp/help/dynamic.asp?helptype=single','','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=600,height=230')" style="color:white; text-decoration:underline">Dynamic Reports.</a><br>
				Select the type of Saved Report you wish to view by clicking on the appropriate title.<br>
				To open a Saved Report click on the desired report and click the button titled Submit.<br>
				(If you cannot see any Saved Reports it means none have been saved.)
			</td>
		</tr>
	</table>
</div>

<div id="sreporthelpdiv03" style="display:none;position:absolute;top:400px;left:10px;background-color:#FF3300;border: 1px solid black;color:white;width:140px">
	<table>
		<tr>
			<td align="right" style="color:white"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
		<tr>
			<td style="color:white">Now, you can select a specific report by clicking on it's heading.</td>
		</tr>
	</table>
</div>

<div id="sreporthelpdiv04" style="display:none;position:absolute;top:175px;left:595px;background-color:#FF3300;border: 1px solid black;color:white;width:80px">
	<table>
		<tr>
			<td style="color:white">To delete, click on a report and click the button titled Delete.</td>
		</tr>
		<tr>
			<td align="left" style="color:white"><img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
	</table>
</div>

<div id="sreporthelpdiv05" style="display:none;position:absolute;top:378px;left:595px;background-color:#FF3300;border: 1px solid black;color:white;width:65px">
	<table>
		<tr>
			<td align="left" style="color:white"><img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
		<tr>
			<td style="color:white">Click submit to view your report.</td>
		</tr>
	</table>
</div>

<div id="search2helpdiv02" style="display:none;position:absolute;top:65px;left:595px;background-color:#FF3300;border: 1px solid black;color:white;width:65px;">
	  To return to the Navigation Wizard <a href="../screens/playerrep.asp" style="color:white; text-decoration:underline">Click here</a>.
</div>

<div id="search2helpdiv03" style="display:none;position:absolute;top:65px;left:595px;background-color:#FF3300;border: 1px solid black;color:white;width:65px;">
	  To return to the Navigation Wizard <a href="../screens/playerrep.asp" style="color:white; text-decoration:underline">Click here</a>.
</div>

	<!--ASTHETICS-->
	<iframe width=168 height=202 name='gToday:normal:agenda.js' id='gToday:normal:agenda.js' src='../includes/js/ipopeng.htm' scrolling='no' frameborder='0' style='visibility:hidden; z-index:65535; position:absolute; top:0px; border:2px ridge;'></iframe>
	<table width="100%" bgcolor="#CcCcCc" border="0" cellpadding="0" cellspacing="0"><tr height="9"><td></td></tr><tr height="1" bgcolor="#eeeeee"><td></td></tr></table>
	<font face="Verdana, Arial, Helvetica, sans-serif" color="#333333" style="FONT-SIZE: 12px"><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff"><tr bgcolor="#dcdcdc"><td rowspan="2" width="181" height="20" valign="top" style="BORDER-BOTTOM: #c0c0c0 1px solid"><font face="Verdana, Arial, Helvetica, sans-serif" size="4" COLOR="#999999">&nbsp;<span name='spnPageHeader' id='spnPageHeader'></span></font></td><td width="19">&nbsp;</td><td>&nbsp;</td></tr><tr><td height="20"><img src="../images/corner2.gif" WIDTH="19" HEIGHT="20"></td><td style="BORDER-TOP: #c0c0c0 1px solid; BACKGROUND-COLOR: #f7f7f7">&nbsp;</td></tr></table>
	<!--END ASTHETICS-->
<%#@~^EggAAA==7@#@&(6P"+;!+kORwW.:vJOaD?;!CNkJ#,x~JrP:tnx@#@&ikWPU5EmNA4D+~@!@*PEJ,O4+U,/DD4+MnP{PEPStn.P?$;l9qf,rUPvJ,[~/O.U;;l9dPLPEbr@#@&7r6Pj+kdbWUvJUmtKW^i/DE#,'~ErPY4nx@#@&didnY,Dk?5EC[dkdY,xP1WUqc+6n^!Yn`rjVn1Y,eP6DKhP9D2?$EC[kPJ,'PkYDq4nDPLPEPG.9+.P(zPU;;C9J#77iB~MO,VrkY,W0,?$;l9/@#@&i+sd@#@&i7BM+/aGU/RSDrYn~r/nV^Y,qUdDkY;ObWUqGS,qUkYbYEDkKUP6DG:,N.2&x/DrY!YkKU~h4+M+~qUdDkOEDrW	q9~bxPcdVnmD~9kdDk	mY,q	dYbY;YbWU~6DWs~2swVKzn+GlDl~h4nM+~J,'PU+ddbWxcEGlOlrbv,b,[,JPmx9~q	/OkDEOrKxPbdP	WY,U;V^#r@#@&d7vM+dwKU/RnU9@#@&77k+OPMdU;;mNdk/DP{~mKxqR6n^!Y+vE/V+1O~q	/DkOEOrKx(fB~q	/OrDEYrG	PWDKh,N.aq	/YbY!OkKx~h4+.n,qxkOkDEYbGUqGPbx~`dn^+^Y,[kkYrU1YP(UkYrY!ObWU,0MW:,2s2VKXn+GlOC,ht.+,JPL~j+k/bWU`E9mYCJ*c,*P'~rPlU[,qU/DrDEObW	PkkP	GY,x;V^#~GMN+M~4HPq	dOkDEDkGxEbid7B,!+DPsrkYPGW,?5Em[k@#@&i+	NPb0@#@&did7did77iddi7diddi77didid7d77id7divPV+O~UYlOr1PCx9~GXUm:bmP"+aGDD/~/m\n[,0WM~Y4k/,KCoPmx[PO4b/~jknD@#@&7dYP.dUl-+9]wGMYkP',mKUFc+a+1EOnvJ/s+1YP"n2WMY&fSP]naW.Y	C:~~[mY+^.lO+9S,m.lD+N(XB~YHwn~,?C-NsMG:,0DKh~DwKDO/~A4+.+,OXa+~r	P`vjDlOk1vBB9Hxm:k1B*~l	N~?m\n[wDWs~',BJLKCo[rB~lU[,Z.+mO+9Az~{PBE',/ODGCDlik+Mqf,[rvPKD[+MP8z,I+aGDDxlsnE#@#@&iNGP;UDksPMd?m\n["+wG.D/ +KW@#@&7ik6PE1lkn`DDr:vDdjm\+9]+aWDDdcJDXa+E#bb,'~JGe1zH(/rPY4n	@#@&di7^kdD&,'P^kkO&,[~J@!r2ObWx,-l^E+{vEPLPM/jl-n9InwK.Yk`E]wW.O&fE#,',Jv,k9'BrPL~Dk?C\N]naWDDd`rYXanE#,[,Jv@*E~LP./UC\N]naWDOdvJ]+aGMYUm:J#,[,E@!JW2YbWU@*rddEOXa+@#@&77db0,xGY~rkx;V^cDk?C-NIn2KDO/v+*#~mx9PDk?m-+9InwKDOdv #,@!@*,JJ,O4+	P^kdY*~{PskkOc,[~E@!rwOrKx~\ms!+xEJ,[PM/UC\N]+aW.Ok`!*~[,JB@*E~[,0KDhlO[mYnYbh+vDdjm\+[]wGDDdv bB\(VW	o9CY#~[,J@!&KwYbGx@*JdE[CYmM+CYn[@#@&7ds/@#@&7idVrdDPxP^rkY~LPr@!raYbGx,\CV!+xvrP[,./Ul\[]+aWMYd`E]wGDD(fr#~',JB~r9'vJ,',DdUl7+N"+aGDD/cJDX2nr#PL~JE@*J,'~Dk?m\nN]naW.YkcJ"+2GMYxChJbPL~r@!&KwDkW	@*r7diBOXa+@#@&iddbWP	WY,rdx!V^`./jC7+[I2WMYdcy##~C	N~Dkjm\n9IwWMYkc *P@!@*,JE~Dt+	~Vb/Y8~xP^kkYqP'~r@!6wDrW	P-C^E+xvrP'PMdUl-N"+wKDDd`Z#~[,Jv@*rP[,WWM:lD[CYYb:n`.dUl-+9]+aW.Ok` bS74sW	L9lO#,[Pr@!JGwDkGx@*J7v9lY^DlY[@#@&idx[PrW@#@&7dMd?m\n["+wG.D/ :K-xnXY@#@&d^WK2@#@&din4CAA==^#~@%>
	<table style="position:absolute;left:40px;top:65px;" cellpadding="3" cellspacing="0" class="tblhere" bordercolor=#999999>
		<tr onclick="showRow('1'); <%#@~^IgAAAA==~b0~U+k/kKxvE1m\PXa+Eb,'PrqJ,YtU~MAoAAA==^#~@%> showNavSquads(); <%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>" style="cursor:hand;"><td class="headinghere" colspan="3"><table cellpadding="0" cellspacing="0" border="0" style="width:100%;"><tr><td style="width:100px;"></td><td style="text-align:center;">Squads</td><td style="width:100px;text-align:right;"><img id="Img1" name="row1" style="display:none;" src="../images/press.gif"></td></tr></table></td></tr>
		<tr id="row1" name="row1" style="display:block;"><td class="headinghere">Not Selected</td><td class="cellhere">&nbsp;</td><td class="headinghere">Selected</td></tr>
		<tr id="row1" name="row1" style="display:block;"><td class="cellhere">
				<select class="pulldown" multiple style="height:240px;width:250px" name="sel1a" id="sel1a" size="8" onDblClick="addCol(this,sel2a)" onkeydown="if(event.keyCode==13){addCol(this,sel2a);};">
		<%#@~^RwEAAA==@#@&d7b0,xWDPMd?$ECNdkdOc+W6~Y4+x@#@&7difKP;xOr^P./U5EmNJrkYRnG6@#@&di7iInkwKx/Rq.kD+~J@!r2ObWx,#l^E+{vEPLPM/j;;C9Sr/D 0b+s[k`!b~LPEB@*E@#@&7idiI+kwKU/R	DbYn~M/?$;l9SkkO 0b+^Nd`qb@#@&7di7I/2G	/+ 	MkO+,E@!z6aYbWx@*J@#@&did7Dk?5;mNSbdYc:W7nU+XY@#@&7d7sKW2@#@&7dx[~b0@#@&@#@&d7/K;Mmn,',J/V8CJ@#@&7d9+dObxlDrW	P',Ed+^ mJ@#@&77TVgAAA==^#~@%>
				</select>
			</td>
		<!--  #include file="buttons.asp" -->
			<td class='cellhere'>
				<select class="pulldown" multiple style="height:240px;width:250px" name="sel2a" id="sel2a" size="8" ondblclick="removeCol(this,sel1a);" onkeydown="if(event.keyCode==13){removeCol(this,sel1a);};"></select>
			</td></tr>
	<%#@~^HgAAAA==(6P2moP@!@*Pr\lk/JWmNn.rPY4nxJAkAAA==^#~@%>
		<tr onclick="showRow('2'); <%#@~^IgAAAA==~b0~U+k/kKxvE1m\PXa+Eb,'PrqJ,YtU~MAoAAA==^#~@%> showNavSaved(); <%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>" style="cursor:hand;"><td class="headinghere" colspan="3"><table cellpadding="0" cellspacing="0" border="0" style="width:100%;" ID="Table3"><tr><td style="width:100px;"></td><td style="text-align:center;">Saved Reports</td><td style="width:100px;text-align:right;"><img id="row2" name="row2" src="../images/press.gif"></td></tr></table></td></tr>
		<tr id="row2" name="row2" style="display:none;"></tr>
		<tr id="row2" name="row2" style="display:none;">
			<td class="cellhere" style="padding:2 2 0 2;cursor:hand;">
				<table cellpadding="0" cellspacing="0" border="0">
				<tr onclick="rowDynamic.style.display='none';imgStatic.style.display='none';rowStatic.style.display='block';imgDynamic.style.display='block';"><td class="headinghere" style="border-right:1px solid #999999;padding:2 0 2 0;cursor:hand;"><table cellpadding="0" cellspacing="0" border="0" style="width:95%;"><tr><td style="width:50px;"></td><td style="text-align:center;">Static Reports</td><td style="width:50px;text-align:right;"><img id="imgStatic" name="imgStatic" src="../images/press.gif"></td></tr></table></td></tr>
				<tr><td id="rowStatic" name="rowStatic" class="cellhere" style="border:0px;display:none;">
				<select align="left" class="pulldown" name="reportname" id="reportname" style="height:227px;width:270px" size="8" onchange="gd('reportname','created','createdby')">
					<%=#@~^BAAAAA==sb/OvAEAAA==^#~@%>
				</select>
				<select align="left" class="pulldown2" name="created" id="created" size="8">
					<%=#@~^BQAAAA==sb/O87QEAAA==^#~@%>
				</select>
			</td></tr>
				<tr onclick="rowStatic.style.display='none';imgDynamic.style.display='none';rowDynamic.style.display='block';imgStatic.style.display='block';"><td class="headinghere" style="border-right:1px solid #999999;padding:2 0 2 0;cursor:hand;"><table cellpadding="0" cellspacing="0" border="0" style="width:95%;" ID="Table6"><tr><td style="width:50px;"></td><td style="text-align:center;">Dynamic Reports</td><td style="width:50px;text-align:right;"><img style="display:none;" id="imgDynamic" name="imgDynamic" src="../images/press.gif"></td></tr></table></td></tr>
			<tr><td id="rowDynamic" name="rowDynamic" class="cellhere" style="border:0px;display:block;">
				<select align="left" class="pulldown" name="Dreportname" id="Dreportname" style="height:227px;width:270px" size="8" onchange="gd('Dreportname','Dcreated','Dcreatedby')">
					<%=#@~^BQAAAA==sb/O27wEAAA==^#~@%>
				</select>
				<select align="left" class="pulldown2" name="Dcreated" id="Dcreated" size="8">
					<%=#@~^BQAAAA==sb/OW8AEAAA==^#~@%>
				</select>
			</td></tr></table>
			</td>
			<td class="cellhere" rowspan="2">
				<table border="0" style="width:268px;">
					<!--TEXT BOXES-->
					<tr><td class="cellhere2">Date&nbsp;Created</td><td><input class="txtboxes" name="dc" id="dc" readonly></td></tr>
					<tr><td></td><td class="cellhere2"><input type='button' value='Delete' class="txtboxes" style='width:170px;height:18px;cursor:hand;background-color:#F3F7F8;' onmouseover="this.style.backgroundColor='#FFFFFF'" onmouseout="this.style.backgroundColor='#F3F7F8'" onclick='del()'></td></tr>
				</table>
			</td>
		</tr>
	<%#@~^BgAAAA==3	N~&05gEAAA==^#~@%>
		<tr><td class="headinghere" align="center" colspan="3"><input type='button' value='Submit' class="txtboxes" style='width:170px;height:18px;cursor:hand;background-color:#F3F7F8;' onmouseover="this.style.backgroundColor='#FFFFFF'" onmouseout="this.style.backgroundColor='#F3F7F8'" onclick='submitter()'></td></tr>
	</table>
	<form name="frmSelSquad" id="frmSelSquad" method=post action='<%=#@~^BAAAAA==KmonfQEAAA==^#~@%>.asp'>
		<input type="hidden" id="nnFunc" name="nnFunc" value="<%=#@~^EwAAAA==~M+5!+kY`rx	oE	mE#,RgYAAA==^#~@%>2">
		<input type=hidden id=txtSquads name=txtSquads >
		<input type=hidden id="reportname" name=reportname >
	</form>
	<script language="javascript">
	function gd(sel1,sel2,sel3){
		eval("frmSelSquad.reportname.value="+sel1+".value")
		eval(sel2+".value="+sel1+".value")
		eval("dc.value = "+sel2+".options["+sel2+".selectedIndex].text")
		eval("frmSelSquad.reportname.value = "+sel1+".value")
	}

	function del(){
		if(frmSelSquad.reportname.value!=''){
			frmSelSquad.action ="../includes/asp/delete.asp"
			frmSelSquad.submit()
		}else{
			alert('Please select a report to delete')
		}
	}
	function submitter(){
		<%#@~^HgAAAA==r6P2moP@!@*Pr\lk/JWmNn.rPY4nxRAkAAA==^#~@%>
		if(row2[1].style.display=="block"&&dc.value!=""){
			frmSelSquad.action = "SavedReport.asp"
			frmSelSquad.submit()
		}else
		<%#@~^BgAAAA==n	N~b0JgIAAA==^#~@%>
		{
			var colsComma1 = ""
			for (i = 0; i < <%=#@~^DQAAAA==~9+dDk	lYbW	~4gQAAA==^#~@%>.children.length; i++){
	 			colsComma1 = colsComma1 + "," +<%=#@~^DQAAAA==~9+dDk	lYbW	~4gQAAA==^#~@%>.children[i].value;
			}
			strCols = colsComma1.substr(1);
			if(strCols==""){alert("Please select at least one Squad")}else{
				frmSelSquad.txtSquads.value = strCols;	
				frmSelSquad.submit()
			}
		}
	}
	//this closes the search frame if that frame was open.
	if (	parent.parent.document.all.sclick.rows == "63,*"){
		parent.parent.document.all.sclick.rows = "19,*";
		parent.parent.frames.Search.document.all.searchystuffy.style.display='none';	
		parent.parent.frames.Search.document.all.gb.src="../../images/down.gif";
		parent.parent.frames.Search.document.all.gb.alt = "Display Search Screen"
	}

	// Function that is used with the new navigation for showing the user how to display saved reports
	function showNavSaved()
	{
		document.all.reporthelpdiv01.style.display = "none";
		document.all.reporthelpdiv02.style.display = "none";
		document.all.reporthelpdiv03.style.display = "none";
		document.all.reporthelpdiv04.style.display = "none";
		document.all.reporthelpdiv05.style.display = "none";
		document.all.reporthelpdiv06.style.display = "none";
		document.all.reporthelpdiv07.style.display = "none";
		document.all.sreporthelpdiv01.style.display = "none";
		
		document.all.reporthelpdiv12.style.display = "none";
		document.all.reporthelpdiv13.style.display = "none";

		<%#@~^JgAAAA==~b0~M+$E+kYvEx	s;x1Jb~{PJ9nVY+r~Otx,OAwAAA==^#~@%>
		document.all.sreporthelpdiv04.style.display = "";
		document.all.search2helpdiv03.style.display = "";
		<%#@~^BgAAAA==~VdP6QEAAA==^#~@%>
		document.all.sreporthelpdiv02.style.display = "";
		document.all.search2helpdiv03.style.display = "";
		<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
	}
	
	function showNavSquads()
	{
		document.all.reporthelpdiv01.style.display = "";
		document.all.reporthelpdiv02.style.display = "";
		document.all.reporthelpdiv03.style.display = "none";
		document.all.reporthelpdiv04.style.display = "none";
		document.all.reporthelpdiv05.style.display = "none";
		document.all.reporthelpdiv06.style.display = "none";
		document.all.reporthelpdiv07.style.display = "none";
		
		document.all.reporthelpdiv12.style.display = "none";
		document.all.reporthelpdiv13.style.display = "none";
		
		document.all.sreporthelpdiv01.style.display = "none";
		document.all.sreporthelpdiv02.style.display = "none";
		//document.all.sreporthelpdiv03.style.display = "none";
		document.all.sreporthelpdiv04.style.display = "none";
		//document.all.sreporthelpdiv05.style.display = "none";
	}
	</script>
<%#@~^6gIAAA==@#@&+sk+@#@&dEPVnY,Y4+,VrdDPW6~/$El9d~l	N,0rVOnMPOt~Vb/O~K0P2smXnDk~(X~mxHPmmxkn+,m.kD+.rmP0K.P!/+M~sWToN~kU @#@&7?$;l9b..mXPx~kwskDcM+5!+kY`rYXO?$ECNkJbSr~J*@#@&i0WM~nl1t,+s+hn	Y~k	~?$EC[zDDCz@#@&7dbW,?nk/bWxvJU^tKWsjk+.E*P',EJ,YtU@#@&idi/OD	4DnP{~/DD	4D+~',JGD,nswsKX+fmYm ?$ECNkPsr0+PEYurP[,ns+s+	Y~[~E-]vPr@#@&idnsk+@#@&7iddYM	4+.P{P/DDq4+M+~[,JG.,+:asWH++GCOlcq	/OkO;DkGx,sk0+~vuJP'~Vn:UDP',JuBPr@#@&7dx[Pb0@#@&ix+XO@#@&d/D.	tDPxPEA4+.+,cJ,[~hbN`dOM4+MnBPf*PLPJ*J@#@&db0~/DD/C	/+~@!@*PJr~Otx,/OD	4DnP{~/DD	4D+~',J~l	[,J~LPkYD;l	d+@#@&d@#@&7vM+/aGxk+RS.rYPkY.4nM+@#@&ivD/2G	/+ n	N@#@&idY~M/dk/DF,xP1WUFc+an1EYcJU+V^OPV+	q9~~obDdYgC:PQ~EPB~Q,?;D	Cs+S,?$El9/,WDK:~2swsGH++GCYmPJ,'~/DDqtnDn~LPEPK.ND~8HPsr.kYUlsnr#@#@&dmd0AAA==^#~@%>
	<html><body>
	<table id="tblMain" name="tblMain" style="position:absolute;left:40px;top:65px;" cellpadding="0" cellspacing="0" bordercolor=#999999><tr><td colspan="3" style="width:100%;border-right:1px solid #999999;">
		<table cellpadding="3" cellspacing="0" style="width:100%">
			<tr onclick="showRow('1')" style="cursor:hand;"><td class="headinghere" colspan="3"><table cellpadding="0" cellspacing="0" border="0" style="width:100%;"><tr><td style="width:100px;"></td><td style="text-align:center;">Players</td><td style="width:100px;text-align:right;"><img id="row1" name="row1" style="display:none;" src="../images/press.gif"></td></tr></table></td></tr>
			<tr style="display:block;" id="row1" name="row1">
				<td class="headinghere">Not Selected</td>
				<td class="cellhere">&nbsp;</td>
				<td class="headinghere">Selected</td>
			</tr>
			<tr style="display:block;" id="row1" name="row1">
				<td class="cellhere">
					<select class="pulldown" multiple style="width:250px;height:<%#@~^TAAAAA==r6PKmo'JtlkdSKl[+MJ~O4+x,]+kwW	dnRqDbYnPE+W!26r~+^/n~"+/2G	/nRq.bYn,J8c!a6rgxkAAA==^#~@%>;" name="sel1" id="sel1" size="8" onDblClick="addCol(this,sel2)" onkeydown="if(event.keyCode==13){addCol(this,sel2);};">
			<%#@~^PAEAAA==@#@&d7ik6PxKY,./dkdY8RnG6PY4nx@#@&di77fKP!xOks~M/JkkOFc+GW@#@&d77id]+k2KxdRqDkD+,E@!}wOkKx~#mVExBrP[,.dSb/DF 0rn^Nd`ZbPLJv@*r@#@&77id7IdaWUk+cDbY~DkSr/DF Wb+V9d`8#@#@&77did"+dwGUk+ MrYPE@!JrwOrKx@*J@#@&id7idM/Sb/DqRsW-+	+aO@#@&di7d^WWa@#@&didx[PrW@#@&@#@&i7dkW;.1+d7x,Jd+^qr@#@&idiN+kYbUlDkGxi'~Ek+VyE@#@&ddiqE4AAA==^#~@%>
					</select>
				</td>
				<!--  #include file="buttons.asp" -->
				<td class="cellhere"><select class="pulldown" multiple style="width:250px;height:<%#@~^TAAAAA==r6PKmo'JtlkdSKl[+MJ~O4+x,]+kwW	dnRqDbYnPE+W!26r~+^/n~"+/2G	/nRq.bYn,J8c!a6rgxkAAA==^#~@%>;" name="sel2" id="sel2" size="8" ondblclick="removeCol(this,sel1);" onkeydown="if(event.keyCode==13){removeCol(this,sel1);};"></select></td>
			</tr>
			<%#@~^SAAAAA==r6PKmoP@!@*Pr\bU?Jrzf3]rPY4nx,I+k2Gxk+c.kOn,J@!zDC4^+@*@!JYN@*@!JY.@*r8RMAAA==^#~@%>