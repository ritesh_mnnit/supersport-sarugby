/*=============================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================*/

var gsAction=" ";	// Default action to be performed on everyday, except agenda days.

function PublicHols(Y) {

	arrMonths = new Array('January','February','March','April','May','June','July','August','September','October','November','December')
	arrMonthsCob = new Array('1','2','3','4','5','6','7','8','9','10','11','12')

    var C = Math.floor(Y/100);
    var N = Y - 19*Math.floor(Y/19);
    var K = Math.floor((C - 17)/25);
    var I = C - Math.floor(C/4) - Math.floor((C - K)/3) + 19*N + 15;
    I = I - 30*Math.floor((I/30));
    I = I - Math.floor(I/28)*(1 - Math.floor(I/28)*Math.floor(29/(I + 1))*Math.floor((21 - N)/11));
    var J = Y + Math.floor(Y/4) + I + 2 - C + Math.floor(C/4);
    J = J - 7*Math.floor(J/7);
    var L = I - J;
    var M = 3 + Math.floor((L + 40)/44);
    var D = L + 28 - 31*Math.floor(M/4);

	parsed = Date.parse(arrMonths[M-1] + ' ' + D + ', ' + Y)
	
	dateEasterFri = new Date(parsed-(86400000*2))
	dateEasterMon = new Date(parsed+86400000)
	
	Goodfriday = dateEasterFri.getDate() + ',' + arrMonthsCob[dateEasterFri.getMonth()] 
	Familyday= dateEasterMon.getDate() + ',' + arrMonthsCob[dateEasterMon.getMonth()]
	
	return Goodfriday
	return Familyday
}

////////////////////////////////////////////////////////////////////////////////
// Holiday PLUG-IN Function -- will return [message,color,action,imgsrc] like agenda!
////////////////////////////////////////////////////////////////////////////////

function fHoliday(y,m,d) {
PublicHols(y)

//PublicHols(y)
  var r=agenda[y+"-"+m+"-"+d]; // check agenda table with designated date format
  
  if (r) return r;	// if there is a defined agenda, then skip the holiday highlights.

gf = Goodfriday.split(",")
fd = Familyday.split(",")
ffd =  fd[0]
ffm =  fd[1]

var td = new Date()

  if (m==12&&d==25)
	r=["Merry Christmas!", "seagreen",gsAction];
  else if (m==12&&d==26)
	r=["Day of Goodwil!", "skyblue",gsAction];
	
  else if (m==1&&d==1)
  r=[" New Year's Day! ", "blue",gsAction];
  
    else if (m==3&&d==23)    
  r=["Human Rights Day! ", "lightGoldenrodyellow",gsAction];
  
    else if (m==4&&d==27)
  r=["Freedom Day! ", "red",gsAction];
  
    else if (m==5&&d==1)
  r=["Workers Day! ", "#EEE8AA",gsAction];
  
    else if (m==6&&d==16)
  r=["Youth Day! ", "#9ACD32",gsAction];
  
    else if (m==8&&d==9)
  r=["National Women�s Day! ", "#ADD8E6",gsAction];
  
    else if (m==9&&d==24)
  r=["Heritage Day! ", "#FFA500",gsAction];
  
    else if (m==12&&d==16)
  r=["Day of Reconciliation'! ", "#00008B",gsAction];

    else if (d==gf[0]&&m==gf[1])
  r=["Goodfriday! ", "#1E90FF",gsAction];

   else if (d==ffd&&m==ffm)
  r=["Familyday!", "#FF8C00",gsAction];
  
  return r;
}

//////// Put all your self-defined functions to the following /////////
function popup(url, framename) {
  var w=parent.open(url,framename,"top=200,left=200,width=400,height=200,scrollbars=1,resizable=1");
  if (w&&!framename) w.focus();
}



