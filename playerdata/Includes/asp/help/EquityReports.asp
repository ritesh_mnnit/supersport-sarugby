<%@ Language=VBScript.Encode %>
<html>
<head><link REL="stylesheet" HREF="css/general.css" type="text/css">
<title>Help with equity reports</title>
</head>
<body bgcolor=#f5f5f5>
<table align="center" style="border-bottom:1px solid #DEDEDE"><tr><th class="header1">Equity Reports</th></tr></table> 
<table><tr><td>To run Equity reports one clicks on this icon <img src="images/Reports.gif"> which can be found in the top left hand corner of the screen.</td></tr></table>
<table><tr><td>On clicking on the icon the user is presented with a list of the equity reports.</td></tr></table>
<table><tr><td>The following is a list of those reports:</td></tr></table>
<table><tr><td>
<li>B2</li> 
<li>B3</li> 
<li>B4</li> 
<li>B5</li> 
<li>C6</li> 
<li>C7</li> 
<li>C8.1</li> 
<li>C8.2</li> 
<li>D9</li> 
<li>E10</li> 
<br><br>
It is important to remember that the reports wont be accurate if the relevant screens containing information needed for the reports aren't filled in properly. The following is a list of fields and screens that will be used to retrieve data for reports:
<br>
<br>
<li>B2 - Employee Details screen: Check that the following fields are filled in correctly: Occupational Category, Gender, Ethnic origin, Employment type.</li> 
<li>B3 - Employee Details screen: All of the above fields and also Disabled.</li> 
<li>B4 - Employee Details screen: All of the fields in the B2 report and also Occupational Level.</li> 
<li>B5 - Employee Details screen: All of the above fields and also Disabled.</li> 
<li>C6 - Employee Details screen: All of the fields for the B4 report and also the Date Employed field.</li> 
<li>C7 - Employee Details screen: Promotions screen. All of the fields above and occupational level. It is important to note that a promotion is defined as a change in occupational level.</li> 
<li>C8.1 - Employee Details screen: All the fields in the B4 report and also the Date of Leaving.</li> 
<li>C8.2 - Employee Details screen: All the above fields and also Reason for Leaving.</li> 
<li>D9 - Employee Details screen: Disciplinary Screen. All of the fields for the B2 report and date fields in the Disciplinary.</li> 
<li>E10 - Employee Details screen: Training screen. All of the fields for the B4 report as well as date fields in the Training.</li> 
<br><br>
It is important to remember that the reports wont be accurate if the relevant screens containing information needed for the reports aren't filled in properly. The following is a list of fields and screens that will be used to retrieve data for reports:
</table>
<%#@~^JwAAAA==~b0~M+$E+kYvEtV2YHwnE*P',E/bxo^nEPDtxKg0AAA==^#~@%>
<br><center><a href="#" onclick="javascript:parent.window.close()" style="font-size:10px; font-weight:bold">Close Window</a></center>
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
</body>
</html>