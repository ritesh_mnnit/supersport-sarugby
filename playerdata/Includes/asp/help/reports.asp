<%@ Language=VBScript.Encode %>
<html>
<head><link REL="stylesheet" HREF="css/general.css" type="text/css"></head>
<title>Help with reports</title>
<body bgcolor=#f5f5f5>
<table align="center" style="border-bottom:1px solid #DEDEDE"><tr><th class="header1">Reporting: New & Saved reports</th></tr></table>
<table><tr><td>To generate custom reports the user clicks on this <img src="images/customreports.gif"> 
 button which can be found in the top left-hand corner of the screen.</td></tr></table>
<tr><td style="height:10px"></td></tr>
<table><tr><td>There are five steps to creating a custom report:</td></tr></table>
<tr><td style="height:10px"></td></tr>
<table><tr><td>
<ol><li>The user has to select which screens are to be used (or searched for data)
in the report. Screens are also referred to as tables. To select which screens 
are going to be used in the report the user simply has to scroll down in the 
complete list of screens in the left hand column and double click on the ones
which are to be used. The selected screens will appear in the right hand column. 
Once finished, the user clicks on Next.</li>
<li>The user now has to select what fields are to be viewed in the report.  
The fields (referred to as columns) are what contain the information within the individual screens.  
The left hand column will contain a complete list of fields for all the screens selected.  
The user just double clicks on the fields which are to be used in the report.  
They will appear in the right hand column of the report generator.</li>
<li>Next the user has the option to add search criteria to the report.  
This is done by first selecting that criteria will be added to the report by checking the check box for Select Search Criteria.  
Then by selecting a specific field and adding to it the criteria for the report. 
The following is a list of criteria which can be used:</li>
</ol>
<li>Is equal to</li> 
<li>Is between</li> 
<li>Is greater Than</li> 
<li>Is Less Than</li> 
<li>Is Greater Than or Equal To</li> 
<li>Is Less Than Or Equal To</li> 
<li>Contains</li> 
<li>Beginning with</li> 
<li>Is Not Equal To</li> 
<li>Is Not Between</li> 
<li>Does not Contain</li> 
<li>Not Beginning with</li> 
<li>Is null</li> 
<li>Is not null</li> 
<li>In list</li> 
<li>Not in list</li> 
<ol start='4'><li>The next step is to decide how the results will be sorted.  
First the user has to select the option to sort results by clicking on the Order by check box.  
The user will then be able to decide on a field (column) to sort the results by and decide whether result should be Ascending or Descending.</li></ol>
<br>
<ol start='5'><li>The next step is to decide how the results will be sorted.  
First the user has to select the option to sort results by clicking on the Order by check box.  
The user will then be able to decide on a field (column) to sort the results by and decide whether result should be Ascending or Descending.</li></ol>
<table><tr><td>By clicking on Generate the system will start running the report.  
The user will be presented with the option of saving the report.  
If the report does not get saved it will still be run.</td></tr></table>
<br>
<table align="center" style="border-bottom:1px solid #DEDEDE"><tr><th class="header1">To view Saved Reports</th></tr></table>
<table>
<tr><td style="height:10px"></td></tr>
<tr><td>To view saved reports the user has to click on this icon <img src="images/existingreport.gif"> in the top left hand corner of the screen.</td></tr>
<tr><td style="height:10px"></td></tr>
<tr><td>On selecting this option the user will be presented with a complete list of reports saved in the system.  To view a report simply:</td></tr>
<tr><td style="height:10px"></td></tr>
<tr><td>
<li>Select it</li>
<li>click on View Data</li>
<li>Or click on Export Data to export it to MS Excel</li>
</td></tr>
<tr><td style="height:10px"></td></tr>
<tr><td>The user can also delete a saved report by clicking on Delete Report</td></tr>
</table>
</table>
<%#@~^JwAAAA==~b0~M+$E+kYvEtV2YHwnE*P',E/bxo^nEPDtxKg0AAA==^#~@%>
<br><center><a href="#" onclick="javascript:parent.window.close()" style="font-size:10px; font-weight:bold">Close Window</a></center>
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
</body>
</html>
