<%@ Language=VBScript.Encode %>
<html>
<head>
<link REL="stylesheet" HREF="css/general.css" type="text/css">
<title>Help with mass capture screen</title>
</head>
<body bgcolor=#f5f5f5 topmargin="0" rightmargin="0">

<table align="center"style="border-bottom:1px solid #DEDEDE"><tr><th class="header1">Mass Capture Screen</th></tr></table>
<br>

The mass capture screen is used to capture the test results for up to twelve players at a time. To begin with,
you need to start capturing the results of the players. You will notice that the players that you selected
are indicated on the top row of the right hand portion of the screen.
<br><br>
<center><img src="images/masscapnames.gif" alt="" width="330" height="64" border="0"></center>
<br><br>
You will notice that on the left hand portion of the screen, there is a list of fields which correspond
to the values in the right hand portion of the screen. For example, age will accordingly have all the players
ages in the corresponding rows on the right hand portion of the screen. By portion I am referring to the
areas of the screen separated by the scroll bars.
<br><br>
<center><img src="images/masscaptureage.gif" alt="" width="402" height="22" border="0"></center>
<br><br>

<center>
<table cellpadding="8">
	<tr>
		<td><img src="images/masscapfields.gif" alt="" width="195" height="235" border="0"></td>
		<td>
			The values listed underneath Age, are the values which you can use the mass capture tool to store. They
			are all fields from the tests and skills pages of the system.
		</td>
	</tr>
</table>
</center>

<br><br>

<center>
<table cellpadding="8">
	<tr>
		<td>
			For each player you can now begin inputting data. Simply read the column on the left hand side of the
			screen, and input the corresponding value in the text box on the right hand side of the screen. You can
			use the <strong>TAB</strong> key on your keyboard or the mouse to move between the input boxes.
		</td>
		<td valign="center">
			<img src="images/masscapinput.gif" alt="" width="283" height="240" border="0">
		</td>
	</tr>
</table>
</center>

<br><br>

Once you have entered all the data for the selected players, and are confidant that you have entered the correct data,
you can save the results by clicking on the save records button in the top left hand corner of the mass capture
screen.

<br><br>
<center><img src="images/masscapsave.gif" alt="" width="115" height="36" border="0"></center>

<%#@~^JwAAAA==~b0~M+$E+kYvEtV2YHwnE*P',E/bxo^nEPDtxKg0AAA==^#~@%>
<br><center><a href="#" onclick="javascript:parent.window.close()" style="font-size:10px; font-weight:bold">Close Window</a></center>
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
</body>
</html>