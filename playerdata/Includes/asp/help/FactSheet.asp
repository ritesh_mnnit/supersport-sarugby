<html>
<head><link REL="stylesheet" HREF="css/general.css" type="text/css">
<title>Help with fact sheet</title>
</head>
<body bgcolor=#f5f5f5>
<table align="center" style="border-bottom:1px solid #DEDEDE">
<tr><th class="header1">Fact Sheet</th></tr>
</table>
<table>
<tr><td>To open the Fact Sheet tool click on this <img src="images/fact.gif">  icon.</td></tr>
<tr><td style="height:5px"></td></tr>
</table>
<table>
<tr><td colspan="2"><img src="images/factsheet.gif"></td></tr>
<tr><td style="height:5px" colspan="2"></td></tr>
<tr><td colspan="2">The Fact Sheet tool has three basic functions.</td></tr>
<tr><td style="height:5px" colspan="2"></td></tr>
<tr><td colspan="2"><ol>
<li>You can browse the current files</li>
<li>You can upload new files</li>
<li>You can delete the current files</li>
</ol></td></tr>
<tr><td colspan="2">To browse a current file double click on the title of the file you want, the list of all the current files is on the left side of the Fact Sheet tool.</td></tr>
<tr><td style="height:5px" colspan="2"></td></tr>
<tr><td colspan="2">This icon is for uploading new files into the Fact File tool.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="images/uploader.gif"></td></tr>
<tr><td style="height:5px" colspan="2"></td></tr>
<tr><td>This icon is for deleting current files from the Fact File tool.&nbsp;&nbsp;&nbsp;<img src="images/delete.gif"></td></tr>
<tr><td colspan="2">To upload a new file click on the upload <img src="images/uploader.gif"> button, when you do so a new button will appear in its place.<br><center><img src="images/browse.gif"></center></td></tr>
<tr><td style="height:5px"></td></tr>
<tr><td>Click on the new browse button, this will open a page that will let you search your pc for the file you want to upload, when you have found the file you want to upload double click on it and it's name will appear in a new box. Simply click the upload button and you are done.</td><td><img src="images/browser.gif"></td></tr>
<tr><td colspan="2">To delete a current file click on the one you wish to delete and click the delete <img src="images/delete.gif"> button.</td></tr>
</table>
<%#@~^JwAAAA==~b0~M+$E+kYvEtV2YHwnE*P',E/bxo^nEPDtxKg0AAA==^#~@%>
<br><center><a href="#" onclick="javascript:parent.window.close()" style="font-size:10px; font-weight:bold">Close Window</a></center>
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
</body>
</html>