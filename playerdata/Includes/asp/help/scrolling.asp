<%@ Language=VBScript.Encode %>
<html>
<head>
<link REL="stylesheet" HREF="css/general.css" type="text/css">
<title>Help with scrolling functionality</title>
</head>
<body bgcolor=#f5f5f5>
<table align="center" style="border-bottom:1px solid #DEDEDE"><tr><th class="header1">Scrolling Functionality</th></tr></table>
<table><tr style="cursor:hand;">
<td><img class="buttons" src="images/prevrecord.gif"></td>
<td><img class="buttons" src="images/prevrec.gif"></td>
<td><img class="buttons" src="images/nextrec.gif"></td>
<td><img class="buttons" src="images/nextrecord.gif"></td>
<td><img class="buttons" src="images/update.gif"></td>
<td><img class="buttons" src="images/delete.gif"></td>
<td><img class="buttons" src="images/new.gif"></td>
<td><img class="buttons" src="images/ok.gif"></td>
<td><img class="buttons" src="images/summary.gif"></td>
</tr></table>
<br>
<table>
<tr><td width="40"><img class="buttons" src="images/prevrecord.gif"></td><td>Go to previous player: This button will take you the user to the previous player's record.</td></tr>
<tr><td><img class="buttons" src="images/nextrecord.gif"></td><td>Go to next player: This button will take you the user to the next player's record.</td></tr>
<tr><td><img class="buttons" src="images/prevrec.gif"></td><td>Go to previous record: Some of the screens have multiple records for a single player, this button is for scrolling to the previous record.</td></tr>
<tr><td><img class="buttons" src="images/nextrec.gif"></td><td>Go to next record: Some of the screens have multiple records for a single player, this button is for scrolling to the next record.</td></tr>
<tr><td><img class="buttons" src="images/update.gif"></td><td>Update record: If you make any changes to any record and you want to save your changes then click the update button.</td></tr>
<tr><td><img class="buttons" src="images/delete.gif"></td><td>Delete record: This button will delete the record currently in view.</td></tr>
<tr><td><img class="buttons" src="images/new.gif"></td><td>Create new record: When you click this button the button shown below will take its place and the current screen will be cleared so you may insert new information. Once completed you can save the new information by clicking the new button that took the old ones place.</td></tr>
<tr><td><img class="buttons" src="images/ok.gif"></td><td>Insert new record: This will submit to the database all the information displayed on the current screen.</td></tr>
<tr><td><img class="buttons" src="images/summary.gif"></td><td>Summary view: This button opens a summary screen to view the information on the current screen in a Excel style view. On the summary screen if you click on a players record it will return to the previous screen with that player's record. By clicking on a column heading, all the information will be ordered descending by the column that has been clicked. Clicking on that column heading again will change the order to ascending.</td></tr>
</table>
<%#@~^JwAAAA==~b0~M+$E+kYvEtV2YHwnE*P',E/bxo^nEPDtxKg0AAA==^#~@%>
<br><center><a href="#" onclick="javascript:parent.window.close()" style="font-size:10px; font-weight:bold">Close Window</a></center>
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
</body>
</html>