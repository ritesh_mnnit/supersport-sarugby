<%@ Language=VBScript.Encode %>
<html>
<head>
<link REL="stylesheet" HREF="css/general.css" type="text/css">
<title>Explanation of the word Static</title>
</head>
<body bgcolor=#f5f5f5>
<table>
	<tr>
		<td align="center"><b><u>An explanation of a "Static Report"</u></b></td>
	</tr>
	<tr>
		<td style="font-size:4pt;">&nbsp;</td>
	</tr>
	<tr>
		<td>When a report is saved as static, it means that the information displayed when the report is created is the information that will always be displayed when the report is opened. <br>(The information does not change, therefore it is static)</td>
	</tr>
	<tr>
		<td style="font-size:15pt;">&nbsp;</td>
	</tr>
	<tr>
		<td align="center"><b><u>An explanation of a "Dynamic Report"</u></b></td>
	</tr>
	<tr>
		<td style="font-size:4pt;">&nbsp;</td>
	</tr>
	<tr>
		<td>When a report is saved as dynamic, it means that the information displayed when the report is created is updated as the information in the system is changed. <br>(The information does change, therefore it is dynamic)</td>
	</tr>
	<tr>
		<td style="font-size:5pt;">&nbsp;</td>
	</tr>
</table>
<%#@~^JwAAAA==~b0~M+$E+kYvEtV2YHwnE*P',E/bxo^nEPDtxKg0AAA==^#~@%>
<br><center><a href="#" onclick="javascript:parent.window.close()" style="font-size:10px; font-weight:bold">Close Window</a></center>
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
</body>
</html>