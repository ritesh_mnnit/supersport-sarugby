<html>
<head>
<link REL="stylesheet" HREF="css/general.css" type="text/css">
<title>Help with file explorer</title>
</head>
<body bgcolor=#f5f5f5>
<table align="center" style="border-bottom:1px solid #DEDEDE">
<tr><th class="header1">File Explorer</th></tr>
</table>
<br>
<table>
<tr><td colspan=3>To open the File Explorer click on this <img src="images/fact.gif"> icon</td></tr>
<tr><td style="height:2px" colspan=3></td></tr>
<tr><td colspan=3><img src="images/FileEx.gif"></td></tr>
<tr><td colspan=3>The file explorer is divided into two windows. The left hand window contains a list of all the documents which are saved on an employee's record. <br>When one highlights a file, the right hand window will display certain details about the document.</td></tr>
<tr><td style="height:2px" colspan=3></td></tr>
<tr><td colspan=3>Below the above windows is the employee section which displays the employee id and name of the employee who's files are being looked at.</td></tr>
<tr><td style="height:2px" colspan=3></td></tr>
<tr><td colspan=3>Then there is a scrolling tool which allows users to:</td></tr>
<tr><td style="height:2px" colspan=3></td></tr>
<tr><td width="27%"><li>Scroll between employees.</li></td><td width=3%><img src="images/prevrecord.gif"></td><td valign=top><img src="images/nextrecord.gif"></td></tr>
<tr><td><li>Upload new files to the file explorer.</li></td><td>&nbsp;</td><td><img src="images/uploader.gif"></td></tr>
<tr><td><li>Delete files from the File Explorer.</li></td><td>&nbsp;</td><td><img src="images/delete.gif"></td></tr>
<tr><td style="height:2px" colspan=3></td></tr>
<tr><td colspan=3>The file explorer allows users to attach documents to an employees record. Users can attach any type or format of document to an employee. This means that any file or document which is pertinent to an employees record (CV, pictures, spreadsheets etc.) can be stored in the employees record on the system.</td></tr>
<tr><td style="height:2px" colspan=3></td></tr>
<tr><td colspan=3>This tool also dynamically links to the mail merge tool which means that if a document is prepared for a certain employee from the mail merge facility, it will automatically log that a document was prepared for the employee in the file explorer if the users selects the option from within the mail merge facility.</td></tr>
</table>
<%#@~^JwAAAA==~b0~M+$E+kYvEtV2YHwnE*P',E/bxo^nEPDtxKg0AAA==^#~@%>
<br><center><a href="#" onclick="javascript:parent.window.close()" style="font-size:10px; font-weight:bold">Close Window</a></center>
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>
</body>
</html>



