<%@ Language=VBScript.encode%>
<!--===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<!-- #include file="../config/DB_Connectivity.asp" -->
<!-- #include file="../config/data.asp" -->
<%

Dim original
original = SetLocale("en-gb")

'Function to return the value for a drop down field
function fnDropDownValue(strValue, strTable, strField)
	if strValue = "null" then
		fnDropDownValue = strValue
	else
		if mid(strField, 1, 3) = "sel" then
			'response.write "Exec prcDropDownValue " & strValue & ",'" & strTable & "','" & mid(strField, 4) & "'"
			'response.end
			set rsTempData = con1.execute("Exec prcDropDownValue " & strValue & ",'" & strTable & "','" & mid(strField, 4) & "'")
				if not rsTempData.eof then
					fnDropDownValue = rsTempData(0)
				else
					fnDropDownValue = strValue
				end if
			set rsTempData = nothing
		else
			fnDropDownValue = strValue
		end if
	end if
end function

Dim DevDB,RepDB
DevDB			= "[PlayerDev]":RepDB = "[PlayerManagerTemp]"
changedfields	= request("changedfields")																															' comma delimited list of fields changed 
table			= request("TableName")
ID				= request("ID")																																		' ID of current record
UKEY			= request("Ukey")																																		' Unique column name
where			= request("hidSQL1")																																	' where string
'The recordset is for getting the previous value for the log file
set rslog = con1.execute("Select * from "& table &" where " & request("UKEY") &" = "& "'" & ID &"'" )

Dim strSQLCols, strSQLVals,Table
strSQLVals = ""
'-----------------GENERAL UPDATE STATEMENT FOR THE TABLE
for each element in request.Form
	if ucase(element) <> "HIDLATESTINSERT" and ucase(element) <> "HIDSQL1" then
		if trim(request.Form(element)) = "" then value = "null" else value = "'" & trim(replace(request.Form(element),"'","''")) & "'"
		strSQLVals = strSQLVals & ", " & "[" & mid(element,4) & "]" & " = " & value
		'creates a html string for the log file only for the changed fields also shows the previous value of the field
		if instr(changedfields,element) <> 0 then
			if isnull(rslog(mid(element,4))) then strOld = "No Previous Value" else strOld = fnDropDownValue(rslog(mid(element,4)), table, element)
			logstring  = logstring & "<Tr><Td width='33%'>" & mid(element,4)&  "</td><td width='33%'>" & strOld  & "</td><td width='33%'>"& replace(fnDropDownValue(value,table,element), "'", "") &"</td></tr>"
		end if
	end if
next
strSQL = " update "& table &" set " & mid(strSQLVals,2) & " where "& request("UKEY") &" = "& "'" & ID &"'"

'---------- changes value to null without single inverted commas if value = nothing. Else puts the commas around the value.
function nuller(value)
	if value ="" then
		value = "null"
	else
		value = "'" & value & "'"
	end if
	nuller =value
end function



if ucase(request("tablename")) = "EMPLOYEEDATA" then
	'This is where the duplicate check will go, before the update so as to ascertain whether or not there are records
	'with similar values
	
	'There are three checks to do:
	'1. Check with the player name, surname and date of birth
	'2. Check with the ID number
	'3. Check with the passport number
	
	'1. Check - Name, Surname, DOB
	
	strCheckSQL = "select GenID from EmployeeData where (Surname = '" & replace(request("txtSurname"), "'", "''") & "' and Firstname = '" & replace(request("txtFirstName"), "'", "''") & "' and DOB = '" & request("txtDOB") & "') and GenID <> '" & ID & "'"
	'response.write strCheckSQL
	'response.end
	Set rsCheck1 = con1.execute(strCheckSQL)
	if rsCheck1.eof = false then
		%>
			<script Language="JavaScript">
				alert("You are attempting to create a duplicate record, there is already a player in the \ndatabase with the same:\n\n- Firstname\n- Surname\n- Date of Birth")
				history.back()
			</script>
		<%
		response.end
	end if
	rsCheck1.close
	Set rsCheck1 = nothing
	
	'2. Check the ID number
	strCheckSQL = "select GenID from EmployeeData where (IDNumber = '" & request("txtIDNumber") & "') and GenID <> '" & request("ID") & "'"
	Set rsCheck1 = con1.execute(strCheckSQL)
	if rsCheck1.eof = false then
		%>
			<script Language="JavaScript">
				alert("You are attempting to create a duplicate record, there is already a player in the \ndatabase with the same:\n\n- ID Number ")
				history.back()
			</script>
		<%
		response.end
	end if
	rsCheck1.close
	Set rsCheck1 = nothing
	
	'3. Check the Passport number
	strCheckSQL = "select GenID from EmployeeData where (PassportNo = '" & request("txtPassportNo") & "')  and GenID <> '" & request("ID") & "'"
	Set rsCheck1 = con1.execute(strCheckSQL)
	if rsCheck1.eof = false then
		%>
			<script Language="JavaScript">
				alert("You are attempting to create a duplicate record, there is already a player in the \ndatabase with the same:\n\n- Passport Number ")
				history.back()
			</script>
		<%
		response.end
	end if
	rsCheck1.close
	Set rsCheck1 = nothing
elseif ucase(request("tablename")) = "SCHOOLMANAGERS" or ucase(request("tablename")) = "MANAGERS" then
	Dim intDupFlag
	intDupFlag = 0

	Set rsTempData = Con1.Execute("Select UID from SchoolManagers where GSecU = '" & request("txtGSecU") & "' and GSecP = '" & request("txtGSecP") & "' and UID <> '" & ID & "' ")
		if rsTempData.eof = false then
			intDupFlag = 1
		end if
	Set rsTempData = nothing
	
	Set rsTempData = Con1.Execute("Select UID from Managers where GSecU = '" & request("txtGSecU") & "' and GSecP = '" & request("txtGSecP") & "' and UID <> '" & ID & "' ")
		if rsTempData.eof = false then
			intDupFlag = 1
		end if
	Set rsTempData = nothing
	
	if intDupFlag = 1 then
		%>
			<script language="javascript">
				alert("You are attempting to create a duplicate record! Please use another username and password for this manager!");
				history.back();
			</script>
		<%
		response.end
	end if
end if

'Due to the fact that the TimePlayed text box on the injury page is disabled, when the form is submitted the value for
'that field is not passed to this page, hence, a hidden text box was created on the injury page, and to make it work
'the name of that text box in the SQL query string has to be changed to the name of the database field
if ucase(request("tablename")) = "INJURY" then
	strSQL = replace(strSQL, "TimeDiff", "TimePlayed")
end if


'Response.Write strSQL
'Response.End
con2.execute(strSQL)


'Log file bkcolor for heading and first part of heading
strColor = "#BDEBC6"
strAction = "Updated"

%><!-- #include file="log.asp" --><%

strModId = ""
if ucase(request("tablename")) = "SCHOOLMANAGERS" or ucase(request("tablename")) = "MANAGERS" then
	strModId = "&hidCurEmpID=" & ID
end if


If Request.QueryString("PageURL") <> "" Then
	If LCASE(Request.QueryString("PageURL")) = "sec.asp" Then
		cleanstring = "../../security/" & Request.QueryString("PageURL")
	Else
		cleanstring = "../../screens/" & Request.QueryString("PageURL")
	End If
Else
	cleanstring = Request.ServerVariables("HTTP_REFERER")
End If

pos = instr(cleanstring,"?")-1
if pos <> -1 then cleanstring = left(request.ServerVariables("http_referer"),pos)
Response.Redirect cleanstring&"?hidSQL="&server.URLEncode(where)&strModId
%>