<%@ Language=VBScript.encode%>
<!--===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<!-- #include file="../config/DB_Connectivity.asp" -->
<!-- #include file="../config/data.asp" -->

<%
Dim original
original = SetLocale("en-gb")
Dim DevDB,RepDB
DevDB			= "[PlayerDev]":RepDB = "[PlayerManagerTemp]"
table			= request("TableName")
ID				= request("ID")																																		' ID of current record
UKEY			= request("Ukey")																																		' Unique column name
where			= request("hidSQL1")																																	' where string

'Check where to scroll to when the screen returns
if table ="EmployeeData" then
	set rsTempData = con1.execute("Select Firstname + ' ' + Surname as 'Player' from EmployeeData where GenID = '" & ID & "'")
		if rsTempData.eof = false then
			strPlayerName = rsTempData(0)
		else
			strPlayerName = "&nbsp;"
		end if
	set rsTempData = nothing
	
	set da = con1.execute("select distinct sO1.name Table1, sO2.colid, sO2.name Col1, sO3.name Table2, sO4.name Col2 from sysforeignkeys sfk join sysobjects sO1 on sfk.fkeyID = sO1.ID join syscolumns sO2 on sfk.fkeyID = so2.id and sfk.fkey = sO2.ColID join sysobjects sO3 on sfk.rkeyID = sO3.ID join syscolumns sO4 on sfk.rkeyID = so4.id and sfk.rkey = sO4.ColID where sO3.name = 'EmployeeData' and sO1.name <> 'EmployeeData'")
	do until da.eof																																							' loop through all tables that have a key constraint to EmployeeData table on GenID.
		strSQL = strSQL & " Delete " &  da("Table1") & " where GenID =" & "'"& ID &"' "																	' concatenate list of tables to delete the players' data
		da.movenext																																						' 
	loop																																										' 
elseif table = "SchoolManagers" or table = "Managers" then
	set rsTempData1 = con1.execute("select Firstname + ' ' + Surname as 'Manager' from " & table & " where " & UKey & " = '" & ID & "'")
		if rsTempData1.eof = false then
			strPlayerName = rsTempData1(0)
		else
			strPlayerName = "&nbsp;"
		end if
	set rsTempData1 = nothing
else
	set rsTempData1 = con1.execute("select GenID from " & table & " where " & UKey & " = '" & ID & "'")
		if rsTempData1.eof = false then
			set rsTempData = con1.execute("Select Firstname + ' ' + Surname as 'Player' from EmployeeData where GenID = '" & rsTempData1(0) & "'")
				if rsTempData.eof = false then
					strPlayerName = rsTempData(0)
				else
					strPlayerName = "&nbsp;"
				end if
			set rsTempData = nothing
		else
			strPlayerName = "&nbsp;"
		end if
	set reTempData1 = nothing
end if																																											'

strSQL = strSQL & " Delete "& table &" where "& UKey &" =" & "'"& ID &"'"																				' standard table delete
logstring = "<tr><td width='33%'>"& UKey &"</td><td width='33%'>"& ID &"</td><td width='33%'>" & strPlayerName & "</td></tr>"
application.Lock																																							' 
	con1.execute(strSQL)																																				' execute strSQL statement
application.UnLock																																						' 

if table ="EmployeeData" then
	set rsScroll = con1.execute("exec scroll 'Employeedata','EmployeeData','','" & session("LE") &"','Backwards','Big','"& where &"','GenID','Surname','GenID','GenID'")
	if rsScroll.eof then set rsScroll = con1.execute("exec scroll 'Employeedata','EmployeeData','','','','','"& where &"','GenID','Surname','GenID','GenID'")
	if not rsScroll.eof then Session("LE") = rsScroll("GenID") else session("LE") = ""
end if

'Log file bkcolor for heading and first part of heading
strColor = "#EE5529"
strAction = "Deleted from"

%><!-- #include file="log.asp" --><%
If Request.QueryString("PageURL") <> "" Then
	If LCASE(Request.QueryString("PageURL")) = "sec.asp" Then
		cleanstring = "../../security/" & Request.QueryString("PageURL")
	Else
		cleanstring = "../../screens/" & Request.QueryString("PageURL")
	End If
Else
	cleanstring = Request.ServerVariables("HTTP_REFERER")
End If

pos = instr(cleanstring,"?")-1
if pos <> -1 then cleanstring = left(cleanstring,pos)
Response.Redirect cleanstring&"?hidSQL="&server.URLEncode(where)
%>