<%@ Language=VBScript %>

<!--'===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<%#@~^awEAAA==@#@&sG^ND1m:7d{dE?1Dnn	/zr@#@&alY4K.+6kXd7'7EcR&J@#@&2swsGH++r[idxdr!x(GJ@#@&Abo}.ND$Xidx7r?EMUls+J@#@&iqGdid7'7EV+UqGE@#@&?hC^Vr.[D$XixiJ!x&fJ@#@&DC4^+7didx7r2:asWH++GCOlr@#@&ZnxO.bmPl(s+i'7EA:wsGH+nfmOmJ@#@&wmo+idi7'iJKVmXn."+wcC/aJ@#@&#CVwkV[/77i'7J(VYXYj5!lNqSU;;l9kD6OwkM/Yglsn~wk./D1ChuYXO?!Dxmhn~UEMxC:nkD6Of}$~GlOnK0Ar.Dtk/shDG7k	m+BnMG\bx^+r@#@&R2kAAA==^#~@%>
<!-- #include file="..\includes\asp\nextprevtop.asp" -->
<%#@~^lAAAAA==@#@&dvqkDtPgl7PXa+SPZPhnmx/,OtPxm-romYbWUPrd,WW0B~F,:nC	/PO4PUl7rTlObW	PkkPKU@#@&dr0,/ndkkW	cJgl\:z2+r#,'~JqE,lUN,.+$EndD`JUUwEUmrb,'~rJ,Ytx@#@&dZywAAA==^#~@%>
		<script Language="JavaScript">
			newWin = window.open('newnav.asp?openhlp=<%=#@~^FAAAAA==~M+5!+kY`rWanx4V2J*P1AYAAA==^#~@%>','newWin','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=750,height=550');
			newWin.focus()
			parent.frames.Search.hideSearchHelp()
			parent.frames.Search.close();
		</script>
	<%#@~^DwAAAA==@#@&dn	N,k0@#@&XQIAAA==^#~@%>

<!--Tabs-->
<td id='tabs' align='center' style='font-size: 11px' onclick="fnTabClick('0');"><a class='clsTabLink' href='javascript:void();'onclick= " this.blur(); return fnTabClick('0');">&nbsp;&nbsp;Contact&nbsp;&nbsp;</a></td>
<td id='tabs' align='center' style='font-size: 11px' onclick="fnTabClick('1');"><a class='clsTabLink' href='javascript:void();'onclick= " this.blur(); return fnTabClick('1');">&nbsp;&nbsp;Squads&nbsp;&nbsp;</a></td>
	<td style="width:100%" style='background-color: #F7F7F7;border-top: 1px solid #C0C0C0'>&nbsp;</td></tr></table>
</tr></table>

<div id="addhelpdiv01" style="display:none;position:absolute;top:80px;left:310px;background-color:#FF3300;border: 1px solid black;color:white;width:70px">
	<img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"> 1. Start here and fill in all the relevant details. You can use the TAB key or your mouse to move to the next field.
</div>

<div id="addhelpdiv02" style="display:none;position:absolute;top:30px;left:330px;background-color:#FF3300;border: 1px solid black;color:white;width:320px">
	<img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"> 2. Before you save, don't forget to click on the squads tab and select the squads the player belongs to.
</div>

<div id="addhelpdiv03" style="display:none;position:absolute;top:374px;left:125px;background-color:#FF3300;border: 1px solid black;color:white;width:150px;">
	<table width="255px">
		<tr>
			<td valign="top" align="center"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px"></td>
		</tr>
		<tr>
			<td valign="top" style="color:white">3. Once you are done, and are ready to save, click the green tick.</td>
		</tr>
	</table>
</div>

<div id="addhelpdiv04" style="display:none;position:absolute;top:325px;left:0px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td align="right" style="color:white">To <strong>CANCEL</strong> saving, simply <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a> to return to the Navigation Wizard.</td>
			<td align="right" width="14px" valign="top"><br><br><img src="../images/Navigation/arrr.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
	</table>
</div>

<div id="search2helpdiv01" style="display:none;position:absolute;top:340px;left:0px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td align="right" style="color:white">2. You can now scroll through your search results, modify, delete or view a summary of all your search results. If you need help with this, <a href="#" onclick="window.open('../includes/asp/help/scrolling.asp?helptype=single','','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=650,height=500')" style="color:white; text-decoration:underline">click here.</a></td>
			<td align="right" width="14px" valign="top"><br><img src="../images/Navigation/arrr.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
	</table>
</div>

<div id="search2helpdiv02" style="display:none;position:absolute;top:394px;left:145px;background-color:#FF3300;border: 1px solid black;color:white;width:150px;">
	3. To return back and be able to view all the players again, you can click on the Player Details icon on the left hand side menu under the Players tab. If you cannot find it, <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a> to return to the Navigation Wizard.
</div>

<div id="search2helpdiv03" style="display:none;position:absolute;top:0px;right:120px;background-color:#FF3300;border: 1px solid black;color:white;width:250px">
	<table>
		<tr>
			<td style="color:white" align="right">This shows the available player records.</td>
			<td align="right" width="14px"><img src="../images/Navigation/arrr.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
		</tr>
	</table>
</div>

<form name=updater id=updater method=post action=''>
<table  width=290 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:17px;top:78px" ID="Table1">
	<input type='hidden' id='hidSQL1' name='hidSQL1' value="<%=#@~^BQAAAA==A4+.GwIAAA==^#~@%>">
	<input type='hidden' id='txtGenID' name='txtGenID' disabled value='<%=#@~^DAAAAA==.kFcrMxqGJ*UgMAAA==^#~@%>'>
	<input type='hidden' id='txtLinkID' name='txtLinkID' disabled value='<%=#@~^DQAAAA==.kFcrSbx3&frbxgMAAA==^#~@%>'>
	<input type='hidden' id='hidLatestInsert' name='hidLatestInsert' value="<%=#@~^EwAAAA==.kFcrSmY+kY&U/DOJ*jQYAAA==^#~@%>">
	<input id='txtemployeeno' name='txtemployeeno' disabled type='hidden' value='<%=#@~^EQAAAA==.kFcr2swVKXn1KJbqAUAAA==^#~@%>'><%#@~^DQQAAA==@#@&d]/aWxk+cADbYnP6kns9/+1cJr~JU;.xm:JSJrUaEO4KaJBFSEr~JESr?;D	Cs+E*@#@&dI/aGxk+ hMkOn,0ksNk+mvEE~rsbDdYHCs+E~rrxaEO8K6JSqBJE~rEBJobDkY[	4k2iglh+r#@#@&iI+k2W	/+cA.kD+,0r+s[k+^`rE~rHr[9V+HCs+dJBEbx2!Y(W6r~8SJr~EJBJ\r9NV'x(/wpHC:/r#@#@&7]/2W	d+ch.rD+PWrV[/^vJEBJnxWSxzdJBJrxaEO8K6JBq~rJ~rESJnxKhU[U8kwIbkE#@#@&7]/wGUk+ hMrD+~6kVNk+1cJr~EMx[nMJ~r[DKwNKAUJBFBJE~EEBJ!+	[+MJb@#@&dIndaWU/ SDrD+,0kV9d+1`EJBJ3O4xk16Dbok	ESJ9DKw[WAUr~q~rE~rJSEAYtUr1[U4k2pr.bobxJ*@#@&7I/2W	/n SDkDnP6k+^[d+1`rJSJ96~JSJ9CYYz2J~qSrJSJ9CD+EBJGlY[	8/ai60Lx8daiAb.Y4J#@#@&7I/aWU/n SDrY~0b+s[k+mcEr~EI!L(Xi	kKxJBJ9.WaNGh	JSqBJJBEJBJI!L8XLx(/2iiUbWUJ*@#@&iIndaWxdnch.kDn,0rV9/+1`rE~rnslHkULd+\sJBJNMG2NKh	JSFSEr~EJBE@!(@*(UkYkO;DkGx=@!J4@*Lx(/wp[	8/ai'x(/2ILx4k2i@!k@*dn-+^@!Jk@*Jb@#@&d]+k2W	/n SDkOn,0r+^[k+^vJr~J&xkOkDEOkKxESrkxa;Y(W6rSq~rJBJE~E@!b@*Hlsn@!Jk@*E*@#@&7]/2W	dRAMkD+P6ksNk+^`rJSEzo+V.W!wJBE[DKw9WAxES8~ENbdl(Vn[r~JESrSn\sJbL[	4/aiV.W!wE#@#@&7]/wKU/RhMrO+,0b+sNdn1`EJBEnK/rObWxESrN.Wa[KhUr~8~Jr~rE~r1GDsls'	4/aInK/kDrGxr#@#@&3zsBAA==^#~@%></table><table id=subDet width=270 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:350px;top:62px;display:block">
	    <tr><td>&nbsp;</td><td>&nbsp;</td><td style="position:relative;top:2;"><div>Area code&nbsp;&nbsp;&nbsp;&nbsp;Number</div></td></tr>
		<tr>
			<td><div align='right' title="Please type in the player's home telephone number">Home Phone</div></td>
	        <td>&nbsp;</td>
	        <td>
			<table border=0  cellspacing=0 cellpadding=0 style="position:relative;left:0;">
				<tr>
				<td align=right><input name="txtHomePhoneArea" onblur="check(this,'number')" class=shorttxtboxes value="<%=#@~^FAAAAA==.kFcrCK:+htKU+zDnlr#pwYAAA==^#~@%>" ID="txtHomePhoneArea"></td>
				<td>&nbsp;</td>
				<td><input name="txtHomePhone" onblur="check(this,'number')" class=mediumtxtboxes value="<%=#@~^EAAAAA==.kFcrCK:+htKU+r#LgUAAA==^#~@%>" ID="txtHomePhone"></td>
				<td>&nbsp;</td>
				</tr>
			</table>
			</td>
	    </tr>
	    <tr>
			<td><div align='right' title="Please type in the player's work telephone number">Work Phone</div></td>
	        <td>&nbsp;</td>
	        <td>
			<table border=0  cellspacing=0 cellpadding=0 style="position:relative;left:0;">
				<tr>
				<td align=right><input name="txtWorkPhoneArea" onblur="check(this,'number')" class=shorttxtboxes value="<%=#@~^FAAAAA==.kFcrKD3htKU+zDnlr#wQYAAA==^#~@%>" ID="txtWorkPhoneArea"></td>
				<td>&nbsp;</td>
				<td><input name="txtWorkPhone" onblur="check(this,'number')" class=mediumtxtboxes value="<%=#@~^EAAAAA==.kFcrKD3htKU+r#SAUAAA==^#~@%>" ID="txtWorkPhone"></td>
				<td>&nbsp;</td>
				</tr>
			</table>
			</td>
	    </tr>
	    <%#@~^6wMAAA==@#@&d]/aWxk+cADbYnP6kns9/+1cJr~J;nsVhtKxnJSEbx2ED8WXJSqBJJSE	Eh4.r~E;+^V[	4k2ihtGxJb@#@&dIdwKx/ ADbYPWkns9/nmvEJBJ)[9D+dd:X2+rSrN.Kw9Wh	JBq~rJSJr~E)9NDd/Lx4k2IKHwJb@#@&7"+dwKU/RA.bY+~Wb+sNkn1`Er~rbN9Dd/8JSJbx2;D4WXE~8~JrSEJBJzN[DndkJb@#@&7I/2G	/+ AMkO+,Wb+s9/m`rJBEb9N.+k/+EBJk	2ED4WXESFBJr~EJSEr#@#@&i]+kwGUk+RA.bYnP6rV[k+1`Jr~r)N9Dn/k&ESrkxa;Y(W6rSq~rJBJE~EE*@#@&d"n/aWUdRh.rD+~0bn^NdmvJJBJz[NM+d/WJSEbxw!O4K6JBqSJr~rJSJEb@#@&7IdwKxdnchDrOPWks9/n1`rJ~rnMG\bx^+r~E[MWw9Gh	J~8SEJBJr~En.G7kUmE#@#@&7]/wGUk+ hMrD+~6kVNk+1cJr~EnK/O/KN+rSJbxw!O8WXJBFSJESrx;:(nDr~EKK/Y'U(/2i;G9+E*@#@&dI/aGxk+ hMkOn,0ksNk+mvEE~r2slrVESrkUw!O4K6ES8~JESr+hlbsr~EA:mkVLx(dwpb[NM+ddr#@#@&7I/wKUd+chMkO+~Wb+sNknmvJESrqfH;s4nDrSrkUaED4WXJBq~rJSJbNESr?bLU4kwi&9'x(/aiHEh8DE#@#@&d"+d2Kx/n SDrY~6kn^Nk+mvJrSJhld/aW.OgWJBEk	wED8G6r~8~EJSEr~E@!(@*nm/d2KDYl@!J4@*[	8kwILx(/wp[	8/ai'x(/2I@!k@*g;:(+D@!&r@*r#@#@&7IndaWU/ hMkOn,0kns9/nmvEr~Ehlk/wKDD(/kEnNr~Er	wED8WXJ~8SEJBJr~E@!r@*;W;xD.X,qdd!+N@!&b@*E#@#@&@#@&YC0BAA==^#~@%></table><table id=subDet width=270 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:356px;top:78px;display:block"><%#@~^ywMAAA==@#@&d]/aWxk+cADbYnP6kns9/+1+`rNDaj5EmNkJSJj5!l[JBE?$EC[8J~Eel.FrSrN.KwGWh	?$CE9/E~8~EEBJJBEH:4.@!4M@*K0lJb@#@&d]+k2W	/n SDkOn,0r+^[k+^y`rNDa?$;l9/E~r?5;mNJBE?$El9+E~r5l. ESrN.Wa9WSxj5mENdEBFSJrSrJSrJ*@#@&iIdwKxd+ch.rD+P6r+^N/^+`rNMwj;;C9/E~rj;!l[EBJ?5;mNfJBEI+CM&r~J9DK2fKhU?$l;[kJ~8SJr~JrSEJ*@#@&d]+d2Kxd+cADbYn~6k+s[k+^ vE9D2U;!lNkJBE?$ECNr~Ej$El9*JBJ5C.cr~rN.W29KhU?$CE9/ES8~JESrJSJrb@#@&7"+kwW	/ hMkO+,0rn^N/^ vJNM2j;!l9/E~Ej$ECNrSJU;;C9*JSEI+CDlEBJ[MWafWSxU5l!NdJBFSEr~JrSJr#@#@&7]+kwKxd+ AMkO+,WkV[dm cE9D2?$;mNdr~r?;!l9E~r?5EmNEBJ5CD+J~r[.WafKhU?5C!NdJBq~rJSEr~JEb@#@&7IdaWUk+chDbY~0b+sNk+^+vJNM2?$El9dE~r?$ECNESr?5Em[Gr~EelD{EBJ[DK2GWA	?$lE9/rSFBJE~rJSEr#@#@&7I/wKUd+chMkO+~Wb+sNknmy`E[Mw?5;mNdJBEU;;mNr~JU;!CNRJSJI+C.RJ~r[DKwfKAU?$l!NdJSqBJE~rE~rJb@#@&dIndaWU/ SDrD+,0kV9d+1 cJ9D2j$El9dJBJ?$;CNr~r?5EC[OJSJInlM,ESrNDG2GWAxU5mE[kJBF~rJBEJBJE#@#@&7]/wKU/RhMrO+,0b+sNdn1 cJ9.wU;;C9/JSEU;;l9EBJj$EmNFZJBE5l.FZJSE9DWa9WSx?$C;NkJBFSJESrJSJrb@#@&6iQBAA==^#~@%></table>
</form>

<iframe name=pin id=pin width='100%' height='100%' border=0 cellspacing=0 cellpadding=0 style="position:absolute;top:0;left:0;display:none;z-index:100" src=""></iframe>
<script>
	spnPageHeader.innerHTML = "Player Details"
	var tabcontrol=true
	var Sumcontrol=false
	if(Sumcontrol==true&&tabcontrol==false){subDet.style.display='Block'}
</script>
	<!-- #include file="..\includes\asp\Xscroll.asp" -->
<script>
	disabler('<%=#@~^EAAAAA==.kFcr4mm3SlM[/r#XQUAAA==^#~@%>,xx,xx,<%=#@~^DwAAAA==.kFcrsKDhmD9dJ*8wQAAA==^#~@%>,1,1,1,1')
	control.style.left='122'
	control.style.display = "block"
	
	function showAddHelpDivs()
	{
		document.all.addhelpdiv01.style.display = "";
		document.all.addhelpdiv02.style.display = "";
		document.all.addhelpdiv03.style.display = "";
		document.all.addhelpdiv04.style.display = "";
	}
	
	function showSearch2Help()
	{
		document.all.search2helpdiv01.style.display = "";
		document.all.search2helpdiv02.style.display = "";
		document.all.search2helpdiv03.style.display = "";
	}

</script>

<%#@~^XQAAAA==@#@&kW,///bW	cJgl-KHwnE*P',EFrPY4nU@#@&dk+s+^O,mC/~D;;nkY`EU	s;x1E*@#@&id1l/PrCN9J@#@&id7LRgAAA==^#~@%>
				<script Language="JavaScript">
					clean()
					updater.txtSurname.focus()
					parent.frames.leftFrame.fnTabClick('0');
					showAddHelpDivs()
				</script>
			<%#@~^GgAAAA==@#@&d71lk+Pr/CD1tE@#@&d770QQAAA==^#~@%>
				<script Language="JavaScript">
					parent.frames.Search.openSearchScreen();
					parent.frames.Search.showSearchHelp()
					parent.frames.leftFrame.fnTabClick('0');
					showSearch2Help()
				</script>
			<%#@~^GwAAAA==@#@&d71lk+Pr/CD1t+J@#@&77iAwUAAA==^#~@%>
				<script Language="JavaScript">
					parent.frames.Search.close();
					parent.frames.leftFrame.fnTabClick('0');
					showSearch2Help()
				</script>
			<%#@~^HQAAAA==@#@&dn	N,/+^+1O@#@&+UN,kW@#@&SwYAAA==^#~@%>

</body>
</html>