<%@ Language=VBScript.encode %>

<!--'===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<%
sTimer = Timer()
FolderName		=	"Screens/"
pathPrefix		=	"../"
Employeeid		=	"GenID"
BigOrderBy		=	"Surname"
UID						=	"GenID"
SmallOrderBy	=	"GenID"
table					=	"EmployeeData"
CentricTable	=	"EmployeeData"
page					=	"PlayerRep.asp"
'if Session("SchoolUser") = "" then
	'ValFields			=	"selSquad1,Squad|txtFirstName,FirstName|txtSurname,Surname|txtDOB,DateofBirth"
'else
	ValFields			=	"txtFirstName,FirstName|txtSurname,Surname|txtDOB,DateofBirth|txtIDNumber,IDNumber"
'end if

%>

<!-- #include file="..\includes\asp\nextprevtop.asp" -->

<%
	'With NavType, 0 means the navigation is off, 1 means the navigation is on
	if session("NavType") = "1" and request("nnFunc") = "" then
	%>
		<script Language="JavaScript">
			newWin = window.open('newnav.asp?openhlp=<%= request("openhlp") %>&displayMenu=<%= request("displayMenu") %>','newWin','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=750,height=550');
			newWin.focus()
			parent.frames.Search.hideSearchHelp()
			parent.frames.Search.close();
		</script>
		
	<%
	end if
	%>
	
			<script language="javascript">
				function setIns()
				{
	<%
	select case rs1("PlayingLevel")
		case "1"
			'It's a school
			%>
					document.all.insSchool.style.display = "";
					document.all.selSchool.value = "<%= rs1("Institution") %>";
			<%
		case "2"
			'It's tertiary
			%>
					document.all.insTertiary.style.display = "";
					document.all.selTertiary.value = "<%= rs1("Institution") %>";
			<%
		case "3"
			'It's a club
			%>
					document.all.insClub.style.display = "";
					document.all.selClub.value = "<%= rs1("Institution") %>";
			<%
	end select
%>
				};
				
				function dispIns()
				{
					switch (document.all.selPlayingLevel.value)
					{
						case "1":
							document.all.insClub.style.display = "none";
							document.all.insTertiary.style.display = "none";
							document.all.insSchool.style.display = "";
							document.all.custddClub.style.display = "none";
							document.all.custddTertiary.style.display = "none";
							document.all.custddSchool.style.display = "";
							document.all.selSchool.value = document.all.txtInstitution.value;
							document.all.selClub.value = document.all.txtInstitution.value;
							document.all.selTertiary.value = document.all.txtInstitution.value;
							break;
						case "2":
							document.all.insClub.style.display = "none";
							document.all.insSchool.style.display = "none";
							document.all.insTertiary.style.display = "";
							document.all.custddClub.style.display = "none";
							document.all.custddSchool.style.display = "none";
							document.all.custddTertiary.style.display = "";
							document.all.selSchool.value = document.all.txtInstitution.value;
							document.all.selClub.value = document.all.txtInstitution.value;
							document.all.selTertiary.value = document.all.txtInstitution.value;
							break;
						case "3":
							document.all.insSchool.style.display = "none";
							document.all.insTertiary.style.display = "none";
							document.all.insClub.style.display = "";
							document.all.custddSchool.style.display = "none";
							document.all.custddTertiary.style.display = "none";
							document.all.custddClub.style.display = "";
							document.all.selSchool.value = document.all.txtInstitution.value;
							document.all.selClub.value = document.all.txtInstitution.value;
							document.all.selTertiary.value = document.all.txtInstitution.value;
							break;
						default:
							document.all.custddSchool.style.display = "none";
							document.all.custddTertiary.style.display = "none";
							document.all.custddClub.style.display = "none";
							document.all.selSchool.value = "";
							document.all.selClub.value = "";
							document.all.selTertiary.value = "";
					};
				};
				
				//Function to take the value returned from the drpsearch page and populate the appropriate
				//text boxes with info
				function drpAssign(strId, strField)
				{
					document.all.txtInstitution.value = strId;
					switch (document.all.selPlayingLevel.value)
					{
						case "1":
							document.all.selSchool.value = strId;
							break;
						case "2":
							document.all.selTertiary.value = strId;
							break;
						case "3":
							document.all.selClub.value = strId;
							break;
					};
				};
				
				//Function to add new values to institution drop downs
				function drpAdd(strValue, strID, strType)
				{
					var oOption = document.createElement("OPTION");
					switch (strType)
					{
						case "1":
							document.all.selSchool.options.add(oOption);
							break;
						case "2":
							document.all.selTertiary.options.add(oOption);
							break;
						case "3":
							document.all.selClub.options.add(oOption);
							break;
						case "4":
						
							<% for i = 1 to 9 %>
							var oOption<%= i %> = document.createElement("OPTION");
							document.all.selSquad<%= i %>.options.add(oOption<%= i %>);
							oOption<%= i %>.innerText = strValue;
							oOption<%= i %>.value = strID;
							<% next %>

							document.all.selSquad10.options.add(oOption);
							break;
					}
					oOption.innerText = strValue;
					oOption.value = strID;
					parent.frames.Search.location.reload();
				};

				//Function to changes values in institution drop downs
				function drpChange(strValue, strID, strType)
				{
					switch (strType)
					{
						case "1":
							for (i = 0; i < document.all.selSchool.options.length; i++)
							{
								if (document.all.selSchool.options[i].value == strID)
								{
									document.all.selSchool.options[i].removeNode(true);
									i = document.all.selSchool.options.length;
								};
							};
							break;
						case "2":
							for (i = 0; i < document.all.selTertiary.options.length; i++)
							{
								if (document.all.selTertiary.options[i].value == strID)
								{
									document.all.selTertiary.options[i].removeNode(true);
									i = document.all.selTertiary.options.length;
								};
							};
							break;
						case "3":
							for (i = 0; i < document.all.selClub.options.length; i++)
							{
								if (document.all.selClub.options[i].value == strID)
								{
									document.all.selClub.options[i].removeNode(true);
									i = document.all.selClub.options.length;
								};
							};
							break;
						case "4":
							var arrSelected = ""
							var intStFlag = 0
							<% for i = 1 to 10 %>
							for (i = 0; i < document.all.selSquad<%= i %>.options.length; i++)
							{
								if (document.all.selSquad<%= i %>.options[i].value == strID)
								{
									if (document.all.selSquad<%= i %>.options[document.all.selSquad<%= i %>.selectedIndex].value == strID)
									{
										if (intStFlag == 0)
										{
											arrSelected = arrSelected + "<%= i %>," + strID
											intStFlag = 1;
										}
										else
										{
											arrSelected = arrSelected + ",<%= i %>," + strID
										};
										//alert(arrSelected);
									};
									document.all.selSquad<%= i %>.options[i].removeNode(true);
									i = document.all.selSquad<%= i %>.options.length;
									//Sub.Dropvalues.
								};
							};
							<% next %>
							break;
					};
					drpAdd(strValue, strID, strType);
					//alert(arrSelected);
					if (arrSelected)
					{
						drpSelect(arrSelected);
					};
				};
				
				//Function to set the squads drop downs back to the squads they were set on after an update
				function drpSelect(arrSelected)
				{
					strSelSquads = arrSelected.split(",");
					for (i=0;i<strSelSquads.length;i++)
					{
						eval("document.all.selSquad"+strSelSquads[i]+".value = " + strSelSquads[i+1] + ";")
						i = i + 1;
					};
				};
				
				function squadDrop(strID)
				{
					<% for i = 1 to 10 %>
					for (i = 0; i < document.all.selSquad<%= i %>.options.length; i++)
					{
						if (document.all.selSquad<%= i %>.options[i].value == strID)
						{
							document.all.selSquad<%= i %>.options[i].removeNode(true);
							i = document.all.selSquad<%= i %>.options.length;
						};
					};
					<% next %>
				}

			</script>

<!--Tabs-->
<td id='tabs' align='center' style='font-size: 11px' onclick="fnTabClick('0');"><a class='clsTabLink' href='javascript:void();'onclick= " this.blur(); return fnTabClick('0');">&nbsp;&nbsp;Contact&nbsp;&nbsp;</a></td>
<td id='tabs' align='center' style='font-size: 11px; <% if Session("SchoolUser") <> "" then %> display:none; <% end if %>' onclick="fnTabClick('1');"><a class='clsTabLink' href='javascript:void();'onclick= " this.blur(); return fnTabClick('1');">&nbsp;&nbsp;Squads&nbsp;&nbsp;</a></td>
	<td style="width:100%" style='background-color: #F7F7F7;border-top: 1px solid #C0C0C0'>&nbsp;</td></tr></table>
</tr></table>

<div id="updatehelpdiv" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:315px;">
	<table>
		<tr>
			<td valign="top" align="right" style="width:193px;"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px"></td>
			<td style="width:122px;"></td>
		</tr>
		<tr>
			<td valign="top" style="color:white" colspan="2">If you make any changes to a record, and you want to save the changes you have made to the system.<br>Click the icon with the black circle.</td>
		</tr>
	</table>
</div>

<div id="summaryhelpdiv" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:288px;">
	<table>
	<tr><td style="color:White;">If you want to see a summary of all the players in the database. <br>Click the icon with the black stripes.</td><td><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px">&nbsp;&nbsp;</td></tr></table>
</div>

<div id="deletehelpdiv" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:315px;">
	<table>
		<tr>
			<td valign="top" align="right" style="width:223px;"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px"></td>
			<td style="width:92px;"></td>
		</tr>
		<tr>
			<td valign="top" style="color:white" colspan="2">To remove a record from the system click the icon with the red X. <br>NB: It will permanently remove this record from the system.</td>
		</tr>
	</table>
</div>

<div id="addhelpdiv01" style="display:none;position:absolute;top:55px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:265px;">
	<table>
	<tr><td style="color:White;">Start here and fill in all the relevant details.</td><td><img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"></td></tr></table>
</div>

<div id="addhelpdiv02" style="display:none;position:absolute;top:32px;left:332px;background-color:#FF3300;border: 1px solid black;color:white;width:200px">
	<img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px">Click the squads tab and select &nbsp;the squads the player belongs to.
</div>

<div id="addhelpdiv03" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:288px;">
	<table>
		<tr>
			<td valign="top" align="right" style="width:267px;"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px"></td>
			<td style="width:33px;"></td>
		</tr>
		<tr>
			<td valign="top" style="color:white" colspan="2">When you are ready to save, click the icon with the green tick.<br><br>NB Please note:<br>You are about to add a new record to the database, are you sure you want to do this?</td>
		</tr>
	</table>
</div>

<div id="addhelpdiv04" style="display:none;position:absolute;top:387px;left:335px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td style="color:white">To <strong>CANCEL</strong> saving, and to return to the Navigation Wizard, <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a></td>
		</tr>
	</table>
</div>

<div id="search2helpdiv01" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:288px;" align="right">
	<table>
		<tr>
			<td style="width:139px;">&nbsp;</td>
			<td align="left" valign="top" style="width:149px"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
		<tr>
			<td style="color:white" colspan="2">You can scroll through the player records. <br>If you need help, <a href="#" onclick="window.open('../includes/asp/help/scrolling.asp?helptype=single','','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=650,height=500')" style="color:white; text-decoration:underline">click here.</a></td>
		</tr>
	</table>
</div>

<div id="search2helpdiv02" style="display:none;position:absolute;top:387px;left:335px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td style="color:white">To return to the Navigation Wizard, <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a></td>
		</tr>
	</table>
</div>

<div id="search2helpdiv04" style="display:none;position:absolute;top:33px;left:333px;background-color:#FF3300;border: 1px solid black;color:white;width:205px">
	<table>
		<tr>
			<td align="right" width="14px"><img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
			<td style="color:white" align="right">Click here for more information.</td>
		</tr>
	</table>
</div>


<form name=updater id=updater method=post action=''>
<table  width=290 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:17px;top:78px">
	<input type='hidden' id='hidSQL1' name='hidSQL1' value="<%=where%>">
	<input type='hidden' id='txtGenID' name='txtGenID' disabled value='<%=rs1("GenID")%>'>
	<input type='hidden' id='txtLinkID' name='txtLinkID' disabled value='<%=rs1("LinkID")%>'>
	<input type='hidden' id='hidLatestInsert' name='hidLatestInsert' value="<%=rs1("LatestInsert")%>">
	<input id='txtemployeeno' name='txtemployeeno' disabled type='hidden' value='<%=rs1("EmployeeNo")%>'><%
	Response.write fieldsec("","Surname","inputbox",1,"","","Surname")
	Response.write fieldsec("","FirstName","inputbox",1,"","","First&nbsp;Name")
	Response.write fieldsec("","MiddleNames","inputbox",1,"","","Middle&nbsp;Names")
	Response.write fieldsec("","KnownAs","inputbox",1,"","","Known&nbsp;As")
	Response.write fieldsec("","Gender","dropdown",1,"","","Gender")
	Response.write fieldsec("","EthnicOrigin","dropdown",1,"","","Ethnic&nbsp;Origin")
	Response.write fieldsec("","DOB","datetype",1,"","date","Date&nbsp;Of&nbsp;Birth")
	Response.write fieldsec("","RugbyUnion","dropdown",1,"","","Rugby&nbsp;Union")
	if Session("SchoolUser") = "" then
		Response.write fieldsec("","PlayingLevel","dropdown",1,"onchange='dispIns()';","","<b>Institution:</b>&nbsp;&nbsp;&nbsp;&nbsp;<i>Level</i>")
	else
		Response.write fieldsec("","PlayingLevel","dropdown",1,"disabled","","<b>Institution:</b>&nbsp;&nbsp;&nbsp;&nbsp;<i>Level</i>")
	end if
	%>
		<input type="hidden" name="txtInstitution" id="txtInstitution" value="<%= rs1("Institution") %>">
	<%
	Response.write fieldsec("","School","dropdownIns",0,"","","<i>Name</i>")
	Response.write fieldsec("","Tertiary","dropdownIns",1,"","","<i>Name</i>")
	Response.write fieldsec("","Club","dropdownIns",1,"","","<i>Name</i>")
	Response.write fieldsec("","AgeGroup","dropdown",1,"disabled","","Level/Age&nbsp;Group")
	Response.write fieldsec("","Position","dropdown",1,"","","Normal&nbsp;Position")
%></table><table id=subDet width=270 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:350px;top:62px;display:block">
	    <tr><td>&nbsp;</td><td>&nbsp;</td><td style="position:relative;top:2;"><div>Area code&nbsp;&nbsp;&nbsp;&nbsp;Number</div></td></tr>
		<tr>
			<td><div align='right' title="Please type in the player's home telephone number">Home Phone</div></td>
	        <td>&nbsp;</td>
	        <td>
			<table border=0  cellspacing=0 cellpadding=0 style="position:relative;left:0;">
				<tr>
				<td align=right><input name="txtHomePhoneArea" onblur="check(this,'number')" class=shorttxtboxes value="<%=rs1("HomePhoneArea")%>" ID="txtHomePhoneArea" maxlength="10"></td>
				<td>&nbsp;</td>
				<td><input name="txtHomePhone" onblur="check(this,'number')" class=mediumtxtboxes value="<%=rs1("HomePhone")%>" ID="txtHomePhone" maxlength="20"></td>
				<td>&nbsp;</td>
				</tr>
			</table>
			</td>
	    </tr>
	    <tr>
			<td><div align='right' title="Please type in the player's work telephone number">Work Phone</div></td>
	        <td>&nbsp;</td>
	        <td>
			<table border=0  cellspacing=0 cellpadding=0 style="position:relative;left:0;">
				<tr>
				<td align=right><input name="txtWorkPhoneArea" onblur="check(this,'number')" class=shorttxtboxes value="<%=rs1("WorkPhoneArea")%>" ID="txtWorkPhoneArea" maxlength="10"></td>
				<td>&nbsp;</td>
				<td><input name="txtWorkPhone" onblur="check(this,'number')" class=mediumtxtboxes value="<%=rs1("WorkPhone")%>" ID="txtWorkPhone" maxlength="20"></td>
				<td>&nbsp;</td>
				</tr>
			</table>
			</td>
	    </tr>
	    <%
	Response.write fieldsec("","CellPhone","inputbox",1," maxlength=""20""","number","Cell&nbsp;Phone")
	Response.write fieldsec("","AddressType","dropdown",1,"","","Address&nbsp;Type")
	Response.write fieldsec("","Address1","inputbox",1,"","","Address")
	Response.write fieldsec("","Address2","inputbox",1,"","","")
	Response.write fieldsec("","Address3","inputbox",1,"","","")
	Response.write fieldsec("","Address4","inputbox",1,"","","")
	Response.write fieldsec("","Province","dropdown",1,"","","Province")
	Response.write fieldsec("","PostCode","inputbox",1," maxlength=""10""","number","Post&nbsp;Code")
	Response.write fieldsec("","Email","inputbox",1,"","email","Email&nbsp;Address")
	Response.write fieldsec("","IDNumber","inputbox",1,"","id","SA&nbsp;ID&nbsp;Number")
	Response.write fieldsec("","PassportNo","inputbox",1,"","","<b>Passport:</b>&nbsp;&nbsp;&nbsp;&nbsp;<i>Number</i>")
	Response.write fieldsec("","PassportIssued","inputbox",1,"","","<i>Country Issued</i>")

%></table>

<%
	if Session("SchoolUser") = "" then
	dim rssquads
	dim strSquadOptions
	if session("strSquadOptions") = "" then
		set rssquads = con1.execute("select [squadid],[squad] from [drpsquads]")
		if not rssquads.eof then
			strSquadOptions = rssquads.getstring(, ,""">","</option><option value=""")	
			strSquadOptions = "<option></option><option value=""" & left(strSquadOptions,len(strSquadOptions)-15)
			rssquads.close
		else
			strSquadOptions = "<option></option>"
		end if
		session("strSquadOptions") = strSquadOptions
	else
		strSquadOptions = session("strSquadOptions")		
	end if
%>
<table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:356px;top:78px;display:block">
	<tr>
		<td>
			<div align='right'>Member<br>of</div>
		</td>
		<td width='5'>&nbsp;</td>
		<td width='210'>
			<select name='selSquad1' class=txtboxes onchange='c(this)' style='width:210px;'>
				<% Response.Write Replace(strSquadOptions, "value=""" & rs1("Squad1") & """","value=""" & rs1("Squad1") & """ selected") %>
			</select>
		</td>
		<td id='custdd' style='display:block'><Input value='::' type=Button class=but onMouseOver='butover(this)' onMouseOut='butout(this)' onclick="ddl('drpSquads')"></td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='210'>
			<select name='selSquad2' class=txtboxes onchange='c(this)' style='width:210px;'>
				<% Response.Write Replace(strSquadOptions, "value=""" & rs1("Squad2") & """","value=""" & rs1("Squad1") & """ selected") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='210'>
			<select name='selSquad3' class=txtboxes onchange='c(this)' style='width:210px;'>
				<% Response.Write Replace(strSquadOptions, "value=""" & rs1("Squad3") & """","value=""" & rs1("Squad1") & """ selected") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='210'>
			<select name='selSquad4' class=txtboxes onchange='c(this)' style='width:210px;'>
				<% Response.Write Replace(strSquadOptions, "value=""" & rs1("Squad4") & """","value=""" & rs1("Squad1") & """ selected") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='210'>
			<select name='selSquad5' class=txtboxes onchange='c(this)' style='width:210px;'>
				<% Response.Write Replace(strSquadOptions, "value=""" & rs1("Squad5") & """","value=""" & rs1("Squad1") & """ selected") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='210'>
			<select name='selSquad6' class=txtboxes onchange='c(this)' style='width:210px;'>
				<% Response.Write Replace(strSquadOptions, "value=""" & rs1("Squad6") & """","value=""" & rs1("Squad1") & """ selected") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='210'>
			<select name='selSquad7' class=txtboxes onchange='c(this)' style='width:210px;'>
				<% Response.Write Replace(strSquadOptions, "value=""" & rs1("Squad7") & """","value=""" & rs1("Squad1") & """ selected") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='210'>
			<select name='selSquad8' class=txtboxes onchange='c(this)' style='width:210px;'>
				<% Response.Write Replace(strSquadOptions, "value=""" & rs1("Squad8") & """","value=""" & rs1("Squad1") & """ selected") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='210'>
			<select name='selSquad9' class=txtboxes onchange='c(this)' style='width:210px;'>
				<% Response.Write Replace(strSquadOptions, "value=""" & rs1("Squad9") & """","value=""" & rs1("Squad1") & """ selected") %>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width='5'>&nbsp;</td>
		<td width='210'>
			<select name='selSquad10' class=txtboxes onchange='c(this)' style='width:210px;'>
				<% Response.Write Replace(strSquadOptions, "value=""" & rs1("Squad10") & """","value=""" & rs1("Squad1") & """ selected") %>
			</select>
		</td>
	</tr>
</table>
<%
	else
%>
<table id=subDet width=280 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:356px;top:78px;display:block"></table>
<%
	end if
%>

</form>

<iframe name=pin id=pin width='100%' height='100%' border=0 cellspacing=0 cellpadding=0 style="position:absolute;top:0;left:0;display:none;z-index:100" src=""></iframe>
<script>
	spnPageHeader.innerHTML = "Player Details"
	var tabcontrol=true
	var Sumcontrol=false
	if(Sumcontrol==true&&tabcontrol==false){subDet.style.display='Block'}
</script>
	<!-- #include file="..\includes\asp\Xscroll.asp" -->
<script>
	disabler('<%=rs1("backwards")%>,xx,xx,<%=rs1("Forwards")%>,1,1,1,1')
	control.style.left='122'
	control.style.display = "block"
	
	function showAddHelpDivs()
	{
		document.all.addhelpdiv01.style.display = "";
		<% if Session("SchoolUser") = "" or isNull(Session("SchoolUser")) then %>
		document.all.addhelpdiv02.style.display = "";
		<% else %>
		document.all.addhelpdiv02.style.display = "none";
		<% end if %>
		document.all.addhelpdiv03.style.display = "";
		document.all.addhelpdiv04.style.display = "";
	}
	
	function showSearch2Help()
	{
		document.all.search2helpdiv01.style.display = "";
		document.all.search2helpdiv02.style.display = "";
		document.all.search2helpdiv04.style.display = "";
		//document.all.search2helpdiv03.style.display = "";
	}

</script>

<%
if session("NavType") = "1" then
select case request("nnFunc")

case "add"
%>
<script Language="JavaScript">
clean()
updater.txtSurname.focus()
parent.frames.leftFrame.fnTabClick('0');
showAddHelpDivs()
</script>
<%

case "view"
%>
<script Language="JavaScript">
parent.frames.Search.openSearchScreen();
parent.frames.Search.showSearchHelp()
parent.frames.Search.setNNFunc('view');
parent.frames.leftFrame.fnTabClick('0');
showSearch2Help()
</script>
<%

case "add"
%>
<script Language="JavaScript">
parent.frames.Search.close();
parent.frames.leftFrame.fnTabClick('0');
showSearch2Help()
</script>
<%

case "update"
%>
<script Language="JavaScript">
//open the search page and display red box - use both lines
parent.frames.Search.openSearchScreen();
parent.frames.Search.showSearchHelp();
parent.frames.Search.setNNFunc('update');
document.all.search2helpdiv04.style.display = "";
document.all.search2helpdiv02.style.display = "";
document.all.updatehelpdiv.style.display = "";
//opens on the players tab
parent.frames.leftFrame.fnTabClick('0');
</script>
<%

case "delete"
%>
<script Language="JavaScript">
//open the search page and display red box
parent.frames.Search.openSearchScreen();
parent.frames.Search.showSearchHelp();
document.all.search2helpdiv04.style.display = "";
document.all.search2helpdiv02.style.display = "";
parent.frames.Search.setNNFunc('delete');
document.all.deletehelpdiv.style.display = "";
//opens on the players tab
parent.frames.leftFrame.fnTabClick('0');
</script>
<%

case "summary"
%>
<script Language="JavaScript">
document.all.search2helpdiv02.style.display = "";
document.all.summaryhelpdiv.style.display = "";
</script>
<%

end select
end if
%>
</body>
</html>

<%
eCounter = Timer()
'Response.Write eCounter - sCounter
%>