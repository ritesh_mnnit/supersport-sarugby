<%@ Language=VBScript.Encode %>
<%#@~^FAAAAA==~alO4nM+0b6,xPrR zrPpwUAAA==^#~@%>

<!-- #include file="..\includes\config\DB_Connectivity.asp" -->
<!-- #include file="..\includes\config\data.asp" -->

<%#@~^AAEAAA==@#@&dr6PU+/kkKU`r?^tKWsik+DrbP{PJr~Otx@#@&7ddOMKzw~',Jd5!lNdE@#@&7+^d@#@&idkYD:XanP{PE/1tGG^/J@#@&dxN,rW@#@&dErY4~gl-KH2+BPT~s+lUd,Y4+,Um\rTlDkW	PbdPK0W~,F~hlxk~Y4+P	C-kTlDkGx~rkPGx@#@&db0~d//rG	`E1m-:X2J*P',J8EPmx[PM+5;/YvEx	sE	^E#,',JEPO4x@#@&i60gAAA==^#~@%>
		<script Language="JavaScript">
			newWin = window.open('newnav.asp?openhlp=<%=#@~^FAAAAA==~M+5!+kY`rWanx4V2J*P1AYAAA==^#~@%>&displayMenu=<%=#@~^GAAAAA==~M+5!+kY`rNbdw^lzHx;E*PaQgAAA==^#~@%>','newWin','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=750,height=550');
			newWin.focus()
			parent.frames.Search.hideSearchHelp()
			parent.frames.Search.close();
		</script>
	<%#@~^DwAAAA==@#@&dn	N,k0@#@&XQIAAA==^#~@%>

<html>
<head>
	<LINK REL="stylesheet" HREF="../includes/css/QBuilder.css" type="text/css">
	<link REL=stylesheet HREF="../includes/css/general.css" type="text/css">
	<script LANGUAGE='javascript' SRC='../includes/js/jsValidator.js'></script>
<style>
.but{
	border:1px solid #222;
	background-Color:#FFF;
	padding:1px;
}
</style></head>
<body background='../images/eebg.gif' topmargin='0' leftmargin='0'>

<div id="reporthelpdiv01" style="display:none;position:absolute;top:0px;left:120px;background-color:#FF3300;border: 1px solid black;color:white;width:390px">
	<table>
		<tr>
			<td style="color:white" colspan="2">Start by selecting the squads you want to select the players from. To select a squad double click on it in the <strong>Not Selected</strong> area.<br>To unselect a squad double click on it in the <strong>Selected</strong> area.</td>
		</tr>
		<tr>
			<td align="left" style="color:white">&nbsp; <img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"> Not Selected</td>
			<td align="right" style="color:white">Selected <img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"> &nbsp; </td>
		</tr>
	</table>
</div>

<div id="reporthelpdiv02" style="display:none;position:absolute;top:380px;left:40px;background-color:#FF3300;border: 1px solid black;color:white;width:350px">
	<table>
		<tr>
			<td width="200px">&nbsp;</td>
			<td align="center" width="150px"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
		<tr>
			<td style="color:white" colspan="2">When you have selected the squads, click the submit button.</td>
		</tr>
	</table>
</div>

<div id="updatehelpdiv05" style="display:none;position:absolute;top:380px;left:472px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td style="color:white">To return to the Navigation Wizard, <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a></td>
		</tr>
	</table>
</div>

<div id="updatehelpdiv06" style="display:none;position:absolute;top:381px;left:263px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td style="color:white">To return to the Navigation Wizard, <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a></td>
		</tr>
	</table>
</div>

<div id="reporthelpdiv03" style="display:none;position:absolute;top:0px;left:46px;background-color:#FF3300;border: 1px solid black;color:white;width:540px">
	<table>
		<tr>
			<td style="color:white" colspan="2">Select the players you want to capture data for (you are restricted to a maximum of twelve). To select a player double click on his / her name in the <strong>Not Selected</strong> area.<br>To unselect a player double click on his / her name in the <strong>Selected</strong> area.</td>
		</tr>
		<tr>
			<td align="left" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"> Not Selected</td>
			<td align="right" style="color:white">Selected <img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
		</tr>
	</table>
</div>

<div id="reporthelpdiv04" style="display:none;position:absolute;top:381px;left:406px;background-color:#FF3300;border: 1px solid black;color:white;width:185px">
	<img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px" align="left">When you have selected the players, click the Capture &nbsp;&nbsp;&nbsp;&nbsp;Results button to continue.
</div>

<div id="reporthelpdiv05" style="display:none;position:absolute;top:381px;left:40px;background-color:#FF3300;border: 1px solid black;color:white;width:200px">
	<img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px" align="right">You can click the <strong>Back</strong> button to go back and make changes to your squad selections.
</div>

<!--ASTHETICS-->
<table width="100%" bgcolor="#CcCcCc" border="0" cellpadding="0" cellspacing="0">
	<tr height="9">
		<td></td>
	</tr>
	<tr height="1" bgcolor="#eeeeee">
		<td></td>
	</tr>
</table>
<font face="Verdana, Arial, Helvetica, sans-serif" color="#333333" style="FONT-SIZE: 12px">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" ID="Table2">
		<tr bgcolor="#dcdcdc">
			<td rowspan="2" width="181" height="20" valign="top" style="BORDER-BOTTOM: #c0c0c0 1px solid"><font face="Verdana, Arial, Helvetica, sans-serif" size="4" COLOR="#999999">
					&nbsp;Mass Capture Tool</font></td>
			<td width="19">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td height="20"><img src="../images/corner2.gif" WIDTH="19" HEIGHT="20"></td>
			<td style="BORDER-TOP: #c0c0c0 1px solid; BACKGROUND-COLOR: #f7f7f7">&nbsp;</td>
		</tr>
	</table>
<!--===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<%#@~^gAMAAA==@#@&qW,I;E/D sKDh`rYaOU;Em[/r#P{~EJ,K4+U@#@&7b0~?$;l9h4nM+P@!@*,JEPD4x~U;!lNSt.+,'~JStn.P?$;l9qf,rUPvJ,[~/O.U;;l9dPLPEbr@#@&7r6Pj+kdbWUvJUmtKW^i/DE#,'~ErPY4nx@#@&didnY,DkSr/Oq,'~mKUFc+an1EYncr?nV^DPM,0MW:,NM2?$ECNkPE~LP?$;l9ht.nPLPrPGD[nMP8X,j;!l[E*dd7v,MnY,sb/O,W6P?$Em[/@#@&7+^/n@#@&ddE.+kwW	dnRSDbYnPEdVnmD~q	/OrDEYrG	q9~,(	/ObY!YkKx,WDK:~NMw(UkYkD;YbWx,A4+M+,qU/OrDEOkKUqGPrU,`/nsmOP9rkYr	mDPq	/DrY!YrW	PW.K:PAhw^WXn9lDl,h4+.n,J~[,j+k/rG	`J9CDlE#v1*P',J,lx9P&U/DkOEDkGU,k/,UWDPx!ss#r@#@&d7B.nkwGxknRx[@#@&dddnDP./drkYq,',mW	Fcn6m;Y`EdV+1OP&x/DrOEDkKx(fS~&xdYbOEDkGU,0DGh,N.w&UkYrDEDkW	PS4+M+~q	/OrDEYbGx&fPbU~`k+^+^Y~[b/Ok	^Y,qUdDkY;ObWUP6.K:~A:aVWH+9lDl~h4+.n,JPL~?//bGU`rfmYCJbcO#~[,EPmx[~&x/OrDEOkKU,kd,xKYP	E^s#,W.ND~8HPq	dYbYEDrGxr#id7B~!Y~VbdY,WW~U;EC[k@#@&dU9Pr6@#@&d@#@&dEd+DP./dkdO8P',^W	FRanm!Y`E?nsmOPC~0MWh~9Dwj5!l[/,E,[~U;!lNSt.+,[~J,W.[DP(zPU;Em[E#@#@&i3h0BAA==^#~@%>
	<br>
	<table style="position:absolute;left:40px;top:65px;" cellpadding="3" cellspacing="0" class="tblhere" bordercolor=#999999>
		<tr><td colspan="3" class="headinghere">Squads</td></tr>
		<tr>
			<td class="headinghere">Not Selected</td>
			<td class="cellhere">&nbsp;</td>
			<td class="headinghere">Selected</td>
		</tr>
		<tr>
			<td class="cellhere">
				<select class="pulldown" multiple style="height:240px;width:250px" name="sel1" id="sel1" size="8" onDblClick="if(sel2.children.length<10){addCol(this,sel2)}else{alert('You may only select 10 <%=#@~^CQAAAA==~kY.:Xa+POwMAAA==^#~@%> at a time!')};" onkeydown="if(event.keyCode==13){if(sel2.children.length<10){addCol(this,sel2)}else{alert('You may only select 10 <%=#@~^CQAAAA==~kY.:Xa+POwMAAA==^#~@%> at a time!')};};">
		<%#@~^IgEAAA==~@#@&7i/DDZKV}2YbWU/,'~ErP@#@&7db0P	GOPM/dkdYq WWPD4+	@#@&7idfG~!xOk^~M/Jb/DFRW6@#@&id7dkY./KVraOkKx/{dOD;W^r2YrG	/~[,E@!}wOrKxP#C^En'EE,[~M/dk/DFcWkV[/v!b~LPJE@*J,P[,.dSb/DF 0rn^Nd`8bPLPE@!JrwOrKx@*J@#@&id7iDkSkkY8 :K\nx6O@#@&ddisWKw@#@&77+	N,kW@#@&7iIn/aGxk+ 	MkYn~kY.ZKs}wObW	/@#@&diKU8AAA==^#~@%>
				</select>
			</td>
			<td class=cellhere style="border-top:0px;width:30px;text-align:center;">
			<table ID="Table4">
			<tr><td><input class="but" type=button value=">" title="Move selected items to the right" style="width:22px;height:22px;text-align:center;" onclick="onClick(sel1,sel2)" ID="Button1" NAME="Button1"></td></tr>
			<tr><td><input class="but" type=button value=">>" title="Move all items to the right" style="width:22px;height:22px;text-align:center;" onclick="allClick(sel1,sel2)" ID="Button2" NAME="Button2"></td></tr>
			<tr><td><input class="but" type=button value="<" title="Move selected items to the left" style="width:22px;height:22px;text-align:center;" onclick="onClick(sel2,sel1)" ID="Button3" NAME="Button3"></td></tr>
			<tr><td><input class="but" type=button value="<<" title="Move all items to the left" style="width:22px;height:22px;text-align:center;" onclick="allClick(sel2,sel1)" ID="Button4" NAME="Button4"></td></tr>
			</table>
			</td>
			<td class="cellhere">
				<select class="pulldown" multiple style="height:240px;width:250px" name="sel2" id="sel2" size="8" ondblclick="removeCol(this,sel1);" onkeydown="if(event.keyCode==13){removeCol(this,sel1);};">
				</select>
			</td>
		</tr>
		<tr><td class="headinghere" align="center" colspan="3"><input type='button' value='Submit' class="txtboxes" style='width:170px;height:18px;cursor:hand;background-color:#F3F7F8;' onmouseover="this.style.backgroundColor='#FFFFFF'" onmouseout="this.style.backgroundColor='#F3F7F8'" onclick='submiter()' ID="Button5" NAME="Button5"></td></tr>
	</table>
	<form name="frmTracking" id="frmTracking" method=post action=''>
		<input type=hidden id=txtSquads name=txtSquads >
		<input type=hidden id=nnFunc name=nnFunc value="<%=#@~^EwAAAA==~M+5!+kY`rx	oE	mE#,RgYAAA==^#~@%>2">
	</form>
	<script language="javascript">
	function submiter(){
		<%#@~^DQAAAA==@#@&d7i@#@&ddWwAAAA==^#~@%>
		var colsComma1 = ""
		for (i = 0; i < sel2.children.length; i++){
	 		colsComma1 = colsComma1 + ",'" +sel2.children[i].value + "'" ;
		}
		strCols = colsComma1.substr(1);
		if(strCols==""){alert("Please select at least one Squad")}else{
			frmTracking.txtSquads.value = strCols;	
			frmTracking.action = 'Massloader.asp'
			frmTracking.submit()
		}
	}
	</script>
<%#@~^xgIAAA==@#@&+sk+@#@&dU;!CNStnDPx~rJ@#@&7?$El9).DmX,'~/2sbYcD2VmmncM+;;nkYcJDaD?5!l9/J*~rvJBJE#BJSE*@#@&iWWMP+m^4PV:nxO~bx~?$;l9b..mX@#@&7ikWPUnk/rKxvJ?1tKGV`/nDr#~x,JJ,Otx@#@&77dU;!l[h4nM+~',j;!l[A4+Dn~LPEWM~:2^WH++GlDCRU;;l9/~sb3+,v]-JPL~nV:xOP'~ruYB,E@#@&d7n^/+@#@&id7?$;mNA4+M+P{PU5EmNAtDn~LPJK.P:w^Gz+fmYCR(UkYrY!OkKx~sb3+~vuJ~[,n^+hxDP[,JuvPr@#@&di+U[,k0@#@&d	+6D@#@&@#@&dU;;l[A4+.+,xPrh4nM+PcE,[~:b[v?5!l9htDSc*P'Pr#E@#@&d@#@&7k6P/^AtDP@!@*~ErPOtUP@#@&77k+mA4DnP{~M+2^l1+`k+1AtDn~,Jvvr~PrvJ*@#@&i7j;!l9h4+.n,'~?$;l9h4nM+P'~rPCx9~rP',/mh4+Mn@#@&dnx9PrW@#@&d@#@&dk+Y,.dSb/D ~'~^KxqRa+1EOnvJ?nsmOPVn	q9BPwkDkYgC:PQPEPv~3P?!.xm:+,W.WsPA:2VGz+9lDCPrP'~U;EC[StnD~LPE,WMN+MP(zPwk./DxChJ#@#@&dOscAAA==^#~@%>
	<html><body>
	<br>
	<table style="position:absolute;left:40px;top:65px;" cellpadding="3" cellspacing="0" class="tblhere" bordercolor=#999999 ID="Table1">
		<tr><td colspan="3" class="headinghere">Players</td></tr>
		<tr>
			<td class="headinghere">Not Selected</td>
			<td class="cellhere">&nbsp;</td>
			<td class="headinghere">Selected</td>
		</tr>
		<tr style="display:block;" id="row1" name="row1">
			<td class="cellhere">
				<select class="pulldown" multiple style="height:240px;width:250px;" name="sel3" id="sel3" size="8" onDblClick="if(sel4.children.length<12){addCol(this,sel4)}" onkeydown="if(event.keyCode==13){if(sel4.children.length<12){addCol(this,sel4)};};">
		<%#@~^IQEAAA==@#@&d7kYMZW^raOkKxdP{PEE,@#@&i7k6PxKO~DkSb/O  nK0~Y4nx@#@&77ifW~;	YrV,.kSrkYyR+K0@#@&did7/DD/G^rwDrW	/'kO.ZKV}wOkGUkP'Pr@!raYrG	P.Cs!+xBr~LP.kSb/YyR6r+^Nd`Z#~'rB@*r~PLPDkJr/D c0r+s[k`q#,'Pr@!&6aYkGU@*J@#@&i7id.kSb/YyRsG\xn6D@#@&7idVKGw@#@&dinUN,k6@#@&d7]/2W	d+c.rD+PdOMZGV}2DkG	/,@#@&idDk8AAA==^#~@%>
				</select>
			</td>
			<td class=cellhere  style="width:30px;text-align:center;border-top:0px;">
				<table>
					<tr><td><input class="but" type=button value=">" title="Move selected items to the right" style="width:22px;height:22px;text-align:center;" onclick="onClick(sel3,sel4)"></td></tr>
					<tr><td><input class="but" type=button value=">>" title="Move all items to the right" style="width:22px;height:22px;text-align:center;" onclick="allClick(sel3,sel4)"></td></tr>
					<tr><td><input class="but" type=button value="<" title="Move selected items to the left" style="width:22px;height:22px;text-align:center;" onclick="onClick(sel4,sel3)"></td></tr>
					<tr><td><input class="but" type=button value="<<" title="Move all items to the left" style="width:22px;height:22px;text-align:center;" onclick="allClick(sel4,sel3)"></td></tr>
				</table>
			</td>
			<td class="cellhere">
				<select class="pulldown" multiple style="height:240px;width:250px;" name="sel4" id="sel4" size="8" ondblclick="removeCol(this,sel3);" onkeydown="if(event.keyCode==13){removeCol(this,sel3);};">
				</select>
			</td>
		</tr>
		<tr>
	<td class="headinghere" colspan="3"><table cellpadding=2 cellspacing=0 border=0><tr>
			<td><input type='button' value='<--Back' class='txtboxes' style='width:170px;height:18px;cursor:hand;background-color:#F3F7F8;' onmouseover="this.style.backgroundColor='#FFFFFF'" onmouseout="this.style.backgroundColor='#F3F7F8'" onclick='javascript:history.go(-1)'></td>
			<td><input type='button' value='Capture Results' class="txtboxes" style='width:170px;height:18px;cursor:hand;background-color:#F3F7F8;' onmouseover="this.style.backgroundColor='#FFFFFF'" onmouseout="this.style.backgroundColor='#F3F7F8'" onclick='submiter()'></td>
	</tr></table></td>
		</tr>
	</table>
	</td></tr></table>
	<form name="frmTracking" id="frmTracking" method=post action=''>
		<input type=hidden id=txtEmployees name=txtEmployees>
		<input type=hidden id=nnFunc name=nnFunc value="<%=#@~^EwAAAA==~M+5!+kY`rx	oE	mE#,RgYAAA==^#~@%>">
	</form>
	<script language="javascript">
	function submiter(){
		var strTempValid = ""
		var colsComma2 = "";for (i = 0; i < sel4.children.length; i++){colsComma2 = colsComma2 + "," +sel4.children[i].value;}
		strCols1 = colsComma2.substr(1);	if (strCols1==""){strTempValid=strTempValid+" - Players\n"};frmTracking.txtEmployees.value = strCols1;
		frmTracking.action = 'MassloaderGrid.asp'
		if (strTempValid==""){
			frmTracking.submit()
		}else{
			alert ("Please select values from the following lists:\n"+strTempValid)
		}
	}
	</script>
	<iframe width=168 height=202 name='gToday:normal:agenda.js' id='gToday:normal:agenda.js' src='../includes/js/ipopeng.htm' scrolling='no' frameborder='0' style='visibility:hidden; z-index:65535; position:absolute; top:0px; border:2px ridge;'></iframe>
<%#@~^BgAAAA==n	N~b0JgIAAA==^#~@%>
<script language="javascript">
//Used to move objects from the one drop down to the other. Selection only.
function onClick(object,destination) {
	if(destination.name=="sel4"){
		if(sel4.children.length==12){alert("Only 12 Players may be selected at a time.")}
		for (var Current=0;Current < object.options.length;Current++) {
			if(sel4.children.length<12){
				if (object.options[Current].selected) {
					var oCloneNode = object.options[Current].cloneNode(true);
					destination.appendChild(oCloneNode);
					object.options[Current].removeNode(true);
					Current = -1
				}
			}
		}
	}else
	{
		if(destination.name=="sel2")
		{
			for (var Current=0;Current < object.options.length;Current++) {
				if(sel2.children.length<10)
				{
					if (object.options[Current].selected) {
						var oCloneNode = object.options[Current].cloneNode(true);
						destination.appendChild(oCloneNode);
						object.options[Current].removeNode(true);
						Current = -1
					}
				}
			}
		}
		else
		{
			for (var Current=0;Current < object.options.length;Current++) {
				if (object.options[Current].selected) {
					var oCloneNode = object.options[Current].cloneNode(true);
					destination.appendChild(oCloneNode);
					object.options[Current].removeNode(true);
					Current = -1
				}
			}
		}
	}
}
//Used to move objects from the one drop down to the other. All at once
function allClick(object,destination) {
	if(destination.name=="sel4"){
		if(sel4.children.length==12){alert("Only 12 Players may be selected at a time.")}
		for (var Current=0;Current < object.options.length;Current++) {
			if(sel4.children.length<12){
				var oCloneNode = object.options[Current].cloneNode(true);
				destination.appendChild(oCloneNode);
				object.options[Current].removeNode(true);
				Current = -1
			}
		}
	}
	else
	{
		if (destination.name=="sel2")
		{
			for (var Current=0;Current < object.options.length;Current++) {
				if(sel2.children.length<10)
				{
					var oCloneNode = object.options[Current].cloneNode(true);
					destination.appendChild(oCloneNode);
					object.options[Current].removeNode(true);
					Current = -1
				}
			}
		}
		else
		{
			for (var Current=0;Current < object.options.length;Current++) {
				var oCloneNode = object.options[Current].cloneNode(true);
				destination.appendChild(oCloneNode);
				object.options[Current].removeNode(true);
				Current = -1
			}
		}
	}
}
//Used to move objects from the one drop down to the other. By Double clicking
function addCol(obj,des){
	if (obj.selectedIndex >= 0){
		var oCloneNode = obj.children[obj.selectedIndex].cloneNode(true);
		des.appendChild(oCloneNode);
		obj.children(obj.selectedIndex).removeNode(true);
	}
}
//Used to move objects from the one drop down to the other. By Double clicking
function removeCol(obj,des,rep){
	if (obj.selectedIndex >= 0){
		var oCloneNode = obj.children(obj.selectedIndex).cloneNode(true);
			if(rep!=false){
				des.insertBefore(oCloneNode);
			}
		obj.children(obj.selectedIndex).removeNode(true);
	}
}
</script>
<%#@~^XQAAAA==@#@&kW,///bW	cJgl-KHwnE*P',EFrPY4nU@#@&dk+s+^O,mC/~D;;nkY`EU	s;x1E*@#@&id1l/PrCN9J@#@&id7LRgAAA==^#~@%>
				<script Language="JavaScript">
					document.all.reporthelpdiv01.style.display = ""
					document.all.reporthelpdiv02.style.display = ""
					document.all.updatehelpdiv05.style.display = ""
					parent.frames.leftFrame.fnTabClick('1');
				</script>
			<%#@~^GAAAAA==@#@&d71lk+Prl9[ r@#@&didtgMAAA==^#~@%>
				<script Language="JavaScript">
					document.all.reporthelpdiv03.style.display = ""
					document.all.reporthelpdiv04.style.display = ""
					document.all.reporthelpdiv05.style.display = ""
					document.all.updatehelpdiv06.style.display = ""
					parent.frames.leftFrame.fnTabClick('1');
				</script>
			<%#@~^HQAAAA==@#@&dn	N,/+^+1O@#@&+UN,kW@#@&SwYAAA==^#~@%>

</body>
</html>