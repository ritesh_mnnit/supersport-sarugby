<%@ Language=VBScript.Encode %>
<!--===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<%
Dim original
original			=	SetLocale("en-gb")
FolderName		=	"Screens/"
pathPrefix		=	"../"
Employeeid		=	"GenID"
BigOrderBy		=	"Surname"
UID				=	"UID"
SmallOrderBy	=	"TestDate"
CentricTable	=	"EmployeeData"
page				=	"TestsIfr.asp"

Frame			=	"Frame"

if request.QueryString("tempTable") <> "" then
	table = request.QueryString("tempTable")
	session("tempTable") = table
else 
	table = session("tempTable")
end if

if request.QueryString("strRowset") <> "" then
	strRowset = request.QueryString("strRowset")
	session("strRowset") = strRowset
else 
	strRowset = session("strRowset")
end if

%>
<!-- #include file="..\includes\asp\nextprevtop.asp" -->
<html>
<head>
	<script language="javascript">
		parent.testFunc('<%= sn %>', '<%= sfe %>', '<%= rn %>', '<%= rfe %>');
		function checkAge()
		{
			<% if rs1("AgeGroup") <> "" then %>
			<% else %>
				updater.txtAgeGroup.value = "<%= rsPlayerData("AgeGroup") %>";
			<% end if %>
			<% if rs1("SASquadComparative") <> "" then %>
			<% else %>
				updater.txtSASquadComparative.value = "<%= rsPlayerData("AgeGroup") %>";
			<% end if %>
		};
	</script>
</head>
<body topmargin='0' leftmargin='0' style="display:none" name="MainBody" id="MainBody" onload="checkAge()">

<%if ((Table="PSquat1") or (Table="PSquat3") or (Table="LegPress1") or (Table="LegPress3") or (Table="FSquat3") or (Table="FSquat1") or (Table="ForeArmGrip") or (Table="BenchThrow") or (Table="JumpSquat") or (Table="VertJump")  or (Table="BenchPress")) then%>
<table style="width:289px" border='0' cellspacing='0' cellpadding='0' bgcolor='#F7F7F7' ID="Table2">
<!--top row of header-->
	<tr><td><img src='../images/corner2.gif' WIDTH='19' HEIGHT='20'></td>
<!--Tabs-->
<td id='tabs' align='center' style='font-size: 11px' onclick="fnTabClick('0');"><a class='clsTabLink' href='javascript:void();'onclick= " this.blur(); return fnTabClick('0');">&nbsp;&nbsp;Results&nbsp;&nbsp;</a></td>
<td id='tabs' align='center' style='font-size: 11px' onclick="fnTabClick('1');"><a class='clsTabLink' href='javascript:void();'onclick= " this.blur(); return fnTabClick('1');">&nbsp;&nbsp;Norms&nbsp;&&nbsp;Percentiles&nbsp;&nbsp;</a></td>
<td class=clsTab style="width:90%">&nbsp;</td>
</tr></table>
<%end if%>
<form name=updater id=updater method=post action=''>
<table id=subDet width=270 border=0 cellspacing=0 cellpadding=0 style="display:block;position:absolute;left:1px;top:30px">
<%	Response.write "<input id='txtGenID' name='txtGenID' type='hidden' value='"&rs1("GenID")&"'>"%>
		<tr><td><div align='right'>Player</div></td><td width='5'>&nbsp;</td><td colspan="5"><input name="txtemployeeno" class=txtboxes disabled ID="txtemployeeno" value="<%response.Write rsPlayerData("FirstName") & " " & rsPlayerData("Surname")%>" maxlength="8"></td></tr>
		<tr><td><div align='right'>Date</div></td><td width='5'>&nbsp;</td><td colspan="5"><input name="txtTestDate" class=txtboxes onBlur="check(this,'date')" title='double click to display calender' ondblclick='gfPop.fPopCalendar(this)' value="<% if rs1("TestDate")<>"" and not isnull( rs1("TestDate")) then response.write(formatdatetime(rs1("TestDate"),1))%>"></td></tr>
		<tr><td><div align='right'>Age Group</div></td><td width='5'>&nbsp;</td><td colspan="5"><select id='txtAgeGroup' name='txtAgeGroup' class='txtboxes'><%=getOptions("AgeGroup",1)%></select></td></tr>
		<tr><td><div align='right'>Squad&nbsp;Comparative</div></td><td width='5'>&nbsp;</td><td colspan="5"><select id="txtSASquadComparative" name='txtSASquadComparative' class='txtboxes'><%=getOptions("SASquadComparative",1)%></select></td></tr>
<%	if ((Table="JumpSquat20") or (Table="RepSprint") or (Table="MESitupsU12") or (Table="MESitups") or (Table="MEFlexedArm") or (Table="MEPullups") or (Table="MEPushups") or (Table="CFShuttleRun") or (Table="CFCoopers") or (Table="CF2400Trial") or (Table="CF3000Trial") or (Table="SitReach") or (Table="AgilityIllinois") or (Table="AgilityTTest") or (Table="BenchThrow20")) then%>
		<tr><td><div align='right'>Result</div></td><td width='5'>&nbsp;</td><td colspan="5"><input maxlength="8" name="txtResult" class=txtboxes onBlur="check(this,'number')" value="<%if not isnull(rs1("Result")) then response.Write rs1("result") else response.Write ""%>"></td></tr>
<%	elseif ((Table="PowerClean") or (Table="BenchPress") or (Table="LegPress3") or (Table="LegPress1") or (Table="FSquat3") or (Table="FSquat1") or (Table="PSquat3") or (Table="PSquat1")) then%>
		<tr><td><div align='right'>Body Weight</div></td><td width='5'>&nbsp;</td><td colspan="5"><input maxlength="8" name="txtBodyWeight" class=txtboxes onBlur="check(this,'number');RelativeResult()" value="<%if not isnull(rs1("BodyWeight")) then response.Write rs1("BodyWeight") else response.Write ""%>"></td></tr>
		<tr><td><div align='right'>Result</div></td><td width='5'>&nbsp;</td><td colspan="5"><input maxlength="8" name="txtResult" onblur="check(this,'number');RelativeResult()" class=txtboxes value="<%if not isnull(rs1("Result")) then response.Write rs1("result") else response.Write ""%>"></td></tr>
		<tr><td><div align='right'>Relative Result</div></td><td width='5'>&nbsp;</td><td colspan="5"><input readonly name="txtRelativeResult" class=txtboxes value="<%if not isnull(rs1("RelativeResult")) then response.Write rs1("RelativeResult") else response.Write ""%>"></td></tr>
<%	elseif ((Table="Speed10m") or (Table="Speed40m")) then%>
		<tr><td><div align='right'>Test Type</div></td><td width='5'>&nbsp;</td><td colspan="5"><Select name="txtTestType" class=txtboxes><%=getOptions("TestType",1)%></select></td></tr>
		<tr><td><div align='right'>Result</div></td><td width='5'>&nbsp;</td><td colspan="5"><input maxlength="8" name="txtResult" onblur="check(this,'number')" class=txtboxes value="<%if not isnull(rs1("Result")) then response.Write rs1("result") else response.Write ""%>"></td></tr>
<%	elseif ((Table="StraightLegRaise") or (Table="ModThomQuad") or (Table="ModThomIlliop")) then%>
		<tr><td></td><td width='5'>&nbsp;</td><td style="width=10"><div align='center'><i>Left</i></div></td><td width='5'>&nbsp;</td><td style="width=10"><div align='center'><i>Right</i></div></td></tr> 
		<tr><td><div align='right'>Result<sup>o</sup></div></td><td width='5'>&nbsp;</td><td width='80px'><input maxlength="8" name="txtResultL" style="width:80px" onBlur="check(this,'number')" class=shorttxtboxes value="<%=rs1("ResultL")%>"></td><td width='5'>&nbsp;</td><td width='80px'><input name="txtResultR" style="width:80px" onBlur="check(this,'number')" class=shorttxtboxes value="<%=rs1("ResultR")%>"></td></tr>
<%	elseif (Table="ForeArmGrip") then%>
		<tr><td><div align='right'>Body Weight</div></td><td width='5'>&nbsp;</td><td colspan="5" width='150'><input maxlength="8" name="txtBodyWeight" onBlur="check(this,'number');RelativeResult()" class=txtboxes value="<%=rs1("BodyWeight")%>" ID="txtBodyWeight"></td></tr>
		<tr><td></td><td width='5'>&nbsp;</td><td style="width=10"><div align='center'><i>Left</i></div></td><td width='5'>&nbsp;</td><td style="width=10"><div align='center'><i>Right</i></div></td></tr> 
		<tr><td><div align='right'>Result kg</div></td><td width='5'>&nbsp;</td><td style="width:80px"><input maxlength="8" name="txtResultL" onblur="check(this,'number');RelativeResult()" style="width:80px" class=txtboxes value="<%=rs1("ResultL")%>" ID="txtResultL"></td><td width='5'>&nbsp;</td><td width='150'><input name="txtResultR" onblur="check(this,'number');RelativeResult()" style="width:80px" class=txtboxes value="<%=rs1("ResultR")%>" ID="txtResultR"></td></tr>
		<tr><td><div align='right'>Relative Result</div></td><td width='5'>&nbsp;</td><td style="width:80px"><input maxlength="8" name="txtRelativeResultL" readonly style="width:80px" class=txtboxes value="<%=rs1("RelativeResultL")%>" ID="txtRelativeResultL"></td><td width='5'>&nbsp;</td><td width='150'><input name="txtRelativeResultR" style="width:80px" readonly class=txtboxes value="<%=rs1("RelativeResultR")%>" ID="txtRelativeResultR"></td></tr>
<%	elseif Table="HIMS" then%>
		<tr><td><div align='right'>Rate&nbsp;Recovery&nbsp;(%)</div></td><td width='5'>&nbsp;</td><td colspan="5" width='150'><input name="txtRateRecover" onBlur="check(this,'number')" class=txtboxes value="<%=rs1("RateRecover")%>"></td></tr>
<%	elseif Table="BenchThrow" then%>
		<tr><td><div align='right'>Result 40</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult40" onblur="check(this,'number');benchthrowmax()" class=txtboxes value="<%=rs1("Result40")%>"></td></tr>
		<tr><td><div align='right'>Result 50</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult50" onblur="check(this,'number');benchthrowmax()" class=txtboxes value="<%=rs1("Result50")%>"></td></tr>
		<tr><td><div align='right'>Result 60</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult60" onblur="check(this,'number');benchthrowmax()" class=txtboxes value="<%=rs1("Result60")%>"></td></tr>
		<tr><td><div align='right'>Result 70</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult70" onblur="check(this,'number');benchthrowmax()" class=txtboxes value="<%=rs1("Result70")%>"></td></tr>
		<tr><td><div align='right'>Result 80</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult80" onblur="check(this,'number');benchthrowmax()" class=txtboxes value="<%=rs1("Result80")%>"></td></tr>
	</table><table id=subDet width=270 border=0 cellspacing=0 cellpadding=0 style="display:none;position:absolute;left:10px;top:30px">
		<tr><td><div align='right'>BT PMax (calculated)</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input name="txtBTPMax" readonly class=txtboxes value="<%=rs1("BTPMax")%>"></td></tr>
<%	elseif Table="JumpSquat" then%>
		<tr><td><div align='right'>Result 20</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult20" onblur="check(this,'number');jumpsquatmax()" class=txtboxes value="<%=rs1("Result20")%>"></td></tr>
		<tr><td><div align='right'>Result 40</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult40" onblur="check(this,'number');jumpsquatmax()" class=txtboxes value="<%=rs1("Result40")%>"></td></tr>
		<tr><td><div align='right'>Result 60</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult60" onblur="check(this,'number');jumpsquatmax()" class=txtboxes value="<%=rs1("Result60")%>"></td></tr>
		<tr><td><div align='right'>Result 80</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult80" onblur="check(this,'number');jumpsquatmax()" class=txtboxes value="<%=rs1("Result80")%>"></td></tr>
		<tr><td><div align='right'>Result 100</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult100" onblur="check(this,'number');jumpsquatmax()" class=txtboxes value="<%=rs1("Result100")%>"></td></tr>
	</table><table id=subDet width=270 border=0 cellspacing=0 cellpadding=0 style="display:none;position:absolute;left:10px;top:30px">
		<tr><td><div align='right'>JS PMax (calculated)</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input name="txtJSPMax" readonly class=txtboxes value="<%=rs1("JSPMax")%>"></td></tr>
<%	elseif Table="VertJump" then%>
		<tr><td><div align='right'>Stand & Jump Height (SJH)</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtStandJump" class=txtboxes onblur="check(this,'number');vertResult()" value="<%=rs1("StandJump")%>"></td></tr>
		<tr><td><div align='right'>Stand & Reach Height (SRH)</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtStandReach" class=txtboxes onblur="check(this,'number');vertResult()" value="<%=rs1("StandReach")%>"></td></tr>
		<tr><td><div align='right'>Result (SJH - SRH)</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input name="txtResult" readonly class=txtboxes value="<%=rs1("Result")%>" ID="txtResult"></td></tr>
	</table><table id=subDet width=270 border=0 cellspacing=0 cellpadding=0 style="display:none;position:absolute;left:10px;top:30px">
<%	elseif Table="countermovement" then%>
		<tr><td><div align='right'>Stand Reach Height (SRH)</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtReachHeight" name="txtReachHeight" class=txtboxes onblur="check(this, 'number');vertResult2()" value="<%=rs1("ReachHeight")%>"></td></tr>
		<tr><td><div align='right'>Jump Height (JH)</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtJumpHeight" id="txtJumpHeight" class=txtboxes onblur="check(this, 'number');vertResult2()" value="<%=rs1("JumpHeight")%>"></td></tr>
		<tr><td><div align='right'>Result (SRH - JH)</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" name="txtResult" id="txtResult" readonly class=txtboxes value="<%=rs1("Result")%>"></td></tr>
<%	end if%>
<%	if Table <> "PSquat1" and Table <> "PSquat3" and Table <> "LegPress1" and Table <> "LegPress3" and Table <> "FSquat3" and Table <> "FSquat3" and Table <> "FSquat3" and Table <> "FSquat1" and Table <> "BenchPress" and Table <> "CFShuttlerun" and Table <> "ModThomIlliop" and Table <> "ModThomQuad" and Table <> "StraightLegRaise" and Table <> "ForeArmGrip" then%>
		<tr><td><div align='right'>Percentile</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" disabled class=txtboxes value="<%=rs1("Percentile")%>"></td></tr>
		<tr><td><div align='right'>Norm</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" disabled class=txtboxes value="<%=rs1("Norm")%>"></td></tr>
<%	elseif Table = "BenchPress" or Table = "PSquat1" or Table = "PSquat3" or Table = "LegPress1" or Table = "LegPress3" or Table = "FSquat1" or Table = "FSquat3" then%>
	</table><table id=subDet width=270 border=0 cellspacing=0 cellpadding=0 style="display:none;position:absolute;left:10px;top:30px">
		<tr><td><div align='right'>Percentile</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" disabled class=txtboxes value="<%=rs1("Percentile")%>"></td></tr>
		<tr><td><div align='right'>Percentile&nbsp;Rel.</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" disabled class=txtboxes value="<%=rs1("PercentileRelative")%>"></td></tr>
		<tr><td><div align='right'>Norm</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" disabled class=txtboxes value="<%=rs1("Norm")%>"></td></tr>
		<tr><td><div align='right'>Norm&nbsp;Relative</div></td><td width='5'>&nbsp;</td><td colspan="5" width='170'><input maxlength="8" disabled class=txtboxes value="<%=rs1("NormRelative")%>"></td></tr>
<%	else%>
	<%	if Table = "ForeArmGrip" then%>
		</table><table id=subDet width=270 border=0 cellspacing=0 cellpadding=0 style="display:none;position:absolute;left:10px;top:30px">
	<%	end if%>
			<tr><td><div align='right'>Percentile</div></td><td width='5'>&nbsp;</td><td width='80px'><input maxlength="8" style="width:80px" disabled class=shorttxtboxes value="<%=rs1("PercentileL")%>"></td><td width='5'>&nbsp;</td><td width='80px'><input style="width:80px" disabled class=shorttxtboxes value="<%=rs1("PercentileR")%>"></td></tr>
			<tr><td><div align='right'>Norm</div></td><td width='5'>&nbsp;</td><td width='80px'><input maxlength="8" style="width:80px" disabled class=shorttxtboxes value="<%=rs1("NormL")%>"></td><td width='5'>&nbsp;</td><td width='80px'><input style="width:80px" disabled class=shorttxtboxes value="<%=rs1("NormR")%>"></td></tr>
	<%	if Table = "ForeArmGrip" then%>
			<tr><td><div align='right'>Percentile&nbsp;Rel.</div></td><td width='5'>&nbsp;</td><td width='80px'><input maxlength="8" style="width:80px" disabled class=shorttxtboxes value="<%=rs1("PercentileL")%>"></td><td width='5'>&nbsp;</td><td width='80px'><input style="width:80px" disabled class=shorttxtboxes value="<%=rs1("PercentileR")%>"></td></tr>
			<tr><td><div align='right'>Norm&nbsp;Relative</div></td><td width='5'>&nbsp;</td><td width='80px'><input maxlength="8" style="width:80px" disabled class=shorttxtboxes value="<%=rs1("NormL")%>"></td><td width='5'>&nbsp;</td><td width='80px'><input style="width:80px" disabled class=shorttxtboxes value="<%=rs1("NormR")%>"></td></tr>
	<%	end if%>
<%	end if%>
</table>

</form>
<script>
	function scrollTest(){
		alert(frmScroll.action)
		updater.txtscrollTest.value = "<%=page%>"
	}
<%if ((Table="PSquat1") or (Table="PSquat3") or (Table="LegPress1") or (Table="LegPress3") or (Table="FSquat3") or (Table="FSquat1") or (Table="ForeArmGrip") or (Table="BenchPress") or (Table="BenchThrow") or (Table="JumpSquat") or (Table="VertJump")) then%>
	var tabcontrol=true
<%else%>
	var tabcontrol=false
<%end if%>
	var Sumcontrol=false
	if(Sumcontrol==true && tabcontrol==false){subDet.style.display='Block'}
</script>
	<!-- #include file="..\includes\asp\Xscroll.asp" -->
<script>
	control.style.left = 50
	control.style.top = 240
	control.style.display = "block"
	disabler('<%=rs1("backwards")%>,<%=rs1("backsmall")%>,<%=rs1("forwardssmall")%>,<%=rs1("Forwards")%>,1,1,1,xx')
	function vertResult(){
		updater.txtResult.value = parseInt(updater.txtStandJump.value) - parseInt(updater.txtStandReach.value);
		if (!parseInt(updater.txtStandJump.value))
		{
			updater.txtResult.value = "";
		}
		if (!parseInt(updater.txtStandReach.value))
		{
			updater.txtResult.value = "";
		}
	}
	function vertResult2(){
		updater.txtResult.value = parseInt(updater.txtReachHeight.value) - parseInt(updater.txtJumpHeight.value);
		if (!parseInt(updater.txtReachHeight.value))
		{
			updater.txtResult.value = "";
		}
		if (!parseInt(updater.txtJumpHeight.value))
		{
			updater.txtResult.value = "";
		}
	}
	function benchthrowmax(){
	var strTempResult = 0
		strTempResult = updater.txtResult40.value
		if (parseFloat(updater.txtResult50.value) > strTempResult){strTempResult = parseFloat(updater.txtResult50.value)}
		if (parseFloat(updater.txtResult60.value) > strTempResult){strTempResult = parseFloat(updater.txtResult60.value)}
		if (parseFloat(updater.txtResult70.value) > strTempResult){strTempResult = parseFloat(updater.txtResult70.value)}
		if (parseFloat(updater.txtResult80.value) > strTempResult){strTempResult = parseFloat(updater.txtResult80.value)}
		updater.txtBTPMax.value = strTempResult
	}
	function jumpsquatmax(){
	var strTempResult = 0
		strTempResult = updater.txtResult20.value
		if (parseFloat(updater.txtResult40.value) > strTempResult){strTempResult = parseFloat(updater.txtResult40.value)}
		if (parseFloat(updater.txtResult60.value) > strTempResult){strTempResult = parseFloat(updater.txtResult60.value)}
		if (parseFloat(updater.txtResult80.value) > strTempResult){strTempResult = parseFloat(updater.txtResult80.value)}
		if (parseFloat(updater.txtResult100.value) > strTempResult){strTempResult = parseFloat(updater.txtResult100.value)}
		updater.txtJSPMax.value = strTempResult
	}
	function RelativeResult(){
<%	if (Table="PowerClean") then	%>
		updater.txtRelativeResult.value = (Math.round((parseFloat(updater.txtResult.value)/(parseFloat(updater.txtBodyWeight.value)*0.58))*100)/100)
		if (!parseFloat(updater.txtResult.value))
		{
			updater.txtRelativeResult.value = "";
		}
		if (!parseFloat(updater.txtBodyWeight.value))
		{
			updater.txtRelativeResult.value = "";
		}
<%	elseif (Table="ForeArmGrip") then	%>
		if (updater.txtResultL.value!=''&&updater.txtBodyWeight.value!=''){
			updater.txtRelativeResultL.value = (Math.round((parseFloat(updater.txtResultL.value)/(parseFloat(updater.txtBodyWeight.value)))*100)/100)
		}
		if (updater.txtResultR.value!=''&&updater.txtBodyWeight.value!=''){
			updater.txtRelativeResultR.value = (Math.round((parseFloat(updater.txtResultR.value)/(parseFloat(updater.txtBodyWeight.value)))*100)/100)
		}
		if (!parseFloat(updater.txtResultL.value))
		{
			updater.txtRelativeResultL.value = "";
		}
		if (!parseFloat(updater.txtBodyWeight.value))
		{
			updater.txtRelativeResultL.value = "";
			updater.txtRelativeResultR.value = "";
		}
		if (!parseFloat(updater.txtResultR.value))
		{
			updater.txtRelativeResultR.value = "";
		}
<%	elseif (Table="BenchPress") then	%>
		if ((updater.txtBodyWeight.value != "")&&(updater.txtResult.value != ""))
		{
			updater.txtRelativeResult.value = (Math.round((parseFloat(updater.txtResult.value)/(parseFloat(updater.txtBodyWeight.value)*0.57))*100)/100)
		}
		else
		{
			updater.txtRelativeResult.value = "";
		};
<%	elseif ((Table="LegPress3") or (Table="LegPress1") or (Table="FSquat3") or (Table="FSquat1") or (Table="PSquat3") or (Table="PSquat1")) then%>
		updater.txtRelativeResult.value = (Math.round((parseFloat(updater.txtResult.value)/(parseFloat(updater.txtBodyWeight.value)*0.60))*100)/100)
		if (!parseFloat(updater.txtResult.value))
		{
			updater.txtRelativeResult.value = "";
		}
		if (!parseFloat(updater.txtBodyWeight.value))
		{
			updater.txtRelativeResult.value = "";
		}
<%	end if	%>
	}
	RelativeResult()
</script>
</body>
</html>