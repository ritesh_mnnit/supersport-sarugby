<%@ Language=VBScript.Encode %>
<!--'===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<%#@~^5QAAAA==@#@&sG^ND1m:7d{dE?1Dnn	/zr@#@&alY4K.+6kXd7'7EcR&J@#@&2swsGH++r[idxdr!x(GJ@#@&Abo}.ND$Xidx7r?EMUls+J@#@&iqGdid7d7xiJiqGE@#@&?hC^Vr.[D$XixiJi&fr@#@&Dl(s+id7di'7EnkYr@#@&;+xD.rm:l(Vndx7r2hw^GX+9CDlJ@#@&alL+i7id7{dr|kDRmdwr@#@&rjYAAA==^#~@%>

<!-- #include file="..\includes\asp\nextprevtop.asp" -->

<%#@~^lAAAAA==@#@&dvqkDtPgl7PXa+SPZPhnmx/,OtPxm-romYbWUPrd,WW0B~F,:nC	/PO4PUl7rTlObW	PkkPKU@#@&dr0,/ndkkW	cJgl\:z2+r#,'~JqE,lUN,.+$EndD`JUUwEUmrb,'~rJ,Ytx@#@&dZywAAA==^#~@%>
		<script Language="JavaScript">
			newWin = window.open('newnav.asp?openhlp=<%=#@~^FAAAAA==~M+5!+kY`rWanx4V2J*P1AYAAA==^#~@%>&displayMenu=<%=#@~^GAAAAA==~M+5!+kY`rNbdw^lzHx;E*PaQgAAA==^#~@%>','newWin','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=750,height=550');
			newWin.focus()
			parent.frames.Search.hideSearchHelp()
			parent.frames.Search.close();
		</script>
	<%#@~^DwAAAA==@#@&dn	N,k0@#@&XQIAAA==^#~@%>

	<!--Tabs-->
	<td style="width:100%" style='background-color: #F7F7F7;border-top: 1px solid #C0C0C0'>&nbsp;</td></tr></table>

<div id="updatehelpdiv01" style="display:none;position:absolute;top:46px;left:135px;background-color:#FF3300;border: 1px solid black;color:white;width:248px">
	<table>
		<tr>
			<td valign="bottom"><img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
			<td style="color:white">Start by filling in the "Date Updated" and any other details you want updated.</td>
		</tr>
	</table>
</div>
	
<div id="summaryhelpdiv" style="display:none;position:absolute;top:376px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:230px;height:50px;">
	<table width="230px">
		<tr>
			<td valign="top" align="right"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px">&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td valign="top" style="color:white">If you want to see a summary of all the players with Kit details. <br>Click the icon with the black stripes.</td>
		</tr>
	</table>
</div>

<div id="updatehelpdiv04" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:315px;">
	<table>
		<tr>
			<td valign="top" align="right" style="width:193px;"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px"></td>
			<td style="width:122px;"></td>
		</tr>
		<tr>
			<td valign="top" style="color:white" colspan="2">When you are sure that you are ready, and you want to save the changes you have made to the system.<br>Click the icon with the black circle.</td>
		</tr>
	</table>
</div>

<div id="viewhelpdiv" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:288px;" align="right">
	<table>
		<tr>
			<td style="width:139px;">&nbsp;</td>
			<td align="left" valign="top" style="width:149px"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
		<tr>
			<td style="color:white" colspan="2">You can scroll through the players Kit records. <br>If you need help, <a href="#" onclick="window.open('../includes/asp/help/scrolling.asp?helptype=single','','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=650,height=500')" style="color:white; text-decoration:underline">click here.</a></td>
		</tr>
	</table>
</div>

<div id="updatehelpdiv05" style="display:none;position:absolute;top:387px;left:335px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td style="color:white">To return to the Navigation Wizard, <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a></td>
		</tr>
	</table>
</div>

<form name=updater id=updater method=post action=''>
<table  width=290 border=0 cellspacing=0 cellpadding=0 style='position:absolute;left:16px;top:79px;'>
	<tr><td><div align='right'>Player</div></td><td width='5'>&nbsp;</td><td><input name="txtemployeeno" class=txtboxes disabled ID="txtemployeeno" value="<%=#@~^OQAAAA==.knsmXDfmYmcJwk./D1ChJ#,'PrPJ,'~Dkn^lz+.9mYC`rjEMxChJ#VhIAAA==^#~@%>"></td></tr><%#@~^mQMAAA==@#@&d]/aWxk+cADbYnPr@!rUaEY,rN{BYXO!+	qGB~xCh'vYXOMx(9EPYz2'vtb[9+UEP7lV!+{vJLDdFvJ!n	qfrb[rB@*r@#@&d"+kwGxdnch.kDnP6kns9/+^crJSJGCD+iaNmY+9JBENmYnYHwnEBF~rE~rNlDnE~rfmYn[U8kwIja[lD+[E*@#@&7]/2W	dRAMkD+P6ksNk+^`rJSE_+kT4Yr~JbU2ED4K6E~qSrPhlXs+	oO4{JJfErJSJ	;s4nMJBJCkT4Yvmh#r#@#@&iI+k2W	/+cA.kD+,0r+s[k+^`rE~rnrTtYESrkUw!O(War~8~J,:maVxLY4'EE2JJrSJ	E:(n.JBJq+ro4Ov3L#rb@#@&d]nkwWUdRADbOPWb+^N/mvEJBJ/t/OEBJk	2ED4WXESFBJ,:C6sn	oOt{EJ2JEEBJx;h(+.JBE;tnkYvm:*J*@#@&iIn/aWUdRhMrYP0bnsNk+1`EJSE;WsVm.JBJrUaEY8GXJSFBE,:CXVxoDt{EJ2JEJBJU;s4+ME~rZW^sCDvms#E#@#@&iIn/aGxk+ AMkYn~6knV9dmcrJBJKMl1V/!kOJBJ[.KwNKAxr~FBEE~rJBJPDC^0/;kDc/bynbr#@#@&7"+dwKUk+ SDbY+,0bnV9/nmvJESrMW^W/4kDDESJ9DKw[WAUr~q~rE~rJSEVWVW'	4dwpj4k.D`kky#rb@#@&d]+kwGUk+RS.kD+P6rnV9/mcJESrI;o(z9DdnHJ~E[MW2NKA	JS8~rJ~rJBEI!o8XLx8dai9./X`kr"+*J*@#@&d]nkwGxknRSDrOP0rn^Nd+1crJSrI!o4H?4GDD/E~rN.GaNWSUJBF~rESJr~rI;o8zLx8/aI?4W.OkJ#7@#@&d@#@&qhkBAA==^#~@%></table><table id=subDet width=290 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:340px;top:79px;display:block"><%#@~^EQMAAA==@#@&d]/aWxk+cADbYnP6kns9/+1cJr~J~sCyDUk"+ESrN.Wa[WSxES8~JESrJSJ~smynM[	4/aiUryJb@#@&d]nkwW	d+chDbOnP6kV[/n^vJE~r$Vmyn.;EYESrN.Wa[KhUr~8~Jr~rE~rAsl.+.'	4/aIZ!YJ*@#@&d"+kwGxdnch.kDnP6kns9/+^crJSJ:.KEdDqlkkYrSJ9DGw9WAUr~FBEJBJJBE@!4@*KMW;/n.=@!&4@*'x(/2ILx4d2p[U4k2p@!r@*mk/D`1h#@!zr@*r#@#@&iI+k2W	/+cA.kD+,0r+s[k+^`rE~rK.G!/+.(	/rNJoEBJbxw!Y(G6r~q~rPhCXV+	LY4'JrfEJr~rx;:8nMJSJ@!r@*&xdr9+[U8kwISLvmh*@!Jk@*r#@#@&d"+dwKxdnchDbO+,0ks[/mvJE~EPMlrxbUoUtGnr~J[.Kw[WSUr~qBJr~Jr~rPDmkUk	o'U(/wpjtK+[	8dwp?bynJb@#@&d]+k2W	/n SDkOn,0r+^[k+^vJr~JUtKnJBJ[DKw[GSxJBq~rJ~rESJUtK+'x8daijk.nJ*@#@&7"+/2G	/nRS.bYn,0b+V9/^`rJSJ~WGO;EYrSJ9DWa[Gh	JBFSJESrJSJ@!8@*~WGO=@!z8@*Lx8/aILx8kwp[x(/aI@!b@*/ED@!&r@*J#@#@&d"+/aGU/RSDrYn~6knV9d+1`EEBJAGGD?OE9dr~E9DKwNKh	E~8~EJBJESr@!k@*jY!N/@!&r@*r#@#@&iuoAAA==^#~@%></table>
</form>

<script>
	spnPageHeader.innerHTML = "Kit Details"
	var tabcontrol		= false
	var Sumcontrol	= false
	if(Sumcontrol==true&&tabcontrol==false){subDet.style.display='Block'}
</script>
	<!-- #include file="..\includes\asp\Xscroll.asp" -->
<script>
	disabler('<%=#@~^EAAAAA==.kFcr4mm3SlM[/r#XQUAAA==^#~@%>,xx,xx,<%=#@~^DwAAAA==.kFcrsKDhmD9dJ*8wQAAA==^#~@%>,1,xx,xx,1')
	control.style.width='70'
	control.style.left='122'
	control.style.display = "block"

	function showUpdateHelp()
	{
		document.all.updatehelpdiv01.style.display = ""
		document.all.updatehelpdiv02.style.display = ""
		//document.all.updatehelpdiv03.style.display = ""
		document.all.updatehelpdiv04.style.display = ""
		document.all.updatehelpdiv05.style.display = ""
	}

</script>

<%#@~^XAAAAA==@#@&kW,///bW	cJgl-KHwnE*P',EFrPY4nU@#@&/VnmO~1ld+,.+$EndD`JUUwEUmrb@#@&@#@&mm/+,J7r+SJ@#@&oBgAAA==^#~@%>
<script Language="JavaScript">
parent.frames.Search.openSearchScreen();
parent.frames.Search.showSearchHelp();
document.all.updatehelpdiv05.style.display = "";
document.all.viewhelpdiv.style.display = "";
parent.frames.Search.setNNFunc('view');
</script>
<%#@~^GQAAAA==@#@&@#@&1lk+PrEa[lD+E@#@&yAQAAA==^#~@%>
<script Language="JavaScript">
parent.frames.Search.openSearchScreen();
parent.frames.Search.showSearchHelp();
document.all.updatehelpdiv05.style.display = "";
document.all.updatehelpdiv01.style.display = "";
document.all.updatehelpdiv04.style.display = "";
parent.frames.Search.setNNFunc('update');
</script>
<%#@~^GgAAAA==@#@&@#@&1lk+Pr/!h:mDzJ@#@&UwUAAA==^#~@%>
<script Language="JavaScript">
document.all.updatehelpdiv05.style.display = "";
document.all.summaryhelpdiv.style.display = "";
</script>
<%#@~^HAAAAA==@#@&+U9Pk+VmD@#@&x[Pb0@#@&QgYAAA==^#~@%>
</body>
</html>