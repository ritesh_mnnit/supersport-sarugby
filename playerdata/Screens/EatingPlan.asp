<%@ Language=VBScript.Encode %>
<!--===========================================================================================
========================EVERYTHING on this site is copyrighted to Genesys=================================
==============Any use without written approval by Genesys is considered infringment.============================
=============================================================================================-->
<%#@~^2QEAAA==@#@&sG^ND1m:7d{dE?1Dnn	/zr@#@&alY4K.+6kXd7'7EcR&J@#@&2swsGH++r[idxdr!x(GJ@#@&Abo}.ND$Xidx7r?EMUls+J@#@&iqGdid7d7xiJiqGE@#@&?hC^Vr.[D$XixiJi&fr@#@&Dl(s+id7di'7EAlYbUohVl	E@#@&;+	Y.k^Pm4s+ixdr2h2^WXnnGlOlr@#@&wCT+iddid{7JAlOk	oKsmxRmdwr@#@&7Cssb+^Ndd77{PEYXOK/O9mY+S9mYnuDaDhnbo4Y~q+bLtDuO6Db[[M+/kPXa+~z[[D/kPPX2n-/nVmUY4DGLKlVdSzxOtMGaWhYMkm,MKCVkud+^1;h(+DUs+wk	LS1!:(+.Pjs+2k	Luk+snmYkULMkLtDSAlObxTPIbo4Ouk+s:+Or	ob	OtMWMKCs/BH+OkUL,bUY4.WaWhnDDk^~VWCVkE@#@&ZZMAAA==^#~@%>

<%#@~^lAAAAA==@#@&dvqkDtPgl7PXa+SPZPhnmx/,OtPxm-romYbWUPrd,WW0B~F,:nC	/PO4PUl7rTlObW	PkkPKU@#@&dr0,/ndkkW	cJgl\:z2+r#,'~JqE,lUN,.+$EndD`JUUwEUmrb,'~rJ,Ytx@#@&dZywAAA==^#~@%>
		<script Language="JavaScript">
			newWin = window.open('newnav.asp?openhlp=<%=#@~^FAAAAA==~M+5!+kY`rWanx4V2J*P1AYAAA==^#~@%>&displayMenu=<%=#@~^GAAAAA==~M+5!+kY`rNbdw^lzHx;E*PaQgAAA==^#~@%>','newWin','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=750,height=550');
			newWin.focus()
			parent.frames.Search.hideSearchHelp()
			parent.frames.Search.close();
		</script>
	<%#@~^DwAAAA==@#@&dn	N,k0@#@&XQIAAA==^#~@%>
<!-- #include file="..\includes\asp\nextprevtop.asp" -->
<%#@~^bgAAAA==~@#@&7k+DPDkn^CXD~',mGU8R+Xnm!Y+vEj+^+1Y~YG2,F~e,WDK:~3swVGz+9lDC,h4DPMx&9P{PvJ,[~.kF`r!+	qfrb~[,JEJb@#@&BB8AAA==^#~@%>
	<td id='tabs' align='center' onclick="fnTabClick('0');"><a class='clsTabLink' href='javascript:void();'onclick= " this.blur(); return fnTabClick('0');" style='font-size: 9px'>&nbsp;Information&nbsp;</a></td>
	<td id='tabs' align='center' onclick="fnTabClick('1');"><a class='clsTabLink' href='javascript:void();'onclick= " this.blur(); return fnTabClick('1');" style='font-size: 9px'>&nbsp;Summary&nbsp;</a></td>
	<td style="width:100%" style='background-color: #F7F7F7;border-top: 1px solid #C0C0C0'>&nbsp;</td></tr></table>

<div id="scrolling" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:288px;height:60px;" align="right">
	<table>
		<tr>
			<td style="width:108px;">&nbsp;</td>
			<td align="left" valign="top" style="width:180px"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
		</tr>
		<tr>
			<td style="color:white" colspan="2">You can scroll through players individual records, or you can scroll through the players themselves. If you need help, <a href="#" onclick="window.open('../includes/asp/help/scrolling.asp?helptype=single','','scrollbars=yes,toolbar=no,menubar=no,0,status=no,width=650,height=500')" style="color:white; text-decoration:underline">click here.</a></td>
		</tr>
	</table>
</div>

<div id="tabing" style="display:none;position:absolute;top:33px;left:350px;background-color:#FF3300;border: 1px solid black;color:white;width:205px">
	<table>
		<tr>
			<td align="right" width="14px"><img src="../images/Navigation/arrl.gif" alt="" border="0" style="width: 14px; height: 8px"></td>
			<td style="color:white" align="right">Click here for more information.</td>
		</tr>
	</table>
</div>

<div id="update" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:315px;">
	<table>
		<tr>
			<td valign="top" align="right" style="width:193px;"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px"></td>
			<td style="width:122px;"></td>
		</tr>
		<tr>
			<td valign="top" style="color:white" colspan="2">If you make any changes to a record, and you want to save the changes you have made to the system.<br>Click the icon with the black circle.</td>
		</tr>
	</table>
</div>

<div id="remove" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:315px;">
	<table>
		<tr>
			<td valign="top" align="right" style="width:223px;"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px"></td>
			<td style="width:92px;"></td>
		</tr>
		<tr>
			<td valign="top" style="color:white" colspan="2">To remove a record from the system click the icon with the red X. <br>NB: It will permanently remove this record from the system.</td>
		</tr>
	</table>
</div>

<div id="save2" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:315px;">
	<table>
		<tr>
			<td valign="top" align="right" style="width:253px;"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px"></td>
			<td style="width:62px;"></td>
		</tr>
		<tr>
			<td valign="top" style="color:white" colspan="2">To add a new record to the system click this icon. <br>It will change to a green tick. <b>NB: Only</b> then insert the information for the new record. When completed click the green tick to save the record to the system.</td>
		</tr>
	</table>
</div>

<div id="save1" style="display:none;position:absolute;top:375px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:315px;">
	<table>
		<tr>
			<td valign="top" align="right" style="width:253px;"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px"></td>
			<td style="width:62px;"></td>
		</tr>
		<tr>
			<td valign="top" style="color:white" colspan="2">To create an Eating Plan record for a player. <br>Fill in the information for the record. <br>When completed, click the icon with the green tick to save the record to the system.</td>
		</tr>
	</table>
</div>

<div id="returntonav" style="display:none;position:absolute;top:387px;left:335px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td style="color:white">To return to the Navigation Wizard, <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a></td>
		</tr>
	</table>
</div>

<div id="returnfromsave" style="display:none;position:absolute;top:387px;left:335px;background-color:#FF3300;border: 1px solid black;color:white;width:120px" align="right">
	<table>
		<tr>
			<td style="color:white">To <strong>CANCEL</strong> saving, and to return to the Navigation Wizard, <a href="playerrep.asp" style="color:white; text-decoration:underline">click here</a></td>
		</tr>
	</table>
</div>

<div id="eatingplan" style="display:none;position:absolute;top:307px;left:30px;background-color:#FF3300;border: 1px solid black;color:white;width:165px;">
	<table>
		<tr>
			<td align="center" width="20px"><img src="../images/Navigation/arrd.gif" alt="" border="0" style="width: 8px; height: 14px"></td>
			<td style="color:white" width="145px">To view the Eating plan, click the icon below.</td>
		</tr>
	</table>
</div>

<div id="summary" style="display:none;position:absolute;top:376px;left:8px;background-color:#FF3300;border: 1px solid black;color:white;width:288px;height:50px;">
	<table width="286px">
		<tr>
			<td valign="top" align="right"><img src="../images/Navigation/arru.gif" alt="" border="0" style="width:8px;height:14px">&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td valign="top" style="color:white">If you want to see a summary of all the players with Eating details. <br>Click the icon with the black stripes.</td>
		</tr>
	</table>
</div>

<form name=updater id=updater method=post action=''>
<table  width=290 border=0 cellspacing=0 cellpadding=0 style='position:absolute;left:14px;top:72px;'><%#@~^ZAAAAA==@#@&d]/aWxk+cADbYnPr@!rUaEY,rN{BYXO!+	qGB~xCh'vYXOMx(9EPYz2'vtb[9+UEP7lV!+{vJLDdFvJ!n	qfrb[rB@*rKx4AAA==^#~@%>
<tr><td><div align='right'>Player</div></td><td width='5'>&nbsp;</td><td><input name="txtemployeeno" class=txtboxes disabled ID="txtemployeeno" value="<%#@~^SAAAAA==./2Kxk+RqDbO+,Ddn^lznMflDC`rskMdO1m:JbP'~rPEPL~DknsCH+D9CDlcJU;MxCs+r#HhgAAA==^#~@%>"></td></tr>
<%#@~^sAAAAA==]/2Kxk+RSDbO+,0r+^Ndn1`JrSJ:+/D9CYJBJ[lOnDX2+rSFBJESrNlOnr~EfmOJb@#@&iI+kwKU/RADbYn~6k+^[/m`rESJS+bo4YESrkUw!O4K6ES8~JhCXVnxTO4'Er%rJJBJ	;:(+.JBJ/;MD+	O[	4/aI	+bo4Y~`VL*Jb/TcAAA==^#~@%>
	<tr><td><div align='right'>Address Type</div></td><td width='5'>&nbsp;</td><td width='170'><select id="txtAddressType" name="txtAddressType" class='txtboxes'><%=#@~^LAEAAA==.wsmm`J@!WaOkKx@*@!JW2ObWx@*@!WaYkKU~\mV!+xBqv@*bOP4G:z~GSxPWCsksX@!&KwObW	@*@!KwDrW	P-l^EnxE B@*6Y4+D,WC:bVH@!&W2ObWU@*@!GwDkGU,\ls;'v&E@*_WdD+^zPM+krNx^+@!zG2DkW	@*@!KwYbGUP7l^En'v*E@*]ET8X,W.Lmxk"n9PWVmOJP4KEk+@!JWaOkKx@*JBJvELPDkq`rbN9.n/kKHwnJb~LPEBrSJEJ'~M/FcEzN[DdkKza+r#PLPrvPk+s+1Yn[r#7F0AAA==^#~@%>'</select></td></tr>
	<tr><td><div align='right'>Address</div></td><td width='5'>&nbsp;</td><td width='170'><input id="ed" title="Data from the Player Details screen" class='txtboxes' disabled value='<%=#@~^FAAAAA==.knsmXD`rb9[D/dFr#3gYAAA==^#~@%>'></td></tr>
	<tr><td><div align='right'></div></td><td width='5'>&nbsp;</td><td width='170'><input id="ed" title="Data from the Player Details screen" class='txtboxes' disabled value='<%=#@~^FAAAAA==.knsmXD`rb9[D/d r#3wYAAA==^#~@%>'></td></tr>
	<tr><td><div align='right'></div></td><td width='5'>&nbsp;</td><td width='170'><input id="ed" title="Data from the Player Details screen" class='txtboxes' disabled value='<%=#@~^FAAAAA==.knsmXD`rb9[D/d&r#4AYAAA==^#~@%>'></td></tr>
	<tr><td><div align='right'>Post Code</div></td><td width='5'>&nbsp;</td><td width='170'><input id="ed" title="Data from the Player Details screen" class='txtboxes' disabled value='<%=#@~^FAAAAA==.knsmXD`rnKdY;W[+r#CAcAAA==^#~@%>'></td></tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td><td style="position:relative;top:2;"><div>Area code&nbsp;&nbsp;&nbsp;&nbsp;Number</div></td></tr>
	<tr><td><div align='right' title="Please type in the player's home telephone number">Home Phone</div></td><td>&nbsp;</td><td>
		<table border=0  cellspacing=0 cellpadding=0 style="position:relative;left:0;"><tr>
			<td align=right><input id="ed" title="Data from the Player Details screen" disabled class=shorttxtboxes value="<%=#@~^GQAAAA==.knsmXD`rCKh+htGxb.nmJ#4wgAAA==^#~@%>"></td><td>&nbsp;</td>
			<td><input disabled id="ed" title="Data from the Player Details screen" class=mediumtxtboxes value="<%=#@~^FQAAAA==.knsmXD`rCKh+htGxJbagcAAA==^#~@%>"></td><td>&nbsp;</td></tr></table></td></tr><tr>
		<td><div align='right'>Work Phone</div></td><td>&nbsp;</td>
	    <td><table border=0 cellspacing=0 cellpadding=0 style="position:relative;left:0;" ID="Table3"><tr>
			<td align=right><input id="ed" title="Data from the Player Details screen" disabled class=shorttxtboxes value="<%=#@~^GQAAAA==.knsmXD`rK.3htGxb.nmJ#/QgAAA==^#~@%>"></td><td>&nbsp;</td>
			<td><input disabled id="ed" title="Data from the Player Details screen" class=mediumtxtboxes value="<%=#@~^FQAAAA==.knsmXD`rK.3htGxJbhAcAAA==^#~@%>"></td><td>&nbsp;</td>
		</tr></table></td>
	</tr>
	<tr><td><div align='right'>Cell Phone</div></td><td width='5'>&nbsp;</td><td width='170'><input class='txtboxes' id="ed" disabled value='<%=#@~^FQAAAA==.knsmXD`rZsVhtGxJbYQcAAA==^#~@%>'></td></tr>

</table><table id=subDet width=300 border=0 cellspacing=0 cellpadding=0 style="position:absolute;left:345px;top:72px;display:block">
	<tr><td><div align='right'>Anthropometric&nbsp;Goals</div></td><td style='width:5px'></td><td align='right' style='height:21px'><select name='selanthrogoals' class=mediumtxtboxes onchange='c(this)' ID="selanthrogoals"><%=#@~^GwAAAA==LYGaYbWxk`rCxDt.WTWCskJ~8b4AkAAA==^#~@%></select></td></tr>
	<tr><td><div align='right'>No&nbsp;People&nbsp;per&nbsp;Sleeping&nbsp;Room?</div></td><td style='width:5px'></td><td align='right' style='height:21px'><select name='selNumberSleeping' class=mediumtxtboxes onchange='c(this)' ID="selNumberSleeping"><%=#@~^HgAAAA==LYGaYbWxk`rHEs4nDUVnnakxTE~8#3goAAA==^#~@%></select></td></tr>
	<tr><td><div align='right'>Are&nbsp;you&nbsp;eating&nbsp;the&nbsp;right&nbsp;Diet?</div></td><td style='width:5px'></td><td align='right' style='height:21px'><select name='seleatingright' class=mediumtxtboxes onchange='c(this);OtherChange(this,spnFactors1)' ID="seleatingright"><%=#@~^GwAAAA==LYGaYbWxk`rnlDkUoMkL4DJ~8b1AkAAA==^#~@%></select></td></tr>
	<tr id='spnFactors1' name='spnFactors1' style='display:none'><td colspan='3' style='height:20px'><div align='left'>Which&nbsp;factors&nbsp;make Eating&nbsp;right&nbsp;Difficult</div></td></tr>
	<tr id='spnFactors2' name='spnFactors2' style='display:none'><td><div align='right'>Is&nbsp;there&nbsp;money&nbsp;to&nbsp;buy&nbsp;food</div></td>			<td style='width:5px'></td><td align='right' style='height:21px'><select name='selNoMoney' class=mediumtxtboxes onchange='c(this)' ID="selNoMoney"><%=#@~^FwAAAA==LYGaYbWxk`rHWtWU+HJSq*AwgAAA==^#~@%></select></td></tr>
	<tr id='spnFactors3' name='spnFactors3' style='display:none'><td><div align='right'>Are&nbsp;you&nbsp;away&nbsp;at&nbsp;mealtimes</div></td>	<td style='width:5px'></td><td align='right' style='height:21px'><select name='selAway' class=mediumtxtboxes onchange='c(this)' ID="selAway"><%=#@~^FAAAAA==LYGaYbWxk`r)hmXE~8#0AYAAA==^#~@%></select></td></tr>
	<tr id='spnFactors4' name='spnFactors4' style='display:none'><td><div align='right'>Do&nbsp;you&nbsp;have&nbsp;a&nbsp;fridge</div></td>							<td style='width:5px'></td><td align='right' style='height:21px'><select name='selFridge' class=mediumtxtboxes onchange='c(this)' ID="selFridge"><%=#@~^FgAAAA==LYGaYbWxk`roDbNL+r~qbjwcAAA==^#~@%></select></td></tr>
	<tr id='spnFactors5' name='spnFactors5' style='display:none'><td><div align='right'>Do&nbsp;you&nbsp;know&nbsp;what&nbsp;to&nbsp;eat</div></td>				<td style='width:5px'></td><td align='right' style='height:21px'><select name='selKnowledge' class=mediumtxtboxes onchange='c(this)' ID="selKnowledge"><%=#@~^GQAAAA==LYGaYbWxk`rFxKhs+9onEBF#3ggAAA==^#~@%></select></td></tr>
	<tr id='spnFactors6' name='spnFactors6' style='display:none'><td><div align='right'>Do&nbsp;you&nbsp;have&nbsp;Electricity</div></td>									<td style='width:5px'></td><td align='right' style='height:21px'><select name='selElectricity' class=mediumtxtboxes onchange='c(this)' ID="selElectricity"><%=#@~^GwAAAA==LYGaYbWxk`r3VmODbmrOHJ~8bvwkAAA==^#~@%></select></td></tr>
	<tr id='spnFactors7' name='spnFactors7' style='display:none'><td><div align='right'>Do&nbsp;you&nbsp;often&nbsp;have&nbsp;no&nbsp;appetite</div></td>		<td style='width:5px'></td><td align='right' style='height:21px'><select name='selAppetite' class=mediumtxtboxes onchange='c(this)' ID="selAppetite"><%=#@~^GAAAAA==LYGaYbWxk`r)wa+OkD+ES8#eggAAA==^#~@%></select></td></tr>
	<tr id='spnFactors8' name='spnFactors8' style='display:none'><td><div align='right'>Other</div></td>																					<td style='width:5px'></td><td align='right' style='height:21px'><select name='selOther' class=mediumtxtboxes onchange='c(this);OtherChange2(this,spneatplan)' ID="selOther"><%=#@~^FQAAAA==LYGaYbWxk`r6Y4+.JBFbQAcAAA==^#~@%></select></td></tr>
	<tr id="spneatplan" name="spneatplan" style="display:none"><td><div align='right'>Other&nbsp;Eating&nbsp;Factors</div></td>												<td style='width:5px'></td><td align='right' style='height:21px'><textarea name='txtEatingOther' class=mediumtxtboxes style='height:50' onchange='c(this)' ID="txtEatingOther" onblur="limitLength(this,'500')" onkeydown="limitLength(this,'500')"><%=#@~^EgAAAA==.kFcr2mYk	o}OtDE#BQYAAA==^#~@%></textarea></td></tr>
	<tr><td><div align='right'>Is&nbsp;Player&nbsp;meeting&nbsp;Anth.&nbsp;Goals?</div></td><td style='width:5px'></td><td align='right' style='height:21px'><select name='selmeetingAnthroGoals' class=mediumtxtboxes onchange='c(this)' ID="selmeetingAnthroGoals"><%=#@~^IgAAAA==LYGaYbWxk`rh+YrxTbUO4DWVGl^/JBqbiQwAAA==^#~@%></select></td></tr>
</table>
</form>

<table border="1" cellspacing="4" bordercolor="#CCCCCC" style="border:1px solid #DDDDDD; position:absolute;top:340px;left:29px"><tr><td><img style="width:20;height:20" src="../images/print.gif" onclick="openprint()" alt="View eating plan"></td></tr></table>
<%#@~^NQAAAA==./2Kxk+RSDbO+,/;:sl.zvJKdYGlYSCxDtMWLWCsk~A+bLtDJbKxQAAA==^#~@%>
<script>
	spnPageHeader.innerHTML = "Eating Plan"
	var tabcontrol=true
	var Sumcontrol=true
	if(Sumcontrol==true&&tabcontrol==false){subDet.style.display='Block'}
</script>
	<!-- #include file="..\includes\asp\Xscroll.asp" -->
<script>
	disabler('<%=#@~^EAAAAA==.kFcr4mm3SlM[/r#XQUAAA==^#~@%>,<%=#@~^EAAAAA==.kFcr4mm3k:msVr#VQUAAA==^#~@%>,<%=#@~^FAAAAA==.kFcr0KDhmD9d/slsVr#LAcAAA==^#~@%>,<%=#@~^DwAAAA==.kFcrsKDhmD9dJ*8wQAAA==^#~@%>,1,1,1,1')
	control.style.display = "block"

	function openprint(){
		if ((updater.txtweight.value!='')&&(updater.selanthrogoals.value!='')){
			page="greensquaddiet.asp?weight="+updater.txtweight.value+"&goals="+updater.selanthrogoals.value
			var mainwin = window.open(page,"","toolbar=no,menubar=no,resizable,scrollbars=yes,status=yes,fullscreen");
			mainwin.focus();
		}else{
			alert("Please enter a value for Weight as well as Anthropometric Goals.")
		}
	}
	function OtherChange(other,field){
		if(other.options[other.selectedIndex].text=='No'){
			spnFactors1.style.display='block'
			spnFactors2.style.display='block'
			spnFactors3.style.display='block'
			spnFactors4.style.display='block'
			spnFactors5.style.display='block'
			spnFactors6.style.display='block'
			spnFactors7.style.display='block'
			spnFactors8.style.display='block'
		}else{
			spnFactors1.style.display='none'
			spnFactors2.style.display='none'
			spnFactors3.style.display='none'
			spnFactors4.style.display='none'
			spnFactors5.style.display='none'
			spnFactors6.style.display='none'
			spnFactors7.style.display='none'
			spnFactors8.style.display='none'
			spneatplan.style.display='none'
		}
	}
	function OtherChange2(other,field){
		if(other.options[other.selectedIndex].text=='Yes'){
			field.style.display='block'
		}else{
			field.style.display='none'
		}
	}

	<%#@~^GwAAAA==r6PUKY,k/	E^s`M/q``q9b*YtUQAkAAA==^#~@%>
	function loadEvents(){
		if (updater.seleatingright.options[updater.seleatingright.selectedIndex].text=="No"){spnFactors1.style.display="block"}
		if (updater.seleatingright.options[updater.seleatingright.selectedIndex].text=="No"){spnFactors2.style.display="block"}
		if (updater.seleatingright.options[updater.seleatingright.selectedIndex].text=="No"){spnFactors3.style.display="block"}
		if (updater.seleatingright.options[updater.seleatingright.selectedIndex].text=="No"){spnFactors4.style.display="block"}
		if (updater.seleatingright.options[updater.seleatingright.selectedIndex].text=="No"){spnFactors5.style.display="block"}
		if (updater.seleatingright.options[updater.seleatingright.selectedIndex].text=="No"){spnFactors6.style.display="block"}
		if (updater.seleatingright.options[updater.seleatingright.selectedIndex].text=="No"){spnFactors7.style.display="block"}
		if (updater.seleatingright.options[updater.seleatingright.selectedIndex].text=="No"){spnFactors8.style.display="block"}
		if (updater.selOther.options[updater.selOther.selectedIndex].text=="Yes"){spneatplan.style.display="block"}
	}
	loadEvents()
	<%#@~^BgAAAA==n	N~b0JgIAAA==^#~@%>
</script>
<%#@~^WAAAAA==@#@&kW,///bW	cJgl-KHwnE*P',EFrPY4nU@#@&/VnmO~1ld+,.+$EndD`JUUwEUmrb@#@&^m/PJ7kAJ@#@&iRgAAA==^#~@%>
<script Language="JavaScript">
parent.frames.Search.openSearchScreen();
parent.frames.Search.showSearchHelp()
document.all.returntonav.style.display = "";
document.all.scrolling.style.display = "";
document.all.tabing.style.display = "";
parent.frames.Search.setNNFunc('view');
</script>
<%#@~^EgAAAA==@#@&mCk+,Jl9Nr@#@&VwMAAA==^#~@%>
<script Language="JavaScript">
parent.frames.Search.openSearchScreen();
parent.frames.Search.showSearchHelp()

<%#@~^GgAAAA==~b0~b/	EV^`MdFvj(f*#~O4+x,LwgAAA==^#~@%>
document.all.save1.style.display = "";
<%#@~^BgAAAA==~VdP6QEAAA==^#~@%>
document.all.save2.style.display = "";
<%#@~^CAAAAA==~x[,k6PZgIAAA==^#~@%>

parent.frames.Search.setNNFunc('add');
document.all.tabing.style.display = "";
document.all.returnfromsave.style.display = "";
</script>
<%#@~^FQAAAA==@#@&mCk+,JEaNmO+r@#@&sQQAAA==^#~@%>
<script Language="JavaScript">
parent.frames.Search.openSearchScreen();
parent.frames.Search.showSearchHelp()
document.all.tabing.style.display = "";
document.all.returntonav.style.display = "";
document.all.update.style.display = "";
parent.frames.Search.setNNFunc('update');
</script>
<%#@~^FQAAAA==@#@&mCk+,JNVO+r@#@&oQQAAA==^#~@%>
<script Language="JavaScript">
parent.frames.Search.openSearchScreen();
parent.frames.Search.showSearchHelp()
document.all.tabing.style.display = "";
document.all.returntonav.style.display = "";
document.all.remove.style.display = "";
parent.frames.Search.setNNFunc('delete');
</script>
<%#@~^FgAAAA==@#@&mCk+,J/!:sCDHJ@#@&PAUAAA==^#~@%>
<script Language="JavaScript">
document.all.returntonav.style.display = "";
document.all.summary.style.display = "";
</script>
<%#@~^FgAAAA==@#@&mCk+,JNYmrVkJ@#@&FAUAAA==^#~@%>
<script Language="JavaScript">
parent.frames.Search.openSearchScreen();
parent.frames.Search.showSearchHelp()
document.all.returntonav.style.display = "";
document.all.eatingplan.style.display = "";
document.all.scrolling.style.display = "";
</script>
<%#@~^HAAAAA==@#@&+U9Pk+VmD@#@&x[Pb0@#@&QgYAAA==^#~@%>
</body>
</html>