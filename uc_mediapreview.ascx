﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_mediapreview.ascx.cs" Inherits="uc_mediapreview" %>

<div id="container_media">

<%=design.buildHeading("GALLERIES", 20, "", "", true)%>

	<div class="tabmedia_container">
		<div id="tab_media_1" class="tabmedia_active" onmouseover="switchTab('tab_media_',this.id,'media_','media_1','2')">
			<div id="top">VIDEOS</div>
		</div>
		<div class="tabmedia_spacer"></div>
		<div id="tab_media_2" class="tabmedia_inactive" onmouseover="switchTab('tab_media_',this.id,'media_','media_2','2')">
			<div id="top">PHOTOS</div>
		</div>
		<div class="tabmedia_spacer"></div>
	</div>

	<div id="media_1" style="display:block; background-color:#061800; width:100%; float:left;">

		<div class="media_nav"><img id="vidNav_back" src="External/Graphics/media_arrow_left.png" border="0" style="position:absolute; left:5px; top:55px; cursor:pointer; visibility:hidden;" onclick="pageVideo(-1,<%=vidPgCnt %>)" /></div>

        <asp:Literal ID="lit_videos" runat="server"></asp:Literal>

        <asp:Repeater ID="rep_videos" runat="server" OnItemDataBound="rep_videos_OnItemDataBound"></asp:Repeater>

		<div class="media_nav"><img id="vidNav_fwd" src="External/Graphics/media_arrow_right.png" border="0" style="position:absolute; left:-16px; top:55px; cursor:pointer;" onclick="pageVideo(1,<%=vidPgCnt %>)" /></div>

		<div class="media_footer txt_white_12"><a href="videos.aspx"><strong>...More videos</strong></a></div>
		
	</div>
	
	<div id="media_2" style="display:none; background-color:#061800; width:100%; float:left;">

		<div class="media_nav"><img id="phoNav_back" src="External/Graphics/media_arrow_left.png" border="0" style="position:absolute; left:5px; top:55px; cursor:pointer; visibility:hidden;" onclick="pagePhoto(-1,<%=phoPgCnt %>)" /></div>

        <asp:Literal ID="lit_photogalleries" runat="server"></asp:Literal>

        <asp:Repeater ID="rep_photogalleries" runat="server" OnItemDataBound="rep_photogalleries_OnItemDataBound"></asp:Repeater>

		<div class="media_nav"><img id="phoNav_fwd" src="External/Graphics/media_arrow_right.png" border="0" style="position:absolute; left:-16px; top:55px; cursor:pointer;" onclick="pagePhoto(1,<%=phoPgCnt %>)" /></div>
		
		<div class="media_footer txt_white_12"><a href="photogalleries.aspx"><strong>...More photos</strong></a></div>
		
	</div>

</div>
