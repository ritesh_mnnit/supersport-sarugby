﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="teams.aspx.cs" Inherits="teams" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

    <%=design.buildHeading("TEAM", 20, "", "", true) %>

    <%=design.makeSpace(10,"",true) %>

    <asp:Panel ID="pan_results" runat="server">

        <%=design.buildHeading("RESULTS", 12, "", "", false)%>
        
        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
        
            <asp:Repeater ID="rep_results" runat="server" OnItemDataBound="rep_results_OnItemDataBound"></asp:Repeater>
        
        </table>
        
        <%=design.makeSpace(10,"",true) %>

    </asp:Panel>

    <asp:Panel ID="pan_fixtures" runat="server">
    
        <%=design.buildHeading("FIXTURES", 12, "", "", false) %>

        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
        
            <asp:Repeater ID="rep_fixtures" runat="server" OnItemDataBound="rep_fixtures_OnItemDataBound"></asp:Repeater>

        </table>
        
        <%=design.makeSpace(10,"",true) %>

    </asp:Panel>

    <asp:Panel ID="pan_squad" runat="server">

        <%=design.buildHeading("SQUAD", 12, "", "", true)%>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
        
            <asp:Literal ID="lit_squad" runat="server"></asp:Literal>
        
        </table>
        
    </asp:Panel>

</asp:Content>