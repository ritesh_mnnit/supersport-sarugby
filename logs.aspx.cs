﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class logs : System.Web.UI.Page
{
    public String reqCategory = "";

    public int leagueID = 0;

    public String leagueName = "";

    private String thisURL = "";

    public int pcurr;

    public int rowCnt = 0;

    private logItem_list logList = new logItem_list();
    private List<logItem> logItemList = new List<logItem>();

    private String curTourLog = "";

    private String AUConfTeam = "";
    private String NZConfTeam = "";
    private String SAConfTeam = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["category"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        if (func.isNumericType(Request.QueryString["pcurr"], "int"))
        {
            pcurr = Convert.ToInt32(Request.QueryString["pcurr"]);
        }
        else
        {
            pcurr = 1;
        }

        thisURL = func.GetCurrentPageName(Request.Url.Query, true);

        if (!IsPostBack)
        {
            btn_prev.Enabled = true;
            btn_next.Enabled = true;

            logItemList = logList.getLogItemList(500, reqCategory, leagueID);

            if(leagueID == 802)
            {
                EP_DISCLAIMER.Visible = true;
            }

            if (logItemList != null)
            {
                PagedDataSource tmpSource = new PagedDataSource();

                tmpSource.DataSource = logItemList;

                tmpSource.AllowPaging = true;

                tmpSource.PageSize = 1000;

                if (pcurr < 1) { pcurr = 1; }

                if (pcurr > tmpSource.PageCount) { pcurr = tmpSource.PageCount; }

                f_page.Text = pcurr.ToString();

                tmpSource.CurrentPageIndex = pcurr - 1;

                if (tmpSource.IsFirstPage) { btn_prev.Enabled = false; }

                if (tmpSource.IsLastPage) { btn_next.Enabled = false; }

                lit_totalpages.Text = tmpSource.PageCount.ToString();

                rep_logs.DataSource = tmpSource;
                rep_logs.DataBind();
            }
            else
            {
                pan_page.Enabled = false;
            }
        }
    }

    protected void rep_logs_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            logItem item = (logItem)e.Item.DataItem;

            leagueName = item.leaguename;

            HtmlGenericControl wrapLog = new HtmlGenericControl("div");

            String team = "";

            if (item.teamsn == "")
            {
                team = item.team;
            }
            else
            {
                team = item.teamsn;
            }

            StringBuilder outStr = new StringBuilder();

            if (leagueID == 795) // SUPER RUGBY TOURNAMENT
            {
                String logColor = "";

                if (curTourLog != item.group)
                {
                    rowCnt = 0;
                    logColor = "#FFFFFF";
                    curTourLog = item.group;

                    outStr.Append("<td colspan=\"12\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\"><strong>" + item.group + "</strong></td>");
                }

                rowCnt++;

                logColor = "#FFFFFF";

                if (item.group != "Combined Log")
                {
                    if (rowCnt == 1)
                    {
                        if (item.group == "Australian Conference")
                        {
                            logColor = "#E1BD5E";
                            AUConfTeam = team;
                        }

                        if (item.group == "New Zealand Conference")
                        {
                            logColor = "#000000";
                            NZConfTeam = team;
                        }

                        if (item.group == "South African Conference")
                        {
                            logColor = "#006600";
                            SAConfTeam = team;
                        }
                    }
                }
                else
                {
                    if (rowCnt > 3 && rowCnt < 7)
                    {
                        logColor = "#999999";
                    }
                    else
                    {
                        if (team == AUConfTeam)
                        {
                            logColor = "#E1BD5E";
                        }

                        if (team == NZConfTeam)
                        {
                            logColor = "#000000";
                        }

                        if (team == SAConfTeam)
                        {
                            logColor = "#006600";
                        }
                    }
                }
                
                outStr.Append("<tr class=\"txt_grey_12\">");
                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><div style=\"float:left; width:10px; height:10px; background-color:"+ logColor +"; margin-right:5px;\"></div><div style=\"float:left;\">" + team + "&nbsp;</div></td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.played + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.won + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.lost + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.drew + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.pointsfor + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.pointsagainst + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesfor + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesagainst + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.lossbonus + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesbonus + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.logpoints + "</td>");
                outStr.Append("</tr>");
            }
            else
            {
                if (curTourLog != item.group)
                {
                    curTourLog = item.group;
                    outStr.Append("<td colspan=\"12\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\"><strong>" + item.group + "</strong></td>");
                }
                
                outStr.Append("<tr class=\"txt_grey_12\">");
                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + team + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.played + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.won + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.lost + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.drew + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.pointsfor + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.pointsagainst + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesfor + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesagainst + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.lossbonus + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesbonus + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.logpoints + "</td>");
                outStr.Append("</tr>");
            }

            wrapLog.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapLog);
        }
    }

    //protected void rep_extra_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
    //    {
    //        logItem item = (logItem)e.Item.DataItem;

    //        HtmlGenericControl wrapLog = new HtmlGenericControl("div");

    //        String team = "";

    //        if (item.teamsn == "")
    //        {
    //            team = item.team;
    //        }
    //        else
    //        {
    //            team = item.teamsn;
    //        }

    //        StringBuilder outStr = new StringBuilder();

    //        if (leagueID == 795) // SUPER RUGBY TOURNAMENT
    //        {
    //            String logColor = "";

    //            if (team == AUConfTeam)
    //            {
    //                logColor = "#E1BD5E";
    //            }

    //            if (team == NZConfTeam)
    //            {
    //                logColor = "#000000";
    //            }

    //            if (team == SAConfTeam)
    //            {
    //                logColor = "#006600";
    //            }
                
    //            outStr.Append("<tr class=\"txt_grey_12\">");
    //            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><div style=\"float:left; width:10px; height:10px; background-color:" + logColor + "; margin-right:5px;\"></div><div style=\"float:left;\">" + team + "&nbsp;</div></td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.played + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.won + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.lost + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.drew + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.pointsfor + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.pointsagainst + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesfor + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesagainst + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.lossbonus + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesbonus + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.logpoints + "</td>");
    //            outStr.Append("</tr>");
    //        }
    //        else
    //        {
    //            outStr.Append("<tr class=\"txt_grey_12\">");
    //            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + team + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.played + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.won + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.lost + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.drew + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.pointsfor + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.pointsagainst + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesfor + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesagainst + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.lossbonus + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.triesbonus + "</td>");
    //            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.logpoints + "</td>");
    //            outStr.Append("</tr>");
    //        }

    //        wrapLog.InnerHtml = outStr.ToString();

    //        e.Item.Controls.Add(wrapLog);
    //    }
    //}

    protected void btn_ok_Click(object sender, EventArgs e)
    {
        Response.Redirect("logs.aspx?pcurr=" + f_page.Text);
    }

    protected void btn_prev_Click(object sender, EventArgs e)
    {
        Response.Redirect("logs.aspx?pcurr=" + (pcurr - 1).ToString());
    }

    protected void btn_next_Click(object sender, EventArgs e)
    {
        Response.Redirect("logs.aspx?pcurr=" + (pcurr + 1).ToString());
    }
}