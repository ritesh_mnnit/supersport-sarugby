﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class profilesearch : System.Web.UI.Page
{
    public String reqCategory = "";

    public int leagueID = 0;

    private String thisURL = "";

    public String search = "";

    private profileItem_list profileList = new profileItem_list();
    private List<profileItem> profileItemList = new List<profileItem>();

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["category"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        search = Request.QueryString["search"];

        if (!Page.IsPostBack)
        {
            f_search.Text = search;

            if((search !="") && (search != null))
            {
                buildResults();
            }
        }
    }

    protected void buildResults()
    {
        profileItemList = profileList.getProfileItemList(search.Replace("'", "''"));

        StringBuilder outStr = new StringBuilder();

        if (profileItemList != null)
        {
            int playerCount = 0;

            outStr.Append("<tr class=\"txt_black_12\">");
            outStr.Append("    <td width=\"33%\" align=\"left\" valign=\"top\">");

            foreach (profileItem item in profileItemList)
            {
                String displayName = "";

                if(item.nickname == "")
                {
                    displayName = item.name + " " + item.surname;
                }
                else
                {
                    displayName = item.name + " (" + item.nickname + ") " + item.surname;
                }

                outStr.Append("        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-bottom:5px; border-bottom:#CCC dotted 1px;\">");
                outStr.Append("            <tr class=\"txt_black_12\">");
                outStr.Append("                <td align=\"left\" valign=\"top\"><a href=\"playerprofile.aspx?id=" + item.id + "&category=" + reqCategory + "&leagueid=" + leagueID + "\">" + displayName + "</a></td>");
                outStr.Append("            </tr>");
                outStr.Append("        </table>");

                playerCount++;

                //if (playerCount % 25 == 0)
                //{
                //    outStr.Append("    </td>");
                //    outStr.Append("    <td width=\"33%\" align=\"left\" valign=\"top\">");
                //}
            }

            outStr.Append("    </td>");
            outStr.Append("</tr>");
        }
        else
        {
            outStr.Append("Sorry, no profiles match your search. Please try again.");
        }

        lit_results.Text = outStr.ToString();
    }

    protected void searchClick(object sender, EventArgs e)
    {
        Response.Redirect("profilesearch.aspx?search=" + f_search.Text + "&category="+ reqCategory +"&leagueid=" + leagueID);
    }
}