﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class capItem
{
    public int personID { get; set; }
    public String player { get; set; }
    public int caps { get; set; }

    public capItem()
    {
    }
}

public class capItem_list
{
    public capItem_list()
    {
    }

    public List<capItem> getCapItemList(int numRecords, String category, int leagueID)
    {
        func func = new func();

        func.setDbConn(1);

        List<capItem> capItemList = new List<capItem>();

        capItemList = (List<capItem>)cache_provider.getObject(cache_keys.CAPS_KEY + leagueID + "_" + numRecords);

        if (capItemList == null)
        {
            capItemList = new List<capItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT TOP " + numRecords + " ");
            outStr.Append("tPLA.Combination As vPLAYER, ");
            outStr.Append("tPLA.TestCaps as vCAPS, ");
            outStr.Append("tPER.ID As vID ");
            outStr.Append("FROM SSZGeneral.dbo.rugbystats_players tPLA ");
            outStr.Append("left join rugby.dbo.persons tPER on (tPER.Surname COLLATE SQL_Latin1_General_CP1_CI_AS = tPLA.surname and tPER.BirthDate = tPLA.BirthDate) ");
            outStr.Append("ORDER BY tPLA.TestCaps DESC");

            //func.stop(outStr.ToString());

            String query = outStr.ToString();

            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    capItem item = new capItem();

                    if (func.isNumericType(func.dbtbl["vID"].ToString(), "int"))
                    {
                        item.personID = Convert.ToInt32(func.dbtbl["vID"]);
                    }
                    
                    item.player = func.dbtbl["vPLAYER"].ToString();
                    item.caps = Convert.ToInt32(func.dbtbl["vCAPS"]);

                    capItemList.Add(item);
                }
            }
            else
            {
                capItemList = null;
            }

            func.dbtbl.Close();
            
            cache_provider.addObject(capItemList, cache_keys.CAPS_KEY + leagueID + "_" + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return capItemList;
    }
}