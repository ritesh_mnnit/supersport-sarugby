﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public class contentItem
{
    public String headline;
    public String body;

	public contentItem(int cid)
	{
        func func = new func();
        func.setDbConn(1);
        StringBuilder outStr = new StringBuilder();
        
        outStr.Append("select Headline as vHEADING, Content as vBODY from supersportzone.dbo.zonecontent where (Id = " + cid + ")");

        
        func.sqlQuery.CommandText = outStr.ToString();
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            while (func.dbtbl.Read())
            {
                headline = func.dbtbl["vHEADING"].ToString();
                body = func.dbtbl["vBODY"].ToString();
            }
        }

        func.dbtbl.Close();
        func.setDbConn(0);
	}
}

public class SitecontentItem
{
    public String body;

    public SitecontentItem(int sid, String type)
    {
        func func = new func();
        func.setDbConn(1);

        StringBuilder outStr = new StringBuilder();

        outStr.Append("SELECT Content as vBODY FROM supersportzone.dbo.ZoneSiteContent WHERE (Site = " + sid + ") AND (Type = '" + type + "')");

        
        func.sqlQuery.CommandText = outStr.ToString();
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            while (func.dbtbl.Read())
            {
                body = func.dbtbl["vBODY"].ToString();
            }
        }

        func.dbtbl.Close();
        func.setDbConn(0);
    }
}