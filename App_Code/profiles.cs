﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public class profileshort
{
    public int id { get; set; }

    public String fullname { get; set; }
    public String image { get; set; }
    public DateTime dob { get; set; }
    public String position { get; set; }
    public String province { get; set; }
    public String height { get; set; }
    public String weight { get; set; }
    public int tests { get; set; }
    public int testtries { get; set; }

    public profileshort(int IDPlayer)
	{
        func func = new func();
        func.setDbConn(1);

        StringBuilder outStr;
        outStr = new StringBuilder();

        if (IDPlayer == -1) // if recID contains -1 then generate a random profile
        {
            outStr.Append("select top 1 ");
            outStr.Append("tPER.ID As vID, ");
            outStr.Append("(select case when tPER.DisplayName <> '' then tPER.DisplayName + ' ' + tPER.Surname ");
            outStr.Append("when tPER.NickName <> '' then tPER.NickName + ' ' + tPER.Surname ");
            outStr.Append("when tPER.FullName <> '' then tPER.FullName + ' ' + tPER.Surname ");
            outStr.Append("else tPER.FullName + ' ' + tPER.Surname end) As vFULLNAME, ");
            outStr.Append("tPER.LargeImage as vIMAGE, ");
            outStr.Append("tPER.BirthDate as vBIRTHDATE, ");
            outStr.Append("tPER.position as vPOSITION, ");
            outStr.Append("tPER.CurrentProvince as vPROVINCE, ");
            outStr.Append("tPER.Height as vHEIGHT, ");
            outStr.Append("tPER.Weight as vWEIGHT ");

            outStr.Append("from Rugby.dbo.squads tSQU ");
            outStr.Append("inner join Rugby.dbo.Persons tPER ON tPER.Id = tSQU.PersonId ");

            outStr.Append("where tSQU.teamId in ");
            
            outStr.Append("(select top 1 tTEA.id from Rugby.dbo.Leagues tLEA ");

            outStr.Append("inner join Rugby.dbo.Tournaments tTOU on tTOU.id = tLEA.TournamentId ");
            outStr.Append("inner join Rugby.dbo.TeamsByLeague tTEA on tTEA.LeagueID = tLEA.ID ");
            outStr.Append("where tLEA.enddate > (GetDate() - 200) ");
            outStr.Append("and tTEA.name like 'South Africa' ");

            //outStr.Append("and tTOU.CategoryID <> 3 ");

            outStr.Append("and tTOU.CategoryID <> 3 ");
            outStr.Append("and tTOU.CategoryID <> 4 ");
            outStr.Append("and tTOU.CategoryID <> 5 ");
            outStr.Append("and tTOU.CategoryID <> 6 ");
            outStr.Append("and tTOU.CategoryID <> 7 ");
            outStr.Append("and tTOU.CategoryID <> 22 ");
            outStr.Append("and tTOU.CategoryID <> 23 ");
            outStr.Append("and tTOU.CategoryID <> 24 ");
            outStr.Append("and tTOU.CategoryID <> 25 ");
            outStr.Append("order by tLEA.startdate, tLEA.enddate) ");
            outStr.Append("and tPER.RoleId = 1 ");
            outStr.Append("order by NEWID(), tPER.Surname, tPER.DisplayName, tPER.NickName, tPER.FullName ");

            //outStr.Append("and tPER.Id = 18977 ");
            //outStr.Append("order by tPER.Surname, tPER.DisplayName, tPER.NickName, tPER.FullName ");

            //func.stop(outStr.ToString());
            
        }
        else
        {
            outStr.Append("SELECT * From Rugby.dbo.Persons WHERE (Id = " + IDPlayer + ")");
        }
        
        int playerID = -1;
        
       // func.stop(outStr.ToString());
        
        func.sqlQuery.CommandText = outStr.ToString();
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            while (func.dbtbl.Read())
            {
                playerID = Convert.ToInt32(func.dbtbl["vID"]);
                
                id = Convert.ToInt32(func.dbtbl["vID"]);

                fullname = func.dbtbl["vFULLNAME"].ToString();
                image = func.dbtbl["vIMAGE"].ToString();
                dob = Convert.ToDateTime(func.dbtbl["vBIRTHDATE"]);
                position = func.dbtbl["vPOSITION"].ToString();
                province = func.dbtbl["vPROVINCE"].ToString();
                height = func.dbtbl["vHEIGHT"].ToString();
                weight = func.dbtbl["vWEIGHT"].ToString();
            }
        }


        func.dbtbl.Close();
        //func.setDbConn(0);

        outStr = new StringBuilder();
        outStr.Append("select TestCaps as Caps FROM SSZGeneral.dbo.rugbystats_players where combination='"+ fullname +"'");
        //func.setDbConn(1);
        func.sqlQuery.CommandText = outStr.ToString();
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            while (func.dbtbl.Read())
            {
                tests = Convert.ToInt32(func.dbtbl["Caps"]);
            }
        }

        func.dbtbl.Close();
        //func.setDbConn(0);

        outStr = new StringBuilder();

        outStr.Append("Select Count(Case WHEN tCOM.EventId = 2 then 1 end) AS Tries, ");
        outStr.Append("Count(Case WHEN tCOM.EventId = 3 then 1 end) AS Conversions, ");
        outStr.Append("Count(Case WHEN tCOM.EventId = 4 then 1 end) AS Penalties, ");
        outStr.Append("Count(Case WHEN tCOM.EventId = 5 then 1 end) AS DropGoals ");
        outStr.Append("From Rugby.dbo.Commentary tCOM ");

        outStr.Append("INNER JOIN Rugby.dbo.Matches tMAT ON tMAT.Id = tCOM.MatchId ");
        outStr.Append("inner join rugby.dbo.leagues tLEA on tLEA.id = tMAT.leagueid ");
        outStr.Append("inner join rugby.dbo.tournaments tTOU on tTOU.id = tLEA.tournamentid ");
        outStr.Append("inner join rugby.dbo.teamsbyleague tTEA on tTEA.id = tMAT.teamaid ");
        outStr.Append("inner join rugby.dbo.teamsbyleague tTEB on tTEB.id = tMAT.teambid ");
        outStr.Append("Where (tCOM.Player1Id = "+ playerID +") AND (tCOM.EventId > 1) ");

        outStr.Append("and tTOU.CategoryID <> 3 ");
        outStr.Append("and tTOU.CategoryID <> 4 ");
        outStr.Append("and tTOU.CategoryID <> 5 ");
        outStr.Append("and tTOU.CategoryID <> 6 ");
        outStr.Append("and tTOU.CategoryID <> 7 ");
        outStr.Append("and tTOU.CategoryID <> 22 ");
        outStr.Append("and tTOU.CategoryID <> 23 ");
        outStr.Append("and tTOU.CategoryID <> 24 ");
        outStr.Append("and tTOU.CategoryID <> 25 ");

        outStr.Append("and (tTEA.name='South Africa' or tTEB.name='South Africa')");

        outStr.Append("Group By tCOM.Player1Id");

        
        func.sqlQuery.CommandText = outStr.ToString();
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            while (func.dbtbl.Read())
            {
                testtries = Convert.ToInt32(func.dbtbl["Tries"]);
            }
        }

        func.dbtbl.Close();
        func.setDbConn(0);
	}
}








            


