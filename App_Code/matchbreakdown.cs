﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class summary
{
    public int count = 0;
    public String teamA;
    public String teamB;
    public String venue;
    public DateTime date;
    public String competition;
    public String finalscore;
    public String tries;
    public String halftimescore;
    public String manofthematch;
    public String conditions;
    public String crowd;

    public summary(int id)
    {
        func func = new func();
        func.setDbConn(1);

        StringBuilder outStr = new StringBuilder();
        String query = "";

        outStr.Append("SELECT top 1 a.HomeTeam as vHOME, ");
        outStr.Append("a.AwayTeam as vAWAY, ");
        outStr.Append("a.Venue as vVENUE, ");
        outStr.Append("a.MatchDateTime as vDATE, ");
        outStr.Append("a.HomeTeamScore as vHOMESCORE, ");
        outStr.Append("a.AwayTeamScore as vAWAYSCORE, ");
        outStr.Append("d.TriesFor AS vHOMETRIES, ");
        outStr.Append("d.TriesAgainst AS vAWAYTRIES, ");
        outStr.Append("b.HalfTime as vHALFTIME, ");
        outStr.Append("b.Completed as vCOMPLETED, ");
        outStr.Append("b.mom as vMOM, ");
        outStr.Append("b.Crowd vCROWD, ");
        outStr.Append("b.Conditions as vCONDITIONS, ");
        outStr.Append("b.TeamAHalfTimeScore as vHOMEHALFSCORE, ");
        outStr.Append("b.TeamBHalfTimeScore as vAWAYHALFSCORE, ");
        outStr.Append("c.Name AS vLEAGUE ");
        outStr.Append("FROM Rugby.dbo.MatchBreakDown a ");
        outStr.Append("INNER JOIN Rugby.dbo.Matches b ON b.ID = a.MatchId ");
        outStr.Append("INNER JOIN Rugby.dbo.Leagues c ON c.ID = a.LeagueId ");
        outStr.Append("INNER JOIN Rugby.dbo.LogDetailsPerMatch d ON d.MatchId = a.MatchId ");
        outStr.Append("WHERE (a.MatchId = "+ id +") ");

        query = outStr.ToString();

        func.sqlQuery.CommandText = query;
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            count++;
            while (func.dbtbl.Read())
            {
                teamA = func.dbtbl["vHOME"].ToString();
                teamB = func.dbtbl["vAWAY"].ToString();
                venue = func.dbtbl["vVENUE"].ToString();
                date = Convert.ToDateTime(func.dbtbl["vDATE"]);
                competition = func.dbtbl["vLEAGUE"].ToString();
                finalscore = func.dbtbl["vHOMESCORE"].ToString() + " - " + func.dbtbl["vAWAYSCORE"].ToString();
                tries = func.dbtbl["vHOMETRIES"].ToString() + " - " + func.dbtbl["vAWAYTRIES"].ToString();
                halftimescore = func.dbtbl["vHOMEHALFSCORE"].ToString() + " - " + func.dbtbl["vAWAYHALFSCORE"].ToString();
                manofthematch = func.dbtbl["vMOM"].ToString();
                conditions = func.dbtbl["vCONDITIONS"].ToString();
                crowd = func.dbtbl["vCROWD"].ToString();
            }
        }

        func.dbtbl.Close();
        func.setDbConn(0);
    }
}

public class officialsItem
{
    public String name;
    public String surname;
    public String role;

    public officialsItem()
    {
    }
}

public class officialsItem_list
{
    public officialsItem_list()
    {
    }

    public List<officialsItem> getOfficialsItemList(int matchID)
    {
        func func = new func();
        func.setDbConn(1);

        List<officialsItem> officialsItemList = null; // new List<officialsItem>();

        officialsItemList = (List<officialsItem>)cache_provider.getObject(cache_keys.MATCHOFFICIALS_KEY + matchID);

        if (officialsItemList == null)
        {
            officialsItemList = new List<officialsItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT (Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, ");
            outStr.Append("b.Surname as Surname, ");
            outStr.Append("c.Name As Role ");
            outStr.Append("From Rugby.dbo.Officials a ");
            outStr.Append("INNER JOIN Rugby.dbo.Persons b ON b.Id = a.PersonId ");
            outStr.Append("INNER JOIN Rugby.dbo.Roles c ON c.Id = a.RoleId ");
            outStr.Append("WHERE (a.MatchId = " + matchID + ") ");
            outStr.Append("ORDER BY c.Ordering, c.Id");

            String query = outStr.ToString();

            //func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    officialsItem item = new officialsItem();

                    item.name = func.dbtbl["FullName"].ToString();
                    item.surname = func.dbtbl["Surname"].ToString();
                    item.role = func.dbtbl["Role"].ToString();

                    officialsItemList.Add(item);
                }
            }
            else
            {
                officialsItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(officialsItemList, cache_keys.MATCHOFFICIALS_KEY + matchID, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return officialsItemList;
    }
}

public class team_A_scoringItem
{
    public String eventtype { get; set; }
    public String name { get; set; }
    public String surname { get; set; }
    public String count { get; set; }

    public team_A_scoringItem()
    {
    }
}

public class team_A_scoringItem_list
{
    public team_A_scoringItem_list()
    {
    }

    public List<team_A_scoringItem> getTeamAScoringItemList(int matchID, int teamID)
    {
        func func = new func();
        func.setDbConn(1);

        List<team_A_scoringItem> team_A_scoringItemList = null; // new List<team_A_scoringItem>();

        team_A_scoringItemList = (List<team_A_scoringItem>)cache_provider.getObject(cache_keys.TEAMSCORINGA_KEY + matchID + "_" + teamID);

        if (team_A_scoringItemList == null)
        {
            team_A_scoringItemList = new List<team_A_scoringItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("Select Distinct a.Player1Id, ");
            outStr.Append("(Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, ");
            outStr.Append("b.Surname as Surname, ");
            outStr.Append("c.Name As Description, ");
            outStr.Append("(Select Count(EventId) ");
            outStr.Append("From Rugby.dbo.Commentary ");
            outStr.Append("Where (EventId = a.EventId) And (MatchId = " + matchID + ") AND (Player1Id = a.Player1Id) AND (TeamId = a.TeamId)) As Total ");
            outStr.Append("From Rugby.dbo.Commentary a ");
            outStr.Append("INNER JOIN Rugby.dbo.Persons b ON b.Id = a.Player1Id ");
            outStr.Append("INNER JOIN Rugby.dbo.Events c ON c.Id = a.EventId ");
            outStr.Append("Where (a.MatchId = " + matchID + ") AND (a.TeamId = " + teamID + ") AND (a.EventId > 1) AND (a.EventId < 7) ");
            outStr.Append("Order By c.Name, FullName, b.Surname");

            String query = outStr.ToString();

            //func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    team_A_scoringItem item = new team_A_scoringItem();

                    item.name = func.dbtbl["FullName"] + " " + func.dbtbl["Surname"];
                    item.eventtype = func.dbtbl["Description"].ToString();
                    item.count = func.dbtbl["Total"].ToString();

                    team_A_scoringItemList.Add(item);
                }
            }
            else
            {
                team_A_scoringItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(team_A_scoringItemList, cache_keys.TEAMSCORINGA_KEY + matchID + "_" + teamID, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return team_A_scoringItemList;
    }
}

public class team_B_scoringItem
{
    public String eventtype { get; set; }
    public String name { get; set; }
    public String surname { get; set; }
    public String count { get; set; }

    public team_B_scoringItem()
    {
    }
}

public class team_B_scoringItem_list
{
    public team_B_scoringItem_list()
    {
    }

    public List<team_B_scoringItem> getTeamBScoringItemList(int matchID, int teamID)
    {
        func func = new func();
        func.setDbConn(1);

        List<team_B_scoringItem> team_B_scoringItemList = null; // new List<team_B_scoringItem>();

        team_B_scoringItemList = (List<team_B_scoringItem>)cache_provider.getObject(cache_keys.TEAMSCORINGB_KEY + matchID + "_" + teamID);

        if (team_B_scoringItemList == null)
        {
            team_B_scoringItemList = new List<team_B_scoringItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("Select Distinct a.Player1Id, ");
            outStr.Append("(Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, ");
            outStr.Append("b.Surname as Surname, ");
            outStr.Append("c.Name As Description, ");
            outStr.Append("(Select Count(EventId) ");
            outStr.Append("From Rugby.dbo.Commentary ");
            outStr.Append("Where (EventId = a.EventId) And (MatchId = " + matchID + ") AND (Player1Id = a.Player1Id) AND (TeamId = a.TeamId)) As Total ");
            outStr.Append("From Rugby.dbo.Commentary a ");
            outStr.Append("INNER JOIN Rugby.dbo.Persons b ON b.Id = a.Player1Id ");
            outStr.Append("INNER JOIN Rugby.dbo.Events c ON c.Id = a.EventId ");
            outStr.Append("Where (a.MatchId = " + matchID + ") AND (a.TeamId = " + teamID + ") AND (a.EventId > 1) AND (a.EventId < 7) ");
            outStr.Append("Order By c.Name, FullName, b.Surname");

            String query = outStr.ToString();

            //func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    team_B_scoringItem item = new team_B_scoringItem();

                    item.name = func.dbtbl["FullName"] + " " + func.dbtbl["Surname"];
                    item.eventtype = func.dbtbl["Description"].ToString();
                    item.count = func.dbtbl["Total"].ToString();

                    team_B_scoringItemList.Add(item);
                }
            }
            else
            {
                team_B_scoringItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(team_B_scoringItemList, cache_keys.TEAMSCORINGB_KEY + matchID + "_" + teamID, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return team_B_scoringItemList;
    }
}

public class lineupItem
{
    public String number { get; set; }
    public String name { get; set; }
    public String surname { get; set; }
    public String captain { get; set; }

    public lineupItem()
    {
    }
}

public class lineupItem_list
{
    public lineupItem_list()
    {
    }

    public List<lineupItem> getLineupItemList(int matchID, int teamID, bool isSevens)
    {
        func func = new func();
        func.setDbConn(1);

        int maxNumber = 15;

        if (isSevens)
        {
            maxNumber = 7;
        }

        List<lineupItem> lineupItemList = null; // new List<lineupItem>();

        lineupItemList = (List<lineupItem>)cache_provider.getObject(cache_keys.MATCHLINEUP_KEY + matchID + "_" + teamID + "_" + isSevens);

        if (lineupItemList == null)
        {
            lineupItemList = new List<lineupItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT a.Number as Number, ");
            outStr.Append("(Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, ");
            outStr.Append("b.Surname, ");
            outStr.Append("(Case When a.Captain = 1 Then '(Captain)' Else '' End) as Captain ");
            outStr.Append("From Rugby.dbo.Lineups a ");
            outStr.Append("INNER JOIN Rugby.dbo.Persons b ON b.Id = a.PersonId ");
            outStr.Append("WHERE (a.MatchId = " + matchID + ") AND (a.TeamId = " + teamID + ") AND (a.Number <= " + maxNumber + ") ORDER BY a.Number");

            String query = outStr.ToString();

            //func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    lineupItem item = new lineupItem();

                    item.name = func.dbtbl["FullName"].ToString();
                    item.surname = func.dbtbl["Surname"].ToString();
                    item.number = func.dbtbl["Number"].ToString();
                    item.captain = func.dbtbl["Captain"].ToString();

                    lineupItemList.Add(item);
                }
            }
            else
            {
                lineupItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(lineupItemList, cache_keys.MATCHLINEUP_KEY + matchID + "_" + teamID + "_" + isSevens, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return lineupItemList;
    }
}

public class reserveItem
{
    public String number { get; set; }
    public String name { get; set; }
    public String surname { get; set; }

    public reserveItem()
    {
    }
}

public class reserveItem_list
{
    public reserveItem_list()
    {
    }

    public List<reserveItem> getReserveItemList(int matchID, int teamID, bool isSevens)
    {
        func func = new func();
        func.setDbConn(1);

        int minNumber = 15;

        if (isSevens)
        {
            minNumber = 7;
        }

        List<reserveItem> reserveItemList = null; // new List<reserveItem>();

        reserveItemList = (List<reserveItem>)cache_provider.getObject(cache_keys.MATCHRESERVES_KEY + matchID + "_" + teamID + "_" + isSevens);

        if (reserveItemList == null)
        {
            reserveItemList = new List<reserveItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT a.Number as Number, ");
            outStr.Append("(Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName, ");
            outStr.Append("b.Surname, ");
            outStr.Append("a.Captain as Captain ");
            outStr.Append("From Rugby.dbo.Lineups a ");
            outStr.Append("INNER JOIN Rugby.dbo.Persons b ON b.Id = a.PersonId ");
            outStr.Append("WHERE (a.MatchId = " + matchID + ") AND (a.TeamId = " + teamID + ") AND (a.Number > " + minNumber + ") ORDER BY a.Number");

            String query = outStr.ToString();

            //func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();


            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    reserveItem item = new reserveItem();

                    item.name = func.dbtbl["FullName"].ToString();
                    item.surname = func.dbtbl["Surname"].ToString();
                    item.number = func.dbtbl["Number"].ToString();

                    reserveItemList.Add(item);
                }
            }
            else
            {
                reserveItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(reserveItemList, cache_keys.MATCHRESERVES_KEY + matchID + "_" + teamID + "_" + isSevens, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return reserveItemList;
    }
}

public class replacementItem
{
    public String minute { get; set; }
    public String nameA { get; set; }
    public String surnameA { get; set; }
    public String nameB { get; set; }
    public String surnameB { get; set; }

    public replacementItem()
    {
    }
}

public class replacementItem_list
{
    public replacementItem_list()
    {
    }

    public List<replacementItem> getReplacementItemList(int matchID, int teamID)
    {
        func func = new func();
        func.setDbConn(1);
        List<replacementItem> replacementItemList = null; //new List<replacementItem>();

        replacementItemList = (List<replacementItem>)cache_provider.getObject(cache_keys.MATCHREPLACEMENTS_KEY + matchID + "_" + teamID);

        if (replacementItemList == null)
        {
            replacementItemList = new List<replacementItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT a.MatchTime as vMinute, ");

            outStr.Append("(Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName1, ");
            outStr.Append("b.Surname AS Surname1, ");

            outStr.Append("(Case When c.DisplayName <> '' Then c.DisplayName Else c.FullName End) As FullName2, ");
            outStr.Append("c.Surname AS Surname2 ");

            outStr.Append("From Rugby.dbo.Commentary a ");

            outStr.Append("INNER JOIN Rugby.dbo.Persons b ON b.Id = a.Player1Id ");
            outStr.Append("INNER JOIN Rugby.dbo.Persons c ON c.Id = a.Player2Id ");
            outStr.Append("WHERE (a.EventId > 11) AND (a.TeamId = " + teamID + ") AND (a.MatchId = " + matchID + ") AND (c.Id <> 1) ");
            outStr.Append("ORDER BY a.MatchTime");

            String query = outStr.ToString();

            //func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    replacementItem item = new replacementItem();

                    item.minute = func.dbtbl["vMinute"].ToString();
                    item.nameA = func.dbtbl["FullName1"].ToString();
                    item.surnameA = func.dbtbl["Surname1"].ToString();
                    item.nameB = func.dbtbl["FullName2"].ToString();
                    item.surnameB = func.dbtbl["Surname2"].ToString();

                    replacementItemList.Add(item);
                }
            }
            else
            {
                replacementItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(replacementItemList, cache_keys.MATCHREPLACEMENTS_KEY + matchID + "_" + teamID, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return replacementItemList;
    }
}

public class runofplayItem
{
    public String minute { get; set; }
    public int eventid { get; set; }
    public String eventtype { get; set; }
    public String nameA { get; set; }
    public String surnameA { get; set; }
    public String nameB { get; set; }
    public String surnameB { get; set; }
    public String team { get; set; }

    public runofplayItem()
    {
    }
}

public class runofplayItem_list
{
    public runofplayItem_list()
    {
    }

    public List<runofplayItem> getRunofplayItemList(int matchID)
    {
        func func = new func();
        func.setDbConn(1);
        List<runofplayItem> runofplayItemList = null; //new List<runofplayItem>();

        runofplayItemList = (List<runofplayItem>)cache_provider.getObject(cache_keys.MATCHRUNOFPLAY_KEY + matchID);

        if (runofplayItemList == null)
        {
            runofplayItemList = new List<runofplayItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT ");
            outStr.Append("a.MatchTime as vMinute, ");
            outStr.Append("a.EventId as vEventId, ");
            outStr.Append("(Case When b.DisplayName <> '' Then b.DisplayName Else b.FullName End) As FullName1, ");
            outStr.Append("b.Surname As Surname1, ");
            outStr.Append("(Case When c.DisplayName <> '' Then c.DisplayName Else c.FullName End) As FullName2, ");
            outStr.Append("c.Surname As Surname2, ");
            outStr.Append("d.Name As Description, ");
            outStr.Append("e.Name As Team ");
            outStr.Append("From Rugby.dbo.Commentary a ");
            outStr.Append("INNER JOIN Rugby.dbo.Persons b ON b.Id = a.Player1Id ");
            outStr.Append("INNER JOIN Rugby.dbo.Persons c ON c.Id = a.Player2Id ");
            outStr.Append("INNER JOIN Rugby.dbo.Events d ON d.Id = a.EventId ");
            outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague e ON e.Id = a.TeamId ");
            outStr.Append("WHERE (a.MatchId = " + matchID + ") AND (a.EventId > 1) ");
            outStr.Append("ORDER BY a.MatchTime, a.Id");

            String query = outStr.ToString();

            //func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    runofplayItem item = new runofplayItem();

                    item.eventid = Convert.ToInt32(func.dbtbl["vEventId"]);
                    item.minute = func.dbtbl["vMinute"].ToString();
                    item.nameA = func.dbtbl["FullName1"].ToString();
                    item.surnameA = func.dbtbl["Surname1"].ToString();
                    item.nameB = func.dbtbl["FullName2"].ToString();
                    item.surnameB = func.dbtbl["Surname2"].ToString();
                    item.team = func.dbtbl["Team"].ToString();
                    item.eventtype = func.dbtbl["Description"].ToString();

                    runofplayItemList.Add(item);
                }
            }
            else
            {
                runofplayItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(runofplayItemList, cache_keys.MATCHRUNOFPLAY_KEY + matchID, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return runofplayItemList;
    }
}