﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class profileItem
{
    public int id { get; set; }
    public String name { get; set; }
    public String nickname { get; set; }
    public String surname { get; set; }

    public profileItem()
    { 
    }
}

public class profileItem_list
{
    public profileItem_list()
    {
    }

    public List<profileItem> getProfileItemList(String search)
    {
        func func = new func();
        func.setDbConn(1);
        List<profileItem> profileItemList = new List<profileItem>();

        profileItemList = (List<profileItem>)cache_provider.getObject(cache_keys.PROFILESSEARCH_KEY + search);

        if (profileItemList == null)
        {
            profileItemList = new List<profileItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT ");
            outStr.Append("a.ID as vID, ");
            outStr.Append("a.FullName as vNAME, ");
            outStr.Append("a.Surname as vSURNAME, ");
            outStr.Append("a.nickname as vNICKNAME ");
            outStr.Append("From Rugby.dbo.Persons a ");
            outStr.Append("WHERE (a.Fullname + ' ' + a.Surname like '%" + search + "%') ");
            outStr.Append("or (a.NickName + ' ' + a.Surname like '%" + search + "%') ");
            outStr.Append("ORDER BY a.Fullname");

            String query = outStr.ToString();

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    profileItem item = new profileItem();

                    item.id = Convert.ToInt32(func.dbtbl["vID"]);
                    item.name = func.dbtbl["vNAME"].ToString();
                    item.surname = func.dbtbl["vSURNAME"].ToString();
                    item.nickname = func.dbtbl["vNICKNAME"].ToString();

                    profileItemList.Add(item);
                }
            }
            else
            {
                profileItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(profileItemList, cache_keys.PROFILESSEARCH_KEY + search, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return profileItemList;
    }
}