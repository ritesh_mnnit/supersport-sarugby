﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class squadItem
{
    public int id { get; set; }
    public String number { get; set; }
    public String name { get; set; }
    public String surname { get; set; }

    public squadItem()
    { 
    }
}

public class squadItem_list
{
    public squadItem_list()
    {
    }

    public List<squadItem> getSquadItemList(int teamID)
    {
        func func = new func();
        func.setDbConn(1);
        List<squadItem> squadItemList = new List<squadItem>();

        squadItemList = (List<squadItem>)cache_provider.getObject(cache_keys.SQUADS_KEY + teamID);

        if (squadItemList == null)
        {
            squadItemList = new List<squadItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT ");
            outStr.Append("a.PersonId as vID, ");
            outStr.Append("a.SquadNumber as vNUMBER, ");
            outStr.Append("b.FullName as vNAME, ");
            outStr.Append("b.Surname as vSURNAME ");
            outStr.Append("From Rugby.dbo.Squads a ");
            outStr.Append("INNER JOIN Rugby.dbo.Persons b ON b.Id = a.PersonId ");
            outStr.Append("WHERE (a.TeamId = " + teamID + ") and (b.roleId = '1') ");
            outStr.Append("ORDER BY a.SquadNumber, b.Surname");

            String query = outStr.ToString();

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    squadItem item = new squadItem();

                    item.id = Convert.ToInt32(func.dbtbl["vID"]);
                    item.number = func.dbtbl["vNUMBER"].ToString();
                    item.name = func.dbtbl["vNAME"].ToString();
                    item.surname = func.dbtbl["vSURNAME"].ToString();

                    squadItemList.Add(item);
                }
            }
            else
            {
                squadItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(squadItemList, cache_keys.FIXTURES_KEY + teamID, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return squadItemList;
    }
}

public class teamresultItem
{
    public int id { get; set; }
    public DateTime date { get; set; }
    public String home { get; set; }
    public String away { get; set; }
    public String homesn { get; set; }
    public String awaysn { get; set; }
    public int homeid { get; set; }
    public int awayid { get; set; }
    public int homescore { get; set; }
    public int awayscore { get; set; }
    public String venue { get; set; }

    public teamresultItem()
    {
    }
}

public class teamresultItem_list
{
    public teamresultItem_list()
    {
    }

    public List<teamresultItem> getTeamResultItemList(int numRecords, String category, int leagueID, int teamID)
    {
        func func = new func();

        List<teamresultItem> teamresultItemList = null; // new List<teamresultItem>();

        teamresultItemList = (List<teamresultItem>)cache_provider.getObject(cache_keys.TEAMRESULTS_KEY + leagueID + "_" + numRecords);

        if (teamresultItemList == null)
        {
            func.setDbConn(1);
            teamresultItemList = new List<teamresultItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("select top " + numRecords + " ");
            outStr.Append("tMAT.MatchId as vMATCHID, ");
            outStr.Append("tMAT.MatchDateTime as vDATE, ");
            outStr.Append("tMAT.HomeTeam as vHOME, ");
            outStr.Append("tMAT.HomeTeamShortName as vHOMESHORT, ");
            outStr.Append("tMAT.AwayTeam as vAWAY, ");
            outStr.Append("tMAT.AwayTeamShortName as vAWAYSHORT, ");
            outStr.Append("tMAT.HomeTeamScore as vHOMESCORE, ");
            outStr.Append("tMAT.AwayTeamScore as vAWAYSCORE, ");
            outStr.Append("tMAT.Venue as vVENUE, ");
            outStr.Append("tMATC.TeamAID as vHOMEID, ");
            outStr.Append("tMATC.TeamBID as vAWAYID, ");
            outStr.Append("tLEA.Name as vTOURNAMENT ");
            outStr.Append("from Rugby.dbo.MatchBreakDown tMAT ");
            outStr.Append("inner join Rugby.dbo.Leagues tLEA on tLEA.ID = tMAT.LeagueId ");

            outStr.Append("inner join Rugby.dbo.Matches tMATC on tMATC.ID = tMAT.matchid ");

            outStr.Append("where (tMAT.LeagueId = " + leagueID + ") ");
            outStr.Append("and (tMAT.Site = 16) ");
            outStr.Append("and (tMAT.Completed = 1) ");

            outStr.Append("and ((tMATC.TeamAID = '" + teamID + "') or (tMATC.TeamBID = '" + teamID + "')) ");

            outStr.Append("order by tMAT.MatchDateTime desc");

            String query = outStr.ToString();

            //func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    teamresultItem item = new teamresultItem();

                    item.id = Convert.ToInt32(func.dbtbl["vMATCHID"]);
                    item.date = Convert.ToDateTime(func.dbtbl["vDATE"]);
                    item.homeid = Convert.ToInt32(func.dbtbl["vHOMEID"]);
                    item.awayid = Convert.ToInt32(func.dbtbl["vAWAYID"]);
                    item.home = func.dbtbl["vHOME"].ToString();
                    item.homesn = func.dbtbl["vHOMESHORT"].ToString();
                    item.away = func.dbtbl["vAWAY"].ToString();
                    item.awaysn = func.dbtbl["vAWAYSHORT"].ToString();
                    item.homescore = Convert.ToInt32(func.dbtbl["vHOMESCORE"]);
                    item.awayscore = Convert.ToInt32(func.dbtbl["vAWAYSCORE"]);
                    item.venue = func.dbtbl["vVENUE"].ToString();

                    teamresultItemList.Add(item);
                }
            }
            else
            {
                teamresultItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(teamresultItemList, cache_keys.TEAMRESULTS_KEY + leagueID + "_" + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return teamresultItemList;
    }
}

public class teamfixtureItem
{
    public int id { get; set; }
    public int islive { get; set; }
    public DateTime date { get; set; }
    public String home { get; set; }
    public String away { get; set; }
    public String homesn { get; set; }
    public String awaysn { get; set; }
    public String venue { get; set; }

    public teamfixtureItem()
    {
    }
}

public class teamfixtureItem_list
{
    public teamfixtureItem_list()
    {
    }

    public List<teamfixtureItem> getTeamFixtureItemList(int numRecords, String category, int leagueID, int teamID)
    {
        func func = new func();
        func.setDbConn(1);
        List<teamfixtureItem> teamfixtureItemList = null; // new List<teamfixtureItem>();

        teamfixtureItemList = (List<teamfixtureItem>)cache_provider.getObject(cache_keys.TRIES_KEY + leagueID + "_" + numRecords);

        if (teamfixtureItemList == null)
        {
            teamfixtureItemList = new List<teamfixtureItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("select top " + numRecords + " ");
            outStr.Append("tMAT.MatchId as vMATCHID, ");
            outStr.Append("tMAT.MatchDateTime as vDATE, ");
            outStr.Append("tMAT.HomeTeam as vHOME, ");
            outStr.Append("tMAT.HomeTeamShortName as vHOMESHORT, ");
            outStr.Append("tMAT.AwayTeam as vAWAY, ");
            outStr.Append("tMAT.AwayTeamShortName as vAWAYSHORT, ");
            outStr.Append("tMAT.Venue as vVENUE, ");
            outStr.Append("tLEA.Name as vTOURNAMENT, ");
            outStr.Append("tMATC.MatchStatus as vISLIVE ");
            outStr.Append("from Rugby.dbo.MatchBreakDown tMAT ");
            outStr.Append("inner join Rugby.dbo.Leagues tLEA on tLEA.ID = tMAT.LeagueId ");

            outStr.Append("inner join Rugby.dbo.Matches tMATC on tMATC.ID = tMAT.matchid ");

            outStr.Append("where (tMAT.LeagueId = " + leagueID + ") ");
            outStr.Append("and (tMAT.Site = 16) ");
            outStr.Append("and (tMAT.Completed = 0) ");

            outStr.Append("and ((tMATC.TeamAID = '" + teamID + "') or (tMATC.TeamBID = '" + teamID + "')) ");

            outStr.Append("order by tMAT.MatchDateTime asc");

            String query = outStr.ToString();

            //func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    teamfixtureItem item = new teamfixtureItem();

                    if (Convert.ToInt32(func.dbtbl["vISLIVE"]) > 2)
                    {
                        item.islive = 1;
                    }
                    else
                    {
                        item.islive = 0;
                    }
                    
                    item.id = Convert.ToInt32(func.dbtbl["vMATCHID"]);
                    item.date = Convert.ToDateTime(func.dbtbl["vDATE"]);
                    item.home = func.dbtbl["vHOME"].ToString();
                    item.homesn = func.dbtbl["vHOMESHORT"].ToString();
                    item.away = func.dbtbl["vAWAY"].ToString();
                    item.awaysn = func.dbtbl["vAWAYSHORT"].ToString();
                    item.venue = func.dbtbl["vVENUE"].ToString();

                    teamfixtureItemList.Add(item);
                }
            }
            else
            {
                teamfixtureItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(teamfixtureItemList, cache_keys.TRIES_KEY + leagueID + "_" + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return teamfixtureItemList;
    }
}