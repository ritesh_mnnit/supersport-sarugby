﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Collections;

public class func
{
    public SqlConnection dbConn = new SqlConnection();
    public SqlCommand sqlQuery = new SqlCommand();
    public SqlDataReader dbtbl = null;

    public void setDbConn(int state)
    {
        sqlQuery.Connection = dbConn;

        switch (state)
        {
            case -1:
                dbConn.Dispose();
                break;

            case 0:
                //if (func.dbtbl != null) func.dbtbl.Close();
                dbConn.Close();
                break;

            case 1:
                //if (func.dbtbl != null) func.dbtbl.Close();
                dbConn.Close();
                dbConn.ConnectionString = ConfigurationManager.ConnectionStrings["connRead"].ConnectionString;
                dbConn.Open();
                break;

            case 2:
                //if (func.dbtbl != null) func.dbtbl.Close();
                dbConn.Close();
                dbConn.ConnectionString = ConfigurationManager.ConnectionStrings["connWrite"].ConnectionString;
                dbConn.Open();
                break;
        }
    }

    public static bool IsMobileBrowser(string UserAgent)
    {
        bool Mobile = false;

        ArrayList Mobiles = new ArrayList();
        Mobiles.Add("cldc");
        Mobiles.Add("midp");
        Mobiles.Add("symbian");
        Mobiles.Add("up.link");
        Mobiles.Add("windows ce");
        Mobiles.Add("iphone");
        Mobiles.Add("mobile safari");

        IEnumerator iEnum = Mobiles.GetEnumerator();
        while (iEnum.MoveNext())
        {
            if (UserAgent.IndexOf(iEnum.Current.ToString()) >= 0)
            {
                Mobile = true;
                break;
            }
        }

        return Mobile;
    }

    public static void stop(String testString)
    {
        HttpContext.Current.Response.Write(testString);
        HttpContext.Current.Response.End();
    }

    public static void write(String testString)
    {
        HttpContext.Current.Response.Write(testString);
    }

    public static void forceFileDownload(String filePath, String fileName)
    {
        FileStream fStream = new FileStream(@filePath + fileName, FileMode.Open, FileAccess.Read);
        byte[] byteBuffer = new byte[(int)fStream.Length];
        fStream.Read(byteBuffer, 0, (int)fStream.Length);
        fStream.Close();
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "application/octet-stream";
        HttpContext.Current.Response.AddHeader("Content-Length", byteBuffer.Length.ToString());
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
        HttpContext.Current.Response.BinaryWrite(byteBuffer);
    }

    public static String getGuid()
    {
        string guidResult = System.Guid.NewGuid().ToString();
        return guidResult;
    }

    public static bool isNumericType(String testValue, String testType)
    {
        bool returnVal = false;

        switch (testType)
        {
            case "int":
                int out_INT;

                if (int.TryParse(testValue, out out_INT))
                {
                    returnVal = true;
                }
                break;

            case "double":
                double out_DOUBLE;

                if (double.TryParse(testValue, out out_DOUBLE))
                {
                    returnVal = true;
                }
                break;
        }

        return returnVal;
    }

    public static string GetCurrentPageName(String qString, bool useCurrentQuerystring)
    {
        String sRet;
        String sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        sRet = oInfo.Name;

        if (useCurrentQuerystring)
        {
            sRet += qString;
        }

        return sRet;
    }

    public static string fixHeadline(string Headline)
    {
        String retText = "";

        Headline = Headline.Trim();

        bool Proceed = true;
        while (Proceed == true)
        {
            string tmpString = Headline.Replace("  ", "");
            Proceed = false;
            if (tmpString.IndexOf("  ") >= 0)
            {
                Proceed = true;
            }
            Headline = tmpString;
        }

        string[] tmpArray = Headline.Split(' ');
        for (int i = 0; i <= tmpArray.GetUpperBound(0); i++)
        {
            if (retText != "")
            {
                retText += "_";
            }
            string pattern = @"[^a-zA-Z0-9]";
            string tmpString = Regex.Replace(tmpArray[i], pattern, "");
            retText += tmpString;
        }

        Proceed = true;
        while (Proceed == true)
        {
            string tmpString = retText.Replace("__", "_");
            Proceed = false;
            if (retText.IndexOf("__") >= 0)
            {
                Proceed = true;
            }
            retText = tmpString;
        }

        return retText;
    }
}
