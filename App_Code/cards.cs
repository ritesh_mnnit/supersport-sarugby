﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class cardItem
{
    public String team { get; set; }
    public String player { get; set; }
    public int teamid { get; set; }
    public int playerid { get; set; }
    public int red { get; set; }
    public int yel { get; set; }
    public int total { get; set; }

    public cardItem()
    {
    }
}

public class cardItem_list
{
    public cardItem_list()
    {
    }

    public List<cardItem> getCardItemList(int numRecords, String category, int leagueID)
    {
        func func = new func();
        func.setDbConn(1);
        List<cardItem> cardItemList = null; // new List<cardItem>();

        cardItemList = (List<cardItem>)cache_provider.getObject(cache_keys.CARDS_KEY + leagueID + "_" + numRecords);

        if (cardItemList == null)
        {
            cardItemList = new List<cardItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT top " + numRecords + " ");
            outStr.Append("b.Id as vPLAYERID, ");
            outStr.Append("(Select Case When b.displayname <> '' then b.displayname + ' ' + b.Surname Else b.FullName + ' ' + b.Surname End) As vPLAYER, ");
            outStr.Append("c.Id As vTEAMID, ");
            outStr.Append("c.Name As vTEAM, ");
            outStr.Append("Sum(a.RedCards) As vRED, ");
            outStr.Append("Sum(a.YellowCards) As vYEL, ");
            outStr.Append("((Sum(a.YellowCards)) + (Sum(a.RedCards))) As vTOTAL ");
            outStr.Append("From Rugby.dbo.Persons b LEFT JOIN Rugby.dbo.Scorers a ON b.Id = a.PlayerId ");
            outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague c ON c.Id = a.TeamId ");
            outStr.Append("INNER JOIN Rugby.dbo.Leagues d ON d.Id = a.LeagueId ");
            outStr.Append("INNER JOIN Rugby.dbo.Tournaments AS e ON e.ID = d.TournamentID ");
            outStr.Append("WHERE (a.LeagueId = '"+ leagueID +"') AND (a.YellowCards > 0 OR a.Redcards > 0) ");
            outStr.Append("group by b.id, b.surname, b.displayname, b.fullname, c.id, c.Name ");
            outStr.Append("ORDER BY vRED DESC, vYEL DESC, vTOTAL DESC, vPLAYER");

            String query = outStr.ToString();

            //        func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    cardItem item = new cardItem();

                    item.team = func.dbtbl["vTEAM"].ToString();
                    item.player = func.dbtbl["vPLAYER"].ToString();
                    item.teamid = Convert.ToInt32(func.dbtbl["vTEAMID"]);
                    item.playerid = Convert.ToInt32(func.dbtbl["vPLAYERID"]);
                    item.red = Convert.ToInt32(func.dbtbl["vRED"]);
                    item.yel = Convert.ToInt32(func.dbtbl["vYEL"]);
                    item.total = Convert.ToInt32(func.dbtbl["vTOTAL"]);

                    cardItemList.Add(item);
                }
            }
            else
            {
                cardItemList = null;
            }

            func.dbtbl.Close();
            
            cache_provider.addObject(cardItemList, cache_keys.CARDS_KEY + leagueID + "_" + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return cardItemList;
    }
}