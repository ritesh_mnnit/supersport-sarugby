﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class tryItem
{
    public int personID { get; set; }
    public String player { get; set; }
    public int tries { get; set; }

    public tryItem()
    {
    }
}

public class tryItem_list
{
    public tryItem_list()
    {
    }

    public List<tryItem> getTryItemList(int numRecords, String category, int leagueID)
    {
        func func = new func();
        func.setDbConn(1);
        List<tryItem> tryItemList = null; // new List<tryItem>();

        tryItemList = (List<tryItem>)cache_provider.getObject(cache_keys.TRIES_KEY + leagueID + "_" + numRecords);

        if (tryItemList == null)
        {
            tryItemList = new List<tryItem>();

            StringBuilder outStr = new StringBuilder();

            if (leagueID == 0)
            {
                outStr.Append("SELECT TOP " + numRecords + " ");
                outStr.Append("'0' as vPLAYERID, ");
                outStr.Append("tPLA.combination as vPLAYER, ");
                outStr.Append("tPLA.tries as vTRIES, ");
                outStr.Append("tPLA.conversions as vCONVERSIONS, ");
                outStr.Append("tPLA.penalties as vPENALTIES, ");
                outStr.Append("tPLA.drops as vDROPS, ");
                outStr.Append("tPLA.points as vTOTAL, ");
                outStr.Append("tPER.id as vID ");
                outStr.Append("FROM SSZGeneral.dbo.rugbystats_players tPLA ");
                outStr.Append("left join rugby.dbo.persons tPER on (tPER.Surname COLLATE SQL_Latin1_General_CP1_CI_AS = tPLA.surname and tPER.BirthDate = tPLA.BirthDate) ");
                outStr.Append("where tPLA.tries > '0' ");
                outStr.Append("and tPER.id is not null ");
                outStr.Append("ORDER BY tPLA.tries DESC");
            }
            else
            {
                outStr.Append("SELECT TOP " + numRecords + " ");
                outStr.Append("b.Id as vPLAYERID, ");
                outStr.Append("(Select Case When b.displayname <> ' ' then b.displayname + ' ' + b.Surname Else b.FullName + ' ' + b.Surname End) As vPLAYER, ");
                outStr.Append("c.Id as vTEAMID, ");
                outStr.Append("c.Name As vTEAM, ");
                outStr.Append("Sum(a.Tries) As vTRIES, Sum(a.Conversions) As vCONVERSIONS, ");
                outStr.Append("Sum(a.Penalties) As vPENALTIES, ");
                outStr.Append("Sum(a.DropGoals) As vDROPS, ");
                outStr.Append("((Sum(a.Tries) * 5) + (Sum(a.Conversions) * 2) + (Sum(a.Penalties) * 3) + (Sum(a.DropGoals) * 3)) As vTOTAL ");
                outStr.Append("From Rugby.dbo.Persons b ");
                outStr.Append("LEFT JOIN Rugby.dbo.Scorers a ON b.Id = a.PlayerId ");
                outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague c ON c.Id = a.TeamId ");
                outStr.Append("INNER JOIN Rugby.dbo.Leagues d ON d.Id = a.LeagueId ");
                outStr.Append("INNER JOIN Rugby.dbo.Tournaments AS e ON e.ID = d.TournamentID ");
                outStr.Append("WHERE (a.LeagueId = " + leagueID + ") AND ((a.Tries > 0) OR (a.Conversions > 0) OR (a.Penalties > 0) OR (a.DropGoals > 0)) ");
                outStr.Append("group by b.id, b.surname, b.displayname, b.fullname, c.id, c.Name ");
                outStr.Append("ORDER BY vTRIES DESC, vTOTAL DESC, vPENALTIES DESC, vCONVERSIONS DESC, vDROPS DESC, vPLAYER");

            }

            String query = outStr.ToString();

            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    tryItem item = new tryItem();


                    item.personID = Convert.ToInt32(func.dbtbl["vPLAYERID"]);
                    item.player = func.dbtbl["vPLAYER"].ToString();
                    item.tries = Convert.ToInt32(func.dbtbl["vTRIES"]);

                    tryItemList.Add(item);
                }
            }
            else
            {
                tryItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(tryItemList, cache_keys.TRIES_KEY + leagueID + "_" + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return tryItemList;
    }
}