﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class videoItem
{
    public int id { get; set; }
    public DateTime date { get; set; }
    public String name { get; set; }
    public String description { get; set; }
    public String image { get; set; }

    public videoItem()
    {
    }
}

public class videoItem_list
{
    public videoItem_list()
    {
    }

    public List<videoItem> getVideoItemList(int numRecords, String category)
    {
        func func = new func();
        func.setDbConn(1);
        List<videoItem> videoItemList = null; // new List<videoItem>();

        videoItemList = (List<videoItem>)cache_provider.getObject(cache_keys.VIDEOITEMS_KEY + numRecords);

        if (videoItemList == null)
        {
            videoItemList = new List<videoItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("select top " + numRecords + " ");
            outStr.Append("tVID.Id as vID, ");
            outStr.Append("tVID.name As vTITLE, ");
            outStr.Append("tVID.created As vDATE, ");
            outStr.Append("tVID.description As vDESCRIPTION, ");
            outStr.Append("tVID.FeatureImage As vIMAGE ");
            outStr.Append("From SSZGeneral.dbo.broadBandVideo tVID ");
            outStr.Append("where tVID.sarugby=1 ");
            outStr.Append("order by tVID.created desc");

            String query = outStr.ToString();

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    videoItem item = new videoItem();

                    item.id = Convert.ToInt32(func.dbtbl["vID"]);
                    item.date = Convert.ToDateTime(func.dbtbl["vDATE"]);
                    item.name = func.dbtbl["vTITLE"].ToString();
                    item.description = func.dbtbl["vDESCRIPTION"].ToString();
                    item.image = func.dbtbl["vIMAGE"].ToString();

                    videoItemList.Add(item);
                }
            }
            else
            {
                videoItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(videoItemList, cache_keys.VIDEOITEMS_KEY + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return videoItemList;
    }
}

public class photogalleryItem
{
    public int id { get; set; }
    public DateTime date { get; set; }
    public String name { get; set; }
    public String description { get; set; }
    public String imageSmall { get; set; }
    public String imageLarge { get; set; }

	public photogalleryItem()
	{
	}
}

public class photogalleryItem_list
{
    public photogalleryItem_list()
    {
    }

    public List<photogalleryItem> getPhotogalleryItemList(int numRecords, String category)
    {
        func func = new func();
        func.setDbConn(1);
        List<photogalleryItem> photogalleryItemList = null; // new List<photogalleryItem>();

        photogalleryItemList = (List<photogalleryItem>)cache_provider.getObject(cache_keys.PHOTOGALLERYITEMS_KEY + numRecords);

        if (photogalleryItemList == null)
        {
            photogalleryItemList = new List<photogalleryItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT TOP " + numRecords + " a.Id as vID, a.Teaser As SmallImage, ");
            outStr.Append("a.Headline As ValHeadline, ");
            outStr.Append("a.Description As ValDescription, ");
            outStr.Append("a.Userdate as vDATE ");
            outStr.Append("FROM SuperSportZone.dbo.ZonePhotoGalleries a INNER JOIN ");
            outStr.Append("SuperSportZone.dbo.ZonePhotoGalleryCategories b ON b.PhotoGallery = a.Id ");
            outStr.Append("WHERE (b.Category = 369) AND (a.Active = 1) ");
            outStr.Append("ORDER BY a.UserDate DESC, a.Id DESC");


            //outStr.Append("SELECT top " + numRecords + " ");
            //outStr.Append("a.Id as vID, ");
            //outStr.Append("a.Teaser As SmallImage, ");
            //outStr.Append("d.LargeImage as LargeImage, ");
            //outStr.Append("a.Headline As ValHeadline, ");
            //outStr.Append("a.Description As ValDescription, ");
            //outStr.Append("a.Userdate as vDATE, ");
            //outStr.Append("min(c.smallimage) as TeaserSmall1, ");
            //outStr.Append("max(c.smallimage) as TeaserSmall2, ");
            //outStr.Append("min(c.largeimage) as TeaserLarge1, ");
            //outStr.Append("max(c.largeimage) as TeaserLarge2 ");
            //outStr.Append("FROM supersportzone.dbo.ZoneCategories e ");
            //outStr.Append("INNER JOIN supersportzone.dbo.ZonePhotoGalleryCategories b ON b.Category = e.Id ");
            //outStr.Append("inner join supersportzone.dbo.zonePhotoGalleries a on a.Id = b.photogallery ");
            //outStr.Append("left join supersportzone.dbo.ZonePhotoGalleryImages as c on c.photogallery = a.id and c.smallImage <> a.Teaser ");
            //outStr.Append("INNER JOIN supersportzone.dbo.ZonePhotoGalleryImages d ON a.teaser = d.smallimage ");
            //outStr.Append("WHERE (e.name in ('sarugby')) AND (a.Active = 1) ");
            //outStr.Append("Group By a.id, a.Teaser, a.headline, a.description, a.userdate,d.LargeImage ");
            //outStr.Append("ORDER BY a.UserDate DESC, a.Id DESC");

            //func.stop(outStr.ToString());

            String query = outStr.ToString();

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    photogalleryItem item = new photogalleryItem();

                    item.id = Convert.ToInt32(func.dbtbl["vID"]);
                    item.date = Convert.ToDateTime(func.dbtbl["vDATE"]);
                    item.name = func.dbtbl["ValHeadline"].ToString();
                    item.description = func.dbtbl["ValDescription"].ToString();
                    item.imageSmall = func.dbtbl["SmallImage"].ToString();
                    //item.imageLarge = func.dbtbl["LargeImage"].ToString();

                    photogalleryItemList.Add(item);
                }
            }
            else
            {
                photogalleryItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(photogalleryItemList, cache_keys.PHOTOGALLERYITEMS_KEY + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return photogalleryItemList;
    }
}

public class photoItem
{
    public int id { get; set; }
    public int itemnum { get; set; }
    public DateTime date { get; set; }
    public String headline { get; set; }
    public String description { get; set; }
    public String imageSmall { get; set; }
    public String imageLarge { get; set; }

    public photoItem()
    {
    }
}

public class photoItem_list
{
    public photoItem_list()
    {
    }

    public List<photoItem> getPhotoItemList(int galID)
    {
        func func = new func();
        func.setDbConn(1);
        List<photoItem> photoItemList = null; //new List<photoItem>();

        photoItemList = (List<photoItem>)cache_provider.getObject(cache_keys.PHOTOITEMS_KEY + galID);

        if (photoItemList == null)
        {
            photoItemList = new List<photoItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT ");
            outStr.Append("id as vID, ");
            outStr.Append("Headline as vHEADLINE, ");
            outStr.Append("Description as vDESCRIPTION, ");
            outStr.Append("Created as vDATE, ");
            outStr.Append("SmallImage as vSMALLIMAGE, ");
            outStr.Append("LargeImage as vLARGEIMAGE ");
            outStr.Append("FROM supersportzone.dbo.ZonePhotoGalleryImages ");
            outStr.Append("WHERE (PhotoGallery = " + galID + ") AND (Active = 1) ORDER BY Rank");

            String query = outStr.ToString();

            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                int cnt = 1;

                while (func.dbtbl.Read())
                {
                    photoItem item = new photoItem();

                    item.id = Convert.ToInt32(func.dbtbl["vID"]);
                    item.itemnum = cnt++;
                    item.date = Convert.ToDateTime(func.dbtbl["vDATE"]);
                    item.headline = func.dbtbl["VHEADLINE"].ToString();
                    item.description = func.dbtbl["vDESCRIPTION"].ToString();
                    item.imageSmall = func.dbtbl["vSMALLIMAGE"].ToString();
                    item.imageLarge = func.dbtbl["vLARGEIMAGE"].ToString();

                    photoItemList.Add(item);
                }
            }
            else
            {
                photoItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(photoItemList, cache_keys.PHOTOITEMS_KEY + galID, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return photoItemList;
    }
}