﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class menuItem
{
    public int ID { get; set; }
    public int parentID { get; set; }
    public int level { get; set; }
    public String sort_key { get; set; }
    public String name { get; set; }
    public String link { get; set; }
    public bool newwin { get; set; }

    public menuItem()
	{
	}
}

public class menuItem_list
{
    public menuItem_list()
    {
    }

    public List<menuItem> getMenuItemList(int parentID)
    {
        func func = new func();
        func.setDbConn(1);

        List<menuItem> menuItemList = null; // new List<menuItem>();

        menuItemList = (List<menuItem>)cache_provider.getObject(cache_keys.MAINMENU_KEY + parentID);

        if (menuItemList == null)
        {
            menuItemList = new List<menuItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("select ");
            outStr.Append("tMEN.id as vID, ");
            outStr.Append("tMEN.parentId as vPARENTID, ");
            outStr.Append("tMEN.Lvl as vLEVEL, ");
            outStr.Append("tMEN.Sort_key as vSORTKEY, ");
            outStr.Append("tMEN.Name as vNAME, ");
            outStr.Append("tMEN.Link as vLINK, ");
            outStr.Append("tMEN.window as vNEWWIN ");
            outStr.Append("from SuperSportZone.dbo.zonetempnavigation tMEN ");
            outStr.Append("where (tMEN.Site = 16) ");
            outStr.Append("and (tMEN.ParentId=" + parentID + ") ");
            outStr.Append("and (tMEN.Active=1) ");
            outStr.Append("order by tMEN.Sort_key");

            String query = outStr.ToString();

            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    menuItem item = new menuItem();

                    item.ID = Convert.ToInt32(func.dbtbl["vID"]);
                    item.parentID = Convert.ToInt32(func.dbtbl["vPARENTID"]);
                    item.level = Convert.ToInt32(func.dbtbl["vLEVEL"]);
                    item.sort_key = func.dbtbl["vSORTKEY"].ToString();
                    item.name = func.dbtbl["vNAME"].ToString();
                    item.link = func.dbtbl["vLINK"].ToString();
                    item.newwin = Convert.ToBoolean(func.dbtbl["vNEWWIN"]);

                    menuItemList.Add(item);
                }
            }
            else
            {
                menuItemList = null;
            }

            func.dbtbl.Close();
            
            cache_provider.addObject(menuItemList, cache_keys.MAINMENU_KEY + parentID, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);

        return menuItemList;
    }
}