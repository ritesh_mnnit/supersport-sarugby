﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class resultItem
{
    public int id { get; set; }
    public DateTime date { get; set; }
    public String home { get; set; }
    public String away { get; set; }
    public String homesn { get; set; }
    public String awaysn { get; set; }
    public int homescore { get; set; }
    public int awayscore { get; set; }
    public String venue { get; set; }
    public String tournament { get; set; }
    public int homeid { get; set; }
    public int awayid { get; set; }

    public resultItem()
    {
    }
}

public class resultItem_list
{
    public resultItem_list()
    {
    }

    public List<resultItem> getResultItemList(int numRecords, String category, int leagueID)
    {
        func func = new func();
        func.setDbConn(1);
        List<resultItem> resultItemList = new List<resultItem>();

        resultItemList = (List<resultItem>)cache_provider.getObject(cache_keys.RESULTS_KEY + leagueID + "_" + numRecords);

        if (resultItemList == null)
        {
            resultItemList = new List<resultItem>();

            StringBuilder outStr = new StringBuilder();

            switch (leagueID)
            {
                case 0:
                    outStr.Append("select top " + numRecords + " ");
                    outStr.Append("tMAT.MatchId as vMATCHID, ");
                    outStr.Append("tMAT.MatchDateTime as vDATE, ");
                    outStr.Append("tMAT.HomeTeam as vHOME, ");
                    outStr.Append("tMAT.HomeTeamShortName as vHOMESHORT, ");
                    outStr.Append("tMAT.AwayTeam as vAWAY, ");
                    outStr.Append("tMAT.AwayTeamShortName as vAWAYSHORT, ");
                    outStr.Append("tMAT.HomeTeamScore as vHOMESCORE, ");
                    outStr.Append("tMAT.AwayTeamScore as vAWAYSCORE, ");
                    outStr.Append("tDET.TeamAID as vTEAMAID, ");
                    outStr.Append("tDET.TeamBID as vTEAMBID, ");
                    outStr.Append("tMAT.Venue as vVENUE, ");
                    outStr.Append("tLEA.Name as vTOURNAMENT ");
                    outStr.Append("from Rugby.dbo.MatchBreakDown tMAT ");
                    outStr.Append("inner join Rugby.dbo.Leagues tLEA on tLEA.ID = tMAT.LeagueId ");
                    outStr.Append("inner join Rugby.dbo.Matches tDET on tDET.ID = tMAT.MatchId ");
                    outStr.Append("where (tMAT.Site = 16) ");
                    outStr.Append("and (tMAT.Completed = 1) ");
                    outStr.Append("and (tMAT.MatchDateTime <= getdate()) ");
                    outStr.Append("order by tMAT.MatchDateTime desc");
                    break;

                case 1:
                    outStr.Append("SELECT DISTINCT top " + numRecords + " ");
                    outStr.Append("a.Id as vMATCHID, ");
                    outStr.Append("a.TeamAID as vTEAMAID, ");
                    outStr.Append("a.TeamBID as vTEAMBID, ");
                    outStr.Append("a.MatchDateTime as vDATE, ");
                    outStr.Append("a.MatchStatus as vISLIVE, ");
                    outStr.Append("tMAT.HomeTeamScore as vHOMESCORE, ");
                    outStr.Append("tMAT.AwayTeamScore as vAWAYSCORE, ");
                    outStr.Append("b.Name AS vHOME, ");
                    outStr.Append("c.Name AS vAWAY, ");
                    outStr.Append("b.ShortName AS vHOMESHORT, ");
                    outStr.Append("c.ShortName AS vAWAYSHORT, ");
                    outStr.Append("d.Name AS vTOURNAMENT, ");
                    outStr.Append("e.Name AS vVENUE, ");
                    outStr.Append("YEAR(a.MatchDateTime) AS MatchYear, ");
                    outStr.Append("MONTH(a.MatchDateTime) AS MatchMonth, ");
                    outStr.Append("DAY(a.MatchDateTime) AS MatchDay, ");
                    outStr.Append("f.CategoryID, ");
                    outStr.Append("d.TournamentID ");
                    outStr.Append("From Rugby.dbo.Matches AS a ");
                    outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague AS b ON b.Id = a.TeamAID ");
                    outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague AS c ON c.Id = a.TeamBID ");
                    outStr.Append("INNER JOIN Rugby.dbo.Leagues AS d ON d.ID = a.LeagueID ");
                    outStr.Append("INNER JOIN Rugby.dbo.VenuesByLeague AS e ON e.Id = a.VenueID ");
                    outStr.Append("INNER JOIN Rugby.dbo.Tournaments AS f ON f.ID = d.TournamentID ");
                    outStr.Append("INNER JOIN Rugby.dbo.MatchBreakDown AS tMAT ON tMAT.MatchID = a.Id ");
                    outStr.Append("WHERE (a.Completed = 1) ");
                    outStr.Append("AND (f.CategoryId = 2) ");
                    outStr.Append("AND (b.Name = 'South Africa' OR b.ShortName = 'South Africa' OR c.Name = 'South Africa' OR c.ShortName = 'South Africa') ");
                    outStr.Append("AND (d.TournamentId <> 63) ");
                    outStr.Append("AND (a.LeagueID <> 739) ");
                    outStr.Append("AND (a.LeagueID <> 744) ");
                    outStr.Append("AND (a.LeagueID <> 740) ");
                    outStr.Append("ORDER BY MatchYear desc, MatchMonth desc, MatchDay desc, vTOURNAMENT");
                    break;

                case 2:
                    outStr.Append("SELECT DISTINCT top " + numRecords + " ");
                    outStr.Append("a.Id as vMATCHID, ");
                    outStr.Append("a.TeamAID as vTEAMAID, ");
                    outStr.Append("a.TeamBID as vTEAMBID, ");
                    outStr.Append("a.MatchDateTime as vDATE, ");
                    outStr.Append("a.MatchStatus as vISLIVE, ");
                    outStr.Append("tMAT.HomeTeamScore as vHOMESCORE, ");
                    outStr.Append("tMAT.AwayTeamScore as vAWAYSCORE, ");
                    outStr.Append("b.Name AS vHOME, ");
                    outStr.Append("c.Name AS vAWAY, ");
                    outStr.Append("b.ShortName AS vHOMESHORT, ");
                    outStr.Append("c.ShortName AS vAWAYSHORT, ");
                    outStr.Append("d.Name AS vTOURNAMENT, ");
                    outStr.Append("e.Name AS vVENUE, ");
                    outStr.Append("YEAR(a.MatchDateTime) AS MatchYear, ");
                    outStr.Append("MONTH(a.MatchDateTime) AS MatchMonth, ");
                    outStr.Append("DAY(a.MatchDateTime) AS MatchDay, ");
                    outStr.Append("f.CategoryID, ");
                    outStr.Append("d.TournamentID ");
                    outStr.Append("From Rugby.dbo.Matches AS a ");
                    outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague AS b ON b.Id = a.TeamAID ");
                    outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague AS c ON c.Id = a.TeamBID ");
                    outStr.Append("INNER JOIN Rugby.dbo.Leagues AS d ON d.ID = a.LeagueID ");
                    outStr.Append("INNER JOIN Rugby.dbo.VenuesByLeague AS e ON e.Id = a.VenueID ");
                    outStr.Append("INNER JOIN Rugby.dbo.Tournaments AS f ON f.ID = d.TournamentID ");
                    outStr.Append("INNER JOIN Rugby.dbo.MatchBreakDown AS tMAT ON tMAT.MatchID = a.Id ");
                    outStr.Append("WHERE (a.Completed = 1) ");
                    outStr.Append("AND (f.CategoryId = 7) ");
                    outStr.Append("AND (b.Name = 'South Africa' OR b.ShortName = 'South Africa' OR c.Name = 'South Africa' OR c.ShortName = 'South Africa') ");
                    outStr.Append("AND (d.TournamentId <> 63) ");
                    outStr.Append("AND (a.LeagueID <> 739) ");
                    outStr.Append("AND (a.LeagueID <> 744) ");
                    outStr.Append("AND (a.LeagueID <> 740) ");
                    outStr.Append("ORDER BY MatchYear desc, MatchMonth desc, MatchDay desc, vTOURNAMENT");
                    break;

                case 3:
                    outStr.Append("SELECT DISTINCT top " + numRecords + " ");
                    outStr.Append("a.Id as vMATCHID, ");
                    outStr.Append("a.TeamAID as vTEAMAID, ");
                    outStr.Append("a.TeamBID as vTEAMBID, ");
                    outStr.Append("a.MatchDateTime as vDATE, ");
                    outStr.Append("a.MatchStatus as vISLIVE, ");
                    outStr.Append("tMAT.HomeTeamScore as vHOMESCORE, ");
                    outStr.Append("tMAT.AwayTeamScore as vAWAYSCORE, ");
                    outStr.Append("b.Name AS vHOME, ");
                    outStr.Append("c.Name AS vAWAY, ");
                    outStr.Append("b.ShortName AS vHOMESHORT, ");
                    outStr.Append("c.ShortName AS vAWAYSHORT, ");
                    outStr.Append("d.Name AS vTOURNAMENT, ");
                    outStr.Append("e.Name AS vVENUE, ");
                    outStr.Append("YEAR(a.MatchDateTime) AS MatchYear, ");
                    outStr.Append("MONTH(a.MatchDateTime) AS MatchMonth, ");
                    outStr.Append("DAY(a.MatchDateTime) AS MatchDay, ");
                    outStr.Append("f.CategoryID, ");
                    outStr.Append("d.TournamentID ");
                    outStr.Append("From Rugby.dbo.Matches AS a ");
                    outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague AS b ON b.Id = a.TeamAID ");
                    outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague AS c ON c.Id = a.TeamBID ");
                    outStr.Append("INNER JOIN Rugby.dbo.Leagues AS d ON d.ID = a.LeagueID ");
                    outStr.Append("INNER JOIN Rugby.dbo.VenuesByLeague AS e ON e.Id = a.VenueID ");
                    outStr.Append("INNER JOIN Rugby.dbo.Tournaments AS f ON f.ID = d.TournamentID ");
                    outStr.Append("INNER JOIN Rugby.dbo.MatchBreakDown AS tMAT ON tMAT.MatchID = a.Id ");
                    outStr.Append("WHERE (a.Completed = 1) ");
                    outStr.Append("AND (f.CategoryId = 6) ");
                    outStr.Append("AND (b.Name = 'South Africa' OR b.ShortName = 'South Africa' OR c.Name = 'South Africa' OR c.ShortName = 'South Africa') ");
                    outStr.Append("AND (d.TournamentId <> 63) ");
                    outStr.Append("AND (a.LeagueID <> 739) ");
                    outStr.Append("AND (a.LeagueID <> 744) ");
                    outStr.Append("AND (a.LeagueID <> 740) ");
                    outStr.Append("ORDER BY MatchYear desc, MatchMonth desc, MatchDay desc, vTOURNAMENT");
                    break;

                case 4:
                    outStr.Append("SELECT DISTINCT top " + numRecords + " ");
                    outStr.Append("a.Id as vMATCHID, ");
                    outStr.Append("a.TeamAID as vTEAMAID, ");
                    outStr.Append("a.TeamBID as vTEAMBID, ");
                    outStr.Append("a.MatchDateTime as vDATE, ");
                    outStr.Append("a.MatchStatus as vISLIVE, ");
                    outStr.Append("tMAT.HomeTeamScore as vHOMESCORE, ");
                    outStr.Append("tMAT.AwayTeamScore as vAWAYSCORE, ");
                    outStr.Append("b.Name AS vHOME, ");
                    outStr.Append("c.Name AS vAWAY, ");
                    outStr.Append("b.ShortName AS vHOMESHORT, ");
                    outStr.Append("c.ShortName AS vAWAYSHORT, ");
                    outStr.Append("d.Name AS vTOURNAMENT, ");
                    outStr.Append("e.Name AS vVENUE, ");
                    outStr.Append("YEAR(a.MatchDateTime) AS MatchYear, ");
                    outStr.Append("MONTH(a.MatchDateTime) AS MatchMonth, ");
                    outStr.Append("DAY(a.MatchDateTime) AS MatchDay, ");
                    outStr.Append("f.CategoryID, ");
                    outStr.Append("d.TournamentID ");
                    outStr.Append("From Rugby.dbo.Matches AS a ");
                    outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague AS b ON b.Id = a.TeamAID ");
                    outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague AS c ON c.Id = a.TeamBID ");
                    outStr.Append("INNER JOIN Rugby.dbo.Leagues AS d ON d.ID = a.LeagueID ");
                    outStr.Append("INNER JOIN Rugby.dbo.VenuesByLeague AS e ON e.Id = a.VenueID ");
                    outStr.Append("INNER JOIN Rugby.dbo.Tournaments AS f ON f.ID = d.TournamentID ");
                    outStr.Append("INNER JOIN Rugby.dbo.MatchBreakDown AS tMAT ON tMAT.MatchID = a.Id ");
                    outStr.Append("WHERE (a.Completed = 1) ");
                    outStr.Append("AND (f.CategoryId = 23) ");
                    outStr.Append("AND (b.Name = 'South Africa' OR b.ShortName = 'South Africa' OR c.Name = 'South Africa' OR c.ShortName = 'South Africa') ");
                    outStr.Append("AND (d.TournamentId <> 63) ");
                    outStr.Append("AND (a.LeagueID <> 739) ");
                    outStr.Append("AND (a.LeagueID <> 744) ");
                    outStr.Append("AND (a.LeagueID <> 740) ");
                    outStr.Append("ORDER BY MatchYear desc, MatchMonth desc, MatchDay desc, vTOURNAMENT");
                    break;

                default:
                    outStr.Append("select top " + numRecords + " ");
                    outStr.Append("tMAT.MatchId as vMATCHID, ");
                    outStr.Append("tMAT.MatchDateTime as vDATE, ");
                    outStr.Append("tMAT.HomeTeam as vHOME, ");
                    outStr.Append("tMAT.HomeTeamShortName as vHOMESHORT, ");
                    outStr.Append("tMAT.AwayTeam as vAWAY, ");
                    outStr.Append("tMAT.AwayTeamShortName as vAWAYSHORT, ");
                    outStr.Append("tMAT.HomeTeamScore as vHOMESCORE, ");
                    outStr.Append("tMAT.AwayTeamScore as vAWAYSCORE, ");
                    outStr.Append("tDET.TeamAID as vTEAMAID, ");
                    outStr.Append("tDET.TeamBID as vTEAMBID, ");
                    outStr.Append("tMAT.Venue as vVENUE, ");
                    outStr.Append("tLEA.Name as vTOURNAMENT ");
                    outStr.Append("from Rugby.dbo.MatchBreakDown tMAT ");
                    outStr.Append("inner join Rugby.dbo.Leagues tLEA on tLEA.ID = tMAT.LeagueId ");
                    outStr.Append("inner join Rugby.dbo.Matches tDET on tDET.ID = tMAT.MatchId ");
                    outStr.Append("where (tMAT.LeagueId = " + leagueID + ") ");
                    outStr.Append("and (tMAT.Site = 16) ");
                    outStr.Append("and (tMAT.Completed = 1) ");
                    outStr.Append("and (tMAT.MatchDateTime <= getdate()) ");
                    outStr.Append("order by tMAT.MatchDateTime desc");
                    break;
            }

            //HttpContext.Current.Response.Write(outStr.ToString());
            //HttpContext.Current.Response.End();

            //if (leagueID == 0)
            //{

            //}
            //else
            //{
                
            //}


            String query = outStr.ToString();

            //func.stop(query);

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    resultItem item = new resultItem();

                    item.id = Convert.ToInt32(func.dbtbl["vMATCHID"]);
                    item.date = Convert.ToDateTime(func.dbtbl["vDATE"]);
                    item.home = func.dbtbl["vHOME"].ToString();
                    item.homesn = func.dbtbl["vHOMESHORT"].ToString();
                    item.away = func.dbtbl["vAWAY"].ToString();
                    item.awaysn = func.dbtbl["vAWAYSHORT"].ToString();
                    item.homescore = Convert.ToInt32(func.dbtbl["vHOMESCORE"]);
                    item.awayscore = Convert.ToInt32(func.dbtbl["vAWAYSCORE"]);
                    item.venue = func.dbtbl["vVENUE"].ToString();
                    item.tournament = func.dbtbl["vTOURNAMENT"].ToString();
                    item.homeid = Convert.ToInt32(func.dbtbl["vTEAMAID"]);
                    item.awayid = Convert.ToInt32(func.dbtbl["vTEAMBID"]);

                    resultItemList.Add(item);
                }
            }
            else
            {
                resultItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(resultItemList, cache_keys.RESULTS_KEY + leagueID + "_" + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return resultItemList;
    }
}