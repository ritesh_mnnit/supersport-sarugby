﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class logItem
{
    public String team { get; set; }
    public String teamsn { get; set; }
    public int played { get; set; }
    public int won { get; set; }
    public int drew { get; set; }
    public int lost { get; set; }
    public int pointsfor { get; set; }
    public int pointsagainst { get; set; }
    public int triesfor { get; set; }
    public int triesagainst { get; set; }
    public int lossbonus { get; set; }
    public int triesbonus { get; set; }
    public int logpoints { get; set; }
    public String group { get; set; }
    public String leaguename { get; set; }

    public logItem()
    {
    }
}

public class logItem_list
{
    public logItem_list()
    {
    }

    public List<logItem> getLogItemList(int numRecords, String category, int leagueID)
    {
        func func = new func();

        func.setDbConn(1);
        List<logItem> logItemList = null; // new List<logItem>();

        logItemList = (List<logItem>)cache_provider.getObject(cache_keys.LOGS_KEY + leagueID + "_" + numRecords);

        if (logItemList == null)
        {
            logItemList = new List<logItem>();

            StringBuilder outStr = new StringBuilder();

            //outStr.Append("SELECT ");
            //outStr.Append("tTEA.Name As vTEAM, ");
            //outStr.Append("tTEA.ShortName as vTEAMSHORT, ");
            //outStr.Append("tTEA.MobileName As vMOBILE, ");
            //outStr.Append("tLOG.Played as vPLAYED, ");
            //outStr.Append("tLOG.Won as vWON, ");
            //outStr.Append("tLOG.Drew as vDREW, ");
            //outStr.Append("tLOG.Lost as vLOST, ");
            //outStr.Append("tLOG.PointsFor as vPOINTSFOR, ");
            //outStr.Append("tLOG.PointsAgainst as vPOINTSAGAINST, ");
            //outStr.Append("tLOG.TriesFor as vTRIESFOR, ");
            //outStr.Append("tLOG.TriesAgainst as vTRIESAGAINST, ");
            //outStr.Append("tLOG.LossBonus as vLOSSBONUS, ");
            //outStr.Append("tLOG.TriesBonus as vTRIESBONUS, ");
            //outStr.Append("tLOG.LogPoints as vLOGPOINTS, ");
            //outStr.Append("tLEA.Name as vLEAGUENAME, ");
            //outStr.Append("tGRO.Name As vGROUP ");
            //outStr.Append("FROM Rugby.dbo.Logs tLOG ");
            //outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague tTEA ON tTEA.Id = tLOG.TeamId ");
            //outStr.Append("INNER JOIN Rugby.dbo.Groups tGRO ON tGRO.Id = tTEA.GroupId ");
            //outStr.Append("INNER JOIN Rugby.dbo.Leagues tLEA ON tLEA.Id = tLOG.LeagueId ");
            //outStr.Append("WHERE (tLOG.LeagueId = " + leagueID + ") ");
            //outStr.Append("ORDER BY tGRO.Name, tLOG.LogPoints DESC, tLOG.PointsFor - tLOG.PointsAgainst DESC, tLOG.TriesFor DESC, tTEA.Name ASC");
			
			String query = outStr.ToString();

            //func.stop(query);

            func.sqlQuery.CommandText = "execute rugby.dbo.logsbyleague " + leagueID;// query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    logItem item = new logItem();

                    item.team = func.dbtbl["Team"].ToString();
                    item.teamsn = func.dbtbl["ShortName"].ToString();
                    item.played = Convert.ToInt32(func.dbtbl["Played"]);
                    item.won = Convert.ToInt32(func.dbtbl["Won"]);
                    item.drew = Convert.ToInt32(func.dbtbl["Drew"]);
                    item.lost = Convert.ToInt32(func.dbtbl["Lost"]);
                    item.pointsfor = Convert.ToInt32(func.dbtbl["PointsFor"]);
                    item.pointsagainst = Convert.ToInt32(func.dbtbl["PointsAgainst"]);
                    item.triesfor = Convert.ToInt32(func.dbtbl["TriesFor"]);
                    item.triesagainst = Convert.ToInt32(func.dbtbl["TriesAgainst"]);
                    item.lossbonus = Convert.ToInt32(func.dbtbl["LossBonus"]);
                    item.triesbonus = Convert.ToInt32(func.dbtbl["TriesBonus"]);
                    item.logpoints = Convert.ToInt32(func.dbtbl["LogPoints"]);
                    item.group = func.dbtbl["GroupName"].ToString();
                    item.leaguename = func.dbtbl["League"].ToString();

                    logItemList.Add(item);
                }
            }
            else
            {
                logItemList = null;
            }
			
			func.dbtbl.Close();
			
            //if (leagueID == 795)
            //{
            //    outStr = new StringBuilder();
            //outStr.Append("SELECT ");
            //outStr.Append("tTEA.Name As vTEAM, ");
            //outStr.Append("tTEA.ShortName as vTEAMSHORT, ");
            //outStr.Append("tTEA.MobileName As vMOBILE, ");
            //outStr.Append("tLOG.Played as vPLAYED, ");
            //outStr.Append("tLOG.Won as vWON, ");
            //outStr.Append("tLOG.Drew as vDREW, ");
            //outStr.Append("tLOG.Lost as vLOST, ");
            //outStr.Append("tLOG.PointsFor as vPOINTSFOR, ");
            //outStr.Append("tLOG.PointsAgainst as vPOINTSAGAINST, ");
            //outStr.Append("tLOG.TriesFor as vTRIESFOR, ");
            //outStr.Append("tLOG.TriesAgainst as vTRIESAGAINST, ");
            //outStr.Append("tLOG.LossBonus as vLOSSBONUS, ");
            //outStr.Append("tLOG.TriesBonus as vTRIESBONUS, ");
            //outStr.Append("tLOG.LogPoints as vLOGPOINTS, ");
            //outStr.Append("tLEA.Name as vLEAGUENAME ");
            //outStr.Append("FROM Rugby.dbo.Logs tLOG ");
            //outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague tTEA ON tTEA.Id = tLOG.TeamId ");
            //outStr.Append("INNER JOIN Rugby.dbo.Groups tGRO ON tGRO.Id = tTEA.GroupId ");
            //outStr.Append("INNER JOIN Rugby.dbo.Leagues tLEA ON tLEA.Id = tLOG.LeagueId ");
            //outStr.Append("WHERE (tLOG.LeagueId = " + leagueID + ") ");
            //outStr.Append("ORDER BY tLOG.LogPoints DESC, tLOG.PointsFor - tLOG.PointsAgainst DESC, tLOG.TriesFor DESC, tTEA.Name ASC");
			
            //query = outStr.ToString();

            ////func.stop(query);

            //func.sqlQuery.CommandText = query;
            //func.dbtbl = func.sqlQuery.ExecuteReader();

            //if (func.dbtbl.HasRows)
            //{
            //    while (func.dbtbl.Read())
            //    {
            //        logItem item = new logItem();

            //        item.team = func.dbtbl["vTEAM"].ToString();
            //        item.teamsn = func.dbtbl["vTEAMSHORT"].ToString();
            //        item.played = Convert.ToInt32(func.dbtbl["vPLAYED"]);
            //        item.won = Convert.ToInt32(func.dbtbl["vWON"]);
            //        item.drew = Convert.ToInt32(func.dbtbl["vDREW"]);
            //        item.lost = Convert.ToInt32(func.dbtbl["vLOST"]);
            //        item.pointsfor = Convert.ToInt32(func.dbtbl["vPOINTSFOR"]);
            //        item.pointsagainst = Convert.ToInt32(func.dbtbl["vPOINTSAGAINST"]);
            //        item.triesfor = Convert.ToInt32(func.dbtbl["vTRIESFOR"]);
            //        item.triesagainst = Convert.ToInt32(func.dbtbl["vTRIESAGAINST"]);
            //        item.lossbonus = Convert.ToInt32(func.dbtbl["vLOSSBONUS"]);
            //        item.triesbonus = Convert.ToInt32(func.dbtbl["vTRIESBONUS"]);
            //        item.logpoints = Convert.ToInt32(func.dbtbl["vLOGPOINTS"]);
            //        item.group = "Combined Log";
            //        item.leaguename = func.dbtbl["vLEAGUENAME"].ToString();

            //        logItemList.Add(item);
            //    }
            //}
            //else
            //{
            //    logItemList = null;
            //}
            //func.dbtbl.Close();
            //}

            

            cache_provider.addObject(logItemList, cache_keys.LOGS_KEY + leagueID + "_" + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return logItemList;
    }
}



//public class logItem_list
//{
//    public logItem_list()
//    {
//    }

//    public List<logItem> getLogItemList(int numRecords, String category, int leagueID)
//    {
//        func func = new func();

//        func.setDbConn(1);
//        List<logItem> logItemList = null; // new List<logItem>();

//        logItemList = (List<logItem>)cache_provider.getObject(cache_keys.LOGS_KEY + leagueID + "_" + numRecords);

//        if (logItemList == null)
//        {
//            logItemList = new List<logItem>();

//            StringBuilder outStr = new StringBuilder();

//            outStr.Append("SELECT ");
//            outStr.Append("tTEA.Name As vTEAM, ");
//            outStr.Append("tTEA.ShortName as vTEAMSHORT, ");
//            outStr.Append("tTEA.MobileName As vMOBILE, ");
//            outStr.Append("tLOG.Played as vPLAYED, ");
//            outStr.Append("tLOG.Won as vWON, ");
//            outStr.Append("tLOG.Drew as vDREW, ");
//            outStr.Append("tLOG.Lost as vLOST, ");
//            outStr.Append("tLOG.PointsFor as vPOINTSFOR, ");
//            outStr.Append("tLOG.PointsAgainst as vPOINTSAGAINST, ");
//            outStr.Append("tLOG.TriesFor as vTRIESFOR, ");
//            outStr.Append("tLOG.TriesAgainst as vTRIESAGAINST, ");
//            outStr.Append("tLOG.LossBonus as vLOSSBONUS, ");
//            outStr.Append("tLOG.TriesBonus as vTRIESBONUS, ");
//            outStr.Append("tLOG.LogPoints as vLOGPOINTS, ");
//            outStr.Append("tLEA.Name as vLEAGUENAME, ");
//            outStr.Append("tGRO.Name As vGROUP ");
//            outStr.Append("FROM Rugby.dbo.Logs tLOG ");
//            outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague tTEA ON tTEA.Id = tLOG.TeamId ");
//            outStr.Append("INNER JOIN Rugby.dbo.Groups tGRO ON tGRO.Id = tTEA.GroupId ");
//            outStr.Append("INNER JOIN Rugby.dbo.Leagues tLEA ON tLEA.Id = tLOG.LeagueId ");
//            outStr.Append("WHERE (tLOG.LeagueId = " + leagueID + ") ");
//            outStr.Append("ORDER BY tGRO.Name, tLOG.LogPoints DESC, tLOG.PointsFor - tLOG.PointsAgainst DESC, tLOG.TriesFor DESC, tTEA.Name ASC");

//            String query = outStr.ToString();

//            //func.stop(query);

//            func.sqlQuery.CommandText = query;
//            func.dbtbl = func.sqlQuery.ExecuteReader();

//            if (func.dbtbl.HasRows)
//            {
//                while (func.dbtbl.Read())
//                {
//                    logItem item = new logItem();

//                    item.team = func.dbtbl["vTEAM"].ToString();
//                    item.teamsn = func.dbtbl["vTEAMSHORT"].ToString();
//                    item.played = Convert.ToInt32(func.dbtbl["vPLAYED"]);
//                    item.won = Convert.ToInt32(func.dbtbl["vWON"]);
//                    item.drew = Convert.ToInt32(func.dbtbl["vDREW"]);
//                    item.lost = Convert.ToInt32(func.dbtbl["vLOST"]);
//                    item.pointsfor = Convert.ToInt32(func.dbtbl["vPOINTSFOR"]);
//                    item.pointsagainst = Convert.ToInt32(func.dbtbl["vPOINTSAGAINST"]);
//                    item.triesfor = Convert.ToInt32(func.dbtbl["vTRIESFOR"]);
//                    item.triesagainst = Convert.ToInt32(func.dbtbl["vTRIESAGAINST"]);
//                    item.lossbonus = Convert.ToInt32(func.dbtbl["vLOSSBONUS"]);
//                    item.triesbonus = Convert.ToInt32(func.dbtbl["vTRIESBONUS"]);
//                    item.logpoints = Convert.ToInt32(func.dbtbl["vLOGPOINTS"]);
//                    item.group = func.dbtbl["vGROUP"].ToString();
//                    item.leaguename = func.dbtbl["vLEAGUENAME"].ToString();

//                    logItemList.Add(item);
//                }
//            }
//            else
//            {
//                logItemList = null;
//            }

//            func.dbtbl.Close();

//            if (leagueID == 795)
//            {
//                outStr = new StringBuilder();
//                outStr.Append("SELECT ");
//                outStr.Append("tTEA.Name As vTEAM, ");
//                outStr.Append("tTEA.ShortName as vTEAMSHORT, ");
//                outStr.Append("tTEA.MobileName As vMOBILE, ");
//                outStr.Append("tLOG.Played as vPLAYED, ");
//                outStr.Append("tLOG.Won as vWON, ");
//                outStr.Append("tLOG.Drew as vDREW, ");
//                outStr.Append("tLOG.Lost as vLOST, ");
//                outStr.Append("tLOG.PointsFor as vPOINTSFOR, ");
//                outStr.Append("tLOG.PointsAgainst as vPOINTSAGAINST, ");
//                outStr.Append("tLOG.TriesFor as vTRIESFOR, ");
//                outStr.Append("tLOG.TriesAgainst as vTRIESAGAINST, ");
//                outStr.Append("tLOG.LossBonus as vLOSSBONUS, ");
//                outStr.Append("tLOG.TriesBonus as vTRIESBONUS, ");
//                outStr.Append("tLOG.LogPoints as vLOGPOINTS, ");
//                outStr.Append("tLEA.Name as vLEAGUENAME ");
//                outStr.Append("FROM Rugby.dbo.Logs tLOG ");
//                outStr.Append("INNER JOIN Rugby.dbo.TeamsByLeague tTEA ON tTEA.Id = tLOG.TeamId ");
//                outStr.Append("INNER JOIN Rugby.dbo.Groups tGRO ON tGRO.Id = tTEA.GroupId ");
//                outStr.Append("INNER JOIN Rugby.dbo.Leagues tLEA ON tLEA.Id = tLOG.LeagueId ");
//                outStr.Append("WHERE (tLOG.LeagueId = " + leagueID + ") ");
//                outStr.Append("ORDER BY tLOG.LogPoints DESC, tLOG.PointsFor - tLOG.PointsAgainst DESC, tLOG.TriesFor DESC, tTEA.Name ASC");

//                query = outStr.ToString();

//                //func.stop(query);

//                func.sqlQuery.CommandText = query;
//                func.dbtbl = func.sqlQuery.ExecuteReader();

//                if (func.dbtbl.HasRows)
//                {
//                    while (func.dbtbl.Read())
//                    {
//                        logItem item = new logItem();

//                        item.team = func.dbtbl["vTEAM"].ToString();
//                        item.teamsn = func.dbtbl["vTEAMSHORT"].ToString();
//                        item.played = Convert.ToInt32(func.dbtbl["vPLAYED"]);
//                        item.won = Convert.ToInt32(func.dbtbl["vWON"]);
//                        item.drew = Convert.ToInt32(func.dbtbl["vDREW"]);
//                        item.lost = Convert.ToInt32(func.dbtbl["vLOST"]);
//                        item.pointsfor = Convert.ToInt32(func.dbtbl["vPOINTSFOR"]);
//                        item.pointsagainst = Convert.ToInt32(func.dbtbl["vPOINTSAGAINST"]);
//                        item.triesfor = Convert.ToInt32(func.dbtbl["vTRIESFOR"]);
//                        item.triesagainst = Convert.ToInt32(func.dbtbl["vTRIESAGAINST"]);
//                        item.lossbonus = Convert.ToInt32(func.dbtbl["vLOSSBONUS"]);
//                        item.triesbonus = Convert.ToInt32(func.dbtbl["vTRIESBONUS"]);
//                        item.logpoints = Convert.ToInt32(func.dbtbl["vLOGPOINTS"]);
//                        item.group = "Combined Log";
//                        item.leaguename = func.dbtbl["vLEAGUENAME"].ToString();

//                        logItemList.Add(item);
//                    }
//                }
//                else
//                {
//                    logItemList = null;
//                }
//                func.dbtbl.Close();
//            }



//            cache_provider.addObject(logItemList, cache_keys.LOGS_KEY + leagueID + "_" + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
//        }

//        func.setDbConn(0);
//        return logItemList;
//    }
//}