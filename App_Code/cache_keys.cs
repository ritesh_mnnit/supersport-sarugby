﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class cache_keys
{
    public const string MAINMENU_KEY = "MAINMENU_";
    public const string FIXTURES_KEY = "FIXTURES_";
    public const string RESULTS_KEY = "RESULTS_";
    public const string LOGS_KEY = "LOGS_";
    public const string CAPS_KEY = "CAPS_";
    public const string CARDS_KEY = "CARDS_";
    public const string MATCHOFFICIALS_KEY = "MATCHOFFICIALS_";
    public const string TEAMSCORINGA_KEY = "TEAMSCORINGA_";
    public const string TEAMSCORINGB_KEY = "TEAMSCORINGB_";
    public const string MATCHLINEUP_KEY = "MATCHLINEUP_";
    public const string MATCHRESERVES_KEY = "MATCHRESERVES_";
    public const string MATCHREPLACEMENTS_KEY = "MATCHREPLACEMENTS_";
    public const string MATCHRUNOFPLAY_KEY = "MATCHRUNOFPLAY_";
    public const string VIDEOITEMS_KEY = "VIDEOITEMS_";
    public const string PHOTOITEMS_KEY = "PHOTOITEMS_";
    public const string PHOTOGALLERYITEMS_KEY = "PHOTOGALLERYITEMS_";
    public const string NEWSLIST_KEY = "NEWSLIST_";
    public const string PLAYERSQUADS_KEY = "PLAYERSQUADS_";
    public const string POINTS_KEY = "POINTS_";
    public const string PROFILESSEARCH_KEY = "PROFILESSEARCH_";
    public const string SQUADS_KEY = "SQUADS_";
    public const string TEAMRESULTS_KEY = "TEAMRESULTS_";
    public const string TEAMFIXTURES_KEY = "TEAMFIXTURES_";
    public const string TRIES_KEY = "TRIES_";
}
