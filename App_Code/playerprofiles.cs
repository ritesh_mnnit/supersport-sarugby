﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public class playerwriteup
{
    public int count = 0;
    public String writeup { get; set; }

    public playerwriteup(int id)
	{
        func func = new func();
        func.setDbConn(1);

        StringBuilder outStr = new StringBuilder();
        String query = "";

        outStr.Append("select comments as vWRITEUP from rugby.dbo.Persons where ID=" + id);

        query = outStr.ToString();

        
        func.sqlQuery.CommandText = query;
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            count++;
            while (func.dbtbl.Read())
            {
                writeup = func.dbtbl["vWRITEUP"].ToString();
            }
        }

        func.dbtbl.Close();
        func.setDbConn(0);
	}
}

public class playersquadItem
{
    public int teamid { get; set; }
    public int leagueid { get; set; }
    public String leaguename { get; set; }
    public DateTime datestart { get; set; }
    public String team { get; set; }

    public playersquadItem()
    {
    }
}

public class playersquadItem_list
{
    public playersquadItem_list()
    {
    }

    public List<playersquadItem> getPlayerSquadItemList(int playerID)
    {
        func func = new func();
        func.setDbConn(1);

        List<playersquadItem> playersquadItemList = null; // new List<playersquadItem>();

        playersquadItemList = (List<playersquadItem>)cache_provider.getObject(cache_keys.PLAYERSQUADS_KEY + playerID);

        if (playersquadItemList == null)
        {
            playersquadItemList = new List<playersquadItem>();

            StringBuilder outStr = new StringBuilder();

            outStr.Append("SELECT ");
            outStr.Append("a.Id AS vLEAGUEID, ");
            outStr.Append("a.Name AS vLEAGUENAME, ");
            outStr.Append("a.StartDate as vDATESTART, ");
            outStr.Append("b.Id As vTEAMID, ");
            outStr.Append("b.Name AS vTEAMNAME ");
            outStr.Append("FROM rugby.dbo.Leagues a ");
            outStr.Append("INNER JOIN rugby.dbo.TeamsByLeague b ON b.LeagueID = a.Id ");
            outStr.Append("INNER JOIN rugby.dbo.Squads c ON c.TeamId = b.Id ");
            outStr.Append("WHERE (c.PersonID = " + playerID + ") ");
            outStr.Append("ORDER BY a.StartDate DESC");

            String query = outStr.ToString();

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    playersquadItem item = new playersquadItem();

                    item.teamid = Convert.ToInt32(func.dbtbl["vTEAMID"]);
                    item.leagueid = Convert.ToInt32(func.dbtbl["vLEAGUEID"]);
                    item.leaguename = func.dbtbl["vLEAGUENAME"].ToString();
                    item.team = func.dbtbl["vTEAMNAME"].ToString();
                    item.datestart = Convert.ToDateTime(func.dbtbl["vDATESTART"]);

                    playersquadItemList.Add(item);
                }
            }
            else
            {
                playersquadItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(playersquadItemList, cache_keys.PLAYERSQUADS_KEY + playerID, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return playersquadItemList;
    }
}

public class personaldetails
{
    public int count = 0;
    public String image;
    public String fullname;
    public String surname;
    public String nickname;
    public String displayname;
    public String height;
    public String weight;
    public DateTime birthdate;
    public String birthplace;
    public String maritialstatus;
    public String favouritefilm;
    public String favouritemusic;
    public String favouritefood;
    public String primaryschool;
    public String secondaryschool;
    public String tertiaryeducation;

    public personaldetails(int id)
	{
        func func = new func();
        func.setDbConn(1);
        StringBuilder outStr = new StringBuilder();
        String query = "";

        outStr.Append("select ");
        outStr.Append("tPER.LargeImage as vIMAGE, ");
        outStr.Append("tPER.FullName as vFULLNAME, ");
        outStr.Append("tPER.Surname as vSURNAME, ");
        outStr.Append("tPER.NickName as vNICKNAME, ");
        outStr.Append("tPER.Displayname as vDISPLAYNAME, ");
        outStr.Append("tPER.Height as vHEIGHT, ");
        outStr.Append("tPER.Weight as vWEIGHT, ");
        outStr.Append("tPER.BirthDate as vBIRTHDATE, ");
        outStr.Append("tPER.BirthPlace as vBIRTHPLACE, ");
        outStr.Append("tPER.MaritalStatus as vMARITALSTATUS, ");
        outStr.Append("tPER.FavFilm as vFAVFILM, ");
        outStr.Append("tPER.FavMusic as vFAVMUSIC, ");
        outStr.Append("tPER.FavFood as vFAVFOOD, ");
        outStr.Append("tPER.PrimarySchool as vPRIMARYSCHOOL, ");
        outStr.Append("tPER.SecondarySchool as vSECONDARYSCHOOL, ");
        outStr.Append("tPER.Tertiary as vTERTIARY ");
        outStr.Append("from rugby.dbo.persons tPER ");
        outStr.Append("where ID=" + id);

        query = outStr.ToString();

        
        func.sqlQuery.CommandText = query;
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            count++;
            while (func.dbtbl.Read())
            {
                image = func.dbtbl["vIMAGE"].ToString();
                fullname = func.dbtbl["vFULLNAME"].ToString();
                surname = func.dbtbl["vSURNAME"].ToString();
                nickname = func.dbtbl["vNICKNAME"].ToString();
                displayname = func.dbtbl["vDISPLAYNAME"].ToString();
                height = func.dbtbl["vHEIGHT"].ToString();
                weight = func.dbtbl["vWEIGHT"].ToString();
                birthdate = Convert.ToDateTime(func.dbtbl["vBIRTHDATE"]);
                birthplace = func.dbtbl["vBIRTHPLACE"].ToString();
                maritialstatus = func.dbtbl["vMARITALSTATUS"].ToString();
                favouritefilm = func.dbtbl["vFAVFILM"].ToString();
                favouritemusic = func.dbtbl["vFAVMUSIC"].ToString();
                favouritefood = func.dbtbl["vFAVFOOD"].ToString();
                primaryschool = func.dbtbl["vPRIMARYSCHOOL"].ToString();
                secondaryschool = func.dbtbl["vSECONDARYSCHOOL"].ToString();
                tertiaryeducation = func.dbtbl["vTERTIARY"].ToString();
            }
        }

        func.dbtbl.Close();
        func.setDbConn(0);
	}
}

public class rugbycareer
{
    public int count = 0;
    public String position;
    public String province;
    public int tries;
    public int conversions;
    public int penalties;
    public int dropgoals;
    public int totalpoints;

    public rugbycareer(int id)
	{
        func func = new func();
        func.setDbConn(1);
        StringBuilder outStr = new StringBuilder();
        String query = "";

        outStr.Append("Select ");
        outStr.Append("Count(Case WHEN a.EventId = 2 then 1 end) AS vTRIES, ");
        outStr.Append("Count(Case WHEN a.EventId = 3 then 1 end) AS vCONVERSIONS, ");
        outStr.Append("Count(Case WHEN a.EventId = 4 then 1 end) AS vPENALTIES, ");
        outStr.Append("Count(Case WHEN a.EventId = 5 then 1 end) AS vDROPGOALS, ");

        outStr.Append("(select position from Rugby.dbo.Persons where ID="+ id +") as vPOSITION, ");

        outStr.Append("(select CurrentProvince from Rugby.dbo.Persons where ID=" + id + ") as vPROVINCE ");

        outStr.Append("From Rugby.dbo.Commentary a ");
        outStr.Append("INNER JOIN Rugby.dbo.Matches b ON b.Id = a.MatchId ");

        outStr.Append("Where (a.Player1Id = "+ id +") AND (a.EventId > 1) ");
        outStr.Append("Group By a.Player1Id");

        //func.stop(outStr.ToString());

        query = outStr.ToString();

        
        func.sqlQuery.CommandText = query;
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            count++;
            while (func.dbtbl.Read())
            {
                position = func.dbtbl["vPOSITION"].ToString();
                province = func.dbtbl["vPROVINCE"].ToString();

                tries = Convert.ToInt32(func.dbtbl["vTRIES"]);
                conversions = Convert.ToInt32(func.dbtbl["vCONVERSIONS"]);
                penalties = Convert.ToInt32(func.dbtbl["vPENALTIES"]);
                dropgoals = Convert.ToInt32(func.dbtbl["vDROPGOALS"]);

                totalpoints = ((tries * 5) + (conversions * 2) +  (penalties * 3) + (dropgoals * 3));
            }
        }

        func.dbtbl.Close();
        func.setDbConn(0);
	}
}

public class springbokcareer
{
    public String debut;
    //public int caps;
    public int tries;
    public int conversions;
    public int penalties;
    public int dropgoals;
    public int testpoints;

    public springbokcareer(int id)
	{
        func func = new func();
        func.setDbConn(1);
        StringBuilder outStr = new StringBuilder();
        String query = "";

        query+="Select ";
        query += "Count(Case WHEN a.EventId = 2 then 1 end) AS Tries, ";
        query += "Count(Case WHEN a.EventId = 3 then 1 end) AS Conversions, ";
        query += "Count(Case WHEN a.EventId = 4 then 1 end) AS Penalties, ";
        query += "Count(Case WHEN a.EventId = 5 then 1 end) AS DropGoals ";
        query += "From Rugby.dbo.Commentary a ";
        query += "INNER JOIN Rugby.dbo.Matches b ON b.Id = a.MatchId ";
        query += "Where (a.Player1Id = " + id + ") AND (a.EventId > 1) ";
        query += "AND (a.MatchID IN (SELECT DISTINCT a.Id As MatchId ";
        query += "From Rugby.dbo.Matches AS a ";
        query += "INNER JOIN Rugby.dbo.TeamsByLeague AS b ON b.Id = a.TeamAID ";
        query += "INNER JOIN Rugby.dbo.TeamsByLeague AS c ON c.Id = a.TeamBID ";
        query += "INNER JOIN Rugby.dbo.Leagues AS d ON d.ID = a.LeagueID ";
        query += "INNER JOIN Rugby.dbo.VenuesByLeague AS e ON e.Id = a.VenueID ";
        query += "INNER JOIN Rugby.dbo.LogDetailsPerMatch AS f ON f.MatchId = a.ID AND a.TeamAID = f.TeamID ";
        query += "INNER JOIN Rugby.dbo.Tournaments AS g ON g.ID = d.TournamentID ";
        query += "WHERE (a.Completed = 1) AND (g.CategoryID = 2) ";
        query += "And (d.TournamentId <> 63) ";
        query += "And ((SELECT CASE WHEN b.ShortName = '' THEN b.Name ELSE b.ShortName END) like '%South Africa%' OR (SELECT CASE WHEN c.ShortName = '' THEN c.Name ELSE c.ShortName END) like '%South Africa%'))) ";
        query += "Group By a.Player1Id";

        //func.stop(query);

        
        func.sqlQuery.CommandText = query;
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            while (func.dbtbl.Read())
            {
                //caps = Convert.ToInt32(func.dbtbl["caps"]);
                tries = Convert.ToInt32(func.dbtbl["tries"]);
                conversions = Convert.ToInt32(func.dbtbl["conversions"]);
                penalties = Convert.ToInt32(func.dbtbl["penalties"]);
                dropgoals = Convert.ToInt32(func.dbtbl["dropgoals"]);

                testpoints = ((tries * 5) + (conversions * 2) + (penalties * 3) + (dropgoals * 3));
            }
        }

        func.dbtbl.Close();
        func.setDbConn(0);
	}
}

public class testrecordItem
{
    public String team { get; set; }
    public int played { get; set; }
    public int won { get; set; }
    public int drew { get; set; }
    public int lost { get; set; }
    public int tries { get; set; }
    public int conversions { get; set; }
    public int penalties { get; set; }
    public int dropgoals { get; set; }
    public int points { get; set; }

    public testrecordItem()
    {
    }
}

public class testrecordItem_list
{
    public testrecordItem_list()
    {
    }

    public List<testrecordItem> getTestrecordItemList(String player)
    {
        func func = new func();
        func.setDbConn(1);
        List<testrecordItem> testrecordItemList = new List<testrecordItem>();

        StringBuilder outStr = new StringBuilder();

        outStr.Append("select ");
        outStr.Append("count(b.result) as Played, ");
        outStr.Append("SUM(a.tries) AS Tries, ");
        outStr.Append("SUM(a.convs) as Convs, ");
        outStr.Append("SUM(a.pens) as Pens, ");
        outStr.Append("SUM(a.drops) as Drops, ");
        outStr.Append("SUM(a.total) as Total, ");
        outStr.Append("b.opponent as Opponent ,");
        outStr.Append("Count(Case WHEN b.result = 'WON' then 1 end) as Won, ");
        outStr.Append("Count(Case WHEN b.result = 'LOST' then 1 end) as Lost, ");
        outStr.Append("Count(Case WHEN b.result = 'DREW' then 1 end) as Drew ");
        outStr.Append("from sszgeneral.dbo.rugbystats_playerspertest a ");
        outStr.Append("inner join sszgeneral.dbo.rugbystats_testmatches b on b.matchdate = a.matchdate ");
        outStr.Append("where comname = '" + player + "' ");
        outStr.Append("group by b.opponent");

        String query = outStr.ToString();

        
        func.sqlQuery.CommandText = query;
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            while (func.dbtbl.Read())
            {
                testrecordItem item = new testrecordItem();

                item.team = func.dbtbl["Opponent"].ToString();
                item.played = Convert.ToInt32(func.dbtbl["Played"]);
                item.won = Convert.ToInt32(func.dbtbl["Won"]);
                item.drew = Convert.ToInt32(func.dbtbl["Drew"]);
                item.lost = Convert.ToInt32(func.dbtbl["Lost"]);
                item.tries = Convert.ToInt32(func.dbtbl["Tries"]);
                item.conversions = Convert.ToInt32(func.dbtbl["Convs"]);
                item.penalties = Convert.ToInt32(func.dbtbl["Pens"]);
                item.dropgoals = Convert.ToInt32(func.dbtbl["Drops"]);
                item.points = Convert.ToInt32(func.dbtbl["Total"]);

                testrecordItemList.Add(item);
            }
        }
        else
        {
            testrecordItemList = null;
        }

        func.dbtbl.Close();
        func.setDbConn(0);

        return testrecordItemList;
    }
}