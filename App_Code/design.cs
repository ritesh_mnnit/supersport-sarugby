﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public class design
{
	public design()
	{
	}

    public static String buildHeading(String label, int size, String urlLabel, String backURL, bool underline)
    {
        String retStr = "";
        String headClass = "heading";

        if (!underline) headClass = "heading_noline";
        
//        retStr = "<div class=\"heading txt_green_20\">"+ text +"</div>";


        retStr += "<div class=\""+ headClass +"\" style=\"float:left; width:100%\">";
        retStr += "    <div class=\"txt_green_"+ size +"\" style=\"float:left;\">" + label + "</div>";
        retStr += "    <div class=\"txt_green_10\" style=\"float:right; line-height:20px\"><strong><em><a href=\""+ backURL +"\">"+ urlLabel +"</a></em></strong></div>";
        retStr += "</div>";
        retStr += "<div style=\"clear:both;\"></div>";

        return retStr;
    }

    public static void buildYears(DropDownList dropField)
    {
        int startYear = DateTime.Now.AddMonths(-1).Year;

        for (int i = 0; i < 3; i++)
        {
            dropField.Items.Insert(i, new ListItem("" + (startYear + i).ToString() + "", "" + (startYear + i).ToString() + ""));
        }

        dropField.Items.Insert(0, new ListItem("", ""));
    }

    public static void buildMonths(DropDownList dropField)
    {
        for (int i = 0; i < 12; i++)
        {
            dropField.Items.Insert(i, new ListItem("" + Convert.ToDateTime(("1900-" + (i + 1).ToString() + "-01")).ToString("MMMM") + "", "" + (i + 1).ToString().PadLeft(2, '0') + ""));
        }

        dropField.Items.Insert(0, new ListItem("", ""));
    }

    public static void buildDays(DropDownList dropField)
    {
        for (int i = 0; i < 31; i++)
        {
            dropField.Items.Insert(i, new ListItem("" + (i + 1).ToString().PadLeft(2, '0') + "", "" + (i + 1).ToString().PadLeft(2, '0') + ""));
        }

        dropField.Items.Insert(0, new ListItem("", ""));
    }

    public static String makeSpace(int height, String bgCol, bool clear)
    {
        String spaceDiv = "";
        String clearVal = "none";
        if (clear == true)
        {
            clearVal = "both";
        }
        spaceDiv += "<div style=\"height:" + height + "px; background-color:" + bgCol + "; clear:" + clearVal + ";\"></div>";
        return spaceDiv;
    }

    public static String clearBoth()
    {
        String clearDiv = "";
        clearDiv += "<div style=\"clear:both;\"></div>";
        return clearDiv;
    }
}
