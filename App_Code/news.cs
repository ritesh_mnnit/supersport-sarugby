﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Runtime.Serialization;

[Serializable]

public class newsItem
{
    public int id { get; set; }
    public DateTime date { get; set; }
    public String headline { get; set; }
    public String blurb { get; set; }
    public String body { get; set; }
    public String author { get; set; }
    public String credit { get; set; }
    public String imageLarge { get; set; }
    public String imageLargeAlt { get; set; }
    public String imageSmall { get; set; }
    public String imageSmallAlt { get; set; }
    public String image3 { get; set; }
    public String image3Alt { get; set; }
    public String catName { get; set; }
    public String cat { get; set; }

	public newsItem()
	{
	}
}

public class newsArticle
{
    public newsArticle()
    {
    }

    public newsItem getArticle(int id)
    {
        func func = new func();
        func.setDbConn(1);
        newsItem item = new newsItem();

        StringBuilder outStr = new StringBuilder();

        outStr.Append("select ");
        outStr.Append("tART.Id as vID, ");
        outStr.Append("tART.Headline as vHEADLINE, ");
        outStr.Append("tART.Blurb as vBLURB, ");
        outStr.Append("tART.Body as vBODY, ");
        outStr.Append("tART.SmallImage As vIMGSMALL, ");
        outStr.Append("tART.SmallImageAlt As vIMGSMALLALT, ");
        outStr.Append("tART.LargeImage As vIMGLARGE, ");
        outStr.Append("tART.LargeImageAlt As vIMGLARGEALT, ");
        outStr.Append("tART.Image3 As vIMG3, ");
        outStr.Append("tART.Image3Alt As vIMG3ALT, ");
        outStr.Append("tCAT.DisplayName As vCATEGORY, ");
        outStr.Append("tCAT.Name As vCAT, ");
        outStr.Append("tAUT.Name As vAUTHOR, ");
        outStr.Append("tCRE.Name As vCREDIT, ");
        outStr.Append("tCID.ArticleDate as vDATE ");
        outStr.Append("From SuperSportZone.dbo.ZoneArticles tART ");
        outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneAuthors tAUT ON tAUT.Id = tART.Author ");
        outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneCredits tCRE ON tCRE.Id = tART.Credit ");
        outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneArticleCategories tCID ON tCID.ArticleId = tART.Id ");
        outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneCategories tCAT ON tCAT.Id = tCID.Category ");
        outStr.Append("where tART.Id='" + id + "' ");

        
        func.sqlQuery.CommandText = outStr.ToString();
        func.dbtbl = func.sqlQuery.ExecuteReader();

        if (func.dbtbl.HasRows)
        {
            while (func.dbtbl.Read())
            {
                item.id = Convert.ToInt32(func.dbtbl["vID"]);

                item.date = Convert.ToDateTime(func.dbtbl["vDATE"]);

                item.headline = func.dbtbl["vHEADLINE"].ToString();
                item.blurb = func.dbtbl["vBLURB"].ToString();
                item.body = func.dbtbl["vBODY"].ToString();

                item.author = func.dbtbl["vAUTHOR"].ToString();
                item.credit = func.dbtbl["vCREDIT"].ToString();

                item.image3 = func.dbtbl["vIMG3"].ToString();
                item.image3Alt = func.dbtbl["vIMG3ALT"].ToString();

                item.imageLarge = func.dbtbl["vIMGLARGE"].ToString();
                item.imageLargeAlt = func.dbtbl["vIMGLARGEALT"].ToString();

                item.imageSmall = func.dbtbl["vIMGSMALL"].ToString();
                item.imageSmallAlt = func.dbtbl["vIMGSMALLALT"].ToString();

                item.catName = func.dbtbl["vCATEGORY"].ToString();
                item.cat = func.dbtbl["vCAT"].ToString();
            }
        }

        func.dbtbl.Close();
        func.setDbConn(0);

        return item;
    }
}


public class newsItem_list
{
    public newsItem_list()
    {
    }

    public List<newsItem> getNewsItemList(int numRecords, String category, bool isHome, String callingPage)
    {
        func func = new func();
        func.setDbConn(1);
        List<newsItem> newsItemList = null; // new List<newsItem>();

        newsItemList = (List<newsItem>)cache_provider.getObject(cache_keys.NEWSLIST_KEY + isHome + "_" + category + "_" + callingPage + "_" + numRecords);

        if (newsItemList == null)
        {
            newsItemList = new List<newsItem>();

            StringBuilder outStr = new StringBuilder();

            if (isHome == true)
            {
                if ((callingPage == "default.aspx")||(callingPage == ""))
                {
                    outStr.Append("Select top " + numRecords + " ");
                    outStr.Append("a.Id as vID, ");
                    outStr.Append("a.Headline as vHEADLINE, ");
                    outStr.Append("a.blurb as vBLURB, ");
                    outStr.Append("b.Body as vBODY, ");
                    outStr.Append("b.SmallImage as vIMGSMALL, ");
                    outStr.Append("b.LargeImage as vIMGLARGE, ");
                    outStr.Append("b.SmallImageAlt As vIMGSMALLALT, ");
                    outStr.Append("b.LargeImageAlt As vIMGLARGEALT, ");
                    outStr.Append("b.Image3 As vIMG3, ");
                    outStr.Append("b.Image3Alt As vIMG3ALT, ");
                    outStr.Append("d.DisplayName As vCATEGORY, ");
                    outStr.Append("d.Name As vCAT, ");
                    outStr.Append("b.Created AS vDATE, ");
                    outStr.Append("b.Author As vAUTHOR, ");
                    outStr.Append("c.Name As vCREDIT, ");
                    outStr.Append("e.Name As vSITE, ");
                    outStr.Append("e.Id As vSITEID, ");
                    outStr.Append("f.SportName as vSPORT ");
                    outStr.Append("From ZoneTopStories a ");
                    outStr.Append("INNER JOIN ZoneArticles b ON b.Id = a.Id ");
                    outStr.Append("INNER JOIN ZoneCredits c ON c.Id = b.Credit ");
                    outStr.Append("INNER JOIN ZoneCategories d ON d.Id = a.Category ");
                    outStr.Append("INNER JOIN ZoneSites e ON e.Id = d.Site ");
                    outStr.Append("INNER JOIN ZoneSports f ON f.Id = e.Sport ");
                    outStr.Append("Where (a.Site = 16) And (b.WebOnly = 0) And (b.Active = 1) And (b.Utilised = 1) ");
                    outStr.Append("Order By a.Pos");
                }
                else
                {
                    outStr.Append("select top " + numRecords + " ");
                    outStr.Append("tART.Id as vID, ");
                    outStr.Append("tART.Headline as vHEADLINE, ");
                    outStr.Append("tART.Blurb as vBLURB, ");
                    outStr.Append("tART.Body as vBODY, ");
                    outStr.Append("tART.SmallImage As vIMGSMALL, ");
                    outStr.Append("tART.SmallImageAlt As vIMGSMALLALT, ");
                    outStr.Append("tART.LargeImage As vIMGLARGE, ");
                    outStr.Append("tART.LargeImageAlt As vIMGLARGEALT, ");
                    outStr.Append("tART.Image3 As vIMG3, ");
                    outStr.Append("tART.Image3Alt As vIMG3ALT, ");
                    outStr.Append("tCAT.DisplayName As vCATEGORY, ");
                    outStr.Append("tCAT.Name As vCAT, ");
                    outStr.Append("tAUT.Name As vAUTHOR, ");
                    outStr.Append("tCRE.Name As vCREDIT, ");
                    outStr.Append("tCID.ArticleDate as vDATE ");
                    outStr.Append("From SuperSportZone.dbo.ZoneArticles tART ");
                    outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneAuthors tAUT ON tAUT.Id = tART.Author ");
                    outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneCredits tCRE ON tCRE.Id = tART.Credit ");
                    outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneArticleCategories tCID ON tCID.ArticleId = tART.Id ");
                    outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneCategories tCAT ON tCAT.Id = tCID.Category ");
                    outStr.Append("where tCAT.Name like 'sarugby%' ");
                    outStr.Append("and not tCAT.Name like 'sarugby/pressrelease' ");
                    outStr.Append("and tART.Active=1 ");
                    outStr.Append("order by tART.Created desc");
                }

            }
            else
            {
                outStr.Append("select top " + numRecords + " ");
                outStr.Append("tART.Id as vID, ");
                outStr.Append("tART.Headline as vHEADLINE, ");
                outStr.Append("tART.Blurb as vBLURB, ");
                outStr.Append("tART.Body as vBODY, ");
                outStr.Append("tART.SmallImage As vIMGSMALL, ");
                outStr.Append("tART.SmallImageAlt As vIMGSMALLALT, ");
                outStr.Append("tART.LargeImage As vIMGLARGE, ");
                outStr.Append("tART.LargeImageAlt As vIMGLARGEALT, ");
                outStr.Append("tART.Image3 As vIMG3, ");
                outStr.Append("tART.Image3Alt As vIMG3ALT, ");
                outStr.Append("tCAT.DisplayName As vCATEGORY, ");
                outStr.Append("tCAT.Name As vCAT, ");
                outStr.Append("tAUT.Name As vAUTHOR, ");
                outStr.Append("tCRE.Name As vCREDIT, ");
                outStr.Append("tCID.ArticleDate as vDATE ");
                outStr.Append("From SuperSportZone.dbo.ZoneArticles tART ");
                outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneAuthors tAUT ON tAUT.Id = tART.Author ");
                outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneCredits tCRE ON tCRE.Id = tART.Credit ");
                outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneArticleCategories tCID ON tCID.ArticleId = tART.Id ");
                outStr.Append("INNER JOIN SuperSportZone.dbo.ZoneCategories tCAT ON tCAT.Id = tCID.Category ");
                outStr.Append("where tCAT.Name='" + category + "' ");
                outStr.Append("and tART.Active=1 ");
                outStr.Append("order by tART.Created desc");
            }

            String query = outStr.ToString();

            
            func.sqlQuery.CommandText = query;
            func.dbtbl = func.sqlQuery.ExecuteReader();

            if (func.dbtbl.HasRows)
            {
                while (func.dbtbl.Read())
                {
                    newsItem item = new newsItem();

                    item.id = Convert.ToInt32(func.dbtbl["vID"]);

                    item.date = Convert.ToDateTime(func.dbtbl["vDATE"]);

                    item.headline = func.dbtbl["vHEADLINE"].ToString();
                    item.blurb = func.dbtbl["vBLURB"].ToString();
                    item.body = func.dbtbl["vBODY"].ToString();

                    item.author = func.dbtbl["vAUTHOR"].ToString();
                    item.credit = func.dbtbl["vCREDIT"].ToString();

                    item.image3 = func.dbtbl["vIMG3"].ToString();
                    item.image3Alt = func.dbtbl["vIMG3ALT"].ToString();

                    item.imageLarge = func.dbtbl["vIMGLARGE"].ToString();
                    item.imageLargeAlt = func.dbtbl["vIMGLARGEALT"].ToString();

                    item.imageSmall = func.dbtbl["vIMGSMALL"].ToString();
                    item.imageSmallAlt = func.dbtbl["vIMGSMALLALT"].ToString();

                    item.catName = func.dbtbl["vCATEGORY"].ToString();
                    item.cat = func.dbtbl["vCAT"].ToString();

                    newsItemList.Add(item);
                }
            }
            else
            {
                newsItemList = null;
            }

            func.dbtbl.Close();
            

            cache_provider.addObject(newsItemList, cache_keys.NEWSLIST_KEY + isHome + "_" + category + "_" + callingPage + "_" + numRecords, Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpire"]));
        }

        func.setDbConn(0);
        return newsItemList;
    }
}