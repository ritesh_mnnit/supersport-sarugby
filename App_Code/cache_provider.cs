﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MemcachedProviders.Cache;
using System.Configuration;

/// <summary>
/// Summary description for cache_provider
/// </summary>
/// 
public class cache_provider
{
    private const string key_prefix = "SARU_";
    //private const bool cache_enabled = true;
    private const bool cache_enabled = true;

    public cache_provider()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void resetCache()
    {
        if (cache_enabled)
        {
            DistCache.RemoveAll();
        }
    }

    public static void removeObject(string key)
    {
        if (cache_enabled)
        {
            key = key_prefix + key;
            DistCache.Remove(key);
        }
    }

    public static void addObject(object obj, string key)
    {
        if (cache_enabled)
        {
            key = key_prefix + key;
            DistCache.Add(key, obj, true);
        }
    }

    public static void addObject(object obj, string key, int minutes)
    {
        if (cache_enabled)
        {
            key = key_prefix + key;
            TimeSpan tmpSpan = new TimeSpan(0, minutes, 0);
            DistCache.Add(key, obj, tmpSpan);
        }

//        func.write(key + "<br />");
    }

    public static object getObject(string key)
    {
        object obj = null;

        if (cache_enabled)
        {
            key = key_prefix + key;
            obj = (object)DistCache.Get(key);
        }

        return obj;
    }
}