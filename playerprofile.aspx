﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="playerprofile.aspx.cs" Inherits="playerprofile" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<%=design.buildHeading("PLAYER PROFILE",20,"<< SEARCH PLAYER PROFILES","profilesearch.aspx?category="+ reqCategory +"&leagueid=" + leagueID,true) %>

<%=design.makeSpace(10,"",true) %>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="135" align="left" valign="top">
                <asp:Literal ID="lit_image" runat="server"></asp:Literal>
            </td>
            
            <td width="20" align="left" valign="top">
                &nbsp;
            </td>
            
            <td align="left" valign="top">
                <asp:Literal ID="lit_personaldetails" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    
    <%=design.makeSpace(10,"",true) %>    
    
    <asp:Panel ID="pan_playerwriteup" runat="server">
        <asp:Literal ID="lit_playerwriteup" runat="server"></asp:Literal>
    </asp:Panel>

    <%=design.makeSpace(10,"",true) %>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        
            <asp:Panel ID="pan_rugbycareer" runat="server">
            
                <td align="left" valign="top">
                    <asp:Literal ID="lit_rugbycareer" runat="server"></asp:Literal>
                </td>
                
            </asp:Panel>
            
            <asp:Panel ID="pan_springbokcareer" runat="server">
            
                <td width="20" align="left" valign="top">
                    &nbsp;
                </td>
                
                <td align="left" valign="top">
                    <asp:Literal ID="lit_springbokcareer" runat="server"></asp:Literal>
                </td>
                    
            </asp:Panel>
            
        </tr>
    </table>
    


    <%=design.makeSpace(10,"",true) %>
    
    <asp:Panel ID="pan_testrecord" runat="server">
        <asp:Literal ID="lit_testrecord" runat="server"></asp:Literal>
    </asp:Panel> 

    <%=design.makeSpace(10,"",true) %>
    
    <asp:Panel ID="pan_squads" runat="server">
        <asp:Literal ID="lit_squads" runat="server"></asp:Literal>
    </asp:Panel>
   
</asp:Content>