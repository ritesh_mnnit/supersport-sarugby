﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class uc_mediapreview : System.Web.UI.UserControl
{
    private String reqCategory = "";

    private videoItem_list vidList = new videoItem_list();
    private List<videoItem> videoItemList = new List<videoItem>();

    private photogalleryItem_list phoList = new photogalleryItem_list();
    private List<photogalleryItem> photogalleryItemList = new List<photogalleryItem>();

    public int vidPgCnt = 0;
    private int vidCnt = 0;

    public int phoPgCnt = 0;
    private int phoCnt = 0;

    private StringBuilder outStrVid = new StringBuilder();
    private StringBuilder outStrPho = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            videoItemList = vidList.getVideoItemList(16, reqCategory);

            if (videoItemList != null)
            {
                rep_videos.DataSource = videoItemList;
                rep_videos.DataBind();

                outStrVid.Append("</div>");

                lit_videos.Text = outStrVid.ToString();
            }

            photogalleryItemList = phoList.getPhotogalleryItemList(16, reqCategory);

            if (photogalleryItemList != null)
            {
                rep_photogalleries.DataSource = photogalleryItemList;
                rep_photogalleries.DataBind();

                outStrPho.Append("</div>");

                lit_photogalleries.Text = outStrPho.ToString();
            }
        }
    }

    protected void rep_videos_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            videoItem item = (videoItem)e.Item.DataItem;

            String vidName = item.name;

            if (item.name.Length > 70)
            {
                vidName = item.name.Substring(0, 60);
            }

            if ((vidCnt % 4 == 0) || (outStrVid.Length == 0))
            {
                vidPgCnt++;

                if (outStrVid.Length > 0)
                {
                    outStrVid.Append("</div>");
                }

                if (vidPgCnt == 1)
                {
                    outStrVid.Append("<div id=\"vidPg_" + vidPgCnt + "\" style=\"display:block;\">");
                }
                else
                {
                    outStrVid.Append("<div id=\"vidPg_" + vidPgCnt + "\" style=\"display:none;\">");
                }
                
            }

            outStrVid.Append("<div class=\"wrap_media\">");
            outStrVid.Append("     <div class=\"media_date txt_gold_10\"><strong>" + item.date.ToString("yyyy-MM-dd") + "</strong></div>");
            outStrVid.Append("     <a href=\"javascript:void(0);\" onClick=\"window.open('http://bb.supersport.co.za/saru_video.asp?play="+ item.id +"','saruvideo','width=450,height=550,top=5,left=5,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizeable=yes');\"><img src=\"http://www.supersport.com/videos/images/" + item.image + "\" border=\"0\" height=\"103\" width=\"136\" /></a>");
            outStrVid.Append("     <div class=\"media_info txt_white_10\">" + vidName + "...</div>");
            outStrVid.Append("</div>");

            vidCnt++;
        }
    }

    protected void rep_photogalleries_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            photogalleryItem item = (photogalleryItem)e.Item.DataItem;

            String phoName = item.name;

            if (item.name.Length > 70)
            {
                phoName = item.name.Substring(0, 60);
            }

            if ((phoCnt % 4 == 0) || (outStrPho.Length == 0))
            {
                phoPgCnt++;

                if (outStrPho.Length > 0)
                {
                    outStrPho.Append("</div>");
                }

                if (phoPgCnt == 1)
                {
                    outStrPho.Append("<div id=\"phoPg_" + phoPgCnt + "\" style=\"display:block;\">");
                }
                else
                {
                    outStrPho.Append("<div id=\"phoPg_" + phoPgCnt + "\" style=\"display:none;\">");
                }

            }

            outStrPho.Append("<div class=\"wrap_media\">");
            outStrPho.Append("     <div class=\"media_date txt_gold_10\"><strong>" + item.date.ToString("yyyy-MM-dd") + "</strong></div>");
            outStrPho.Append("     <a href=\"photos.aspx?galid=" + item.id + "\"><img src=\"http://images.supersport.co.za/" + item.imageSmall + "\" border=\"0\" height=\"103\" width=\"136\" /></a>");
            outStrPho.Append("     <div class=\"media_info txt_white_10\">" + phoName + "...</div>");
            outStrPho.Append("</div>");

            phoCnt++;
        }
    }
}
