﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_scorerspreview.ascx.cs" Inherits="uc_scorerspreview" %>

<div style="position:relative;">

	<%=design.buildHeading("TOP PERFORMERS", 20, "", "", true)%>
	
	<div class="tabarrow_container">
	
		<div id="tab_ScoreData_1" class="tabarrow_active" onmouseover="switchTab('tab_ScoreData_',this.id,'scorersinfo_','scorersinfo_1','<%=tabCnt %>')">
			<div id="top">MOST POINTS
				<div id="bot">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			</div>
		</div>
		<div class="tabarrow_spacer"></div>
		
		<div id="tab_ScoreData_2" class="tabarrow_inactive" onmouseover="switchTab('tab_ScoreData_',this.id,'scorersinfo_','scorersinfo_2','<%=tabCnt %>')">
			<div id="top">MOST TRIES
				<div id="bot">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			</div>
		</div>
		<div class="tabarrow_spacer"></div>
		
		<asp:Panel ID="pan_nonhometab" runat="server">
		
		    <div id="tab_ScoreData_3" class="tabarrow_inactive" onmouseover="switchTab('tab_ScoreData_',this.id,'scorersinfo_','scorersinfo_3','<%=tabCnt %>')">
			    <div id="top">MOST CAPS
				    <div id="bot">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			    </div>
		    </div>
		    <div class="tabarrow_spacer"></div>
		
		</asp:Panel>

	</div>
	
	<div id="scorersinfo_1" style="display:block">
	
        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
	        <tr class="txt_grey_12">
		        <td bgcolor="#F2F2F2"><strong>PLAYER</strong></td>
		        <td width="30" bgcolor="#F2F2F2"><strong>T</strong></td>
		        <td width="30" bgcolor="#F2F2F2"><strong>C</strong></td>
		        <td width="30" bgcolor="#F2F2F2"><strong>P</strong></td>
		        <td width="30" bgcolor="#F2F2F2"><strong>D</strong></td>
		        <td width="30" bgcolor="#F2F2F2"><strong>PTS</strong></td>
	        </tr>
	
	        <asp:Repeater ID="rep_points" runat="server" OnItemDataBound="rep_points_OnItemDataBound">
            </asp:Repeater>
            
        </table>
        
		<div class="stats_more_link txt_green_12"><a href="scorers.aspx?categoryid=<%=reqCategory %>&leagueid=<%=leagueID %>&fullview=true"><strong>...All points scorers</strong></a></div>
	</div>
	
	<div id="scorersinfo_2" style="display:none;">
	
        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
	        <tr class="txt_grey_12">
		        <td bgcolor="#F2F2F2"><strong>PLAYER</strong></td>
		        <td width="50" bgcolor="#F2F2F2"><strong>TRIES</strong></td>
	        </tr>
	        
	        <asp:Repeater ID="rep_tries" runat="server" OnItemDataBound="rep_tries_OnItemDataBound">
	        </asp:Repeater>
	    
	    </table>
	    
		<div class="stats_more_link txt_green_12"><a href="tryscorers.aspx?categoryid=<%=reqCategory %>&leagueid=<%=leagueID %>&fullview=true"><strong>...All try scorers</strong></a></div>	    

	</div>

    <asp:Panel ID="pan_nonhomedata" runat="server">

	    <div id="scorersinfo_3" style="display:none;">
    	
            <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
	            <tr class="txt_grey_12">
		            <td bgcolor="#F2F2F2"><strong>PLAYER</strong></td>
		            <td width="50" bgcolor="#F2F2F2"><strong>CAPS</strong></td>
	            </tr>
    	
		        <asp:Repeater ID="rep_caps" runat="server" OnItemDataBound="rep_caps_OnItemDataBound">
	            </asp:Repeater>
    	        
            </table>
            
		    <div class="stats_more_link txt_green_12"><a href="cappedplayers.aspx?categoryid=<%=reqCategory %>&leagueid=<%=leagueID %>&fullview=true"><strong>...All capped players</strong></a></div>	    
	    </div>
	
	</asp:Panel>
	
</div>


<%--<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
	<tr class="txt_grey_12">
		<td bgcolor="#F2F2F2"><strong>PLAYER</strong></td>
		<td width="50" bgcolor="#F2F2F2"><strong>XXX</strong></td>
	</tr>
	<tr class="txt_grey_12">
		<td bgcolor="#FFFFFF">xxx</td>
		<td bgcolor="#FFFFFF">xxx</td>
	</tr>
</table>--%>