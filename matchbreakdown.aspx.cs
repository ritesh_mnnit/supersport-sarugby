﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class matchbreakdown : System.Web.UI.Page
{
    public String reqCategory = "";

    public int leagueID = 0;

    private String thisURL = "";

    private int id = -1;

    private int homeid = 0;
    private int awayid = 0;

    private bool IsSevens = false;

    private String homeName = "";
    private String awayName = "";

    private summary summary;

    officialsItem_list officialsList = new officialsItem_list();
    List<officialsItem> officialsItemList = new List<officialsItem>();

    team_A_scoringItem_list team_A_scoringList = new team_A_scoringItem_list();
    List<team_A_scoringItem> team_A_scoringItemList = new List<team_A_scoringItem>();

    team_B_scoringItem_list team_B_scoringList = new team_B_scoringItem_list();
    List<team_B_scoringItem> team_B_scoringItemList = new List<team_B_scoringItem>();

    lineupItem_list lineupList = new lineupItem_list();
    List<lineupItem> lineupItemList = new List<lineupItem>();

    reserveItem_list reserveList = new reserveItem_list();
    List<reserveItem> reserveItemList = new List<reserveItem>();

    replacementItem_list replacementList = new replacementItem_list();
    List<replacementItem> replacementItemList = new List<replacementItem>();

    runofplayItem_list runofplayList = new runofplayItem_list();
    List<runofplayItem> runofplayItemList = new List<runofplayItem>();

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["category"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        if (func.isNumericType(Request.QueryString["id"], "int"))
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
        }

        if (func.isNumericType(Request.QueryString["homeid"], "int"))
        {
            homeid = Convert.ToInt32(Request.QueryString["homeid"]);
        }

        if (func.isNumericType(Request.QueryString["awayid"], "int"))
        {
            awayid = Convert.ToInt32(Request.QueryString["awayid"]);
        }

        lit_print.Text = "<a href=\"matchbreakdown_print.aspx?id="+ id +"&category="+ reqCategory +"&leagueid="+ leagueID +"&homeid="+ homeid +"&awayid=" + awayid + "\" target=\"_blank\">Show Printer Friently Version</a>";

        summary = new summary(id);

        if (summary.count > 0)
        {
            buildSummary();
        }

        officialsItemList = officialsList.getOfficialsItemList(id);

        if (officialsItemList != null)
        {
            buildOfficials();
        }

        team_A_scoringItemList = team_A_scoringList.getTeamAScoringItemList(id,homeid);

        if (team_A_scoringItemList != null)
        {
            buildScoringA();
        }

        team_B_scoringItemList = team_B_scoringList.getTeamBScoringItemList(id,awayid);

        if (team_B_scoringItemList != null)
        {
            buildScoringB();
        }

        lineupItemList = lineupList.getLineupItemList(id, homeid, IsSevens);

        if (lineupItemList != null)
        {
            buildLineupA();
        }

        lineupItemList = lineupList.getLineupItemList(id, awayid, IsSevens);

        if (lineupItemList != null)
        {
            buildLineupB();
        }

        reserveItemList = reserveList.getReserveItemList(id, homeid, IsSevens);

        if (reserveItemList != null)
        {
            buildReserveA();
        }

        reserveItemList = reserveList.getReserveItemList(id, awayid, IsSevens);

        if (reserveItemList != null)
        {
            buildReserveB();
        }

        replacementItemList = replacementList.getReplacementItemList(id, homeid);

        if (replacementItemList != null)
        {
            buildReplacementsTeamA();
        }

        replacementItemList = replacementList.getReplacementItemList(id, awayid);

        if (replacementItemList != null)
        {
            buildReplacementsTeamB();
        }

        runofplayItemList = runofplayList.getRunofplayItemList(id);

        if (runofplayItemList != null)
        {
            buildRunofplay();
        }
    }

    protected void buildRunofplay()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        foreach (runofplayItem item in runofplayItemList)
        {

            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"30\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\">"+ item.minute +"</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.eventtype + "</td>");

            if (item.eventid == 12 || item.eventid == 13 || item.eventid == 14 || item.eventid == 15 || item.eventid == 16 || item.eventid == 17 || item.eventid == 26 || item.eventid == 29 || item.eventid == 30 || item.eventid == 31 || item.eventid == 33) // If it's a substitution
            {
                outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.nameB + " " + item.surnameB + " for " + item.nameA + " " + item.surnameA + "</td>");
            }
            else
            {
                outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.nameA + " " + item.surnameA + "</td>");
            }

            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.team + "</td>");
            outStr.Append("	</tr>");
        }

        outStr.Append("</table>");

        lit_runofplay.Text = outStr.ToString();
    }

    protected void buildReplacementsTeamA()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        outStr.Append("<tr class=\"txt_grey_12\">");
        outStr.Append("    <td colspan=\"4\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">" + homeName + "</td>");
        outStr.Append("</tr>");

        foreach (replacementItem item in replacementItemList)
        {
            outStr.Append("    <tr class=\"txt_grey_12\">");
            outStr.Append("	    <td width=\"30\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\">"+ item.minute +"</td>");
            outStr.Append("	    <td align=\"right\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.nameB + " " + item.surnameB + "</td>");
            outStr.Append("	    <td width=\"30\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">for</td>");
            outStr.Append("	    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.nameA + " " + item.surnameA + "</td>");
            outStr.Append("	    </tr>");
        }

        outStr.Append("</table>");

        lit_replacementsA.Text = outStr.ToString();
    }

    protected void buildReplacementsTeamB()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        outStr.Append("<tr class=\"txt_grey_12\">");
        outStr.Append("    <td colspan=\"4\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">" + awayName + "</td>");
        outStr.Append("</tr>");

        foreach (replacementItem item in replacementItemList)
        {

            outStr.Append("    <tr class=\"txt_grey_12\">");
            outStr.Append("	    <td width=\"30\" align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\">" + item.minute + "</td>");
            outStr.Append("	    <td align=\"right\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.nameB + " " + item.surnameB + "</td>");
            outStr.Append("	    <td width=\"30\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">for</td>");
            outStr.Append("	    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.nameA + " " + item.surnameA + "</td>");
            outStr.Append("	    </tr>");
        }

        outStr.Append("</table>");

        lit_replacementsB.Text = outStr.ToString();
    }

    protected void buildLineupA()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");
        outStr.Append("    <tr>");
        outStr.Append("	    <td colspan=\"2\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\"><span class=\"txt_black_12\">" + homeName + "</span></td>");
        outStr.Append("    </tr>");

        foreach (lineupItem item in lineupItemList)
        {
            outStr.Append("    <tr>");
            outStr.Append("	    <td width=\"30\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\"><span class=\"txt_black_12\">" + item.number + "</span></td>");
            outStr.Append("	    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><span class=\"txt_black_12\">" + item.name + " " + item.surname + " " + item.captain + "</span></td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_lineupA.Text = outStr.ToString();
    }

    protected void buildLineupB()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");
        outStr.Append("    <tr>");
        outStr.Append("	    <td colspan=\"2\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\"><span class=\"txt_black_12\">" + awayName + "</span></td>");
        outStr.Append("    </tr>");

        foreach (lineupItem item in lineupItemList)
        {
            outStr.Append("    <tr>");
            outStr.Append("	    <td width=\"30\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\"><span class=\"txt_black_12\">" + item.number + "</span></td>");
            outStr.Append("	    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><span class=\"txt_black_12\">" + item.name + " " + item.surname + " " + item.captain + "</span></td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_lineupB.Text = outStr.ToString();
    }

    protected void buildReserveA()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");
        outStr.Append("    <tr>");
        outStr.Append("	    <td colspan=\"2\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\"><span class=\"txt_black_12\">" + homeName + "</span></td>");
        outStr.Append("    </tr>");

        foreach (reserveItem item in reserveItemList)
        {
            outStr.Append("    <tr>");
            outStr.Append("	    <td width=\"30\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\"><span class=\"txt_black_12\">" + item.number + "</span></td>");
            outStr.Append("	    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><span class=\"txt_black_12\">" + item.name + " " + item.surname + "</span></td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_reservesA.Text = outStr.ToString();
    }

    protected void buildReserveB()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");
        outStr.Append("    <tr>");
        outStr.Append("	    <td colspan=\"2\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\"><span class=\"txt_black_12\">" + awayName + "</span></td>");
        outStr.Append("    </tr>");

        foreach (reserveItem item in reserveItemList)
        {
            outStr.Append("    <tr>");
            outStr.Append("	    <td width=\"30\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\"><span class=\"txt_black_12\">" + item.number + "</span></td>");
            outStr.Append("	    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><span class=\"txt_black_12\">" + item.name + " " + item.surname + "</span></td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_reservesB.Text = outStr.ToString();
    }

    protected void buildScoringA()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        outStr.Append("<tr class=\"txt_grey_12\">");
        outStr.Append("    <td colspan=\"3\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">" + homeName + "</td>");
        outStr.Append("</tr>");

        foreach (team_A_scoringItem item in team_A_scoringItemList)
        {
            outStr.Append("    <tr class=\"txt_grey_12\">");
            outStr.Append("	    <td width=\"100\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">"+ item.eventtype +"</td>");
            outStr.Append("	    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">"+ item.name + " " + item.surname +"</td>");
            outStr.Append("	    <td width=\"30\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">"+ item.count +"</td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_scoringteamA.Text = outStr.ToString();
    }

    protected void buildScoringB()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        outStr.Append("<tr class=\"txt_grey_12\">");
        outStr.Append("    <td colspan=\"3\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">"+ awayName +"</td>");
        outStr.Append("</tr>");

        foreach (team_B_scoringItem item in team_B_scoringItemList)
        {
            outStr.Append("    <tr class=\"txt_grey_12\">");
            outStr.Append("	    <td width=\"100\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">" + item.eventtype + "</td>");
            outStr.Append("	    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.name + " " + item.surname + "</td>");
            outStr.Append("	    <td width=\"30\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.count + "</td>");
            outStr.Append("    </tr>");
        }

        outStr.Append("</table>");

        lit_scoringteamB.Text = outStr.ToString();
    }

    protected void buildOfficials()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append(design.buildHeading("MATCH OFFICIALS", 12, "", "", false));

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        foreach (officialsItem item in officialsItemList)
        {
            outStr.Append("<tr class=\"txt_grey_12\">");
            outStr.Append("    <td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">" + item.role + "</td>");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.name + " " + item.surname + "</td>");
            outStr.Append("</tr>");
        }

        outStr.Append("</table>");

        lit_officials.Text = outStr.ToString();
    }

    protected void buildSummary()
    {
        if ((summary.competition.IndexOf("Sevens") > 0) || (summary.competition.IndexOf("sevens") > 0))
        {
            IsSevens = true;
        }

        homeName = summary.teamA;

        awayName = summary.teamB;

        StringBuilder outStr = new StringBuilder();

        outStr.Append(design.buildHeading("MATCH SUMMARY", 12, "", "", false));

        outStr.Append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#CCCCCC\">");

        if ((summary.teamA != null) && (summary.teamA != "") && (summary.teamB != null) && (summary.teamB != ""))
        {
            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">TEAMS</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">"+ summary.teamA + " vs " + summary.teamB +"</td>");
            outStr.Append("	</tr>");
        }

        if ((summary.venue != null) && (summary.venue != ""))
        {
            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">VENUE</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + summary.venue + "</td>");
            outStr.Append("	</tr>");
        }

        if (summary.date != null)
        {
            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">DATE</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + summary.date.ToString("dd MMMM yyyy HH:mm") + "</td>");
            outStr.Append("	</tr>");
        }

        if ((summary.competition != null) && (summary.competition != ""))
        {
            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">COMPETITION</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + summary.competition + "</td>");
            outStr.Append("	</tr>");
        }

        if ((summary.finalscore != null) && (summary.finalscore != ""))
        {
            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">FINAL SCORE</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + summary.finalscore + "</td>");
            outStr.Append("	</tr>");
        }

        if ((summary.tries != null) && (summary.tries != ""))
        {
            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">TRIES</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + summary.tries + "</td>");
            outStr.Append("	</tr>");
        }

        if ((summary.halftimescore != null) && (summary.halftimescore != ""))
        {
            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">HALFTIME SCORE</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + summary.halftimescore + "</td>");
            outStr.Append("	</tr>");
        }

        if ((summary.manofthematch != null) && (summary.manofthematch != ""))
        {
            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">MAN OF THE MATCH</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + summary.manofthematch + "</td>");
            outStr.Append("	</tr>");
        }

        if ((summary.conditions != null) && (summary.conditions != ""))
        {
            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">CONDITIONS</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + summary.conditions + "</td>");
            outStr.Append("	</tr>");
        }

        if ((summary.crowd != null) && (summary.crowd != ""))
        {
            outStr.Append("	<tr class=\"txt_grey_12\">");
            outStr.Append("		<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">CROWD</td>");
            outStr.Append("		<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + summary.crowd + "</td>");
            outStr.Append("	</tr>");
        }
        outStr.Append("</table>");

        lit_summary.Text = outStr.ToString();
    }
}











