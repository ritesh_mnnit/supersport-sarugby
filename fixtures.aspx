﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="fixtures.aspx.cs" Inherits="fixtures" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<%=design.buildHeading("ALL FIXTURES", 20, "", "", true)%>

<%=design.makeSpace(10,"",true) %>

<div class="txt_black_10" style="margin-bottom:10px; text-align:right;"><asp:Literal ID="lit_print" runat="server"></asp:Literal></div>

<%=design.makeSpace(10,"",true) %>

    <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">

        <asp:Repeater ID="rep_fixtures" runat="server" OnItemDataBound="rep_fixtures_OnItemDataBound"></asp:Repeater>

    </table>

</asp:Content>