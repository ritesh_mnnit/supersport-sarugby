﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class uc_newspreview : System.Web.UI.UserControl
{
    private String reqCategory = "";

    private newsItem_list list = new newsItem_list();

    private List<newsItem> newsItemList = new List<newsItem>();

    private bool isHome = false;

    public String archiveURL = "";

    public int storyCnt;
    private int artNum;
    private int preNum;

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = Request.QueryString["category"];
        //reqCategory = Page.RouteData.Values["category"].ToString();

        //RouteValueDictionary parameters = new RouteValueDictionary 
        //{ 
        //    {"category", reqCategory}
        //};

        if ((System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath).ToLower() == "default.aspx") && ((HttpContext.Current.Request.QueryString["category"] == "sarugby") || (HttpContext.Current.Request.QueryString["category"] == "") || (HttpContext.Current.Request.QueryString["category"] == null)))
        //if ((System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath).ToLower() == "default.aspx") && ((Page.RouteData.Values["category"].ToString() == "sarugby") || (Page.RouteData.Values["category"].ToString() == "") || (Page.RouteData.Values["category"].ToString() == null)))
        {
            isHome = true;
            archiveURL = "category=sarugby";
            //archiveURL = "sarugby";
        }
        else
        {
            archiveURL = "category=" + reqCategory;
            //archiveURL = reqCategory;
        }

        if (!IsPostBack)
        {
            if(isHome == true)
            {
                newsItemList = list.getNewsItemList(9, reqCategory, true, System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath).ToLower());
                //newsItemList = list.getNewsItemList(9, "sarugby/" + reqCategory, true, System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath).ToLower());
            }
            else
            {
                newsItemList = list.getNewsItemList(9, reqCategory, false, System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath).ToLower());
                //newsItemList = list.getNewsItemList(9, "sarugby/" + reqCategory, false, System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath).ToLower());
            }
            

            if (newsItemList != null)
            {
                storyCnt = newsItemList.Count;
                artNum = 1;
                preNum = 1;

                rep_newsheadlines.DataSource = newsItemList;
                rep_newsheadlines.DataBind();

                rep_newsinfo.DataSource = newsItemList;
                rep_newsinfo.DataBind();
            }
        }
    }

    protected void rep_newsinfo_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            newsItem item = (newsItem)e.Item.DataItem;

            HtmlGenericControl divPre = new HtmlGenericControl("div");

            divPre.Attributes.Add("id", "pre_" + preNum++);
            divPre.Attributes.Add("class", "article_preview");

            StringBuilder outStr = new StringBuilder();

            outStr.Append("<img class=\"article_preview_image\" src=\"http://images.supersport.co.za/" + item.image3 + "\">");
            outStr.Append("<div class=\"article_blurb\" onclick=\"document.location.href='article.aspx?id=" + item.id + "'\">");
            outStr.Append("    <span class=\"txt_white_16\"><strong>" + item.headline + "</strong></span><br />");
            outStr.Append("    <span class=\"txt_white_12\">" + item.blurb + "</span>");
            outStr.Append("</div>");

            divPre.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(divPre);
        }
    }

    protected void rep_newsheadlines_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            newsItem item = (newsItem)e.Item.DataItem;

            HtmlGenericControl divArt = new HtmlGenericControl("div");

            divArt.Attributes.Add("id", "art_" + artNum++);
            divArt.Attributes.Add("class", "article_entry txt_grey_12");
            divArt.Attributes.Add("onmouseover", "setActiveNewsStory('" + Convert.ToInt32(artNum - 1).ToString() + "','" + storyCnt + "')");

            StringBuilder outStr = new StringBuilder();

            outStr.Append("<a href=\"article.aspx?category=" + item.cat + "&id=" + item.id + "\">" + item.headline + "</a>");
            //outStr.Append("<a href=\"/" + item.cat.Replace("sarugby/", "") + "/articles/" + item.id + "/" + func.fixHeadline(item.headline) + "\">" + item.headline + "</a>");

            divArt.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(divArt);
        }
    }
}
