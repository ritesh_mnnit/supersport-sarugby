﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class uc_statspreview : System.Web.UI.UserControl
{
    public String reqCategory = "";

    public int leagueID = 0;

    public int tabCnt = 4;

    private fixtureItem_list fixList = new fixtureItem_list();
    private List<fixtureItem> fixtureItemList = new List<fixtureItem>();

    private resultItem_list resList = new resultItem_list();
    private List<resultItem> resultItemList = new List<resultItem>();

    private logItem_list logList = new logItem_list();
    private List<logItem> logItemList = new List<logItem>();

    private cardItem_list cardList = new cardItem_list();
    private List<cardItem> cardItemList = new List<cardItem>();

    private DateTime curDate = Convert.ToDateTime("1900-01-01");

    private String curTourFix = "";
    private String curTourRes = "";
    private String curTourLog = "";
    private String curTourCAR = "";

    public int rowCnt = 0;

    private String AUConfTeam = "";
    private String NZConfTeam = "";
    private String SAConfTeam = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["category"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        if (!IsPostBack)
        {
            if ((reqCategory == "") || (reqCategory == "sarugby") || (reqCategory == null) || (leagueID == 1) || (leagueID == 2) || (leagueID == 3) || (leagueID == 4))
            {
                tabCnt = 2;
                pan_hometabdata.Visible = false;
                pan_homedata.Visible = false;
            }

            fixtureItemList = fixList.getFixtureItemList(10, "sarugby/" + reqCategory, leagueID);

            if (fixtureItemList != null)
            {
                rep_fixtures.DataSource = fixtureItemList;
                rep_fixtures.DataBind();
            }

            resultItemList = resList.getResultItemList(10, "sarugby/" + reqCategory, leagueID);

            if (resultItemList != null)
            {
                rep_results.DataSource = resultItemList;
                rep_results.DataBind();
            }

            logItemList = logList.getLogItemList(10, "sarugby/" + reqCategory, leagueID);

            if (logItemList != null)
            {
                rep_logs.DataSource = logItemList;
                rep_logs.DataBind();
            }

            cardItemList = cardList.getCardItemList(10, "sarugby/" + reqCategory, leagueID);

            if (cardItemList != null)
            {
                rep_cards.DataSource = cardItemList;
                rep_cards.DataBind();
            }
        }
    }

    protected void rep_fixtures_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            fixtureItem item = (fixtureItem)e.Item.DataItem;

            HtmlGenericControl wrapFix = new HtmlGenericControl("div");

            String teamA = "", teamB = "";

            String islive = "";

            if (item.homesn == "")
            {
                teamA = item.home;
            }
            else
            {
                teamA = item.homesn;
            }

            if (item.awaysn == "")
            {
                teamB = item.away;
            }
            else
            {
                teamB = item.awaysn;
            }

            StringBuilder outStr = new StringBuilder();

            if (curTourFix != item.tournament)
            {
                curTourFix = item.tournament;
                outStr.Append("<div class=\"stats_date txt_grey_12\" style=\"background-color:#F2F2F2\"><strong>" + item.tournament + "</strong></div>");
            }

            if (curDate.ToString("yyyy-MM-dd") != item.date.ToString("yyyy-MM-dd"))
            {
                curDate = item.date;
                outStr.Append("<div class=\"stats_date txt_grey_12\"><strong>" + item.date.ToString("dddd, MMMM dd yyyy") + "</strong></div>");
            }

            if (item.islive == 1)
            {
                islive = "<span class=\"txt_red_12\"><a href=\"javascript:void(0)\" onclick=\"window.open('live.aspx?id="+ item.id +"','live_rugby_17001','title=test,width=325,height=180,top=5,left=5,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizeable=yes');\"><strong>LIVE SCORING</strong></a> </span>";
            }
            
            outStr.Append("<div class=\"stats_entry txt_grey_12\">"+ islive + item.date.ToString("HH:mm") + " - <a href=\"teams.aspx?id=" + item.homeid + "&category=" + reqCategory + "&leagueid=" + leagueID + "\">" + teamA + "</a> vs <a href=\"teams.aspx?id=" + item.awayid + "\">" + teamB + "</a><br /><span class=\"txt_black_10\">" + item.venue + "</span></div>");
            
            

            wrapFix.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapFix);
        }
    }

    protected void rep_results_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            resultItem item = (resultItem)e.Item.DataItem;

            HtmlGenericControl wrapRes = new HtmlGenericControl("div");

            String teamA = "", teamB = "";
            if (item.homesn == "")
            {
                teamA = item.home;
            }
            else
            {
                teamA = item.homesn;
            }

            if (item.awaysn == "")
            {
                teamB = item.away;
            }
            else
            {
                teamB = item.awaysn;
            }

            StringBuilder outStr = new StringBuilder();

            if (curTourRes != item.tournament)
            {
                curTourRes = item.tournament;
                outStr.Append("<div class=\"stats_date txt_grey_12\" style=\"background-color:#F2F2F2\"><strong>" + item.tournament + "</strong></div>");
            }

            if (curDate.ToString("yyyy-MM-dd") != item.date.ToString("yyyy-MM-dd"))
            {
                curDate = item.date;
                outStr.Append("<div class=\"stats_date txt_grey_12\"><strong>" + item.date.ToString("dddd, MMMM dd yyyy") + "</strong></div>");
            }

            outStr.Append("<div class=\"stats_entry txt_grey_12\"><a href=\"matchbreakdown.aspx?id="+ item.id +"&category="+ reqCategory +"&leagueid="+ leagueID +"&homeid="+ item.homeid +"&awayid="+ item.awayid +"&fullview=true\">" + teamA + " " + item.homescore + " - " + item.awayscore + " " + teamB + "</a></div>");

            wrapRes.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapRes);
        }
    }

    protected void rep_logs_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            logItem item = (logItem)e.Item.DataItem;

            HtmlGenericControl wrapLog = new HtmlGenericControl("div");

            String team = "";
            if (item.teamsn == "")
            {
                team = item.team;
            }
            else
            {
                team = item.teamsn;
            }

            StringBuilder outStr = new StringBuilder();

            if (leagueID == 795) // SUPER RUGBY TOURNAMENT
            {
                String logColor = "";

                if (curTourLog != item.group)
                {
                    rowCnt = 0;
                    logColor = "#FFFFFF";
                    curTourLog = item.group;

                    outStr.Append("<td colspan=\"4\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">" + item.group + "</td>");
                }

                rowCnt++;

                logColor = "#FFFFFF";

                if (item.group != "Combined Log")
                {
                    if (rowCnt == 1)
                    {
                        if (item.group == "Australian Conference")
                        {
                            logColor = "#E1BD5E";
                            AUConfTeam = team;
                        }

                        if (item.group == "New Zealand Conference")
                        {
                            logColor = "#000000";
                            NZConfTeam = team;
                        }

                        if (item.group == "South African Conference")
                        {
                            logColor = "#006600";
                            SAConfTeam = team;
                        }
                    }
                }
                else
                {
                    if (rowCnt > 3 && rowCnt < 7)
                    {
                        logColor = "#999999";
                    }
                    else
                    {
                        if (team == AUConfTeam)
                        {
                            logColor = "#E1BD5E";
                        }

                        if (team == NZConfTeam)
                        {
                            logColor = "#000000";
                        }

                        if (team == SAConfTeam)
                        {
                            logColor = "#006600";
                        }
                    }
                }

                outStr.Append("<tr class=\"txt_grey_12\">");
                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><div style=\"float:left; width:10px; height:10px; background-color:" + logColor + "; margin-right:5px;\"></div><div style=\"float:left;\">" + team + "&nbsp;</div></td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.played + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.won + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.logpoints + "</td>");
                outStr.Append("</tr>");
            }
            else
            {
                if (curTourLog != item.group)
                {
                    curTourLog = item.group;
                    outStr.Append("<td colspan=\"4\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_black_12\">" + item.group + "</td>");
                }

                outStr.Append("<tr class=\"txt_grey_12\">");
                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + team + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.played + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.won + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.logpoints + "</td>");
                outStr.Append("</tr>");
            }

            wrapLog.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapLog);
        }
    }

    protected void rep_cards_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            cardItem item = (cardItem)e.Item.DataItem;

            HtmlGenericControl wrapCard = new HtmlGenericControl("div");

            StringBuilder outStr = new StringBuilder();

            outStr.Append("<tr class=\"txt_grey_12\">");
            outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"playerprofile.aspx?id="+ item.playerid +"\">" + item.player + "</td>");
            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.red + "</td>");
            outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.yel + "</td>");
            outStr.Append("</tr>");

            wrapCard.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapCard);
        }
    }
}


