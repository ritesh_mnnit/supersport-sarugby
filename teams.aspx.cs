﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class teams : System.Web.UI.Page
{
    public String reqCategory = "";

    public int leagueID = 0;

    private String thisURL = "";

    private int id = 0;//8007;

    private squadItem_list squadList = new squadItem_list();
    private List<squadItem> squadItemList = new List<squadItem>();

    private teamfixtureItem_list teamfixList = new teamfixtureItem_list();
    private List<teamfixtureItem> teamfixtureItemList = new List<teamfixtureItem>();
    private DateTime curDateFix = Convert.ToDateTime("1900-01-01");

    private teamresultItem_list teamresList = new teamresultItem_list();
    private List<teamresultItem> teamresultItemList = new List<teamresultItem>();
    private DateTime curDateRes = Convert.ToDateTime("1900-01-01");

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = HttpContext.Current.Request.QueryString["category"];

        if (func.isNumericType(HttpContext.Current.Request.QueryString["leagueID"], "int"))
        {
            leagueID = Convert.ToInt32(HttpContext.Current.Request.QueryString["leagueID"]);
        }

        if (func.isNumericType(Request.QueryString["id"], "int"))
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
        }

        thisURL = func.GetCurrentPageName(Request.Url.Query, true);

        if (!IsPostBack)
        {
            int playerCount = 0;

            StringBuilder outStr = new StringBuilder();

            squadItemList = squadList.getSquadItemList(id);

            if (squadItemList != null)
            {
                outStr.Append("<tr class=\"txt_black_12\">");
                outStr.Append("    <td width=\"33%\" align=\"left\" valign=\"top\">");

                foreach (squadItem item in squadItemList)
                {
                    outStr.Append("        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin:3px; border-bottom:#CCC dotted 1px;\">");
                    outStr.Append("            <tr class=\"txt_black_12\">");
                    outStr.Append("                <td align=\"left\" valign=\"top\"><a href=\"playerprofile.aspx?id=" + item.id + "&category=" + reqCategory + "&leagueid=" + leagueID + "\">" + item.name + "&nbsp;" + item.surname + "</a></td>");
                    outStr.Append("            </tr>");
                    outStr.Append("        </table>");

                    playerCount++;

                    if (playerCount % 20 == 0)
                    {
                        outStr.Append("    </td>");
                        outStr.Append("    <td width=\"33%\" align=\"left\" valign=\"top\">");
                    }
                }

                outStr.Append("    </td>");
                outStr.Append("</tr>");
            }

            lit_squad.Text = outStr.ToString();

            teamresultItemList = teamresList.getTeamResultItemList(100, reqCategory, leagueID, id);

            if (teamresultItemList != null)
            {
                rep_results.DataSource = teamresultItemList;
                rep_results.DataBind();
            }

            teamfixtureItemList = teamfixList.getTeamFixtureItemList(100, reqCategory, leagueID,id);

            if (teamfixtureItemList != null)
            {
                rep_fixtures.DataSource = teamfixtureItemList;
                rep_fixtures.DataBind();
            }
        }
    }

    protected void rep_results_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            teamresultItem item = (teamresultItem)e.Item.DataItem;

            StringBuilder outStr = new StringBuilder();

            HtmlGenericControl wrapRes = new HtmlGenericControl("div");


            if (curDateRes.ToString("MMMM") != item.date.ToString("MMMM"))
            {
                curDateRes = item.date;

                outStr.Append("<tr class=\"txt_black_12\">");
                outStr.Append("     <td colspan=\"7\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">" + item.date.ToString("MMMM") + "</td>");
                outStr.Append("</tr>");
            }

            outStr.Append("<tr class=\"txt_black_12\">");
            outStr.Append("	<td width=\"30\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.date.ToString("dd") + "</td>");
            outStr.Append("	<td align=\"right\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.homesn + "</td>");
            outStr.Append("	<td width=\"30\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.homescore + "</td>");
            outStr.Append("	<td width=\"30\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.awayscore + "</td>");
            outStr.Append("	<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.awaysn + "</td>");
            outStr.Append("	<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.venue + "</td>");
            outStr.Append("	<td class=\"txt_grey_10\" width=\"51\" align=\"right\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"matchbreakdown.aspx?id=" + item.id + "&category=" + reqCategory + "&leagueid=" + leagueID + "&homeid=" + item.homeid + "&awayid=" + item.awayid + "&fullview=true\">Breakdown</a></td>");
            outStr.Append("</tr>");

            wrapRes.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapRes);
        }
    }

    protected void rep_fixtures_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            teamfixtureItem item = (teamfixtureItem)e.Item.DataItem;

            StringBuilder outStr = new StringBuilder();

            HtmlGenericControl wrapFix = new HtmlGenericControl("div");

            if (curDateFix.ToString("MMMM") != item.date.ToString("MMMM"))
            {
                curDateFix = item.date;

                outStr.Append("<tr class=\"txt_black_12\">");
                outStr.Append("     <td colspan=\"5\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">" + item.date.ToString("MMMM") + "</td>");
                outStr.Append("</tr>");
            }

            outStr.Append("<tr class=\"txt_black_12\">");
            outStr.Append("	<td width=\"30\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.date.ToString("dd") + "</td>");
            outStr.Append("	<td align=\"right\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.homesn + "</td>");
            outStr.Append("	<td width=\"30\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\">vs</td>");
            outStr.Append("	<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.awaysn + "</td>");
            outStr.Append("	<td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\">" + item.venue + "</td>");
            outStr.Append("</tr>");

            wrapFix.InnerHtml = outStr.ToString();

            e.Item.Controls.Add(wrapFix);
        }
    }
}