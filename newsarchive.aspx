﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="newsarchive.aspx.cs" Inherits="newsarchive" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<%=design.buildHeading("NEWS ARTICLE ARCHIVE", 20, "", "", true)%>

<%=design.makeSpace(10,"",true) %>

    <div class="wrap_interface_top">

        <asp:Panel ID="pan_page" runat="server" DefaultButton="btn_ok">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="40" class="txt_black_10">&nbsp;PAGE</td>
                    <td width="40"><asp:TextBox CssClass="edit_page" ID="f_page" MaxLength="4" runat="server" /></td>
                    <td width="40" class="txt_black_10"> of <asp:Literal ID="lit_totalpages" runat="server"></asp:Literal></td>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle" style="padding: 0 2px 0 0;"><asp:Button ID="btn_ok" Text="OK" runat="server" CssClass="btn_grey" OnClick="btn_ok_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 2px 0 2px;"><asp:Button ID="btn_prev" Text="PREVIOUS" runat="server" CssClass="btn_grey" OnClick="btn_prev_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 0 0 2px;"><asp:Button ID="btn_next" Text="NEXT" runat="server" CssClass="btn_grey" OnClick="btn_next_Click" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>

    <%=design.makeSpace(20,"",true) %>

    <asp:Repeater ID="rep_archive" runat="server">
        <ItemTemplate>
            <div id="art_2" class="article_entry txt_grey_12"><a href="article.aspx?category=<%=reqCategory %>&pcurr=<%=pcurr %>&id=<%# DataBinder.Eval(Container.DataItem, "id")%>"><%# DataBinder.Eval(Container.DataItem, "headline")%></a>&nbsp;-&nbsp;<span class="txt_black_10"><strong><%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "date")).ToString("MMMM dd, yyyy") %></strong></span></div>
        </ItemTemplate>
    </asp:Repeater>

</asp:Content>