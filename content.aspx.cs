﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class article : System.Web.UI.Page
{
    private String reqCategory = "sarugby/curriecup";

    private int contentid = -1;

    private String thisURL = "";

    public contentItem content;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (func.isNumericType(Request.QueryString["contentid"], "int"))
        {
            contentid = Convert.ToInt32(Request.QueryString["contentid"]);
            content = new contentItem(contentid);
        }

        if (content != null)
        {
            lit_content.Text = content.body;
        }
    }
}