﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="article.aspx.cs" Inherits="article" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<%=design.buildHeading("NEWS ARTICLES", 20, "<< TO NEWS ARCHIVE", "newsarchive.aspx?category=" + reqCategory + "&pcurr=" + pcurr, true)%>

<%=design.makeSpace(10,"",true) %>

    <div>
        <div style="float:left; margin:0 10px 0 0; border:#000 solid 1px;"><asp:Image ID="img_image" runat="server" /></div>
        <div class="txt_black_10" style="margin-bottom:10px; text-align:right;"><asp:Literal ID="lit_print" runat="server"></asp:Literal></div>
        <div class="txt_green_20"><asp:Literal ID="lit_headline" runat="server"></asp:Literal></div>
        <div class="txt_black_12" style="margin-bottom:10px;"><em><asp:Literal ID="lit_author" runat="server"></asp:Literal></em></div>
        <div class="txt_black_10" style="margin-bottom:10px;"><em><asp:Literal ID="lit_credit" runat="server"></asp:Literal></em></div>
        <div class="txt_black_10" style="margin-bottom:10px;"><em><asp:Literal ID="lit_date" runat="server"></asp:Literal></em></div>
        <div class="txt_black_12"><asp:Literal ID="lit_body" runat="server"></asp:Literal></div>
    </div>

</asp:Content>