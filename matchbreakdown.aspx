﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="matchbreakdown.aspx.cs" Inherits="matchbreakdown" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<%=design.buildHeading("MATCH BREAKDOWN",20,"","",true) %>

<%=design.makeSpace(10,"",true) %>

<div class="txt_black_10" style="margin-bottom:10px; text-align:right;"><asp:Literal ID="lit_print" runat="server"></asp:Literal></div>

<%=design.makeSpace(10,"",true) %>

<asp:Literal ID="lit_summary" runat="server"></asp:Literal>

<%=design.makeSpace(20,"",true) %>

<asp:Literal ID="lit_officials" runat="server"></asp:Literal>
    
<%=design.makeSpace(20,"",true) %>

<%=design.buildHeading("SCORING SUMMARY", 12, "", "", false)%>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top">
		    
		    <asp:Literal ID="lit_scoringteamA" runat="server"></asp:Literal>

		</td>
		<td width="20" align="left" valign="top">&nbsp;</td>
		<td align="left" valign="top">
		
		    <asp:Literal ID="lit_scoringteamB" runat="server"></asp:Literal>

		</td>
	</tr>
</table>

<%=design.makeSpace(20,"",true) %>

<%=design.buildHeading("LINE-UP", 12, "", "", false)%>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top">
		
		    <asp:Literal ID="lit_lineupA" runat="server"></asp:Literal>
		    
		</td>
		<td width="20" align="left" valign="top">&nbsp;</td>
		<td align="left" valign="top">
		
		    <asp:Literal ID="lit_lineupB" runat="server"></asp:Literal>
		    
		</td>
	</tr>
</table>


<%=design.makeSpace(20,"",true) %>

<%=design.buildHeading("RESERVES", 12, "", "", false)%>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top">
		
		    <asp:Literal ID="lit_reservesA" runat="server"></asp:Literal>
		    
		</td>
		<td width="20" align="left" valign="top">&nbsp;</td>
		<td align="left" valign="top">
		
		    <asp:Literal ID="lit_reservesB" runat="server"></asp:Literal>
		    
		</td>
	</tr>
</table>

<%=design.makeSpace(20,"",true) %>

<%=design.buildHeading("REPLACEMENTS", 12, "", "", false)%>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top">

            <asp:Literal ID="lit_replacementsA" runat="server"></asp:Literal>

		</td>
		<td width="20" align="left" valign="top">&nbsp;</td>
		<td align="left" valign="top">

		    <asp:Literal ID="lit_replacementsB" runat="server"></asp:Literal>

		</td>
	</tr>
</table>

<%=design.makeSpace(20,"",true) %>

<%=design.buildHeading("RUN OF PLAY", 12, "", "", false)%>

<asp:Literal ID="lit_runofplay" runat="server"></asp:Literal>

</asp:Content>








