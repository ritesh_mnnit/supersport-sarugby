﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="photo.aspx.cs" Inherits="photo" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<%=design.buildHeading("GALLERY PHOTO", 20, "<< BACK TO PHOTOS", "photos.aspx?galid=" + galid, true)%>

<%=design.makeSpace(10,"",true) %>

    <img src="http://images.supersport.co.za/<%=photox %>" border="0" style="max-width:620px;" />

</asp:Content>