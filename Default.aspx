﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<%@ Register TagPrefix="uc_newspreview" TagName="uc_newspreview" Src="~/uc_newspreview.ascx" %>

<%@ Register TagPrefix="uc_mediapreview" TagName="uc_mediapreview" Src="~/uc_mediapreview.ascx" %>

<%@ Register TagPrefix="uc_scorerspreview" TagName="uc_scorerspreview" Src="~/uc_scorerspreview.ascx" %>

<%@ Register TagPrefix="uc_profilespreview" TagName="uc_profilespreview" Src="~/uc_profilespreview.ascx" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<asp:Literal ID="TEST" runat="server"></asp:Literal>

    <div class="col_left_620">
        <uc_newspreview:uc_newspreview ID="uc_newspreview" runat="server" />
    </div>
    
    <div class="col_left_620">
        <uc_mediapreview:uc_mediapreview ID="uc_mediapreview" runat="server" />
    </div>
    
    <div class="col_left_300">
    
        <uc_scorerspreview:uc_scorerspreview ID="uc_scorerspreview" runat="server" />
        
        <%=design.makeSpace(10,"",true) %>
        
        <uc_profilespreview:uc_profilespreview ID="uc_profilespreview" runat="server" />
        
    </div>
        
    <div class="col_right_300">
    
        <div><asp:Literal ID="sc_middle_1" runat="server"></asp:Literal><%--<img src="External/Graphics/absa2.jpg" width="300" height="95" />--%></div>
    
        <%=design.makeSpace(10,"",true) %>
        
        <div><asp:Literal ID="sc_middle_2" runat="server"></asp:Literal><%--<img src="External/Graphics/absa1.jpg" width="300" height="95" />--%></div>
    
	    <%=design.makeSpace(20,"",true) %>
	                
	    <div><asp:Literal ID="sc_middle_3" runat="server"></asp:Literal><%--<img src="External/Graphics/wallpaper.jpg" width="300" height="101" />--%></div>
	                
	    <%=design.makeSpace(20,"",true) %>
	                
	    <div><asp:Literal ID="sc_middle_4" runat="server"></asp:Literal><%--<img src="External/Graphics/win.jpg" width="300" height="101" />--%></div>

    </div>
        
</asp:Content>