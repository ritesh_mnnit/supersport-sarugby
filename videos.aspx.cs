﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class videos : System.Web.UI.Page
{
    private String reqCategory = "sarugby/curriecup";

    private String thisURL = "";

    public int pcurr;

    private videoItem_list list = new videoItem_list();
    private List<videoItem> videoItemList = new List<videoItem>();

    private StringBuilder outStr = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {


        if (func.isNumericType(Request.QueryString["pcurr"], "int"))
        {
            pcurr = Convert.ToInt32(Request.QueryString["pcurr"]);
        }
        else
        {
            pcurr = 1;
        }

        thisURL = func.GetCurrentPageName(Request.Url.Query, true);

        if (!IsPostBack)
        {
            btn_prev.Enabled = true;
            btn_next.Enabled = true;

            videoItemList = list.getVideoItemList(500, reqCategory);

            if (videoItemList != null)
            {
                PagedDataSource tmpSource = new PagedDataSource();

                tmpSource.DataSource = videoItemList;

                tmpSource.AllowPaging = true;

                tmpSource.PageSize = 20;

                if (pcurr < 1) { pcurr = 1; }

                if (pcurr > tmpSource.PageCount) { pcurr = tmpSource.PageCount; }

                f_page.Text = pcurr.ToString();

                tmpSource.CurrentPageIndex = pcurr - 1;

                if (tmpSource.IsFirstPage) { btn_prev.Enabled = false; }

                if (tmpSource.IsLastPage) { btn_next.Enabled = false; }

                lit_totalpages.Text = tmpSource.PageCount.ToString();

                rep_videos.DataSource = tmpSource;

                rep_videos.DataBind();
            }
            else
            {
                pan_page.Enabled = false;
            }
        }
    }

    protected void btn_ok_Click(object sender, EventArgs e)
    {
        Response.Redirect("videos.aspx?pcurr=" + f_page.Text);
    }

    protected void btn_prev_Click(object sender, EventArgs e)
    {
        Response.Redirect("videos.aspx?pcurr=" + (pcurr - 1).ToString());
    }

    protected void btn_next_Click(object sender, EventArgs e)
    {
        Response.Redirect("videos.aspx?pcurr=" + (pcurr + 1).ToString());
    }
}