﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="content.aspx.cs" Inherits="article" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

    <%=design.buildHeading(content.headline.ToUpper(), 20, "", "", true) %>

    <%=design.makeSpace(10,"",true) %>

    <div class="txt_black_12">

        <asp:Literal ID="lit_content" runat="server"></asp:Literal>

    </div>

</asp:Content>