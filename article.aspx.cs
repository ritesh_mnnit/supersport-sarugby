﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class article : System.Web.UI.Page
{
    public String reqCategory = "";

    public int id;

    public int pcurr;

    private String thisURL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        reqCategory = Request.QueryString["category"];

        //reqCategory = Page.RouteData.Values["category"].ToString();

        if (func.isNumericType(Request.QueryString["id"], "int"))
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
        }

        //if (func.isNumericType(Page.RouteData.Values["id"].ToString(), "int"))
        //{
        //    id = Convert.ToInt32(Page.RouteData.Values["id"].ToString());
        //}

        if (func.isNumericType(Request.QueryString["pcurr"], "int"))
        {
            pcurr = Convert.ToInt32(Request.QueryString["pcurr"]);
        }

        //if (func.isNumericType(Page.RouteData.Values["pcurr"].ToString(), "int"))
        //{
        //    pcurr = Convert.ToInt32(Page.RouteData.Values["pcurr"].ToString());
        //}



        //RouteValueDictionary parameters = new RouteValueDictionary 
        //{ 
        //    {"category", reqCategory},
        //    {"id", id}
        //};

        thisURL = func.GetCurrentPageName(Request.Url.Query, true);

        if (!IsPostBack)
        {
            newsArticle currArticle = new newsArticle();
            newsItem item = currArticle.getArticle(id);

            lit_print.Text = "<a href=\"article_print.aspx?id="+ id +"\" target=\"_blank\">Show Printer Friendly Version</a>";
            lit_headline.Text = item.headline;
            lit_author.Text = item.author;
            lit_credit.Text = item.credit;
            lit_date.Text = item.date.ToString("MMMM dd, yyyy");
            lit_body.Text = item.body;
            img_image.ImageUrl = "http://images.supersport.co.za/" + item.imageLarge;
        }
    }
}