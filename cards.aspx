﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="cards.aspx.cs" Inherits="cards" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<%=design.buildHeading("ALL CARDS", 20, "", "", true) %>

    <div class="wrap_interface_top">

        <asp:Panel ID="pan_page" runat="server" DefaultButton="btn_ok" Visible="false">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="40" class="txt_black_10">&nbsp;PAGE</td>
                    <td width="40"><asp:TextBox CssClass="edit_page" ID="f_page" MaxLength="4" runat="server" /></td>
                    <td width="40" class="txt_black_10"> of <asp:Literal ID="lit_totalpages" runat="server"></asp:Literal></td>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle" style="padding: 0 2px 0 0;"><asp:Button ID="btn_ok" Text="OK" runat="server" CssClass="btn_grey" OnClick="btn_ok_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 2px 0 2px;"><asp:Button ID="btn_prev" Text="PREVIOUS" runat="server" CssClass="btn_grey" OnClick="btn_prev_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 0 0 2px;"><asp:Button ID="btn_next" Text="NEXT" runat="server" CssClass="btn_grey" OnClick="btn_next_Click" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>

<%=design.makeSpace(10,"",true) %>

    <asp:Repeater ID="rep_profile" runat="server"></asp:Repeater>

        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
	        <tr class="txt_grey_12">
		        <td align="left" bgcolor="#F2F2F2"><strong>PLAYER</strong></td>
		        <td align="left" bgcolor="#F2F2F2"><strong>TEAM</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>RED</strong></td>
		        <td width="50" align="center" bgcolor="#F2F2F2"><strong>YELLOW</strong></td>
	        </tr>
	        
            <asp:Repeater ID="rep_cards" runat="server">
                <ItemTemplate>
                    <tr class="txt_grey_12">
                        <td align="left" valign="top" bgcolor="#FFFFFF"><a href="playerprofile.aspx?id=<%# DataBinder.Eval(Container.DataItem, "playerid")%>"><%# DataBinder.Eval(Container.DataItem, "player")%></a></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF"><a href="teams.aspx?id=<%# DataBinder.Eval(Container.DataItem, "teamid")%>&category=<%=reqCategory %>&leagueid=<%=leagueID %>"><%# DataBinder.Eval(Container.DataItem, "team")%></a></td>
                        <td align="center" valign="top" bgcolor="#FFFFFF"><%# DataBinder.Eval(Container.DataItem, "red")%></td>
                        <td align="center" valign="top" bgcolor="#FFFFFF"><%# DataBinder.Eval(Container.DataItem, "yel")%></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            
        </table>

</asp:Content>