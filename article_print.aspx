﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="article_print.aspx.cs" Inherits="article_print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">

        <link href="/External/Styles/text.css?v=<%=ConfigurationManager.AppSettings["FileVersion"] %>" rel="stylesheet" type="text/css" />

        <link href="/External/Styles/general.css?v=<%=ConfigurationManager.AppSettings["FileVersion"] %>" rel="stylesheet" type="text/css" />

        <title>S.A. RUGBY - Print Article</title>
    </head>
    <body>
        <form id="formm_print" runat="server">
            <div style="width:900px;">
                <div style="float:left; margin:0 10px 0 0; border:#000 solid 1px;"><asp:Image ID="img_image" runat="server" /></div>
                <div class="txt_green_20"><asp:Literal ID="lit_headline" runat="server"></asp:Literal></div>
                <div class="txt_black_12" style="margin-bottom:10px;"><em><asp:Literal ID="lit_author" runat="server"></asp:Literal></em></div>
                <div class="txt_black_10" style="margin-bottom:10px;"><em><asp:Literal ID="lit_credit" runat="server"></asp:Literal></em></div>
                <div class="txt_black_10" style="margin-bottom:10px;"><em><asp:Literal ID="lit_date" runat="server"></asp:Literal></em></div>
                <div class="txt_black_12"><asp:Literal ID="lit_body" runat="server"></asp:Literal></div>
            </div>
        </form>
    </body>
</html>
