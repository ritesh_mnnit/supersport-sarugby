﻿<%@ Page Language="C#" MasterPageFile="~/_master.master" AutoEventWireup="true" CodeFile="tryscorers.aspx.cs" Inherits="tryscorers" %>

<%@ MasterType VirtualPath="~/_master.master" %>

<asp:Content ID="con_primary" ContentPlaceHolderID="cp_primary" Runat="Server">

<%=design.buildHeading("TRY SCORERS", 20, "", "", true)%>

<%=design.makeSpace(10,"",true) %>

    <div class="wrap_interface_top">

        <asp:Panel ID="pan_page" runat="server" DefaultButton="btn_ok">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="40" class="txt_black_10">&nbsp;PAGE</td>
                    <td width="40"><asp:TextBox CssClass="edit_page" ID="f_page" MaxLength="4" runat="server" /></td>
                    <td width="40" class="txt_black_10"> of <asp:Literal ID="lit_totalpages" runat="server"></asp:Literal></td>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle" style="padding: 0 2px 0 0;"><asp:Button ID="btn_ok" Text="OK" runat="server" CssClass="btn_grey" OnClick="btn_ok_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 2px 0 2px;"><asp:Button ID="btn_prev" Text="PRIOR" runat="server" CssClass="btn_grey" OnClick="btn_prev_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 0 0 2px;"><asp:Button ID="btn_next" Text="NEXT" runat="server" CssClass="btn_grey" OnClick="btn_next_Click" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>

<%=design.makeSpace(10,"",true) %>

    <asp:Repeater ID="rep_profile" runat="server"></asp:Repeater>

        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
	        <tr class="txt_grey_12">
		        <td bgcolor="#F2F2F2"><strong>PLAYER</strong></td>
		        <td bgcolor="#F2F2F2"><strong>TRIES</strong></td>
	        </tr>
	        
            <asp:Repeater ID="rep_scorers" runat="server">
                <ItemTemplate>
                    <tr class="txt_grey_12">
                        <td align="left" valign="top" bgcolor="#FFFFFF"><%# DataBinder.Eval(Container.DataItem, "player")%></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF"><%# DataBinder.Eval(Container.DataItem, "tries")%></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            
        </table>

</asp:Content>