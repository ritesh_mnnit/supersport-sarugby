﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class photo : System.Web.UI.Page
{
    public String photox = "";

    public int galid;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (func.isNumericType(Request.QueryString["galid"], "int"))
        {
            galid = Convert.ToInt32(Request.QueryString["galid"]);
        }

        photox = Request.QueryString["img"];
    }
}