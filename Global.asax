﻿<%@ Application Language="C#" %>

<%@ Import Namespace="System.Web.Routing" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        RegisterRoutes(RouteTable.Routes);

    }

    void RegisterRoutes(RouteCollection routes)
    {
        //routes.MapPageRoute("DefaultFrontpage1", "", "~/Default.aspx");
        
        //routes.MapPageRoute("DefaultFrontpage" , "{category}", "~/Default.aspx");

        //routes.MapPageRoute("TournamentFrontpage", "{category}/{leagueid}", "~/Default.aspx");
        
        //routes.MapPageRoute("ArticleArchive" , "{category}/articles", "~/newsarchive.aspx");
        //routes.MapPageRoute("Article" , "{category}/articles/{id}/{title}", "~/article.aspx");
    }

    void Application_beginRequest(object sender, EventArgs e)
    {
        string fullOrigionalpath = Request.RawUrl.ToString();

        // Mobile check
        string UserAgent = "";
        if (!(Request.ServerVariables["HTTP_USER_AGENT"] == null))
        {
            UserAgent = Request.ServerVariables["HTTP_USER_AGENT"].Trim().ToLower();
        }
        bool Mobile = func.IsMobileBrowser(UserAgent);

        if (Request.QueryString["mobile"] != null)
        {
            Mobile = true;
        }

        if (Mobile == true)
        {
            if (fullOrigionalpath != null)
            {
                string tmpUrl = "http://m.sarugby.co.za" + fullOrigionalpath;
                Response.Redirect(tmpUrl);
            }
            else
            {
                Response.Redirect("http://m.sarugby.co.za");
            }
        }
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    {
        // Database errors
        System.Exception myError = Server.GetLastError();

        System.Data.SqlClient.SqlConnection DatabaseConn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connWrite"].ConnectionString);
        string ipAddress = "";
        string Referrer = "";
        string errorText = myError.ToString();
        bool logError = true;

        if (Request.Url.ToString().IndexOf(".beta") >= 0)
        {
            logError = false;
        }
        if (errorText.IndexOf("The file") >= 0 && errorText.IndexOf("does not exist") >= 0)
        {
            logError = false;
        }

        if (HttpContext.Current.Request.ServerVariables["Remote_Addr"] != null)
        {
            ipAddress = HttpContext.Current.Request.ServerVariables["Remote_Addr"].ToString();
        }

        if (HttpContext.Current.Request.UrlReferrer != null)
        {
            Referrer = HttpContext.Current.Request.UrlReferrer.ToString().Replace("'", "''");
        }

        try
        {
            if (logError == true)
            {
                System.Data.SqlClient.SqlCommand SqlQuery = new System.Data.SqlClient.SqlCommand("Insert Into SuperSportZone.dbo.zonewebsiteerrors (siteId, url, referrer, error, ipAddress) Values (16, '" + Request.Url.ToString().Replace("'", "''") + "', '" + Referrer + "', '" + errorText.Replace("'", "''") + "', '" + ipAddress.Replace("'", "''") + "')", DatabaseConn);
                DatabaseConn.Open();
                SqlQuery.ExecuteNonQuery();
                DatabaseConn.Close();
            }
        }
        catch
        {

        }
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
