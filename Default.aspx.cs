﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Web.Routing;

public partial class _Default : System.Web.UI.Page
{
    public bool isHome = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        SitecontentItem sc1 = new SitecontentItem(16, "SAR2_MIDDLE_1");

        if (sc1 != null)
        {
            sc_middle_1.Text = sc1.body;
        }

        SitecontentItem sc2 = new SitecontentItem(16, "SAR2_MIDDLE_2");

        if (sc2 != null)
        {
            sc_middle_2.Text = sc2.body;
        }

        SitecontentItem sc3 = new SitecontentItem(16, "SAR2_MIDDLE_3");

        if (sc3 != null)
        {
            sc_middle_3.Text = sc3.body;
        }

        SitecontentItem sc4 = new SitecontentItem(16, "SAR2_MIDDLE_4");

        if (sc4 != null)
        {
            sc_middle_4.Text = sc4.body;
        }
    }
}